 
This version is '1.01'
============================================================
For installation, please open folder "document".
go to "index.html". Read "installation" section.

============================================================

==v1.01== 16/06/2013
- add banner link
	header.php
	include/goodlayers-option.php
	
- fix filosofo warning message
	include/plugin/filosofo-image/filosofo-custom-image-sizes.php
	
- fix twitter
	function.php
	include/plugin/custom-widget/twitter-widget.php
	include/plugin/custom-widget/twitteroauth.php
	include/plugin/custom-widget/social-counter folder
	
	include/goodlayers-option.php
	footer.php
	
- fix the next post link
	single.php
	
- fix both sidebar 2 responsive size / social counter responsive size
	stylesheet/foundation-responsive.css
	
- fix the compatibility with jwplayer
	include/plugin/misc.php
	include/plugin/shortcode-generator.php
	
- add 1/1 blog list layout to archive page
	include/goodlayers-option.php
	search.php
	archive.php

==v1.00== 12/06/2013
* initial released 
