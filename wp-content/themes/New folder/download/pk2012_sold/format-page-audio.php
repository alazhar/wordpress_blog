<?php
	$audio_urls = explode(',', get_post_meta(get_post_thumbnail_id(), '_audio_urls', true));
	
	if (is_array($audio_urls) && count($audio_urls) > 0 && $audio_urls[0] != '') : 
		
		$supplied = '';
		$media = '';
		
		for ($i = 0; $i < count($audio_urls); $i++) :
			
			$supplied .= str_replace('.', '', strrchr(trim($audio_urls[$i]), '.')).(($i < count($audio_urls) - 1) ? ',' : '');
			$media .= str_replace(array('.'), array(''), strrchr(trim($audio_urls[$i]), '.')).':"'.trim($audio_urls[$i]).'"'.(($i < count($audio_urls) - 1) ? ',' : '');
			
		endfor;
?>

					<div id="jp_container_<?php the_ID(); ?>" class="jp-video jp-video-normal-width">
						<div id="jquery_jplayer_<?php the_ID(); ?>" class="jp-jplayer"></div>
						<?php echo PK_JPLAYER_GUI; ?>

					</div>
					<script type="text/javascript">
						jQuery(document).ready(function(){
							jQuery("#jquery_jplayer_<?php the_ID(); ?>").jPlayer({
								ready: function () {
									jQuery(this).jPlayer("setMedia", {
										<?php echo $media; ?>
	
									});
								},
								play: function() {
									jQuery(this).jPlayer("pauseOthers");
								},
								swfPath:"<?php echo PK_THEME_DIR; ?>/js",
								solution:"flash,html",
								supplied:"<?php echo $supplied; ?>",
								cssSelectorAncestor:"#jp_container_<?php the_ID(); ?>",
								sizeFull: {
									width:"100%",
									height:"100%",
									cssClass:"jp-video-full"
								}
							});
						});
					</script>
<?php
	endif;
?>
					<?php
	add_filter('embed_defaults', 'pk_page_embed_defaults'); the_content();
?>
