<?php
	$image_link = get_post_meta(get_post_thumbnail_id(), '_image_link', true);
	$image_link_target = get_post_meta(get_post_thumbnail_id(), '_image_link_target', true);
?>

					<article id="post-<?php the_ID(); ?>" <?php post_class('pk_entry pk_entry_blog pk_entry_image pk_boxed pk_full_content pk_entry_single'); ?>>
<?php
	if (has_post_thumbnail()) : 
?>
						<div class="pk_image">
							<div class="pk_image_wrapper<?php if ($image_link) echo ' pk_link_icon'; ?>">
<?php
		if ($image_link) : 
?>
								<a href="<?php echo esc_url($image_link); ?>" title="<?php echo __('Click to visit:', 'pk_translate').' '.$image_link; ?>"<?php echo ($image_link_target) ? ' target="'.$image_link_target.'"' : ' target="_self"'; ?>>
<?php
		endif;
?>
								<?php the_post_thumbnail(((pk_sidebar()) ? 'big' : 'full-width'), array('title' => '')); ?>

<?php
		if ($image_link) : 
?>
								<span class="pk_image_button_back_ground"></span>
								<span class="pk_image_button_icon"></span>
								</a>
<?php
		endif;
?>
							</div>
						</div>
<?php
	endif;
?>
						<div class="pk_entry_content">
							<header>
								<h3><?php the_title(); ?></h3>
								<ul class="pk_entry_meta">
									<li class="pk_entry_author"><?php _e('By:', 'pk_translate'); ?> <?php the_author_posts_link(); ?></li>
									<li class="pk_entry_date"><?php echo get_the_date(); ?></li>
									<li class="pk_entry_categories"><?php the_terms(get_the_ID(), 'taxonomy_portfolio'); ?></li>
									<li class="pk_comments_count"><?php comments_popup_link(__('0', 'pk_translate'), __('1', 'pk_translate'), __('%', 'pk_translate'), 'pk_meta_tot_comments', __('Off', 'pk_translate')); ?></li>
								</ul>
							</header>
							<?php add_filter('embed_defaults', 'pk_post_embed_defaults'); the_content(); ?>
						</div>
					</article>
