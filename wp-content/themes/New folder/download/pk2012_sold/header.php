<?php
	global $welcome_image;
	
	if (trim(pk_get_option('welcome_image', '')) != '' && !isset($_COOKIE['welcome_image'])) : 
		
		setcookie('welcome_image', 'ok');
		
		$welcome_image = 1;
		
	endif;
?>
<!DOCTYPE html>
<!--[if IE 7 ]><html class="ie7" <?php language_attributes(); ?>><![endif]-->
<!--[if IE 8 ]><html class="ie8" <?php language_attributes(); ?>><![endif]-->
<!--[if IE 9 ]><html class="ie9" <?php language_attributes(); ?>><![endif]--> 
<!--[if (gte IE 9)|!(IE)]><!--><html <?php language_attributes(); ?>><!--<![endif]-->

<head>

<meta charset="<?php bloginfo('charset'); ?>" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

<title><?php pk_title(); ?></title>

<link rel="shortcut icon" href="<?php echo pk_get_option('favicon', PK_THEME_DIR.'/images/favicon.ico'); ?>">
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />

<script type="text/javascript">
	document.write('<link rel="stylesheet" type="text/css" media="all" href="<?php echo PK_THEME_DIR.'/css/'; ?>pk_hide_on_load.css" />');
</script>

<?php
	wp_head();
?>
<!--[if lt IE 9]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script><![endif]-->

<noscript>
	<link rel="stylesheet" href="<?php echo PK_THEME_DIR.'/css/'; ?>pk_no_script.css" type="text/css" />
</noscript>

<style type="text/css">

	.pk_logo {
		width:<?php echo pk_get_option('logo_width', '130'); ?>px;
		height:<?php echo pk_get_option('logo_height', '60'); ?>px;
		background-image:url('<?php echo pk_get_option('logo', PK_THEME_DIR.'/images/logo.png'); ?>');
		background-position:0px 0px;
		background-repeat:no-repeat;
	}

</style>

<?php
	if (pk_get_option('css_typography', '') != '') : 
?>
<style type="text/css">

<?php
		echo pk_get_option('css_typography', '');
?>


</style>

<?php
	endif;
	
	if (pk_get_option('css_custom', '') != '') : 
?>
<style type="text/css">

<?php
		echo pk_get_option('css_custom', '');
?>


</style>

<?php
	endif;
?>
</head>

<?php
	function pk_body_class() {
		
		if (is_page_template('page-home-agency.php')) return 'pk_agency_home';
		if (is_page_template('page-home-business.php')) return 'pk_business_home';
		if (is_page_template('page-home-freelance.php')) return 'pk_freelance_home';
		if (is_page_template('page-home-standard.php')) return 'pk_standard_home';
		if (is_page_template('page-home-jigoshop.php')) return 'pk_jigoshop_home';
		if (is_page_template('page-home-woocommerce.php')) return 'pk_woocommerce_home';
		
	}
?>
<body <?php body_class(pk_body_class()); ?>>

<!-- pk start wrapper all -->
<div id="pk_wrapper_all">

	<!-- pk start header -->
	<header class="pk_wrapper pk_header pk_clearfix">

		<!-- pk start center box -->
		<div class="pk_center_box">
			<h1 class="pk_logo"><a href="<?php echo (function_exists('icl_get_home_url')) ? icl_get_home_url() : home_url(); ?>" title="<?php _e('Home', 'pk_translate'); ?>"><?php echo get_bloginfo('name'); ?></a></h1>
			<p class="pk_logo_caption"><?php echo get_bloginfo('description'); ?></p>
			<div class="pk_right_info">
				<p class="pk_header_text"><?php echo pk_get_option('header_text', get_bloginfo('description')); if (get_option('users_can_register')) : ?> / <?php endif; ?></p>
<?php
	if (get_option('users_can_register') && !is_user_logged_in()) : 
?>
				<p><a href="#pk_register_form" class="pk_button_mini pk_button_<?php echo pk_get_option('register_myaccount_button_color', 'white'); ?> pk_register"><span><?php _e('Register', 'pk_translate'); ?></span></a></p>
<?php
	else : 
		
		if (get_option('users_can_register') || is_user_logged_in()) : 
?>
				<p><a href="<?php
			if (current_user_can('edit_posts')) {
				
				echo home_url('/wp-admin/');
				
			} else {
				
				if (function_exists('jigoshop_get_page_id')) {
					
					echo get_permalink((function_exists('icl_object_id')) ? icl_object_id(jigoshop_get_page_id('myaccount'), 'page', true) : jigoshop_get_page_id('myaccount'));
					
				} else {
					
					if (function_exists('woocommerce_get_page_id')) {
						
						echo get_permalink((function_exists('icl_object_id')) ? icl_object_id(woocommerce_get_page_id('myaccount'), 'page', true) : woocommerce_get_page_id('myaccount'));
						
					} else {
						
						echo home_url('/wp-admin/profile.php');
						
					}
					
				}
				
			}
?>" class="pk_button_mini <?php echo (current_user_can('edit_posts')) ? 'pk_button_red' : 'pk_button_'.pk_get_option('register_myaccount_button_color', 'white'); ?> pk_register"><span><?php echo (current_user_can('edit_posts')) ? __('Dashboard', 'pk_translate') : __('My account', 'pk_translate'); ?></span></a></p>
<?php
		endif;
		
	endif;
?>
			</div>
		</div>
		<!-- pk start center box -->

	</header>
	<!-- pk end header -->

	<!-- pk start wrapper -->
	<div class="pk_wrapper pk_main_navigation pk_top_shadow pk_clearfix">

		<!-- pk start center box -->
		<div class="pk_center_box pk_navigation_wrapper">

			<!-- pk start navigation -->
			<div class="pk_main_menu">
				<a href="#menu-main-menu" class="pk_button_menu"><?php _e('Menu', 'pk_translate'); ?></a>
				<?php wp_nav_menu(array('theme_location' => 'pk_main_menu_'.((is_user_logged_in()) ? 'li' : 'lo'), 'container' => 'nav', 'fallback_cb' => 'pk_set_up_menu_message')); ?>
			</div>
			<!-- pk end navigation -->

			<!-- pk start option nav -->
			<div class="pk_option_nav">
				<ul>
					<li class="pk_option_item">
						<form role="search" method="get" id="pk_search_form" action="<?php echo (function_exists('icl_get_home_url')) ? icl_get_home_url() : home_url(); ?>">
							<div>
								<label class="screen-reader-text" for="search"><?php _e('Search', 'pk_translate'); ?></label>
								<input type="text" value="" name="s" id="search" />
								<input type="submit" id="pk_search_submit" value="<?php _e('Search', 'pk_translate'); ?>" />
<?php
	if (defined('ICL_LANGUAGE_CODE')) : 
?>
								<input type="hidden" name="lang" value="<?php echo ICL_LANGUAGE_CODE; ?>"/>
<?php
	endif;
?>
							</div>
						</form>
						<span class="pk_option_item_divider"></span>
					</li>
<?php
	if (get_option('users_can_register') || is_user_logged_in()) : 
?>
					<li class="pk_option_item">
						<a href="<?php echo (is_user_logged_in()) ? wp_logout_url(get_permalink()) : '#pk_login_form'; ?>" class="pk_login"><span><?php echo (is_user_logged_in()) ? __('Log out', 'pk_translate') : __('Log in', 'pk_translate'); ?></span></a>
						<span class="pk_option_item_divider"></span>
					</li>
<?php
	endif;
	
	if (function_exists('jigoshop_get_page_id')) : 
?>
					<li class="pk_option_item">
						<a href="<?php echo get_permalink((function_exists('icl_object_id')) ? icl_object_id(jigoshop_get_page_id('cart'), 'page', true) : jigoshop_get_page_id('cart')); ?>" class="mini-cart-button"><span><?php echo jigoshop_cart::$cart_contents_count.__(' items', 'pk_translate'); ?> &ndash; <?php echo jigoshop_cart::get_cart_total(); ?></span></a>
						<span class="pk_option_item_divider"></span>
					</li>
<?php
	endif;
	
	if (function_exists('woocommerce_get_page_id')) : 
		
			global $woocommerce;
?>
					<li class="pk_option_item">
						<a href="<?php echo $woocommerce -> cart -> get_cart_url(); ?>" class="mini-cart-button"><span><?php echo $woocommerce -> cart -> cart_contents_count.__(' items', 'pk_translate'); ?> &ndash; <?php echo  $woocommerce -> cart -> get_cart_total(); ?></span></a>
						<span class="pk_option_item_divider"></span>
					</li>
<?php
	endif;
?>
				</ul>
			</div>
			<!-- pk end option nav -->

		</div>
		<!-- pk end center box -->

	</div>
	<!-- pk end wrapper -->
<?php
	if (is_page_template('page-home-agency.php')) get_template_part('format', 'home-agency-slider');
	if (is_page_template('page-home-business.php')) get_template_part('format', 'home-business-slider');
	if (is_page_template('page-home-freelance.php')) get_template_part('format', 'home-freelance-slider');
	if (is_page_template('page-home-standard.php')) get_template_part('format', 'home-standard-slider');
	if (is_page_template('page-home-jigoshop.php')) get_template_part('format', 'home-jigoshop-slider');
	if (is_page_template('page-home-woocommerce.php')) get_template_part('format', 'home-woocommerce-slider');
?>

	<!-- pk start wrapper : pk_call_to_action -->
	<div class="pk_wrapper pk_bottom_shadow">
		<div class="pk_center_box pk_call_to_action pk_clearfix">
			<div class="pk_call_to_action_content<?php echo (is_page_template('page-home-freelance.php')) ? ' pk_center_text' : ''; ?>">
<?php	
	global $post;
	if (is_singular('page')) $post_meta = get_post_custom($post -> ID);
	
	if (is_page_template('page-home-agency.php')) : 
		
		if (is_active_sidebar('sidebar_home_page_agency_left') || is_active_sidebar('sidebar_home_page_agency_middle') || is_active_sidebar('sidebar_home_page_agency_right')) : 
?>

				<!-- pk start widgets areas -->
				<section id="pk_home_agency_widgets" class="pk_clearfix">
					<div class="pk_one_third pk_fixed">

<?php if (dynamic_sidebar('sidebar_home_page_agency_left')) : endif; ?>
					</div>
					<div class="pk_one_third pk_fixed">

<?php if (dynamic_sidebar('sidebar_home_page_agency_middle')) : endif; ?>
					</div>
					<div class="pk_one_third pk_fixed pk_last">

<?php if (dynamic_sidebar('sidebar_home_page_agency_right')) : endif; ?>
					</div>
				</section>
				<!-- pk end widgets areas -->
<?php
		endif;
		
	elseif (is_page_template('page-home-business.php') && pk_get_option('homepages_feed_reader', 'true') == 'true') : 
?>
				<div class="pk_call_to_action_news"><p><?php _e('NEWS:', 'pk_translate'); ?></p><div><?php echo pk_any_feed(((isset($post_meta['_feed_url'][0])) ? $post_meta['_feed_url'][0] : ''), ((isset($post_meta['_feed_total_items'][0])) ? $post_meta['_feed_total_items'][0] : 5)); ?></div></div>
<?php
	elseif (is_page_template('page-home-freelance.php')) : 
?>
				<h2><?php if (isset($post_meta['_call_to_action_text'][0])) echo $post_meta['_call_to_action_text'][0]; ?></h2>
<?php
	elseif (is_page_template('page-home-standard.php')) : 
?>
				<h2><?php if (isset($post_meta['_call_to_action_text'][0])) echo $post_meta['_call_to_action_text'][0]; ?></h2>
				<a href="<?php if (isset($post_meta['_call_to_action_button_link'][0])) echo $post_meta['_call_to_action_button_link'][0]; ?>" title="<?php if (isset($post_meta['_call_to_action_button_label'][0])) echo $post_meta['_call_to_action_button_label'][0]; ?>" class="pk_button_big pk_button_<?php if (isset($post_meta['_call_to_action_button_color'][0])) echo $post_meta['_call_to_action_button_color'][0]; ?>"><span><?php if (isset($post_meta['_call_to_action_button_label'][0])) echo $post_meta['_call_to_action_button_label'][0]; ?></span></a>
<?php
	elseif (is_page_template('page-home-jigoshop.php') || is_page_template('page-home-woocommerce.php')) : 
?>
				<h2><?php if (isset($post_meta['_call_to_action_text'][0])) echo $post_meta['_call_to_action_text'][0]; ?></h2>
				<a href="<?php if (isset($post_meta['_call_to_action_button_link'][0])) echo $post_meta['_call_to_action_button_link'][0]; ?>" title="<?php if (isset($post_meta['_call_to_action_button_label'][0])) echo $post_meta['_call_to_action_button_label'][0]; ?>" class="pk_button_big pk_button_<?php if (isset($post_meta['_call_to_action_button_color'][0])) echo $post_meta['_call_to_action_button_color'][0]; ?>"><span><?php if (isset($post_meta['_call_to_action_button_label'][0])) echo $post_meta['_call_to_action_button_label'][0]; ?></span></a>
<?php
	else : 
?>
				<h2><?php
		if (is_singular(array('page', 'product')) && !pk_is_portfolio()) the_title();
		if (pk_is_portfolio() || is_singular('portfolio')) echo ((function_exists('icl_object_id')) ? icl_object_id(pk_get_option('portfolio_page_id', ''), 'page', true) : pk_get_option('portfolio_page_id', '')) ? get_the_title((function_exists('icl_object_id')) ? icl_object_id(pk_get_option('portfolio_page_id', ''), 'page', true) : pk_get_option('portfolio_page_id', '')) : __('Portfolio', 'pk_translate');
		if ((is_home() || is_archive() || is_singular('post')) && !is_tax() && !is_post_type_archive() && !is_author()) echo ((function_exists('icl_object_id')) ? icl_object_id(get_option('page_for_posts'), 'page', true) : get_option('page_for_posts')) ? get_the_title((function_exists('icl_object_id')) ? icl_object_id(get_option('page_for_posts'), 'page', true) : get_option('page_for_posts')) : __('Blog', 'pk_translate');
		if (is_author()) echo __('All posts by ', 'pk_translate').get_userdata(get_query_var('author')) -> display_name;
		if (is_search()) _e('Search', 'pk_translate');
		if (is_404()) _e('Page not found!', 'pk_translate');
		if (function_exists('is_jigoshop') && is_jigoshop() && !is_singular(array('page', 'product'))) echo ((function_exists('icl_object_id')) ? icl_object_id(jigoshop_get_page_id('shop'), 'page', true) : jigoshop_get_page_id('shop')) ? get_the_title((function_exists('icl_object_id')) ? icl_object_id(jigoshop_get_page_id('shop'), 'page', true) : jigoshop_get_page_id('shop')) : __('Products', 'pk_translate');
		if (function_exists('is_woocommerce') && is_woocommerce() && !is_singular(array('page', 'product'))) echo ((function_exists('icl_object_id')) ? icl_object_id(woocommerce_get_page_id('shop'), 'page', true) : woocommerce_get_page_id('shop')) ? get_the_title((function_exists('icl_object_id')) ? icl_object_id(woocommerce_get_page_id('shop'), 'page', true) : woocommerce_get_page_id('shop')) : __('Products', 'pk_translate');
		if (function_exists('is_bbpress') && is_bbpress()) _e('Forum', 'pk_translate');
?></h2>
<?php
		if (function_exists('bcn_display')) : 
?>
				<div class="breadcrumbs">
					<?php @bcn_display(); ?>

				</div>
<?php
		endif;
		
	endif;
?>
			</div>
		</div>
	</div>
	<!-- pk end wrapper : pk_call_to_action -->

	<!-- pk start wrapper : pk_page -->
	<div class="pk_wrapper pk_middle_shadow">

		<!-- pk start page -->
		<div class="pk_center_box pk_page pk_clearfix">

			<div class="<?php if (pk_sidebar()) : ?>pk_<?php echo pk_get_option('sidebar_position', 'right'); ?>_sidebar<?php else : ?>pk_without_sidebar<?php endif; ?>">

				<!-- pk start main -->
				<div id="pk_main" class="pk_clearfix">
