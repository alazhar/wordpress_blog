<?php
/*
Template Name: FAQ Template
*/
?>
<?php
	get_header();
	
	while (have_posts()) : 
		
		the_post();
		
		$post_meta = get_post_custom(get_the_ID());
		$type = (isset($post_meta['_faq_type'][0]) && $post_meta['_faq_type'][0] == 'accordion') ? ' pk_accordion' : '';
?>
					<?php
		add_filter('embed_defaults', 'pk_full_width_page_embed_defaults'); the_content();
	
	endwhile;
	
	$temp_query = $wp_query;
	
	$query_args['post_type'] = 'faq';
	$query_args['posts_per_page'] = -1;
	$query_args['orderby'] = 'menu_order';
	$query_args['order'] = 'ASC';
	
	$wp_query = new WP_Query($query_args);
?>

					<span class"pk_faq_separator"></span>
					<div class="pk_toggles<?php echo $type; ?>" style="width:100%;">
<?php
	while (have_posts()) : 
		
		the_post();
?>

						<div class="pk_toggle">
							<p class="pk_toggle_button"><?php the_title(); ?></p>
							<div class="pk_toggle_content_wrapper">
								<div class="pk_toggle_content">
									<?php add_filter('embed_defaults', 'pk_faq_page_embed_defaults'); the_content(); ?>
								</div>
							</div>
						</div>
<?php	
	endwhile;
?>

					</div>
<?php
	
	$wp_query = $temp_query;
	wp_reset_query();
	
	get_footer();
?>