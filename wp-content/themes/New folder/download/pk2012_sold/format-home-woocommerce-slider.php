<?php
	global $post;
	
	$post_meta = get_post_custom($post -> ID);
	$post_images = pk_get_post_gallery_images('full');
?>

	<!-- pk start wrapper : pk_featured -->
	<div class="pk_wrapper pk_featured pk_middle_shadow pk_clearfix">

		<!-- pk start center box -->
		<div class="pk_center_box">

			<!-- pk start featured slider -->
			<div id="pk_featured_slider" class="pk_content_slider">
				<div class="pk_slider_content">
<?php
	if (count($post_images) == 0) : 
?>
					<div class="pk_slider_item">
						<div class="pk_slider_item_media">
							<?php if (get_header_image()) : ?><img src="<?php header_image(); ?>" /><?php endif; ?>

						</div>
					</div>
<?php
	endif;
	
	for ($i = 0; $i < count($post_images); $i++) : 
	
		$title_properties = get_post_meta($post_images[$i]['id'], '_title_properties', true);
		$description_properties = get_post_meta($post_images[$i]['id'], '_description_properties', true);
		$snap_points = get_post_meta($post_images[$i]['id'], '_snap_points', true);
		$video_url = get_post_meta($post_images[$i]['id'], '_video_url', true);
		$image_link = get_post_meta($post_images[$i]['id'], '_image_link', true);
		$image_link_target = get_post_meta($post_images[$i]['id'], '_image_link_target', true);
?>
					<div class="pk_slider_item">
						<div class="pk_slider_item_media">
							<?php
		if ($video_url || $image_link) : 
	
?><a <?php if (!$image_link) : ?>class="prettyPhoto<?php echo $post -> ID; ?>" <?php endif; ?>href="<?php if ($image_link) : echo esc_url($image_link); else : echo esc_url($video_url); endif; ?>"<?php echo ($image_link_target) ? ' target="'.$image_link_target.'"' : ' target="_self"'; ?>><?php
		
		endif;
		
		echo $post_images[$i]['img'];
		
		if ($video_url || $image_link) : 
?></a><?php
		
		endif;
?>

						</div>
<?php
		if ($title_properties) : 
?>
							<h3 class="pk_slider_item_title" data-properties="properties=<?php if ($title_properties) echo $title_properties; ?>"><?php echo $post_images[$i]['title']; ?></h3>
<?php
		endif;
		
		if (isset($post_images[$i]['description']) && $post_images[$i]['description']) : 
?>
						<div class="pk_slider_item_caption" data-properties="properties=<?php echo ($description_properties) ? $description_properties : '{\'x\':\'100\', \'y\':\'150\', \'width\':\'350\', \'color\':\'#ffffff\', \'bg_color\':\'none\'}'; ?>">
							<?php echo '<p>'.$post_images[$i]['description'].'</p>'; ?>

						</div>
<?php
		endif;
		
		if ($snap_points) : 
?>
						<div class="pk_snap_points">
<?php
			echo $snap_points;
?>

						</div>
<?php
		endif;
?>
					</div>	
<?php	
	endfor;
?>

				</div>
			</div>
			<script type="text/javascript">
				jQuery(document).ready(function() {
					jQuery("#pk_featured_slider").pk_content_slider({
						sliderWidth:980,
						sliderHeight:530,
						sliderNavigationAlign:'<?php if (isset($post_meta['_slider_navigation_align'][0])) : echo $post_meta['_slider_navigation_align'][0]; else : echo 'center'; endif; ?>',
						transition:'<?php if (isset($post_meta['_slider_transition'][0])) : echo $post_meta['_slider_transition'][0]; else : echo 'fade'; endif; ?>',
						sliderBackground:'<?php if (isset($post_meta['_slider_background'][0])) echo $post_meta['_slider_background'][0]; ?>',
						slideshowAutoStart:<?php if (isset($post_meta['_slider_slideshow_auto_start'][0])) : echo $post_meta['_slider_slideshow_auto_start'][0]; else : echo 'true'; endif; ?>,
						slideshowInterval:<?php if (isset($post_meta['_slider_slideshow_interval'][0])) : echo $post_meta['_slider_slideshow_interval'][0]; else : echo '7'; endif; ?>		
					});
					jQuery(".prettyPhoto<?php echo $post -> ID; ?>").attr('rel', 'prettyPhoto[home_slider]').prettyPhoto(<?php echo PK_PRETTYPHOTO_PARAMS; ?>);
				});
			</script>
			<!-- pk end featured slider -->

		</div>
		<!-- pk end center box -->

	</div>
	<!-- pk end wrapper : pk_featured -->
