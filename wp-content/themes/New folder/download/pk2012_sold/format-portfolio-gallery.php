<?php
	$post_images = pk_get_post_gallery_images('full');
?>

					<article id="post-<?php the_ID(); ?>" <?php post_class('pk_entry pk_entry_blog pk_entry_gallery pk_boxed pk_full_content pk_entry_single'); ?>>
<?php
	if (has_post_thumbnail()) : 
?>
						<div class="pk_image">
							<div class="pk_image_wrapper pk_zoom_icon">
								<a class="prettyPhoto<?php the_ID(); ?>" href="<?php echo pk_get_featured_image(); ?>" title="<?php the_title_attribute(); ?>">
									<?php the_post_thumbnail(((pk_sidebar()) ? 'big' : 'full-width'), array('title' => '')); ?>

									<span class="pk_image_button_back_ground"></span>
									<span class="pk_image_button_icon"></span>
								</a>
<?php
		for ($i = 0; $i < count($post_images); $i++) : 
			
			$video_url = get_post_meta($post_images[$i]['id'], '_video_url', true);
?>
								<a class="prettyPhoto<?php the_ID(); ?>" href="<?php echo ($video_url) ? esc_url($video_url) : $post_images[$i]['url']; ?>" title="<?php echo $post_images[$i]['description']; ?>"></a>
<?php
		endfor;
?>
								<script type="text/javascript">
									jQuery(document).ready(function() {
										jQuery(".prettyPhoto<?php the_ID(); ?>").attr('rel', 'prettyPhoto[gallery_<?php the_ID(); ?>]').prettyPhoto(<?php echo PK_PRETTYPHOTO_PARAMS; ?>);
									});
								</script>
							</div>
						</div>
<?php
	endif;
?>					
						<div class="pk_entry_content">
							<header>
								<h3><?php the_title(); ?></h3>
								<ul class="pk_entry_meta">
									<li class="pk_entry_author"><?php _e('By:', 'pk_translate'); ?> <?php the_author_posts_link(); ?></li>
									<li class="pk_entry_date"><?php echo get_the_date(); ?></li>
									<li class="pk_entry_categories"><?php the_terms(get_the_ID(), 'taxonomy_portfolio'); ?></li>
									<li class="pk_comments_count"><?php comments_popup_link(__('0', 'pk_translate'), __('1', 'pk_translate'), __('%', 'pk_translate'), 'pk_meta_tot_comments', __('Off', 'pk_translate')); ?></li>
								</ul>
							</header>
							<?php add_filter('embed_defaults', 'pk_post_embed_defaults'); the_content(); ?>
						</div>
					</article>
