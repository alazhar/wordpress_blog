<?php

add_filter('post_gallery', 'pk_post_gallery', 10, 2);

function pk_post_gallery($output, $attr) {
	
	if (isset($attr['link']) && $attr['link'] == 'file') : 
?>

<script type="text/javascript">
	jQuery(document).ready(function() {
		jQuery(".gallery a").attr('rel', 'wp_gallery').prettyPhoto(<?php echo PK_PRETTYPHOTO_PARAMS; ?>);
	});
</script>

<?php
	endif;
	
}

?>