<?php

remove_action('jigoshop_sidebar', 'jigoshop_get_sidebar');
remove_action('jigoshop_before_main_content', 'jigoshop_breadcrumb', 20, 0);

remove_action('woocommerce_sidebar', 'woocommerce_get_sidebar');
remove_action('woocommerce_before_main_content', 'woocommerce_breadcrumb', 20, 0);

function pk_ecommerce_after_main_content() {
?>

<script type="text/javascript">
	jQuery(document).ready(function() {
		jQuery("a.zoom").attr('rel', "prettyPhoto[thumbnails]").prettyPhoto(<?php echo PK_PRETTYPHOTO_PARAMS; ?>);
		jQuery('#review_form_wrapper').hide();
		jQuery('a.show_review_form').pk_review_form({
  			 button_close_label:"<?php _e('Cancel', 'pk_translate'); ?>"
  		});
	});
</script>

<?php
}

add_action('jigoshop_after_main_content', 'pk_ecommerce_after_main_content');
add_action('woocommerce_after_main_content', 'pk_ecommerce_after_main_content');

if (!function_exists('loop_columns')) {
	
	function loop_columns() {
		
		return 3;
		
	}
	
}

if (function_exists('is_woocommerce')) add_filter('loop_shop_columns', 'loop_columns');

if (function_exists('is_woocommerce')) add_filter('loop_shop_per_page', create_function('$cols', 'return 9;'));

function pk_woocommerce_set_body_class($classes) {
	
	if (function_exists('woocommerce_get_page_id') && woocommerce_get_page_id('thanks') == get_the_ID()) $classes[] = 'woocommerce-thankyou';
	return $classes;
	
}

add_filter('body_class','pk_woocommerce_set_body_class');

?>