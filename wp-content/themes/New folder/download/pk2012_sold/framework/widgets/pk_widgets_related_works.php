<?php

class PK_Widget_Related_Works extends WP_Widget {
	
	function PK_Widget_Related_Works() {
		
		$widget_ops = array('classname' => 'widget_related_works', 'description' => __('Displays the related works', 'pk_translate'));
		$this -> WP_Widget('pk-related-works', __('Related Works', 'pk_translate'), $widget_ops);
		$this -> alt_option_name = 'widget_related_works';

		add_action('save_post', array(&$this, 'flush_widget_cache'));
		add_action('deleted_post', array(&$this, 'flush_widget_cache'));
		add_action('switch_theme', array(&$this, 'flush_widget_cache'));
		
	}
	
	function widget($args, $instance) {
		
		$cache = wp_cache_get('widget_related_works', 'widget');
		
		$title = apply_filters('widget_title', $instance['title']);
		
		if (empty($title)) $title = false;
		
		$number = absint($instance['number']);

		if (!is_array($cache)) {
			
			$cache = array();
			
		}

		if (isset($cache[$args['widget_id']])) {
			
			echo $cache[$args['widget_id']];
			return;
			
		}
		
		ob_start();
		extract($args);
		
		global $post;
		
		if (isset($post) && $post -> post_type == 'portfolio') {
			
			$terms = wp_get_post_terms($post -> ID, 'taxonomy_portfolio');
			
			$terms_ids = array();
			$post_in = array();
			
		}
		
		if (isset($terms) && $terms) {
			
			foreach ($terms as $term) {
				
				$terms_ids[] = $term -> term_id;
				
			}
			
			$works_to_query = array_unique(get_objects_in_term($terms_ids, 'taxonomy_portfolio'));
			
			$total_works_to_query = count($works_to_query);
			
			for ($i = 0; $i < $total_works_to_query; $i++) {
				
				if (isset($works_to_query[$i]) && $works_to_query[$i] != $post -> ID) {
					
					$post_in[] = $works_to_query[$i];
					
				}
				
			}
			
			$r = new WP_Query(array('post_type' => $post -> post_type, 'posts_per_page' => $number, 'post__in' => $post_in, 'nopaging' => 0, 'post_status' => 'publish', 'ignore_sticky_posts' => 1, 'orderby' => 'comment_count', 'order' => 'DESC'));
			
			if ($r -> have_posts()) : 
				
				echo '<!-- pk start pk-related-works widget -->
'.$before_widget.'
	';
				
				if ($title) echo $before_title.$title.$after_title;
?>

	<ul>
<?php
				while ($r -> have_posts()) : 
					
					$r -> the_post();
?>
		<li<?php if (!has_post_thumbnail()) echo ' class="pk_no_thumbnail"'; ?>>
			<?php if (has_post_thumbnail()) the_post_thumbnail('thumb'); ?>

			<div>
				<h5><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h5>
				<?php add_filter('excerpt_length', 'pk_widgets_excerpt_filter'); add_filter('excerpt_more', 'pk_excerpt_more'); the_excerpt(); ?>
			</div>
		</li>
<?php				
				endwhile;
?>
	</ul>
<?php
				echo $after_widget.'
<!-- pk end pk-related-works widget -->

';
			
			endif;
			
			wp_reset_postdata();
			
			$cache[$args['widget_id']] = ob_get_flush();
			
			wp_cache_set('widget_related_works', $cache, 'widget');
			
		}
		
	}

	function update($new_instance, $old_instance) {
		
		$instance = $old_instance;
		
		$instance['title'] = strip_tags($new_instance['title']);
		$instance['number'] = (int) $new_instance['number'];
		
		$this -> flush_widget_cache();

		$alloptions = wp_cache_get('alloptions', 'options');
		
		if (isset($alloptions['widget_related_works'])) {
			
			delete_option('widget_related_works');
			
		}
		
		return $instance;
		
	}

	function flush_widget_cache() {
		
		wp_cache_delete('widget_related_works', 'widget');
		
	}
	
	function form($instance) {
		
		$title = isset($instance['title']) ? esc_attr($instance['title']) : '';
		$number = isset($instance['number']) ? absint($instance['number']) : 5;
?>
		<p><label for="<?php echo $this -> get_field_id('title'); ?>"><?php _e('Title:', 'pk_translate'); ?></label>
		<input class="widefat" id="<?php echo $this -> get_field_id('title'); ?>" name="<?php echo $this -> get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" /></p>
		
		<p><label for="<?php echo $this -> get_field_id('number'); ?>"><?php _e('Number of works to show:', 'pk_translate'); ?></label>
		<input class="widefat" id="<?php echo $this -> get_field_id('number'); ?>" name="<?php echo $this -> get_field_name('number'); ?>" type="text" value="<?php echo $number; ?>" size="3" /></p>
<?php
	}
	
}

function pk_widgets_related_works() {
	
	register_widget('PK_Widget_Related_Works');
	
}

add_action('widgets_init', 'pk_widgets_related_works');

?>