<?php

class PK_Widget_Links extends WP_Widget_Links {
	
	function widget($args, $instance) {
		
		extract($args, EXTR_SKIP);
		
		$show_description = isset($instance['description']) ? $instance['description'] : false;
		$show_name = isset($instance['name']) ? $instance['name'] : false;
		$show_rating = isset($instance['rating']) ? $instance['rating'] : false;
		$show_images = isset($instance['images']) ? $instance['images'] : true;
		$category = isset($instance['category']) ? $instance['category'] : false;
		
		if (is_admin() && !$category) {
			
			echo $before_widget.$before_title. __('All Links', 'pk_translate').$after_title.$after_widget;
			
			return;
			
		}
		
		$before_widget = preg_replace('/id="[^"]*"/','id="%id"', $before_widget);
		
		$before_widget = '<!-- pk start pk-links widget -->
'.$before_widget.'
	';
		
		$after_widget = $after_widget.'
<!-- pk end pk-links widget -->
';
		
		$output = wp_list_bookmarks(apply_filters('widget_links_args', array(
			'title_before' => $before_title, 'title_after' => $after_title,
			'category_before' => $before_widget, 'category_after' => $after_widget,
			'show_images' => $show_images, 'show_description' => $show_description,
			'show_name' => $show_name, 'show_rating' => $show_rating,
			'category' => $category, 'class' => 'linkcat widget',
			'echo' => 0
		)));
		
		echo str_replace(array("</a>\n", "\n</li>", "<li>", "\n\n\t"), array("</a>", "</li>", "\t\t<li>", "\n\t"), $output);
		
	}
	
}

class PK_Widget_Calendar extends WP_Widget_Calendar {
	
	function widget($args, $instance) {
		
		extract($args);
		
		$title = apply_filters('widget_title', empty($instance['title']) ? '&nbsp;' : $instance['title'], $instance, $this -> id_base);
		
		echo '<!-- pk start pk-calendar widget -->
'.$before_widget.'
';
		
		if ($title) echo $before_title.$title.$after_title;
		
		echo '
<div id="calendar_wrap" class="table rounded">
	';
		
		get_calendar();
		
		echo '
</div>
';
		
		echo $after_widget.'
<!-- pk end pk-calendar widget -->

';
		
	}
	
}

class PK_Widget_Meta extends WP_Widget_Meta {
	
	function PK_Widget_Meta() {
		
		$widget_ops = array('classname' => 'widget_meta', 'description' => __('Log in/out, feed and WP links', 'pk_translate'));
		$this -> WP_Widget('meta', __('Meta', 'pk_translate'), $widget_ops);
		
	}
	
	function widget($args, $instance) {
		
		extract($args);
		
		$title = apply_filters('widget_title', empty($instance['title']) ? __('Meta', 'pk_translate') : $instance['title'], $instance, $this -> id_base);
		
		echo '<!-- pk start pk-meta widget -->
'.$before_widget.'
	';
		
		if ($title) echo $before_title.$title.$after_title;
?>

	<ul>
<?php
	if (get_option('users_can_register') && !is_user_logged_in()) : 
?>
		<li><a href="#pk_register_form" class="pk_meta_register"><?php _e('Register', 'pk_translate'); ?></a></li>
<?php
	endif;
	
	if (is_user_logged_in() || !get_option('users_can_register')) : 
?>
		<li><?php wp_loginout(); ?></li>
<?php
	else : 
?>
		<li><a href="#pk_login_form" class="pk_meta_login"><?php _e('Log in', 'pk_translate'); ?></a></li>
<?php
	endif;
?>
		<li><a href="<?php bloginfo('rss2_url'); ?>" title="<?php echo esc_attr(__('Syndicate this site using RSS 2.0', 'pk_translate')); ?>"><?php _e('Entries <abbr title="Really Simple Syndication">RSS</abbr>', 'pk_translate'); ?></a></li>
		<li><a href="<?php bloginfo('comments_rss2_url'); ?>" title="<?php echo esc_attr(__('The latest comments to all posts in RSS', 'pk_translate')); ?>"><?php _e('Comments <abbr title="Really Simple Syndication">RSS</abbr>', 'pk_translate'); ?></a></li>
		<li><a href="http://wordpress.org/" title="<?php echo esc_attr(__('Powered by WordPress, state-of-the-art semantic personal publishing platform.', 'pk_translate')); ?>">WordPress.org</a></li>
<?php
		wp_meta();
?>
	</ul>
<?php
		echo $after_widget.'
<!-- pk end pk-meta widget -->

';
		
	}
	
}

class PK_Widget_Categories extends WP_Widget_Categories {
	
	function widget($args, $instance) {
		
		extract($args);
		
		$title = apply_filters('widget_title', empty($instance['title']) ? __('Categories', 'pk_translate') : $instance['title'], $instance, $this -> id_base);
		$c = $instance['count'] ? '1' : '0';
		$h = $instance['hierarchical'] ? '1' : '0';
		$d = $instance['dropdown'] ? '1' : '0';
		
		echo '<!-- pk start pk-categories widget -->
'.$before_widget.'
	';
		
		if ($title) echo $before_title.$title.$after_title;
		
		$cat_args = array('orderby' => 'name', 'show_count' => $c, 'hierarchical' => $h, 'echo' => 0);
		
		if ($d) {
			
			$cat_args['show_option_none'] = __('Select Category', 'pk_translate');
			
			echo '
'.str_replace(array("<select", "<option", "</select"), array("\t<select", "\t<option", "\t</select"), wp_dropdown_categories(apply_filters('widget_categories_dropdown_args', $cat_args)));
?>
	<script type='text/javascript'>
	/* <![CDATA[ */
		var dropdown = document.getElementById("cat");
		function onCatChange() {
			if (dropdown.options[dropdown.selectedIndex].value > 0) {
				location.href = "<?php echo home_url(); ?>/?cat="+dropdown.options[dropdown.selectedIndex].value;
			}
		}
		dropdown.onchange = onCatChange;
	/* ]]> */
	</script>
<?php
		} else {
?>

	<ul>
<?php
echo str_replace(array("\n</a>", "\n</li>", "<li"), array("</a>", "</li>", "\t<li"), wp_list_categories(apply_filters('widget_categories_args', $cat_args)));
?>
	</ul>
<?php
		}
		
		echo $after_widget.'
<!-- pk end pk-categories widget -->

';
		
	}
	
}

class PK_Widget_Recent_Comments extends WP_Widget_Recent_Comments {
	
	function widget($args, $instance) {
		
		global $comments, $comment;
		
		$cache = wp_cache_get('widget_recent_comments', 'widget');
		
		if (!is_array($cache)) $cache = array();
		
		if (isset($cache[$args['widget_id']])) {
			
			echo $cache[$args['widget_id']];
			
			return;
			
		}
		
		extract($args, EXTR_SKIP);
		
		$output = '';
		
		$title = apply_filters('widget_title', empty($instance['title']) ? __('Recent Comments', 'pk_translate') : $instance['title']);
		
		if (!$number = absint($instance['number'])) $number = 5;
		
		$comments = get_comments(array('number' => $number, 'status' => 'approve'));
		
		$output .= '<!-- pk start pk-recent-comments widget -->
'.$before_widget.'
	';
		
		if ($title) $output .= $before_title.$title.$after_title;
		
		$output .= '
	<ul id="recentcomments">';
		
		if ($comments) {
			
			foreach ((array)$comments as $comment) {
				
				$output .= '
		<li class="recentcomments">'.sprintf(__('%1$s on %2$s', 'pk_translate'), get_comment_author_link(), '<a href="'.esc_url(get_comment_link($comment -> comment_ID)).'">'.get_the_title($comment -> comment_post_ID).'</a>').'</li>';
				
			}
			
		}
		
		$output .= '
	</ul>
';
		$output .= $after_widget.'
<!-- pk end pk-recent-comments widget -->

';
		
		echo $output;
		
		$cache[$args['widget_id']] = $output;
		
		wp_cache_set('widget_recent_comments', $cache, 'widget');
		
	}
	
	function form($instance) {
		
		$title = isset($instance['title']) ? esc_attr($instance['title']) : '';
		$number = isset($instance['number']) ? absint($instance['number']) : 5;
?>
		<p><label for="<?php echo $this -> get_field_id('title'); ?>"><?php _e('Title:', 'pk_translate'); ?></label>
		<input class="widefat" id="<?php echo $this -> get_field_id('title'); ?>" name="<?php echo $this -> get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" /></p>

		<p><label for="<?php echo $this -> get_field_id('number'); ?>"><?php _e('Number of posts to show:', 'pk_translate'); ?></label>
		<input class="widefat" id="<?php echo $this -> get_field_id('number'); ?>" name="<?php echo $this -> get_field_name('number'); ?>" type="text" value="<?php echo $number; ?>" /></p>
<?php
	}
	
}

class PK_Widget_Pages extends WP_Widget_Pages {
	
	function widget($args, $instance) {
		
		extract($args);
		
		$title = apply_filters('widget_title', empty($instance['title']) ? __('Pages', 'pk_translate') : $instance['title'], $instance, $this -> id_base);
		$sortby = empty($instance['sortby']) ? 'menu_order' : $instance['sortby'];
		$exclude = empty($instance['exclude']) ? '' : $instance['exclude'];
		
		if ($sortby == 'menu_order') $sortby = 'menu_order, post_title';
		
		$output = wp_list_pages(apply_filters('widget_pages_args', array('title_li' => '', 'echo' => 0, 'sort_column' => $sortby, 'exclude' => $exclude)));
		
		if (!empty($output)) {
			
			echo '<!-- pk start pk-pages widget -->
'.$before_widget.'
';
			
			if ($title) echo $before_title.$title.$after_title;
?>

<ul>
<?php
	echo $output;
?>
</ul>
<?php
			echo $after_widget.'
<!-- pk end pk-pages widget -->

';
		
		}
		
	}
	
	function form($instance) {
		
		$instance = wp_parse_args((array) $instance, array('sortby' => 'post_title', 'title' => '', 'exclude' => ''));
		$title = esc_attr($instance['title']);
		$exclude = empty($instance['exclude']) ? '' : esc_attr($instance['exclude']);
?>
		<p><label for="<?php echo $this -> get_field_id('title'); ?>"><?php _e('Title:', 'pk_translate'); ?></label>
		<input class="widefat" id="<?php echo $this -> get_field_id('title'); ?>" name="<?php echo $this -> get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" /></p>
		
		<p>
			<label for="<?php echo $this -> get_field_id('sortby'); ?>"><?php _e('Sort by:', 'pk_translate'); ?></label>
			<select name="<?php echo $this -> get_field_name('sortby'); ?>" id="<?php echo $this -> get_field_id('sortby'); ?>" class="widefat">
				<option value="menu_order"<?php selected($instance['sortby'], 'menu_order'); ?>><?php _e('Menu order', 'pk_translate'); ?></option>
				<option value="post_title"<?php selected($instance['sortby'], 'post_title'); ?>><?php _e('Page title', 'pk_translate'); ?></option>
				<option value="ID"<?php selected($instance['sortby'], 'ID'); ?>><?php _e('Page ID', 'pk_translate'); ?></option>
			</select>
		</p>
		
		<p>
			<label for="<?php echo $this -> get_field_id('exclude'); ?>"><?php _e('Exclude:', 'pk_translate'); ?></label>
			<input class="widefat" id="<?php echo $this -> get_field_id('exclude'); ?>" name="<?php echo $this -> get_field_name('exclude'); ?>" type="text" value="<?php echo $exclude; ?>" />
			<br /><small><?php _e('Page IDs, separated by commas.' ,'pk_translate'); ?></small>
		</p>
<?php
	}
	
}

class PK_Widget_Archives extends WP_Widget_Archives {
	
	function widget($args, $instance) {
		
		extract($args);
		
		$c = $instance['count'] ? '1' : '0';
		$d = $instance['dropdown'] ? '1' : '0';
		$title = apply_filters('widget_title', empty($instance['title']) ? __('Archives', 'pk_translate') : $instance['title'], $instance, $this -> id_base);
		
		echo '<!-- pk start pk-archives widget -->
'.$before_widget.'
	';
		
		if ($title) echo $before_title.$title.$after_title;
		
		if ($d) {
?>

	<select name="archive-dropdown" onchange='document.location.href=this.options[this.selectedIndex].value;'>
		<option value=""><?php echo esc_attr(__('Select Month', 'pk_translate')); ?></option>
<?php
			echo str_replace(array("<option", "> ", " </"), array("\t<option", ">", "</"), wp_get_archives(apply_filters('widget_archives_dropdown_args', array('type' => 'monthly', 'format' => 'option', 'show_post_count' => $c, 'echo' => 0))));
?>
	</select>
<?php
		} else {
?>

	<ul>
<?php
	echo str_replace("<li>", "\t<li>", wp_get_archives(apply_filters('widget_archives_args', array('type' => 'monthly', 'show_post_count' => $c, 'echo' => 0))));
?>
	</ul>
<?php
		}
		
		echo $after_widget.'
<!-- pk end pk-archives widget -->

';
		
	}
	
}

class PK_Nav_Menu_Widget extends WP_Nav_Menu_Widget {
	
	function PK_Nav_Menu_Widget() {
		
		$widget_ops = array('description' => __('Add one of your custom menus', 'pk_translate'));
		parent::WP_Widget('nav_menu', __('Custom Menu', 'pk_translate'), $widget_ops);
		
	}
	
	function widget($args, $instance) {
		
		$nav_menu = wp_get_nav_menu_object($instance['nav_menu']);
		
		if (!$nav_menu) return;
		
		$instance['title'] = apply_filters('widget_title', $instance['title'], $instance, $this -> id_base);
		
		echo '<!-- pk start pk-custom-menu widget -->
'.$args['before_widget'].'
';
		
		if (!empty($instance['title'])) echo $args['before_title'].$instance['title'].$args['after_title'].'
';
		
		wp_nav_menu(array('fallback_cb' => '', 'menu' => $nav_menu));
		
		echo '
'.$args['after_widget'].'
<!-- pk end pk-custom-menu widget -->

';
		
	}
	
	function form($instance) {
		
		$title = isset($instance['title']) ? $instance['title'] : '';
		$nav_menu = isset($instance['nav_menu']) ? $instance['nav_menu'] : '';
		
		$menus = get_terms('nav_menu', array('hide_empty' => false));
		
		if (!$menus) {
			
			echo '<p>'. sprintf(__('No menus have been created yet. <a href="%s">Create some</a>.', 'pk_translate'), admin_url('nav-menus.php')) .'</p>';
			return;
			
		}
?>
		<p>
			<label for="<?php echo $this -> get_field_id('title'); ?>"><?php _e('Title:', 'pk_translate') ?></label>
			<input type="text" class="widefat" id="<?php echo $this -> get_field_id('title'); ?>" name="<?php echo $this -> get_field_name('title'); ?>" value="<?php echo $title; ?>" />
		</p>
		<p>
			<label for="<?php echo $this -> get_field_id('nav_menu'); ?>"><?php _e('Select Menu:', 'pk_translate'); ?></label>
			<select class="widefat" id="<?php echo $this -> get_field_id('nav_menu'); ?>" name="<?php echo $this -> get_field_name('nav_menu'); ?>">
<?php
	foreach ($menus as $menu) {
		
		$selected = $nav_menu == $menu -> term_id ? ' selected="selected"' : '';
			echo '<option'. $selected .' value="'. $menu -> term_id .'">'. $menu -> name .'</option>';
		
	}
?>
			</select>
		</p>
<?php
	}
	
}

class PK_Widget_Text extends WP_Widget_Text {
	
	function PK_Widget_Text() {
		
		$widget_ops = array('description' => __('Arbitrary text, shortcodes or HTML', 'pk_translate'));
		$control_ops = array('width' => 400, 'height' => 350);
		$this -> WP_Widget('text', __('Text', 'pk_translate'), $widget_ops, $control_ops);
		
	}
	
	function widget($args, $instance) {
		
		extract($args);
		
		$title = apply_filters('widget_title', empty($instance['title']) ? '' : $instance['title'], $instance, $this -> id_base);
		$text = apply_filters('widget_text', $instance['text'], $instance);
		
		echo '<!-- pk start pk-text widget -->
'.$before_widget.'
	';
		
		if (!empty($title)) {
			
			echo $before_title.$title.$after_title;
			
		}
		
?>

	<div class="textwidget">
		<?php echo (isset($instance['filter']) && $instance['filter']) ? wpautop($text) : $text; ?>

	</div>
<?php
		
		echo $after_widget.'
<!-- pk end pk-text widget -->

';
		
	}
	
	function update($new_instance, $old_instance) {
		
		do_action('pk_ah_save_text_widget');
		
		$instance = $old_instance;
		
		$instance['title'] = strip_tags($new_instance['title']);
		
		if (current_user_can('unfiltered_html')) {
		
			$instance['text'] = $new_instance['text'];
			
		} else {
			
			$instance['text'] = stripslashes(wp_filter_post_kses(addslashes($new_instance['text'])));
			
		}
		
		$instance['filter'] = isset($new_instance['filter']);
		
		return $instance;
		
	}
	
	function form($instance) {
		
		$instance = wp_parse_args((array)$instance, array('title' => '', 'text' => ''));
		$title = strip_tags($instance['title']);
		$text = esc_textarea($instance['text']);
?>
		<p><label for="<?php echo $this -> get_field_id('title'); ?>"><?php _e('Title:', 'pk_translate'); ?></label>
		<input class="widefat" id="<?php echo $this -> get_field_id('title'); ?>" name="<?php echo $this -> get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" /></p>
		
		<textarea class="widefat" rows="16" cols="20" id="<?php echo $this -> get_field_id('text'); ?>" name="<?php echo $this -> get_field_name('text'); ?>"><?php echo $text; ?></textarea>
		
		<p><input id="<?php echo $this -> get_field_id('filter'); ?>" name="<?php echo $this -> get_field_name('filter'); ?>" type="checkbox" <?php checked(isset($instance['filter']) ? $instance['filter'] : 0); ?> />&nbsp;<label for="<?php echo $this -> get_field_id('filter'); ?>"><?php _e('Automatically add paragraphs', 'pk_translate'); ?></label></p>
<?php
	}
	
}

class PK_Widget_Search extends WP_Widget_Search {
	
	function widget($args, $instance) {
		
		extract($args);
		
		$title = apply_filters('widget_title', $instance['title'], $instance, $this -> id_base);
		
		echo '<!-- pk start pk-search widget -->
'.$before_widget.'
	';
		
		if ($title) echo $before_title.$title.$after_title.'
	';
		
		get_search_form();
		
		echo '
'.$after_widget.'
<!-- pk end pk-search widget -->

';
		
	}
	
}

class PK_Widget_RSS extends WP_Widget_RSS {
	
	function widget($args, $instance) {
		
		if (isset($instance['error']) && $instance['error']) return;
		
		extract($args, EXTR_SKIP);
		
		$url = $instance['url'];
		
		while (stristr($url, 'http') != $url) $url = substr($url, 1);
		
		if (empty($url)) return;
		
		if ($url == site_url() || $url == home_url()) return;
		
		$rss = fetch_feed($url);
		
		$title = $instance['title'];
		
		$desc = '';
		
		$link = '';
		
		if (!is_wp_error($rss)) {
			
			$desc = esc_attr(strip_tags(@html_entity_decode($rss -> get_description(), ENT_QUOTES, get_option('blog_charset'))));
			
			if (empty($title)) $title = esc_html(strip_tags($rss -> get_title()));
			
			$link = esc_url(strip_tags($rss -> get_permalink()));
			
			while (stristr($link, 'http') != $link) $link = substr($link, 1);
			
		}
		
		if (empty($title)) $title = empty($desc) ? __('Unknown Feed', 'pk_translate') : $desc;
		
		$title = apply_filters('widget_title', $title, $instance, $this -> id_base);
		
		$url = esc_url(strip_tags($url));
		
		$icon = includes_url('images/rss.png');
		
		if ($title) $title = "<a class='rsswidget' href='$url' title='".esc_attr__('Syndicate this content', 'pk_translate')."'><img style='border:0' width='14' height='14' src='$icon' alt='RSS' /></a><a class='rsswidget' href='$link' title='$desc'>$title</a>";
		
		echo '<!-- pk start pk-rss widget -->
'.$before_widget.'
	';
		
		if ($title) echo $before_title.$title.$after_title;
		
		wp_widget_rss_output($rss, $instance);
		
		echo '
'.$after_widget.'
<!-- pk end pk-rss widget -->

';
		
		if (!is_wp_error($rss)) $rss -> __destruct();
		
		unset($rss);
		
	}
	
}

class PK_Widget_Tag_Cloud extends WP_Widget_Tag_Cloud {
	
	function widget($args, $instance) {
		
		extract($args);
		
		$current_taxonomy = $this -> _get_current_taxonomy($instance);
		
		if (!empty($instance['title'])) {
			
			$title = $instance['title'];
			
		} else {
			
			if ('post_tag' == $current_taxonomy) {
				
				$title = __('Tags', 'pk_translate');
				
			} else {
				
				$tax = get_taxonomy($current_taxonomy);
				$title = $tax -> labels -> name;
				
			}
			
		}
		
		$title = apply_filters('widget_title', $title, $instance, $this -> id_base);
		
		echo '<!-- pk start pk-tag-cloud widget -->
'.$before_widget.'
	';
		
		if ($title) echo $before_title.$title.$after_title;
		
		echo '
	<div class="tagcloud">
';
		
		echo str_replace("<a", "\t\t<a", wp_tag_cloud(apply_filters('widget_tag_cloud_args', array('taxonomy' => $current_taxonomy, 'echo' => 0))));
		
		echo '
	</div>
';
		
		echo $after_widget.'
<!-- pk end pk-tag-cloud widget -->

';
		
	}
	
}

function pk_widgets_overrides() {
	
	unregister_widget('WP_Widget_Calendar');
	unregister_widget('WP_Widget_Links');
	unregister_widget('WP_Widget_Meta');
	unregister_widget('WP_Widget_Categories');
	unregister_widget('WP_Widget_Recent_Comments');
	unregister_widget('WP_Widget_Pages');
	unregister_widget('WP_Widget_Archives');
	unregister_widget('WP_Nav_Menu_Widget');
	unregister_widget('WP_Widget_Text');
	unregister_widget('WP_Widget_Tag_Cloud');
	unregister_widget('WP_Widget_RSS');
	unregister_widget('WP_Widget_Search');
	
	register_widget('PK_Widget_Calendar');
	register_widget('PK_Widget_Links');
	register_widget('PK_Widget_Meta');
	register_widget('PK_Widget_Categories');
	register_widget('PK_Widget_Recent_Comments');
	register_widget('PK_Widget_Pages');
	register_widget('PK_Widget_Archives');
	register_widget('PK_Nav_Menu_Widget');
	register_widget('PK_Widget_Text');
	register_widget('PK_Widget_Tag_Cloud');
	register_widget('PK_Widget_RSS');
	register_widget('PK_Widget_Search');
	
}

add_action('widgets_init', 'pk_widgets_overrides');

?>