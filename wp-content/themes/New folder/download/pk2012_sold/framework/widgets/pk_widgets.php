<?php

define('PK_THEME_WIDGETS', PK_FRAMEWORK.'/widgets');

require_once(PK_THEME_WIDGETS.'/pk_widgets_overrides.php');
require_once(PK_THEME_WIDGETS.'/pk_widgets_portfolio_categories.php');
require_once(PK_THEME_WIDGETS.'/pk_widgets_advertising_125_125.php');
require_once(PK_THEME_WIDGETS.'/pk_widgets_advertising_300_250.php');
require_once(PK_THEME_WIDGETS.'/pk_widgets_twitter.php');
require_once(PK_THEME_WIDGETS.'/pk_widgets_dribbble.php');
require_once(PK_THEME_WIDGETS.'/pk_widgets_twitter.php');
require_once(PK_THEME_WIDGETS.'/pk_widgets_flickr.php');
require_once(PK_THEME_WIDGETS.'/pk_widgets_google_maps.php');
require_once(PK_THEME_WIDGETS.'/pk_widgets_sub_navigation.php');
require_once(PK_THEME_WIDGETS.'/pk_widgets_featured_posts.php');
require_once(PK_THEME_WIDGETS.'/pk_widgets_popular_posts.php');
require_once(PK_THEME_WIDGETS.'/pk_widgets_recent_posts.php');
require_once(PK_THEME_WIDGETS.'/pk_widgets_related_posts.php');
require_once(PK_THEME_WIDGETS.'/pk_widgets_featured_works.php');
require_once(PK_THEME_WIDGETS.'/pk_widgets_popular_works.php');
require_once(PK_THEME_WIDGETS.'/pk_widgets_recent_works.php');
require_once(PK_THEME_WIDGETS.'/pk_widgets_related_works.php');
require_once(PK_THEME_WIDGETS.'/pk_widgets_testimonials.php');

?>