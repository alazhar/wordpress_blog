<?php

class PK_Widget_Featured_Works extends WP_Widget {
	
	function PK_Widget_Featured_Works() {
		
		$widget_ops = array('classname' => 'widget_featured_works', 'description' => __('Displays the featured works', 'pk_translate'));
		$this -> WP_Widget('pk-featured-works', __('Featured Works', 'pk_translate'), $widget_ops);
		$this -> alt_option_name = 'widget_featured_works';

		add_action('save_post', array(&$this, 'flush_widget_cache'));
		add_action('deleted_post', array(&$this, 'flush_widget_cache'));
		add_action('switch_theme', array(&$this, 'flush_widget_cache'));
		
	}
	
	function widget($args, $instance) {
		
		$cache = wp_cache_get('widget_featured_works', 'widget');
		
		$title = apply_filters('widget_title', $instance['title']);
		$number = absint($instance['number']);
		
		if (empty($title)) $title = false;
		
		$selected_portfolio_categories = (isset($instance['selected_portfolio_categories']) && !empty($instance['selected_portfolio_categories'])) ? $instance['selected_portfolio_categories'] : array();
		$selected_order_by = (isset($instance['selected_order_by']) && !empty($instance['selected_order_by'])) ? $instance['selected_order_by'] : 'date';

		if (!is_array($cache)) {
			
			$cache = array();
			
		}

		if (isset($cache[$args['widget_id']])) {
			
			echo $cache[$args['widget_id']];
			return;
			
		}
		
		ob_start();
		extract($args);
		
		$r = new WP_Query(array('post_type' => 'portfolio', 'posts_per_page' => $number, 'tax_query' => array(array('taxonomy' => 'taxonomy_portfolio', 'terms' => $selected_portfolio_categories)), 'nopaging' => 0, 'post_status' => 'publish', 'ignore_sticky_posts' => 1, 'orderby' => $selected_order_by, 'order' => ($selected_order_by == 'menu_order') ? 'ASC' : 'DESC'));
		
		if ($r -> have_posts()) : 
			
			echo '<!-- pk start pk-featured-works widget -->
'.$before_widget.'
	';
			
			if ($title) echo $before_title.$title.$after_title;
?>

	<ul>
<?php
				while ($r -> have_posts()) : 
					
					$r -> the_post();
?>
		<li<?php if (!has_post_thumbnail()) echo ' class="pk_no_thumbnail"'; ?>>
			<?php if (has_post_thumbnail()) the_post_thumbnail('thumb'); ?>

			<div>
				<h5><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h5>
				<?php add_filter('excerpt_length', 'pk_widgets_excerpt_filter'); add_filter('excerpt_more', 'pk_excerpt_more'); the_excerpt(); ?>
			</div>
		</li>
<?php				
				endwhile;
?>
	</ul>
<?php
			echo $after_widget.'
<!-- pk end pk-featured-works widget -->

';
		
		endif;
		
		wp_reset_postdata();
		
		$cache[$args['widget_id']] = ob_get_flush();
		
		wp_cache_set('widget_featured_works', $cache, 'widget');
		
	}
	
	function update($new_instance, $old_instance) {
		
		$instance = $old_instance;
		$instance['title'] = strip_tags($new_instance['title']);
		$instance['number'] = (int) $new_instance['number'];
		$instance['selected_portfolio_categories'] = $new_instance['selected_portfolio_categories'];
		$instance['selected_order_by'] = $new_instance['selected_order_by'];
		
		$this -> flush_widget_cache();

		$alloptions = wp_cache_get('alloptions', 'options');
		
		if (isset($alloptions['widget_featured_works'])) {
			
			delete_option('widget_featured_works');
			
		}
		
		return $instance;
		
	}
	
	function flush_widget_cache() {
		
		wp_cache_delete('widget_featured_works', 'widget');
		
	}
	
	function form($instance) {
		
		$title = isset($instance['title']) ? esc_attr($instance['title']) : '';
		$number = isset($instance['number']) ? absint($instance['number']) : 5;
		$selected_portfolio_categories = (isset($instance['selected_portfolio_categories']) && !empty($instance['selected_portfolio_categories'])) ? $instance['selected_portfolio_categories'] : array();
		$selected_order_by = (isset($instance['selected_order_by']) && !empty($instance['selected_order_by'])) ? $instance['selected_order_by'] : 'date';
?>
		<p><label for="<?php echo $this -> get_field_id('title'); ?>"><?php _e('Title:', 'pk_translate'); ?></label>
		<input class="widefat" id="<?php echo $this -> get_field_id('title'); ?>" name="<?php echo $this -> get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" /></p>
		
		<p><label for="<?php echo $this -> get_field_id('number'); ?>"><?php _e('Number of works to show:', 'pk_translate'); ?></label>
		<input class="widefat" id="<?php echo $this -> get_field_id('number'); ?>" name="<?php echo $this -> get_field_name('number'); ?>" type="text" value="<?php echo $number; ?>" size="3" /></p>
		
		<p>
		<label for="<?php echo $this -> get_field_id('selected_portfolio_categories'); ?>"><?php _e('Select categories:', 'pk_translate'); ?></label>
		<select class="widefat" id="<?php echo $this -> get_field_id('selected_portfolio_categories'); ?>" name="<?php echo $this -> get_field_name('selected_portfolio_categories'); ?>[]" multiple="multiple" style="height:auto"><?php
			
			$categories = get_categories(array('hide_empty' => 1, 'hierarchical' => false, 'taxonomy' => 'taxonomy_portfolio'));
			
			if ($categories) {
			
				foreach ($categories as $category) {
					
					if (in_array($category -> term_id, $selected_portfolio_categories)) {
						
						$selected_string = ' selected="selected"';
						
					} else {
						
						$selected_string = '';
						
					}
					
					echo '
				<option value="'.$category -> term_id.'"'.$selected_string.'>'.$category -> name.'</option>';
					
				}
				
			}
				
		?></select>
		</p>
		
		<p>
		<label for="<?php echo $this -> get_field_id('selected_order_by'); ?>"><?php _e('Order works by:', 'pk_translate'); ?></label>
		<select class="widefat" id="<?php echo $this -> get_field_id('selected_order_by'); ?>" name="<?php echo $this -> get_field_name('selected_order_by'); ?>">
			<option value="menu_order"<?php if ($selected_order_by == 'menu_order') echo ' selected="selected"';?>><?php _e('Manual Sort', 'pk_translate'); ?></option>
			<option value="date"<?php if ($selected_order_by == 'date') echo ' selected="selected"';?>><?php _e('Date', 'pk_translate'); ?></option>
			<option value="comment_count"<?php if ($selected_order_by == 'comment_count') echo ' selected="selected"';?>><?php _e('Comments Count', 'pk_translate'); ?></option>
			<option value="title"<?php if ($selected_order_by == 'title') echo ' selected="selected"';?>><?php _e('Title', 'pk_translate'); ?></option>
			<option value="author"<?php if ($selected_order_by == 'author') echo ' selected="selected"';?>><?php _e('Author', 'pk_translate'); ?></option>
			<option value="rand"<?php if ($selected_order_by == 'rand') echo ' selected="selected"';?>><?php _e('Random', 'pk_translate'); ?></option>
		</select>
		</p>
<?php
	}
	
}

function pk_widgets_featured_works() {
	
	register_widget('PK_Widget_Featured_Works');
	
}

add_action('widgets_init', 'pk_widgets_featured_works');

?>