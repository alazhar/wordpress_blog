<?php

class PK_Widget_Google_Maps extends WP_Widget {
	
	function PK_Widget_Google_Maps() {
		
		$widget_ops = array('classname' => 'widget_google_maps', 'description' => __('Displays Google Maps', 'pk_translate'));	
		$this -> WP_Widget('pk-google-maps', __('Google Maps', 'pk_translate'), $widget_ops);
		
	}
	
	function widget($args, $instance) {
		
		extract($args);
		
		$title = apply_filters('widget_title', $instance['title']);
		
		if (empty($title)) $title = false;
		
		$map_url = isset($instance['map_url']) ? esc_url($instance['map_url']) : '';
		$map_height = absint($instance['map_height']);
		
		echo '<!-- pk start pk-google-maps widget -->
'.$before_widget.'
	';
		
		if ($title) {
			
			echo $before_title;
			echo $title;
			echo $after_title;
			
		}
?>

	<div class="pk_widget_google_maps">
		<iframe src="<?php echo $map_url.'&amp;output=embed'; ?>" style="width:100%; height:<?php echo $map_height; ?>px;"></iframe>
	</div>
<?php
		echo $after_widget.'
<!-- pk end pk-google-maps widget -->

';
		
	}
	
	function update($new_instance, $old_instance) {
		
		$instance = $old_instance;
		
		$instance['title'] = strip_tags($new_instance['title']);
		$instance['map_url'] = esc_url($new_instance['map_url']);
		$instance['map_height'] = $new_instance['map_height'];
		
		return $instance;
		
	}
	
	function form($instance) {
		
		$title = isset($instance['title']) ? $instance['title'] : '';
		$map_url = isset($instance['map_url']) ? esc_url($instance['map_url']) : '';
		$map_height = isset($instance['map_height']) ? absint($instance['map_height']) : 300;
?>
		<p><label for="<?php echo $this -> get_field_id('title'); ?>"><?php _e('Title:', 'pk_translate'); ?></label>
		<input class="widefat" id="<?php echo $this -> get_field_id('title'); ?>" name="<?php echo $this -> get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" /></p>
		
		<p><label for="<?php echo $this -> get_field_id('map_url'); ?>"><?php _e('Map URL:', 'pk_translate'); ?></label>
		<input class="widefat" id="<?php echo $this -> get_field_id('map_url'); ?>" name="<?php echo $this -> get_field_name('map_url'); ?>" type="text" value="<?php echo $map_url; ?>" /></p>
		
		<p><label for="<?php echo $this -> get_field_id('map_height'); ?>"><?php _e('Map height:', 'pk_translate'); ?></label>
		<input class="widefat" id="<?php echo $this -> get_field_id('map_height'); ?>" name="<?php echo $this -> get_field_name('map_height'); ?>" type="text" value="<?php echo $map_height; ?>" /></p>
<?php
	}
	
}

function pk_widgets_google_maps() {
	
	register_widget('PK_Widget_Google_Maps');
	
}

add_action('widgets_init', 'pk_widgets_google_maps');

?>