<?php

class PK_Widget_Testimonials extends WP_Widget {
	
	function PK_Widget_Testimonials() {
		
		$widget_ops = array('classname' => 'widget_testimonials', 'description' => __('Displays testimonials', 'pk_translate'));	
		$this -> WP_Widget('pk-testimonials', __('Testimonials', 'pk_translate'), $widget_ops);
		
	}
	
	function widget($args, $instance) {
		
		extract($args);
		
		$title = apply_filters('widget_title', $instance['title']);
		
		if (empty($title)) $title = false;
		
		$number = absint($instance['number']);
		$instance_testimonials_cite = array();
		$instance_testimonials_quote = array();
		
		for ($i = 1; $i <= $number; $i++) {
			
			$testimonials_thumb = 'testimonials_'.$i.'_thumb';
			$instance_testimonials_thumb[$i] = isset($instance[$testimonials_thumb]) ? $instance[$testimonials_thumb] : '';
			$testimonials_cite = 'testimonials_'.$i.'_cite';
			$instance_testimonials_cite[$i] = isset($instance[$testimonials_cite]) ? $instance[$testimonials_cite] : '';
			$testimonials_quote = 'testimonials_'.$i.'_quote';
			$instance_testimonials_quote[$i] = isset($instance[$testimonials_quote]) ? $instance[$testimonials_quote] : '';
			
		}
		
		echo '<!-- pk start pk-testimonials widget -->
'.$before_widget.'
	';
		
		if ($title) {
			
			echo $before_title;
			echo $title;
			echo $after_title;
			
		}
?>

	<div class="pk_testimonials">
		<ul>
<?php
		for ($i = 1; $i <= $number; $i++) : 
?>
			<li<?php if (trim($instance_testimonials_thumb[$i]) == '') echo ' class="pk_no_thumbnail"'; ?>>
<?php if (trim($instance_testimonials_thumb[$i]) != '') : ?>
				<img src="<?php echo trim($instance_testimonials_thumb[$i]); ?>" alt="" />
<?php endif; ?>
				<blockquote>
					<p><?php echo $instance_testimonials_quote[$i]; ?></p>
					<cite><?php echo $instance_testimonials_cite[$i];?></cite>
				</blockquote>
			</li>
<?php	
		endfor;
?>
		</ul>
	</div>
<?php

		echo $after_widget.'
<!-- pk end pk-testimonials widget -->

';
		
	}
	
	function update($new_instance, $old_instance) {
		
		$instance = $old_instance;
		
		$instance['title'] = strip_tags($new_instance['title']);
		$instance['number'] = $new_instance['number'];
		for ($i = 1; $i <= absint($instance['number']); $i++) {
			
			$instance['testimonials_'.$i.'_thumb'] = $new_instance['testimonials_'.$i.'_thumb'];
			$instance['testimonials_'.$i.'_cite'] = $new_instance['testimonials_'.$i.'_cite'];
			$instance['testimonials_'.$i.'_quote'] = $new_instance['testimonials_'.$i.'_quote'];
			
		}
		
		return $instance;
		
	}
	
	function form($instance) {
		
		$title = isset($instance['title']) ? esc_attr($instance['title']) : '';
		$number = isset($instance['number']) ? absint($instance['number']) : 1;
		$instance_testimonials_thumb = array();
		$instance_testimonials_cite = array();
		$instance_testimonials_quote = array();
		
		for ($i = 1; $i <= $number; $i++) {
			
			$testimonials_thumb = 'testimonials_'.$i.'_thumb';
			$instance_testimonials_thumb[$i] = isset($instance[$testimonials_thumb]) ? $instance[$testimonials_thumb] : '';
			$testimonials_cite = 'testimonials_'.$i.'_cite';
			$instance_testimonials_cite[$i] = isset($instance[$testimonials_cite]) ? $instance[$testimonials_cite] : '';
			$testimonials_quote = 'testimonials_'.$i.'_quote';
			$instance_testimonials_quote[$i] = isset($instance[$testimonials_quote]) ? $instance[$testimonials_quote] : '';
			
		}
?>
		<p><label for="<?php echo $this -> get_field_id('title'); ?>"><?php _e('Title:', 'pk_translate'); ?></label>
		<input class="widefat" id="<?php echo $this -> get_field_id('title'); ?>" name="<?php echo $this -> get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" /></p>
		
		<p><label for="<?php echo $this -> get_field_id('number'); ?>"><?php _e('Number of testimonials to show:', 'pk_translate'); ?></label>
		<input class="widefat" id="<?php echo $this -> get_field_id('number'); ?>" name="<?php echo $this -> get_field_name('number'); ?>" type="text" value="<?php echo $number; ?>" /></p>
		
		<div>
<?php for ($i = 1; $i <= $number; $i++) : $testimonials_thumb = 'testimonials_'.$i.'_thumb'; $testimonials_cite = 'testimonials_'.$i.'_cite'; $testimonials_quote = 'testimonials_'.$i.'_quote'; ?>
			<div>
				<p><strong><?php _e('Testimonial', 'pk_translate'); ?> <?php echo $i; ?>:</strong></p>
				<p><label for="<?php echo $this -> get_field_id($testimonials_thumb); ?>"><?php _e('Thumbnail URL:', 'pk_translate'); ?></label>
				<input class="widefat" id="<?php echo $this -> get_field_id($testimonials_thumb); ?>" name="<?php echo $this -> get_field_name($testimonials_thumb); ?>" type="text" value="<?php echo $instance_testimonials_thumb[$i]; ?>" /></p>
				<p><label for="<?php echo $this -> get_field_id($testimonials_cite); ?>"><?php _e('Cite:', 'pk_translate'); ?></label>
				<input class="widefat" id="<?php echo $this -> get_field_id($testimonials_cite); ?>" name="<?php echo $this -> get_field_name($testimonials_cite); ?>" type="text" value="<?php echo $instance_testimonials_cite[$i]; ?>" /></p>
				<p><label for="<?php echo $this -> get_field_id($testimonials_quote); ?>"><?php _e('Quote:', 'pk_translate'); ?></label>
				<textarea class="widefat" rows="10" cols="10" id="<?php echo $this -> get_field_id($testimonials_quote); ?>" name="<?php echo $this -> get_field_name($testimonials_quote); ?>"><?php echo $instance_testimonials_quote[$i]; ?></textarea></p>
			</div>
<?php endfor;?>
		</div>
<?php
	}
	
}

function pk_widgets_testimonials() {
	
	register_widget('PK_Widget_Testimonials');
	
}

add_action('widgets_init', 'pk_widgets_testimonials');

?>