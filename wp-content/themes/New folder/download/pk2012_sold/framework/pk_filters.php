<?php

function pk_highlight_search_results($content) {
	
	$s = esc_html(get_search_query());
	
	if (!$s) return $content;
	
	$s = trim(ereg_replace(' +', ' ', $s));
	
	$keys = explode(' ', $s);
	
	return preg_replace('/('.implode('|', $keys).')/iu', '<span class="pk_highlight">\0</span>', $content);
	
}

function pk_add_post_types_to_author_page_query_string($query_string) {
	
	if (isset($query_string['author_name'])) $query_string['post_type'] = array('page', 'post', 'portfolio', 'faq', 'product');
	
	return $query_string;
	
}

add_filter('request', 'pk_add_post_types_to_author_page_query_string');

function pk_change_wp_login_url() {
	
	return home_url();
	
}

add_filter('login_headerurl', 'pk_change_wp_login_url');

function pk_change_wp_login_title() {
	
	return 'Powered by '.get_bloginfo('name');
	
}

add_filter('login_headertitle', 'pk_change_wp_login_title');

function pk_redirect_after_login($redirect_to, $request, $user) {
	
	if (isset($user -> roles) && is_array($user -> roles )) {
		
		if (in_array('administrator', $user -> roles))
			return home_url( '/wp-admin/' );
		else
			return $_SERVER[HTTP_REFERER];
		
	}
	
}

add_filter('login_redirect', 'pk_redirect_after_login', 10, 3);

function pk_redirect_after_logout() {
	
	wp_redirect(home_url());
	exit();
	
}

add_filter('wp_logout', 'pk_redirect_after_logout');

function pk_pre_get_posts($query) {
	
	if (is_admin() || is_singular()) {
		
		return $query;
		
	}
	
	if ((is_home() || is_archive() || is_search() || is_author()) && !is_tax() && !is_post_type_archive() && !is_feed()) {
		
		$query -> set('posts_per_page', (int)pk_get_option('blog_layout', '10') * (int)pk_get_option('blog_layout_rows', '1'));
		
		return $query;
		
	}
	
	if (is_tax('taxonomy_portfolio') || is_post_type_archive('portfolio')) {
		
		$query -> set('posts_per_page', (int)pk_get_option('portfolio_layout', '4') * (int)pk_get_option('portfolio_layout_rows', '3'));
		
		return $query;
		
	}
	
	return $query;
	
}

add_filter('pre_get_posts', 'pk_pre_get_posts');

function pk_blog_excerpt_filter($length) {
	
	return 30;
	
}

function pk_grid_excerpt_filter($length) {
	
	return 15;
	
}

function pk_widgets_excerpt_filter($length) {
	
	return 15;
	
}

function pk_excerpt_more($more) {
	
	return '&hellip;';
	
}

add_filter('excerpt_more', 'pk_excerpt_more');

function pk_portfolio_embed_defaults($embed_size){
	
	$embed_size['width'] = (pk_sidebar()) ? 580 : 940;
	
	return $embed_size;
	
}

function pk_blog_embed_defaults($embed_size){
	
	$embed_size['width'] = (pk_sidebar()) ? 580 : 940;
	
	return $embed_size;
	
}

function pk_post_embed_defaults($embed_size){
	
	$embed_size['width'] = (pk_sidebar()) ? 490 : 850;
	
	return $embed_size;
	
}

function pk_full_width_page_embed_defaults($embed_size){
	
	$embed_size['width'] = 940;
	
	return $embed_size;
	
}

function pk_page_embed_defaults($embed_size){
	
	$embed_size['width'] = (is_page_template('page-full-width.php')) ? 940 : 580;
	
	return $embed_size;
	
}

function pk_faq_page_embed_defaults($embed_size){
	
	$embed_size['width'] = 870;
	
	return $embed_size;
	
}

function pk_categories_widget($categories_args) {
	
	$categories_args['title_li'] = '';
		
 	return $categories_args;
	
}

add_filter('widget_categories_args', 'pk_categories_widget');

function pk_embed_oembed_html($html, $url, $attr, $post_id) {
	
	return '<div class="pk_video">'.$html.'</div>';
	
}

add_filter('embed_oembed_html', 'pk_embed_oembed_html', 99, 4);

function pk_exclude_from_gallery_fields($attachment) {
	
	$value = get_post_meta($attachment -> ID, '_exclude_from_gallery', true);
	
	if (!isset($value) || $value == '') $value = 'no';
	
	return "<input type='radio' name='attachments[{$attachment -> ID}][exclude_from_gallery]' id='exclude_from_gallery_{$attachment -> ID}' value='yes'".('yes' == $value ? " checked='checked'" : "")." /><label for='exclude_from_gallery_{$attachment -> ID}'>".__('Yes', 'pk_translate')."</label><input type='radio' name='attachments[{$attachment -> ID}][exclude_from_gallery]' id='exclude_from_gallery_{$attachment -> ID}' value='no'".('no' == $value ? " checked='checked'" : "")." /><label for='exclude_from_gallery_{$attachment -> ID}'>".__('No', 'pk_translate')."</label>";
	
}

function pk_attachment_fields($form_fields, $attachment) {
	
	$form_fields['exclude_from_gallery'] = array(
		'label' => __('Exclude from gallery', 'pk_translate'),
		'input' => 'html',
		'html' 	=> pk_exclude_from_gallery_fields($attachment),
		'helps' => __('Select to exclude or not this image from the post\'s gallery..', 'pk_translate')
	);
	
	$form_fields['image_link'] = array(
		'label' => __('Image Custom Link', 'pk_translate'),
		'input' => 'text',
		'value' => get_post_meta($attachment -> ID, '_image_link', true),
		'helps' => __('Add a custom link, internal or external, to open on click over the image.', 'pk_translate')
	);
	
	$form_fields['image_link_target'] = array(
		'label' => __('Image Custom Link Target', 'pk_translate'),
		'input' => 'text',
		'value' => get_post_meta($attachment -> ID, '_image_link_target', true),
		'helps' => __('Add the custom link target, "_self" or "_blank". If unset the default value will be "_self".', 'pk_translate')
	);
	
	$form_fields['video_url'] = array(
		'label' => __('Video URL', 'pk_translate'),
		'input' => 'text',
		'value' => get_post_meta($attachment -> ID, '_video_url', true),
		'helps' => __('Add video URL (YouTube, Vimeo, DailyMotion, MetaCafe, Twitvid).', 'pk_translate')
	);
	
	$form_fields['video_urls'] = array(
		'label' => __('Video File/s', 'pk_translate'),
		'input' => 'text',
		'value' => get_post_meta($attachment -> ID, '_video_urls', true),
		'helps' => __('Add video file/s URL: m4v (mandatory), ogv, webmv, flv. Enter a comma separated list of different formats of the same video file, for the best cross browser compatibility.', 'pk_translate')
	);
	
	$form_fields['audio_urls'] = array(
		'label' => __('Audio File/s', 'pk_translate'),
		'input' => 'text',
		'value' => get_post_meta($attachment -> ID, '_audio_urls', true),
		'helps' => __('Add audio file/s URL: mp3 (mandatory), m4a (mandatory), oga, webma, wav, fla. Enter a comma separated list of different formats of the same audio file, for the best cross browser compatibility.', 'pk_translate')
	);
	
	$form_fields['title_properties'] = array(
		'label' => __('Title Properties', 'pk_translate'),
		'input' => 'textarea',
		'value' => get_post_meta($attachment -> ID, '_title_properties', true),
		'helps' => __('Add slide title properties. This option is used only by homepages sliders. Example: <strong>{\'x\':\'100\', \'y\':\'100\', \'width\':\'350\', \'color\':\'#ffffff\', \'bg_color\':\'#000000\'}</strong>', 'pk_translate')
	);
	
	$form_fields['description_properties'] = array(
		'label' => __('Description Properties', 'pk_translate'),
		'input' => 'textarea',
		'value' => get_post_meta($attachment -> ID, '_description_properties', true),
		'helps' => __('Add slide description properties. This option is used only by homepages sliders. Example: <strong>{\'x\':\'100\', \'y\':\'150\', \'width\':\'350\', \'color\':\'#ffffff\', \'bg_color\':\'#000000\'}</strong>', 'pk_translate')
	);
	
	$form_fields['snap_points'] = array(
		'label' => __('Snap Points', 'pk_translate'),
		'input' => 'textarea',
		'value' => get_post_meta($attachment -> ID, '_snap_points', true),
		'helps' => __('Add image snap points. This option is used only by homepages sliders.', 'pk_translate')
	);
	
	return $form_fields;
	
}

add_filter('attachment_fields_to_edit', 'pk_attachment_fields', 10, 2);

function pk_attachment_fields_save($post, $attachment) {
	
	if (isset($attachment['exclude_from_gallery'])) update_post_meta($post['ID'], '_exclude_from_gallery', $attachment['exclude_from_gallery']);
	if (isset($attachment['image_link'])) update_post_meta($post['ID'], '_image_link', $attachment['image_link']);
	if (isset($attachment['image_link_target'])) update_post_meta($post['ID'], '_image_link_target', $attachment['image_link_target']);
	if (isset($attachment['video_url'])) update_post_meta($post['ID'], '_video_url', $attachment['video_url']);
	if (isset($attachment['video_urls'])) update_post_meta($post['ID'], '_video_urls', $attachment['video_urls']);
	if (isset($attachment['audio_urls'])) update_post_meta($post['ID'], '_audio_urls', $attachment['audio_urls']);
	if (isset($attachment['title_properties'])) update_post_meta($post['ID'], '_title_properties', $attachment['title_properties']);
	if (isset($attachment['description_properties'])) update_post_meta($post['ID'], '_description_properties', $attachment['description_properties']);
	if (isset($attachment['snap_points'])) update_post_meta($post['ID'], '_snap_points', $attachment['snap_points']);
	
	return $post;
	
}

add_filter('attachment_fields_to_save', 'pk_attachment_fields_save', 10, 2);

function pk_wmode_transparent($html, $url, $args) { 
	
	if (strpos($html, '<param name="movie"') !== false) $html = preg_replace('|</param>|', '</param><param name="wmode" value="transparent"></param>', $html, 1);
	if (strpos($html, '<embed') !== false) $html = str_replace('<embed', '<embed wmode="transparent"', $html);
	
	return $html;
	
}

add_filter('oembed_result', 'pk_wmode_transparent', 10, 3);

function pk_add_custom_field_automatically($post_id) {
	
	$post_type = get_post_type($post_id);
	$post_format = '';
	
	if ($post_type == 'post') $post_format = (!get_post_format($post_id)) ? 'standard' : get_post_format($post_id);
	if ($post_type == 'portfolio') $post_format = get_post_meta($post_id, '_work_format', true);
	
	if ($post_type == 'page') {
		
		$post_template = get_post_meta($post_id, '_wp_page_template', true);
		
		if (in_array($post_template, array('page-blog.php', 'page-portfolio.php', 'page-faq.php', 'page-home-agency.php', 'page-home-business.php', 'page-home-freelance.php', 'page-home-standard.php', 'page-home-jigoshop.php', 'page-home-woocommerce.php'))) {
			
			$post_format = $post_template;
			
		} else {
			
			$post_format = get_post_meta($post_id, '_page_format', true);
			
		}
		
	}
	
	if (!wp_is_post_revision($post_id)) {
		
		if ($post_type == 'post') add_post_meta($post_id, '_sidebar', 'show', true);
		if ($post_type == 'portfolio') add_post_meta($post_id, '_sidebar', 'show', true);
		if ($post_type == 'product') add_post_meta($post_id, '_sidebar', 'show', true);
		
		if ($post_type == 'post' && $post_format == 'link') add_post_meta($post_id, '_link', '#', true);
		if ($post_type == 'post' && $post_format == 'quote') add_post_meta($post_id, '_quote', 'Enter the quote text in the dedicated custom field. Edit this post to change this value!', true);
		if ($post_type == 'post' && $post_format == 'quote') add_post_meta($post_id, '_cite', 'parker&amp;kent team', true);
		if ($post_type == 'post' && $post_format == 'gallery') add_post_meta($post_id, '_slider_info_button_open_label', 'info', true);
		if ($post_type == 'post' && $post_format == 'gallery') add_post_meta($post_id, '_slider_info_button_close_label', 'close info', true);
		if ($post_type == 'post' && $post_format == 'gallery') add_post_meta($post_id, '_slider_slideshow', 'true', true);
		if ($post_type == 'post' && $post_format == 'gallery') add_post_meta($post_id, '_slider_slideshow_auto_start', 'false', true);
		if ($post_type == 'post' && $post_format == 'gallery') add_post_meta($post_id, '_slider_slideshow_interval', '5', true);
		
		if ($post_type == 'portfolio' && $post_format == 'slider') add_post_meta($post_id, '_slider_info_button_open_label', 'info', true);
		if ($post_type == 'portfolio' && $post_format == 'slider') add_post_meta($post_id, '_slider_info_button_close_label', 'close info', true);
		if ($post_type == 'portfolio' && $post_format == 'slider') add_post_meta($post_id, '_slideshow', 'true', true);
		if ($post_type == 'portfolio' && $post_format == 'slider') add_post_meta($post_id, '_slider_slideshow_auto_start', 'false', true);
		if ($post_type == 'portfolio' && $post_format == 'slider') add_post_meta($post_id, '_slider_slideshow_interval', '5', true);
				
		if ($post_type == 'page' && $post_format == 'slider') add_post_meta($post_id, '_slider_info_button_open_label', 'info', true);
		if ($post_type == 'page' && $post_format == 'slider') add_post_meta($post_id, '_slider_info_button_close_label', 'close info', true);
		if ($post_type == 'page' && $post_format == 'slider') add_post_meta($post_id, '_slider_slideshow', 'true', true);
		if ($post_type == 'page' && $post_format == 'slider') add_post_meta($post_id, '_slider_slideshow_auto_start', 'false', true);
		if ($post_type == 'page' && $post_format == 'slider') add_post_meta($post_id, '_slider_slideshow_interval', '5', true);
		
		if ($post_type == 'page' && $post_format == 'page-faq.php') add_post_meta($post_id, '_faq_type', 'accordion', true);
		
		if ($post_type == 'page' && $post_format == 'page-blog.php') add_post_meta($post_id, '_sidebar', 'show', true);
		if ($post_type == 'page' && $post_format == 'page-blog.php') add_post_meta($post_id, '_grid_layout', 'normal', true);
		if ($post_type == 'page' && $post_format == 'page-blog.php') add_post_meta($post_id, '_columns', '1', true);
		if ($post_type == 'page' && $post_format == 'page-blog.php') add_post_meta($post_id, '_rows', '10', true);
		if ($post_type == 'page' && $post_format == 'page-blog.php') add_post_meta($post_id, '_show_categories_filter', 'false', true);
		if ($post_type == 'page' && $post_format == 'page-blog.php') add_post_meta($post_id, '_blog_categories', '', true);
		
		if ($post_type == 'page' && $post_format == 'page-portfolio.php') add_post_meta($post_id, '_sidebar', 'show', true);
		if ($post_type == 'page' && $post_format == 'page-portfolio.php') add_post_meta($post_id, '_grid_layout', 'normal', true);
		if ($post_type == 'page' && $post_format == 'page-portfolio.php') add_post_meta($post_id, '_columns', '4', true);
		if ($post_type == 'page' && $post_format == 'page-portfolio.php') add_post_meta($post_id, '_rows', '3', true);
		if ($post_type == 'page' && $post_format == 'page-portfolio.php') add_post_meta($post_id, '_show_categories_filter', 'false', true);
		if ($post_type == 'page' && $post_format == 'page-portfolio.php') add_post_meta($post_id, '_portfolio_categories', '', true);
		
		if ($post_type == 'page' && $post_format == 'page-home-agency.php') add_post_meta($post_id, '_slider_navigation_align', 'right', true);	
		if ($post_type == 'page' && $post_format == 'page-home-agency.php') add_post_meta($post_id, '_slider_transition', 'fade', true);	
		if ($post_type == 'page' && $post_format == 'page-home-agency.php') add_post_meta($post_id, '_slider_background', '', true);
		if ($post_type == 'page' && $post_format == 'page-home-agency.php') add_post_meta($post_id, '_slider_slideshow_auto_start', 'true', true);
		if ($post_type == 'page' && $post_format == 'page-home-agency.php') add_post_meta($post_id, '_slider_slideshow_interval', '7', true);
		if ($post_type == 'page' && $post_format == 'page-home-agency.php') add_post_meta($post_id, '_total_works', '4', true);
		if ($post_type == 'page' && $post_format == 'page-home-agency.php') add_post_meta($post_id, '_portfolio_categories', '', true);
		if ($post_type == 'page' && $post_format == 'page-home-agency.php') add_post_meta($post_id, '_view_all_button_color', 'white', true);
		
		if ($post_type == 'page' && $post_format == 'page-home-business.php') add_post_meta($post_id, '_slider_navigation_align', 'center', true);	
		if ($post_type == 'page' && $post_format == 'page-home-business.php') add_post_meta($post_id, '_slider_transition', 'fade', true);
		if ($post_type == 'page' && $post_format == 'page-home-business.php') add_post_meta($post_id, '_slider_background', '', true);
		if ($post_type == 'page' && $post_format == 'page-home-business.php') add_post_meta($post_id, '_slider_slideshow_auto_start', 'true', true);
		if ($post_type == 'page' && $post_format == 'page-home-business.php') add_post_meta($post_id, '_slider_slideshow_interval', '7', true);
		if ($post_type == 'page' && $post_format == 'page-home-business.php') add_post_meta($post_id, '_feed_url', 'http://rss.cnn.com/rss/edition_business.rss', true);
		if ($post_type == 'page' && $post_format == 'page-home-business.php') add_post_meta($post_id, '_feed_total_items', '5', true);
		if ($post_type == 'page' && $post_format == 'page-home-business.php') add_post_meta($post_id, '_tabs_button_color', 'white', true);
		if ($post_type == 'page' && $post_format == 'page-home-business.php') add_post_meta($post_id, '_tabs_button_link', '#', true);
		if ($post_type == 'page' && $post_format == 'page-home-business.php') add_post_meta($post_id, '_tabs_button_label', 'Button Label', true);
		
		if ($post_type == 'page' && $post_format == 'page-home-freelance.php') add_post_meta($post_id, '_slider_navigation_align', 'center', true);	
		if ($post_type == 'page' && $post_format == 'page-home-freelance.php') add_post_meta($post_id, '_slider_transition', 'fade', true);
		if ($post_type == 'page' && $post_format == 'page-home-freelance.php') add_post_meta($post_id, '_slider_background', '', true);
		if ($post_type == 'page' && $post_format == 'page-home-freelance.php') add_post_meta($post_id, '_slider_slideshow_auto_start', 'true', true);
		if ($post_type == 'page' && $post_format == 'page-home-freelance.php') add_post_meta($post_id, '_slider_slideshow_interval', '7', true);
		if ($post_type == 'page' && $post_format == 'page-home-freelance.php') add_post_meta($post_id, '_call_to_action_text', 'Call to action text!', true);
		if ($post_type == 'page' && $post_format == 'page-home-freelance.php') add_post_meta($post_id, '_total_works', '4', true);
		if ($post_type == 'page' && $post_format == 'page-home-freelance.php') add_post_meta($post_id, '_portfolio_categories', '', true);
		if ($post_type == 'page' && $post_format == 'page-home-freelance.php') add_post_meta($post_id, '_view_all_button_color', 'white', true);
		
		if ($post_type == 'page' && $post_format == 'page-home-standard.php') add_post_meta($post_id, '_slider_navigation_align', 'left', true);	
		if ($post_type == 'page' && $post_format == 'page-home-standard.php') add_post_meta($post_id, '_slider_transition', 'fade', true);
		if ($post_type == 'page' && $post_format == 'page-home-standard.php') add_post_meta($post_id, '_slider_background', '', true);
		if ($post_type == 'page' && $post_format == 'page-home-standard.php') add_post_meta($post_id, '_slider_slideshow_auto_start', 'true', true);
		if ($post_type == 'page' && $post_format == 'page-home-standard.php') add_post_meta($post_id, '_slider_slideshow_interval', '7', true);
		if ($post_type == 'page' && $post_format == 'page-home-standard.php') add_post_meta($post_id, '_call_to_action_text', 'Call to action text!', true);
		if ($post_type == 'page' && $post_format == 'page-home-standard.php') add_post_meta($post_id, '_call_to_action_button_color', 'white', true);
		if ($post_type == 'page' && $post_format == 'page-home-standard.php') add_post_meta($post_id, '_call_to_action_button_link', '#', true);
		if ($post_type == 'page' && $post_format == 'page-home-standard.php') add_post_meta($post_id, '_call_to_action_button_label', 'Button Label', true);
		if ($post_type == 'page' && $post_format == 'page-home-standard.php') add_post_meta($post_id, '_total_works', '3', true);
		if ($post_type == 'page' && $post_format == 'page-home-standard.php') add_post_meta($post_id, '_portfolio_categories', '', true);
		if ($post_type == 'page' && $post_format == 'page-home-standard.php') add_post_meta($post_id, '_total_posts', '3', true);
		if ($post_type == 'page' && $post_format == 'page-home-standard.php') add_post_meta($post_id, '_blog_categories', '', true);
		if ($post_type == 'page' && $post_format == 'page-home-standard.php') add_post_meta($post_id, '_view_all_button_color', 'white', true);
		
		if ($post_type == 'page' && $post_format == 'page-home-jigoshop.php') add_post_meta($post_id, '_slider_navigation_align', 'center', true);	
		if ($post_type == 'page' && $post_format == 'page-home-jigoshop.php') add_post_meta($post_id, '_slider_transition', 'fade', true);
		if ($post_type == 'page' && $post_format == 'page-home-jigoshop.php') add_post_meta($post_id, '_slider_background', '', true);
		if ($post_type == 'page' && $post_format == 'page-home-jigoshop.php') add_post_meta($post_id, '_slider_slideshow_auto_start', 'true', true);
		if ($post_type == 'page' && $post_format == 'page-home-jigoshop.php') add_post_meta($post_id, '_slider_slideshow_interval', '7', true);
		if ($post_type == 'page' && $post_format == 'page-home-jigoshop.php') add_post_meta($post_id, '_call_to_action_text', 'Call to action text!', true);
		if ($post_type == 'page' && $post_format == 'page-home-jigoshop.php') add_post_meta($post_id, '_call_to_action_button_color', 'white', true);
		if ($post_type == 'page' && $post_format == 'page-home-jigoshop.php') add_post_meta($post_id, '_call_to_action_button_link', ((function_exists('jigoshop_get_page_id')) ? get_permalink((function_exists('icl_object_id')) ? icl_object_id(jigoshop_get_page_id('shop'), 'page', true) : jigoshop_get_page_id('shop')) : '#'), true);
		if ($post_type == 'page' && $post_format == 'page-home-jigoshop.php') add_post_meta($post_id, '_call_to_action_button_label', 'Button Label', true);
		if ($post_type == 'page' && $post_format == 'page-home-jigoshop.php') add_post_meta($post_id, '_total_recent_products', '2', true);
		if ($post_type == 'page' && $post_format == 'page-home-jigoshop.php') add_post_meta($post_id, '_total_featured_products', '3', true);
		if ($post_type == 'page' && $post_format == 'page-home-jigoshop.php') add_post_meta($post_id, '_view_all_button_color', 'white', true);
		
		if ($post_type == 'page' && $post_format == 'page-home-woocommerce.php') add_post_meta($post_id, '_slider_navigation_align', 'center', true);	
		if ($post_type == 'page' && $post_format == 'page-home-woocommerce.php') add_post_meta($post_id, '_slider_transition', 'fade', true);
		if ($post_type == 'page' && $post_format == 'page-home-woocommerce.php') add_post_meta($post_id, '_slider_background', '', true);
		if ($post_type == 'page' && $post_format == 'page-home-woocommerce.php') add_post_meta($post_id, '_slider_slideshow_auto_start', 'true', true);
		if ($post_type == 'page' && $post_format == 'page-home-woocommerce.php') add_post_meta($post_id, '_slider_slideshow_interval', '7', true);
		if ($post_type == 'page' && $post_format == 'page-home-woocommerce.php') add_post_meta($post_id, '_call_to_action_text', 'Call to action text!', true);
		if ($post_type == 'page' && $post_format == 'page-home-woocommerce.php') add_post_meta($post_id, '_call_to_action_button_color', 'white', true);
		if ($post_type == 'page' && $post_format == 'page-home-woocommerce.php') add_post_meta($post_id, '_call_to_action_button_link', ((function_exists('woocommerce_get_page_id')) ? get_permalink((function_exists('icl_object_id')) ? icl_object_id(woocommerce_get_page_id('shop'), 'page', true) : woocommerce_get_page_id('shop')) : '#'), true);
		if ($post_type == 'page' && $post_format == 'page-home-woocommerce.php') add_post_meta($post_id, '_call_to_action_button_label', 'Button Label', true);
		if ($post_type == 'page' && $post_format == 'page-home-woocommerce.php') add_post_meta($post_id, '_total_recent_products', '2', true);
		if ($post_type == 'page' && $post_format == 'page-home-woocommerce.php') add_post_meta($post_id, '_total_featured_products', '3', true);
		if ($post_type == 'page' && $post_format == 'page-home-woocommerce.php') add_post_meta($post_id, '_view_all_button_color', 'white', true);
		
	}
	
}

add_action('publish_page', 'pk_add_custom_field_automatically');
add_action('update_page', 'pk_add_custom_field_automatically');
add_action('publish_post', 'pk_add_custom_field_automatically');
add_action('update_post', 'pk_add_custom_field_automatically');
add_action('publish_portfolio', 'pk_add_custom_field_automatically');
add_action('update_portfolio', 'pk_add_custom_field_automatically');
add_action('publish_product', 'pk_add_custom_field_automatically');
add_action('update_product', 'pk_add_custom_field_automatically');

function pk_ot_update_options($options) {
	
	if (defined('ICL_LANGUAGE_CODE') && ICL_LANGUAGE_CODE != 'all') : 
		
		update_option(PK_THEME_SLUG.'_option_tree_'.ICL_LANGUAGE_CODE, $options);
		return $options;
		
	endif;
	
	if (function_exists('qtrans_getLanguage') && qtrans_getLanguage() != '') : 
		
		update_option(PK_THEME_SLUG.'_option_tree_'.qtrans_getLanguage(), $options);
		return $options;
		
	endif;
	
	update_option(PK_THEME_SLUG.'_option_tree', $options);
	return $options;	
	
}

add_filter('pre_update_option_option_tree', 'pk_ot_update_options');

function pk_ot_get_options($options) {
	
	if (defined('ICL_LANGUAGE_CODE') && ICL_LANGUAGE_CODE != 'all') : 
		
		$trans_options = get_option(PK_THEME_SLUG.'_option_tree_'.ICL_LANGUAGE_CODE);
		
		if (!$trans_options || $trans_options == '') {
			
			global $sitepress;
			$trans_options = get_option(PK_THEME_SLUG.'_option_tree_'.$sitepress -> get_default_language());
			
		}
		
		return $trans_options;
		
	endif;
	
	if (function_exists('qtrans_getLanguage') && qtrans_getLanguage() != '') : 
		
		$trans_options = get_option(PK_THEME_SLUG.'_option_tree_'.qtrans_getLanguage());
		
		if (!$trans_options || $trans_options == '') {
			
			$trans_options = get_option(PK_THEME_SLUG.'_option_tree_'.get_option('qtranslate_default_language'));
			
		}
		
		return $trans_options;
		
	endif;
	
	return get_option(PK_THEME_SLUG.'_option_tree');	
	
}

add_filter('option_option_tree', 'pk_ot_get_options');

function pk_ot_update_layouts($options) {
	
	if (defined('ICL_LANGUAGE_CODE') && ICL_LANGUAGE_CODE != 'all') : 
		
		update_option(PK_THEME_SLUG.'_option_tree_layouts_'.ICL_LANGUAGE_CODE, $options);
		return $options;
		
	endif;
	
	if (function_exists('qtrans_getLanguage') && qtrans_getLanguage() != '') : 
		
		update_option(PK_THEME_SLUG.'_option_tree_layouts_'.qtrans_getLanguage(), $options);
		return $options;
		
	endif;
	
	update_option(PK_THEME_SLUG.'_option_tree_layouts', $options);
	return $options;	
	
}

add_filter('pre_update_option_option_tree_layouts', 'pk_ot_update_layouts');

function pk_ot_get_layouts($options) {
	
	if (defined('ICL_LANGUAGE_CODE') && ICL_LANGUAGE_CODE != 'all') : 
		
		return get_option(PK_THEME_SLUG.'_option_tree_layouts_'.ICL_LANGUAGE_CODE);
		
	endif;
	
	if (function_exists('qtrans_getLanguage') && qtrans_getLanguage() != '') : 
		
		return get_option(PK_THEME_SLUG.'_option_tree_layouts_'.qtrans_getLanguage());
		
	endif;
	
	return get_option(PK_THEME_SLUG.'_option_tree_layouts');	
	
}

add_filter('option_option_tree_layouts', 'pk_ot_get_layouts');

remove_filter('comment_text', 'make_clickable', 9);

if (!current_user_can('edit_posts')) add_filter('show_admin_bar', '__return_false');

?>