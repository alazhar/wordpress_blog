<?php

function custom_meta_boxes() {
	
	$post_id = (isset($_GET['post'])) ? $_GET['post'] : ((isset($_POST['post_ID'])) ? $_POST['post_ID'] : false);
	
	if (!$post_id) return;
	
	$post_type = get_post_type($post_id);
	
	$post_format = '';
	
	if ($post_type == 'post') {
		
		$post_format = (!get_post_format($post_id)) ? 'standard' : get_post_format($post_id);
		
	}
	
	if ($post_type == 'portfolio') {
		
		$post_format = get_post_meta($post_id, '_work_format', true);
		
	}
	
	if ($post_type == 'page') {
		
		$post_template = get_post_meta($post_id, '_wp_page_template', true);
		
		if (in_array($post_template, array('page-blog.php', 'page-portfolio.php', 'page-faq.php', 'page-home-agency.php', 'page-home-business.php', 'page-home-freelance.php', 'page-home-standard.php', 'page-home-jigoshop.php', 'page-home-woocommerce.php'))) {
			
			$post_format = $post_template;
			
		} else {
			
			$post_format = get_post_meta($post_id, '_page_format', true);
			
		}
		
	}
	
	$page_faq = array(
		'id' => 'page_faq',
		'title' => __('Options', 'pk_translate'),
		'desc' => '',
		'pages' => array('page'),
		'context' => 'normal',
		'priority' => 'high',
		'fields' => array(
			array(
				'id' => '_faq_type',
				'label' => __('FAQ component type', 'pk_translate'),
				'desc' => __('Select an option.', 'pk_translate'),
				'std' => 'accordion',
				'type' => 'radio',
				'choices' => array(
					array(
						'value' => 'accordion',
						'label' => __('Accordion', 'pk_translate')
					),
					array(
						'value' => 'toggle',
						'label' => __('Toggle', 'pk_translate')
					)
				)
			)
		)
	);
	
	$page_blog = array(
		'id' => 'page_blog',
		'title' => __('Options', 'pk_translate'),
		'desc' => '',
		'pages' => array('page'),
		'context' => 'normal',
		'priority' => 'high',
		'fields' => array(
			array(
				'id' => '_sidebar',
				'label' => __('Sidebar', 'pk_translate'),
				'desc' => __('Select "Hide" for a full width layout.', 'pk_translate'),
				'std' => 'show',
				'type' => 'radio',
				'choices' => array(
					array(
						'value' => 'show',
						'label' => __('Show', 'pk_translate')
					),
					array(
						'value' => 'hide',
						'label' => __('Hide', 'pk_translate')
					)
				)
			),
			array(
				'id' => '_grid_layout',
				'label' => __('Grid layout', 'pk_translate'),
				'desc' => __('Select the layout for the grid. This option is valid only if you select a 2+ columns layout in the next option.&lt;br /&gt;&lt;br /&gt;The &quot;normal&quot; layout follows an horizontal scheme, while the &quot;special&quot; layout follows a vertical scheme with uncropped thumbnails.', 'pk_translate'),
				'std' => 'normal',
				'type' => 'radio',
				'choices' => array(
					array(
						'value' => 'normal',
						'label' => __('Normal', 'pk_translate')
					),
					array(
						'value' => 'special',
						'label' => __('Special', 'pk_translate')
					)
				)
			),
			array(
				'id' => '_columns',
				'label' => __('Layout (columns)', 'pk_translate'),
				'desc' => __('Select the number of columns.', 'pk_translate'),
				'std' => '1',
				'type' => 'radio',
				'choices' => array(
					array(
						'value' => '1',
						'label' => __('1 Column', 'pk_translate')
					),
					array(
						'value' => '2',
						'label' => __('2 Columns', 'pk_translate')
					),
					array(
						'value' => '3',
						'label' => __('3 Columns', 'pk_translate')
					),
					array(
						'value' => '4',
						'label' => __('4 Columns', 'pk_translate')
					)
				)
			),
			array(
				'id' => '_rows',
				'label' => __('Layout (rows)', 'pk_translate'),
				'desc' => __('Enter the number of rows.', 'pk_translate'),
				'std' => '10',
				'type' => 'text'
			),
			array(
				'id' => '_show_categories_filter',
				'label' => __('Show categories filter', 'pk_translate'),
				'desc' => __('Select an option.', 'pk_translate'),
				'std' => 'false',
				'type' => 'radio',
				'choices' => array(
					array(
						'value' => 'true',
						'label' => __('Yes', 'pk_translate')
					),
					array(
						'value' => 'false',
						'label' => __('No', 'pk_translate')
					)
				)
			),
			array(
				'id' => '_blog_categories',
				'label' => __('Include blog categories', 'pk_translate'),
				'desc' => __('Select one or more blog categories to include. Don&#039;t select any category if you want to include them all.', 'pk_translate'),
				'std' => '',
				'type' => 'category-checkbox'
			)
		)
	);
	
	$page_portfolio = array(
		'id' => 'page_portfolio',
		'title' => __('Options', 'pk_translate'),
		'desc' => '',
		'pages' => array('page'),
		'context' => 'normal',
		'priority' => 'high',
		'fields' => array(
			array(
				'id' => '_sidebar',
				'label' => __('Sidebar', 'pk_translate'),
				'desc' => __('Select "Hide" for a full width layout.', 'pk_translate'),
				'std' => 'show',
				'type' => 'radio',
				'choices' => array(
					array(
						'value' => 'show',
						'label' => __('Show', 'pk_translate')
					),
					array(
						'value' => 'hide',
						'label' => __('Hide', 'pk_translate')
					)
				)
			),
			array(
				'id' => '_grid_layout',
				'label' => __('Grid layout', 'pk_translate'),
				'desc' => __('Select the layout for the grid. This option is valid only if you select a 2+ columns layout in the next option.&lt;br /&gt;&lt;br /&gt;The &quot;normal&quot; layout follows an horizontal scheme, while the &quot;special&quot; layout follows a vertical scheme with uncropped thumbnails.', 'pk_translate'),
				'std' => 'normal',
				'type' => 'radio',
				'choices' => array(
					array(
						'value' => 'normal',
						'label' => __('Normal', 'pk_translate')
					),
					array(
						'value' => 'special',
						'label' => __('Special', 'pk_translate')
					)
				)
			),
			array(
				'id' => '_columns',
				'label' => __('Layout (columns)', 'pk_translate'),
				'desc' => __('Select the number of columns.', 'pk_translate'),
				'std' => '4',
				'type' => 'radio',
				'choices' => array(
					array(
						'value' => '1',
						'label' => __('1 Column', 'pk_translate')
					),
					array(
						'value' => '2',
						'label' => __('2 Columns', 'pk_translate')
					),
					array(
						'value' => '3',
						'label' => __('3 Columns', 'pk_translate')
					),
					array(
						'value' => '4',
						'label' => __('4 Columns', 'pk_translate')
					)
				)
			),
			array(
				'id' => '_rows',
				'label' => __('Layout (rows)', 'pk_translate'),
				'desc' => __('Enter the number of rows.', 'pk_translate'),
				'std' => '3',
				'type' => 'text'
			),
			array(
				'id' => '_show_categories_filter',
				'label' => __('Show categories filter', 'pk_translate'),
				'desc' => __('Select an option.', 'pk_translate'),
				'std' => 'false',
				'type' => 'radio',
				'choices' => array(
					array(
						'value' => 'true',
						'label' => __('Yes', 'pk_translate')
					),
					array(
						'value' => 'false',
						'label' => __('No', 'pk_translate')
					)
				)
			),
			array(
				'id' => '_portfolio_categories',
				'label' => __('Include portfolio categories', 'pk_translate'),
				'desc' => __('Select one or more portfolio categories to include. Don&#039;t select any category if you want to include them all.', 'pk_translate'),
				'std' => '',
				'type' => 'taxonomy-checkbox',
				'taxonomy' => 'taxonomy_portfolio'
			)
		)
	);
	
	$page_home_agency = array(
		'id' => 'page_home_agency',
		'title' => __('Options', 'pk_translate'),
		'desc' => '',
		'pages' => array('page'),
		'context' => 'normal',
		'priority' => 'high',
		'fields' => array(
			array(
				'id' => '_slider_navigation_align',
				'label' => __('Slider navigation position', 'pk_translate'),
				'desc' => __('Select an option.', 'pk_translate'),
				'std' => 'right',
				'type' => 'radio',
				'choices' => array(
					array(
						'value' => 'left',
						'label' => __('Left', 'pk_translate')
					),
					array(
						'value' => 'center',
						'label' => __('Center', 'pk_translate')
					),
					array(
						'value' => 'right',
						'label' => __('Right', 'pk_translate')
					)
				)
			),
			array(
				'id' => '_slider_transition',
				'label' => __('Slider transition', 'pk_translate'),
				'desc' => __('Select an option.', 'pk_translate'),
				'std' => 'fade',
				'type' => 'radio',
				'choices' => array(
					array(
						'value' => 'fade',
						'label' => __('Fade', 'pk_translate')
					),
					array(
						'value' => 'slide',
						'label' => __('Slide', 'pk_translate')
					)
				)
			),
			array(
					'id' => '_slider_background',
					'label' => __('Slider background image', 'pk_translate'),
					'desc' => __('Enter the URL of a background image for the slider. Do not add the background image as attachment of the page, otherwise it will be displayed within the slider too, upload it through the Media -> Library admin page. This image will be visible only if the slider images are transparent png files.', 'pk_translate'),
					'type' => 'text',
					'std' => ''
				),
			array(
				'id' => '_slider_slideshow_auto_start',
				'label' => __('Slider slideshow auto start', 'pk_translate'),
				'desc' => __('Select an option.', 'pk_translate'),
				'std' => 'true',
				'type' => 'radio',
				'choices' => array(
					array(
						'value' => 'true',
						'label' => __('Yes', 'pk_translate')
					),
					array(
						'value' => 'false',
						'label' => __('No', 'pk_translate')
					)
				)
			),
			array(
				'id' => '_slider_slideshow_interval',
				'label' => __('Slider slideshow interval', 'pk_translate'),
				'desc' => __('Enter the number of seconds.', 'pk_translate'),
				'std' => '7',
				'type' => 'text'
			),
			array(
				'id' => '_total_works',
				'label' => __('Total works', 'pk_translate'),
				'desc' => __('Enter the number of portfolio works to load.', 'pk_translate'),
				'std' => '4',
				'type' => 'text'
			),
			array(
				'id' => '_portfolio_categories',
				'label' => __('Include portfolio categories', 'pk_translate'),
				'desc' => __('Select one or more portfolio categories to include. Don&#039;t select any category if you want to include them all.', 'pk_translate'),
				'std' => '',
				'type' => 'taxonomy-checkbox',
				'taxonomy' => 'taxonomy_portfolio'
			),
			array(
				'id' => '_view_all_button_color',
				'label' => __('"View all" button color', 'pk_translate'),
				'desc' => __('Select a color for the "View all" button.', 'pk_translate'),
				'std' => 'white',
				'type' => 'select',
				'choices' => array(
					array(
						'value' => 'white',
						'label' => __('White', 'pk_translate')
					),
					array(
						'value' => 'grey',
						'label' => __('Grey', 'pk_translate')
					),
					array(
						'value' => 'black',
						'label' => __('Black', 'pk_translate')
					),
					array(
						'value' => 'lime',
						'label' => __('Lime', 'pk_translate')
					),
					array(
						'value' => 'orange',
						'label' => __('Orange', 'pk_translate')
					),
					array(
						'value' => 'yellow',
						'label' => __('Yellow', 'pk_translate')
					),
					array(
						'value' => 'green',
						'label' => __('Green', 'pk_translate')
					),
					array(
						'value' => 'blue',
						'label' => __('Blue', 'pk_translate')
					),
					array(
						'value' => 'purple',
						'label' => __('Purple', 'pk_translate')
					),
					array(
						'value' => 'red',
						'label' => __('Red', 'pk_translate')
					)
				)
			)
		)
	);
	
	$page_home_business = array(
		'id' => 'page_home_business',
		'title' => __('Options', 'pk_translate'),
		'desc' => '',
		'pages' => array('page'),
		'context' => 'normal',
		'priority' => 'high',
		'fields' => array(
			array(
				'id' => '_slider_navigation_align',
				'label' => __('Slider navigation position', 'pk_translate'),
				'desc' => __('Select an option.', 'pk_translate'),
				'std' => 'center',
				'type' => 'radio',
				'choices' => array(
					array(
						'value' => 'left',
						'label' => __('Left', 'pk_translate')
					),
					array(
						'value' => 'center',
						'label' => __('Center', 'pk_translate')
					),
					array(
						'value' => 'right',
						'label' => __('Right', 'pk_translate')
					)
				)
			),
			array(
				'id' => '_slider_transition',
				'label' => __('Slider transition', 'pk_translate'),
				'desc' => __('Select an option.', 'pk_translate'),
				'std' => 'fade',
				'type' => 'radio',
				'choices' => array(
					array(
						'value' => 'fade',
						'label' => __('Fade', 'pk_translate')
					),
					array(
						'value' => 'slide',
						'label' => __('Slide', 'pk_translate')
					)
				)
			),
			array(
					'id' => '_slider_background',
					'label' => __('Slider background image', 'pk_translate'),
					'desc' => __('Enter the URL of a background image for the slider. Do not add the background image as attachment of the page, otherwise it will be displayed within the slider too, upload it through the Media -> Library admin page. This image will be visible only if the slider images are transparent png files.', 'pk_translate'),
					'type' => 'text',
					'std' => ''
				),
			array(
				'id' => '_slider_slideshow_auto_start',
				'label' => __('Slider slideshow auto start', 'pk_translate'),
				'desc' => __('Select an option.', 'pk_translate'),
				'std' => 'true',
				'type' => 'radio',
				'choices' => array(
					array(
						'value' => 'true',
						'label' => __('Yes', 'pk_translate')
					),
					array(
						'value' => 'false',
						'label' => __('No', 'pk_translate')
					)
				)
			),
			array(
				'id' => '_slider_slideshow_interval',
				'label' => __('Slider slideshow interval', 'pk_translate'),
				'desc' => __('Enter the number of seconds.', 'pk_translate'),
				'std' => '7',
				'type' => 'text'
			),
			array(
				'id' => '_feed_url',
				'label' => __('Feed URL', 'pk_translate'),
				'desc' => __('Enter the url of any RSS feed that you want to load in the news rotator below the slider.', 'pk_translate'),
				'std' => 'http://rss.cnn.com/rss/edition_business.rss',
				'type' => 'text'
			),
			array(
				'id' => '_feed_total_items',
				'label' => __('Feed total items', 'pk_translate'),
				'desc' => __('Enter the number of feed items to load.', 'pk_translate'),
				'std' => '5',
				'type' => 'text'
			),
			array(
				'id' => '_tabs_button_color',
				'label' => __('Tabs button color', 'pk_translate'),
				'desc' => __('Select a color.', 'pk_translate'),
				'std' => 'white',
				'type' => 'select',
				'choices' => array(
					array(
						'value' => 'white',
						'label' => __('White', 'pk_translate')
					),
					array(
						'value' => 'grey',
						'label' => __('Grey', 'pk_translate')
					),
					array(
						'value' => 'black',
						'label' => __('Black', 'pk_translate')
					),
					array(
						'value' => 'lime',
						'label' => __('Lime', 'pk_translate')
					),
					array(
						'value' => 'orange',
						'label' => __('Orange', 'pk_translate')
					),
					array(
						'value' => 'yellow',
						'label' => __('Yellow', 'pk_translate')
					),
					array(
						'value' => 'green',
						'label' => __('Green', 'pk_translate')
					),
					array(
						'value' => 'blue',
						'label' => __('Blue', 'pk_translate')
					),
					array(
						'value' => 'purple',
						'label' => __('Purple', 'pk_translate')
					),
					array(
						'value' => 'red',
						'label' => __('Red', 'pk_translate')
					)
				)
			),
			array(
				'id' => '_tabs_button_link',
				'label' => __('Tabs button link', 'pk_translate'),
				'desc' => __('Enter an URL for the main tabs button of the page.', 'pk_translate'),
				'std' => '#',
				'type' => 'text'
			),
			array(
				'id' => '_tabs_button_label',
				'label' => __('Tabs button label', 'pk_translate'),
				'desc' => __('Enter a label for the main tabs button of the page.', 'pk_translate'),
				'std' => 'Button Label',
				'type' => 'text'
			)
		)
	);
	
	$page_home_freelance = array(
		'id' => 'page_home_freelance',
		'title' => __('Options', 'pk_translate'),
		'desc' => '',
		'pages' => array('page'),
		'context' => 'normal',
		'priority' => 'high',
		'fields' => array(
			array(
				'id' => '_slider_navigation_align',
				'label' => __('Slider navigation position', 'pk_translate'),
				'desc' => __('Select an option.', 'pk_translate'),
				'std' => 'center',
				'type' => 'radio',
				'choices' => array(
					array(
						'value' => 'left',
						'label' => __('Left', 'pk_translate')
					),
					array(
						'value' => 'center',
						'label' => __('Center', 'pk_translate')
					),
					array(
						'value' => 'right',
						'label' => __('Right', 'pk_translate')
					)
				)
			),
			array(
				'id' => '_slider_transition',
				'label' => __('Slider transition', 'pk_translate'),
				'desc' => __('Select an option.', 'pk_translate'),
				'std' => 'fade',
				'type' => 'radio',
				'choices' => array(
					array(
						'value' => 'fade',
						'label' => __('Fade', 'pk_translate')
					),
					array(
						'value' => 'slide',
						'label' => __('Slide', 'pk_translate')
					)
				)
			),
			array(
					'id' => '_slider_background',
					'label' => __('Slider background image', 'pk_translate'),
					'desc' => __('Enter the URL of a background image for the slider. Do not add the background image as attachment of the page, otherwise it will be displayed within the slider too, upload it through the Media -> Library admin page. This image will be visible only if the slider images are transparent png files.', 'pk_translate'),
					'type' => 'text',
					'std' => ''
				),
			array(
				'id' => '_slider_slideshow_auto_start',
				'label' => __('Slider slideshow auto start', 'pk_translate'),
				'desc' => __('Select an option.', 'pk_translate'),
				'std' => 'true',
				'type' => 'radio',
				'choices' => array(
					array(
						'value' => 'true',
						'label' => __('Yes', 'pk_translate')
					),
					array(
						'value' => 'false',
						'label' => __('No', 'pk_translate')
					)
				)
			),
			array(
				'id' => '_slider_slideshow_interval',
				'label' => __('Slider slideshow interval', 'pk_translate'),
				'desc' => __('Enter the number of seconds.', 'pk_translate'),
				'std' => '7',
				'type' => 'text'
			),
			array(
				'id' => '_call_to_action_text',
				'label' => __('Call to action text', 'pk_translate'),
				'desc' => __('Enter a text to display below the slider.', 'pk_translate'),
				'std' => 'Call to action text!',
				'type' => 'text'
			),
			array(
				'id' => '_total_works',
				'label' => __('Total works', 'pk_translate'),
				'desc' => __('Enter the number of portfolio works to load.', 'pk_translate'),
				'std' => '4',
				'type' => 'text'
			),
			array(
				'id' => '_portfolio_categories',
				'label' => __('Include portfolio categories', 'pk_translate'),
				'desc' => __('Select one or more portfolio categories to include. Don&#039;t select any category if you want to include them all.', 'pk_translate'),
				'std' => '',
				'type' => 'taxonomy-checkbox',
				'taxonomy' => 'taxonomy_portfolio'
			),
			array(
				'id' => '_view_all_button_color',
				'label' => __('"View all" button color', 'pk_translate'),
				'desc' => __('Select a color for the "View all" button.', 'pk_translate'),
				'std' => 'white',
				'type' => 'select',
				'choices' => array(
					array(
						'value' => 'white',
						'label' => __('White', 'pk_translate')
					),
					array(
						'value' => 'grey',
						'label' => __('Grey', 'pk_translate')
					),
					array(
						'value' => 'black',
						'label' => __('Black', 'pk_translate')
					),
					array(
						'value' => 'lime',
						'label' => __('Lime', 'pk_translate')
					),
					array(
						'value' => 'orange',
						'label' => __('Orange', 'pk_translate')
					),
					array(
						'value' => 'yellow',
						'label' => __('Yellow', 'pk_translate')
					),
					array(
						'value' => 'green',
						'label' => __('Green', 'pk_translate')
					),
					array(
						'value' => 'blue',
						'label' => __('Blue', 'pk_translate')
					),
					array(
						'value' => 'purple',
						'label' => __('Purple', 'pk_translate')
					),
					array(
						'value' => 'red',
						'label' => __('Red', 'pk_translate')
					)
				)
			)
		)
	);
	
	$page_home_standard = array(
		'id' => 'page_home_standard',
		'title' => __('Options', 'pk_translate'),
		'desc' => '',
		'pages' => array('page'),
		'context' => 'normal',
		'priority' => 'high',
		'fields' => array(
			array(
				'id' => '_slider_navigation_align',
				'label' => __('Slider navigation position', 'pk_translate'),
				'desc' => __('Select an option.', 'pk_translate'),
				'std' => 'left',
				'type' => 'radio',
				'choices' => array(
					array(
						'value' => 'left',
						'label' => __('Left', 'pk_translate')
					),
					array(
						'value' => 'center',
						'label' => __('Center', 'pk_translate')
					),
					array(
						'value' => 'right',
						'label' => __('Right', 'pk_translate')
					)
				)
			),
			array(
				'id' => '_slider_transition',
				'label' => __('Slider transition', 'pk_translate'),
				'desc' => __('Select an option.', 'pk_translate'),
				'std' => 'fade',
				'type' => 'radio',
				'choices' => array(
					array(
						'value' => 'fade',
						'label' => __('Fade', 'pk_translate')
					),
					array(
						'value' => 'slide',
						'label' => __('Slide', 'pk_translate')
					)
				)
			),
			array(
					'id' => '_slider_background',
					'label' => __('Slider background image', 'pk_translate'),
					'desc' => __('Enter the URL of a background image for the slider. Do not add the background image as attachment of the page, otherwise it will be displayed within the slider too, upload it through the Media -> Library admin page. This image will be visible only if the slider images are transparent png files.', 'pk_translate'),
					'type' => 'text',
					'std' => ''
				),
			array(
				'id' => '_slider_slideshow_auto_start',
				'label' => __('Slider slideshow auto start', 'pk_translate'),
				'desc' => __('Select an option.', 'pk_translate'),
				'std' => 'true',
				'type' => 'radio',
				'choices' => array(
					array(
						'value' => 'true',
						'label' => __('Yes', 'pk_translate')
					),
					array(
						'value' => 'false',
						'label' => __('No', 'pk_translate')
					)
				)
			),
			array(
				'id' => '_slider_slideshow_interval',
				'label' => __('Slider slideshow interval', 'pk_translate'),
				'desc' => __('Enter the number of seconds.', 'pk_translate'),
				'std' => '7',
				'type' => 'text'
			),
			array(
				'id' => '_call_to_action_text',
				'label' => __('Call to action text', 'pk_translate'),
				'desc' => __('Enter a text to display below the slider.', 'pk_translate'),
				'std' => 'Call to action text!',
				'type' => 'text'
			),
			array(
				'id' => '_call_to_action_button_color',
				'label' => __('Call to action button color', 'pk_translate'),
				'desc' => __('Select a color.', 'pk_translate'),
				'std' => 'white',
				'type' => 'select',
				'choices' => array(
					array(
						'value' => 'white',
						'label' => __('White', 'pk_translate')
					),
					array(
						'value' => 'grey',
						'label' => __('Grey', 'pk_translate')
					),
					array(
						'value' => 'black',
						'label' => __('Black', 'pk_translate')
					),
					array(
						'value' => 'lime',
						'label' => __('Lime', 'pk_translate')
					),
					array(
						'value' => 'orange',
						'label' => __('Orange', 'pk_translate')
					),
					array(
						'value' => 'yellow',
						'label' => __('Yellow', 'pk_translate')
					),
					array(
						'value' => 'green',
						'label' => __('Green', 'pk_translate')
					),
					array(
						'value' => 'blue',
						'label' => __('Blue', 'pk_translate')
					),
					array(
						'value' => 'purple',
						'label' => __('Purple', 'pk_translate')
					),
					array(
						'value' => 'red',
						'label' => __('Red', 'pk_translate')
					)
				)
			),
			array(
				'id' => '_call_to_action_button_link',
				'label' => __('Call to action button link', 'pk_translate'),
				'desc' => __('Enter an URL for the call to action button.', 'pk_translate'),
				'std' => '#',
				'type' => 'text'
			),
			array(
				'id' => '_call_to_action_button_label',
				'label' => __('Call to action button label', 'pk_translate'),
				'desc' => __('Enter a label for the call to action button.', 'pk_translate'),
				'std' => 'Button Label',
				'type' => 'text'
			),
			array(
				'id' => '_total_works',
				'label' => __('Total works', 'pk_translate'),
				'desc' => __('Enter the number of portfolio works to load.', 'pk_translate'),
				'std' => '3',
				'type' => 'text'
			),
			array(
				'id' => '_portfolio_categories',
				'label' => __('Include portfolio categories', 'pk_translate'),
				'desc' => __('Select one or more portfolio categories to include. Don&#039;t select any category if you want to include them all.', 'pk_translate'),
				'std' => '',
				'type' => 'taxonomy-checkbox',
				'taxonomy' => 'taxonomy_portfolio'
			),
			array(
				'id' => '_total_posts',
				'label' => __('Total posts', 'pk_translate'),
				'desc' => __('Enter the number of blog posts to load.', 'pk_translate'),
				'std' => '3',
				'type' => 'text'
			),
			array(
				'id' => '_blog_categories',
				'label' => __('Include blog categories', 'pk_translate'),
				'desc' => __('Select one or more blog categories to include. Don&#039;t select any category if you want to include them all.', 'pk_translate'),
				'std' => '',
				'type' => 'category-checkbox'
			),
			array(
				'id' => '_view_all_button_color',
				'label' => __('"View all" button color', 'pk_translate'),
				'desc' => __('Select a color for the "View all" button.', 'pk_translate'),
				'std' => 'white',
				'type' => 'select',
				'choices' => array(
					array(
						'value' => 'white',
						'label' => __('White', 'pk_translate')
					),
					array(
						'value' => 'grey',
						'label' => __('Grey', 'pk_translate')
					),
					array(
						'value' => 'black',
						'label' => __('Black', 'pk_translate')
					),
					array(
						'value' => 'lime',
						'label' => __('Lime', 'pk_translate')
					),
					array(
						'value' => 'orange',
						'label' => __('Orange', 'pk_translate')
					),
					array(
						'value' => 'yellow',
						'label' => __('Yellow', 'pk_translate')
					),
					array(
						'value' => 'green',
						'label' => __('Green', 'pk_translate')
					),
					array(
						'value' => 'blue',
						'label' => __('Blue', 'pk_translate')
					),
					array(
						'value' => 'purple',
						'label' => __('Purple', 'pk_translate')
					),
					array(
						'value' => 'red',
						'label' => __('Red', 'pk_translate')
					)
				)
			)
		)
	);
	
	$page_home_jigoshop = array(
		'id' => 'page_home_jigoshop',
		'title' => __('Options', 'pk_translate'),
		'desc' => '',
		'pages' => array('page'),
		'context' => 'normal',
		'priority' => 'high',
		'fields' => array(
			array(
				'id' => '_slider_navigation_align',
				'label' => __('Slider navigation position', 'pk_translate'),
				'desc' => __('Select an option.', 'pk_translate'),
				'std' => 'center',
				'type' => 'radio',
				'choices' => array(
					array(
						'value' => 'left',
						'label' => __('Left', 'pk_translate')
					),
					array(
						'value' => 'center',
						'label' => __('Center', 'pk_translate')
					),
					array(
						'value' => 'right',
						'label' => __('Right', 'pk_translate')
					)
				)
			),
			array(
				'id' => '_slider_transition',
				'label' => __('Slider transition', 'pk_translate'),
				'desc' => __('Select an option.', 'pk_translate'),
				'std' => 'slide',
				'type' => 'radio',
				'choices' => array(
					array(
						'value' => 'fade',
						'label' => __('Fade', 'pk_translate')
					),
					array(
						'value' => 'slide',
						'label' => __('Slide', 'pk_translate')
					)
				)
			),
			array(
					'id' => '_slider_background',
					'label' => __('Slider background image', 'pk_translate'),
					'desc' => __('Enter the URL of a background image for the slider. Do not add the background image as attachment of the page, otherwise it will be displayed within the slider too, upload it through the Media -> Library admin page. This image will be visible only if the slider images are transparent png files.', 'pk_translate'),
					'type' => 'text',
					'std' => ''
				),
			array(
				'id' => '_slider_slideshow_auto_start',
				'label' => __('Slider slideshow auto start', 'pk_translate'),
				'desc' => __('Select an option.', 'pk_translate'),
				'std' => 'true',
				'type' => 'radio',
				'choices' => array(
					array(
						'value' => 'true',
						'label' => __('Yes', 'pk_translate')
					),
					array(
						'value' => 'false',
						'label' => __('No', 'pk_translate')
					)
				)
			),
			array(
				'id' => '_slider_slideshow_interval',
				'label' => __('Slider slideshow interval', 'pk_translate'),
				'desc' => __('Enter the number of seconds.', 'pk_translate'),
				'std' => '7',
				'type' => 'text'
			),
			array(
				'id' => '_call_to_action_text',
				'label' => __('Call to action text', 'pk_translate'),
				'desc' => __('Enter a text to display below the slider.', 'pk_translate'),
				'std' => 'Call to action text!',
				'type' => 'text'
			),
			array(
				'id' => '_call_to_action_button_color',
				'label' => __('Call to action button color', 'pk_translate'),
				'desc' => __('Select a color.', 'pk_translate'),
				'std' => 'white',
				'type' => 'select',
				'choices' => array(
					array(
						'value' => 'white',
						'label' => __('White', 'pk_translate')
					),
					array(
						'value' => 'grey',
						'label' => __('Grey', 'pk_translate')
					),
					array(
						'value' => 'black',
						'label' => __('Black', 'pk_translate')
					),
					array(
						'value' => 'lime',
						'label' => __('Lime', 'pk_translate')
					),
					array(
						'value' => 'orange',
						'label' => __('Orange', 'pk_translate')
					),
					array(
						'value' => 'yellow',
						'label' => __('Yellow', 'pk_translate')
					),
					array(
						'value' => 'green',
						'label' => __('Green', 'pk_translate')
					),
					array(
						'value' => 'blue',
						'label' => __('Blue', 'pk_translate')
					),
					array(
						'value' => 'purple',
						'label' => __('Purple', 'pk_translate')
					),
					array(
						'value' => 'red',
						'label' => __('Red', 'pk_translate')
					)
				)
			),
			array(
				'id' => '_call_to_action_button_link',
				'label' => __('Call to action button link', 'pk_translate'),
				'desc' => __('Enter an URL for the call to action button.', 'pk_translate'),
				'std' => ((function_exists('jigoshop_get_page_id')) ? get_permalink((function_exists('icl_object_id')) ? icl_object_id(jigoshop_get_page_id('shop'), 'page', true) : jigoshop_get_page_id('shop')) : '#'),
				'type' => 'text'
			),
			array(
				'id' => '_call_to_action_button_label',
				'label' => __('Call to action button label', 'pk_translate'),
				'desc' => __('Enter a label for the call to action button.', 'pk_translate'),
				'std' => 'Button Label',
				'type' => 'text'
			),
			array(
				'id' => '_total_recent_products',
				'label' => __('Total recent products', 'pk_translate'),
				'desc' => __('Enter the number of recent products to load.', 'pk_translate'),
				'std' => '2',
				'type' => 'text'
			),
			array(
				'id' => '_total_featured_products',
				'label' => __('Total featured products', 'pk_translate'),
				'desc' => __('Enter the number of featured products to load.', 'pk_translate'),
				'std' => '3',
				'type' => 'text'
			),
			array(
				'id' => '_view_all_button_color',
				'label' => __('"View all" button color', 'pk_translate'),
				'desc' => __('Select a color for the "View all" button.', 'pk_translate'),
				'std' => 'white',
				'type' => 'select',
				'choices' => array(
					array(
						'value' => 'white',
						'label' => __('White', 'pk_translate')
					),
					array(
						'value' => 'grey',
						'label' => __('Grey', 'pk_translate')
					),
					array(
						'value' => 'black',
						'label' => __('Black', 'pk_translate')
					),
					array(
						'value' => 'lime',
						'label' => __('Lime', 'pk_translate')
					),
					array(
						'value' => 'orange',
						'label' => __('Orange', 'pk_translate')
					),
					array(
						'value' => 'yellow',
						'label' => __('Yellow', 'pk_translate')
					),
					array(
						'value' => 'green',
						'label' => __('Green', 'pk_translate')
					),
					array(
						'value' => 'blue',
						'label' => __('Blue', 'pk_translate')
					),
					array(
						'value' => 'purple',
						'label' => __('Purple', 'pk_translate')
					),
					array(
						'value' => 'red',
						'label' => __('Red', 'pk_translate')
					)
				)
			)
		)
	);
	
	$page_home_woocommerce = array(
		'id' => 'page_home_woocommerce',
		'title' => __('Options', 'pk_translate'),
		'desc' => '',
		'pages' => array('page'),
		'context' => 'normal',
		'priority' => 'high',
		'fields' => array(
			array(
				'id' => '_slider_navigation_align',
				'label' => __('Slider navigation position', 'pk_translate'),
				'desc' => __('Select an option.', 'pk_translate'),
				'std' => 'center',
				'type' => 'radio',
				'choices' => array(
					array(
						'value' => 'left',
						'label' => __('Left', 'pk_translate')
					),
					array(
						'value' => 'center',
						'label' => __('Center', 'pk_translate')
					),
					array(
						'value' => 'right',
						'label' => __('Right', 'pk_translate')
					)
				)
			),
			array(
				'id' => '_slider_transition',
				'label' => __('Slider transition', 'pk_translate'),
				'desc' => __('Select an option.', 'pk_translate'),
				'std' => 'slide',
				'type' => 'radio',
				'choices' => array(
					array(
						'value' => 'fade',
						'label' => __('Fade', 'pk_translate')
					),
					array(
						'value' => 'slide',
						'label' => __('Slide', 'pk_translate')
					)
				)
			),
			array(
					'id' => '_slider_background',
					'label' => __('Slider background image', 'pk_translate'),
					'desc' => __('Enter the URL of a background image for the slider. Do not add the background image as attachment of the page, otherwise it will be displayed within the slider too, upload it through the Media -> Library admin page. This image will be visible only if the slider images are transparent png files.', 'pk_translate'),
					'type' => 'text',
					'std' => ''
				),
			array(
				'id' => '_slider_slideshow_auto_start',
				'label' => __('Slider slideshow auto start', 'pk_translate'),
				'desc' => __('Select an option.', 'pk_translate'),
				'std' => 'true',
				'type' => 'radio',
				'choices' => array(
					array(
						'value' => 'true',
						'label' => __('Yes', 'pk_translate')
					),
					array(
						'value' => 'false',
						'label' => __('No', 'pk_translate')
					)
				)
			),
			array(
				'id' => '_slider_slideshow_interval',
				'label' => __('Slider slideshow interval', 'pk_translate'),
				'desc' => __('Enter the number of seconds.', 'pk_translate'),
				'std' => '7',
				'type' => 'text'
			),
			array(
				'id' => '_call_to_action_text',
				'label' => __('Call to action text', 'pk_translate'),
				'desc' => __('Enter a text to display below the slider.', 'pk_translate'),
				'std' => 'Call to action text!',
				'type' => 'text'
			),
			array(
				'id' => '_call_to_action_button_color',
				'label' => __('Call to action button color', 'pk_translate'),
				'desc' => __('Select a color.', 'pk_translate'),
				'std' => 'white',
				'type' => 'select',
				'choices' => array(
					array(
						'value' => 'white',
						'label' => __('White', 'pk_translate')
					),
					array(
						'value' => 'grey',
						'label' => __('Grey', 'pk_translate')
					),
					array(
						'value' => 'black',
						'label' => __('Black', 'pk_translate')
					),
					array(
						'value' => 'lime',
						'label' => __('Lime', 'pk_translate')
					),
					array(
						'value' => 'orange',
						'label' => __('Orange', 'pk_translate')
					),
					array(
						'value' => 'yellow',
						'label' => __('Yellow', 'pk_translate')
					),
					array(
						'value' => 'green',
						'label' => __('Green', 'pk_translate')
					),
					array(
						'value' => 'blue',
						'label' => __('Blue', 'pk_translate')
					),
					array(
						'value' => 'purple',
						'label' => __('Purple', 'pk_translate')
					),
					array(
						'value' => 'red',
						'label' => __('Red', 'pk_translate')
					)
				)
			),
			array(
				'id' => '_call_to_action_button_link',
				'label' => __('Call to action button link', 'pk_translate'),
				'desc' => __('Enter an URL for the call to action button.', 'pk_translate'),
				'std' => ((function_exists('woocommerce_get_page_id')) ? get_permalink((function_exists('icl_object_id')) ? icl_object_id(woocommerce_get_page_id('shop'), 'page', true) : woocommerce_get_page_id('shop')) : '#'),
				'type' => 'text'
			),
			array(
				'id' => '_call_to_action_button_label',
				'label' => __('Call to action button label', 'pk_translate'),
				'desc' => __('Enter a label for the call to action button.', 'pk_translate'),
				'std' => 'Button Label',
				'type' => 'text'
			),
			array(
				'id' => '_total_recent_products',
				'label' => __('Total recent products', 'pk_translate'),
				'desc' => __('Enter the number of recent products to load.', 'pk_translate'),
				'std' => '2',
				'type' => 'text'
			),
			array(
				'id' => '_total_featured_products',
				'label' => __('Total featured products', 'pk_translate'),
				'desc' => __('Enter the number of featured products to load.', 'pk_translate'),
				'std' => '3',
				'type' => 'text'
			),
			array(
				'id' => '_view_all_button_color',
				'label' => __('"View all" button color', 'pk_translate'),
				'desc' => __('Select a color for the "View all" button.', 'pk_translate'),
				'std' => 'white',
				'type' => 'select',
				'choices' => array(
					array(
						'value' => 'white',
						'label' => __('White', 'pk_translate')
					),
					array(
						'value' => 'grey',
						'label' => __('Grey', 'pk_translate')
					),
					array(
						'value' => 'black',
						'label' => __('Black', 'pk_translate')
					),
					array(
						'value' => 'lime',
						'label' => __('Lime', 'pk_translate')
					),
					array(
						'value' => 'orange',
						'label' => __('Orange', 'pk_translate')
					),
					array(
						'value' => 'yellow',
						'label' => __('Yellow', 'pk_translate')
					),
					array(
						'value' => 'green',
						'label' => __('Green', 'pk_translate')
					),
					array(
						'value' => 'blue',
						'label' => __('Blue', 'pk_translate')
					),
					array(
						'value' => 'purple',
						'label' => __('Purple', 'pk_translate')
					),
					array(
						'value' => 'red',
						'label' => __('Red', 'pk_translate')
					)
				)
			)
		)
	);
	
	$blog_posts_link = array(
		'id' => 'blog_posts_link',
		'title' => __('Options', 'pk_translate'),
		'desc' => '',
		'pages' => array('post'),
		'context' => 'normal',
		'priority' => 'high',
		'fields' => array(
			array(
				'id' => '_sidebar',
				'label' => __('Sidebar', 'pk_translate'),
				'desc' => __('Select "Hide" for a full width layout.', 'pk_translate'),
				'std' => 'show',
				'type' => 'radio',
				'choices' => array(
					array(
						'value' => 'show',
						'label' => __('Show', 'pk_translate')
					),
					array(
						'value' => 'hide',
						'label' => __('Hide', 'pk_translate')
					)
				)
				
			),
			array(
				'id' => '_link',
				'label' => __('Post custom link', 'pk_translate'),
				'desc' => __('Enter an URL.', 'pk_translate'),
				'std' => '#',
				'type' => 'text'
			)
		)
	);
	
	$blog_posts_quote = array(
		'id' => 'blog_posts_quote',
		'title' => __('Options', 'pk_translate'),
		'desc' => '',
		'pages' => array('post'),
		'context' => 'normal',
		'priority' => 'high',
		'fields' => array(
			array(
				'id' => '_sidebar',
				'label' => __('Sidebar', 'pk_translate'),
				'desc' => __('Select "Hide" for a full width layout.', 'pk_translate'),
				'std' => 'show',
				'type' => 'radio',
				'choices' => array(
					array(
						'value' => 'show',
						'label' => __('Show', 'pk_translate')
					),
					array(
						'value' => 'hide',
						'label' => __('Hide', 'pk_translate')
					)
				)
				
			),
			array(
				'id' => '_quote',
				'label' => __('Quote', 'pk_translate'),
				'desc' => __('Enter a quote.', 'pk_translate'),
				'std' => 'Enter the quote text in the dedicated custom field. Edit this post to change this value!',
				'type' => 'textarea',
				'rows' => '3'
			),
			array(
				'id' => '_cite',
				'label' => __('Cite', 'pk_translate'),
				'desc' => __('Cite someone.', 'pk_translate'),
				'std' => 'parker&amp;kent team',
				'type' => 'text'
			)
		)
	);
	
	$blog_posts_gallery = array(
		'id' => 'blog_posts_gallery',
		'title' => __('Options', 'pk_translate'),
		'desc' => '',
		'pages' => array('post'),
		'context' => 'normal',
		'priority' => 'high',
		'fields' => array(
			array(
				'id' => '_sidebar',
				'label' => __('Sidebar', 'pk_translate'),
				'desc' => __('Select "Hide" for a full width layout.', 'pk_translate'),
				'std' => 'show',
				'type' => 'radio',
				'choices' => array(
					array(
						'value' => 'show',
						'label' => __('Show', 'pk_translate')
					),
					array(
						'value' => 'hide',
						'label' => __('Hide', 'pk_translate')
					)
				)
				
			),
			array(
				'id' => '_slider_info_button_open_label',
				'label' => __('Slider info button (open label)', 'pk_translate'),
				'desc' => __('Enter a label.', 'pk_translate'),
				'std' => 'info',
				'type' => 'text'
			),
			array(
				'id' => '_slider_info_button_close_label',
				'label' => __('Slider info button (close label)', 'pk_translate'),
				'desc' => __('Enter a label.', 'pk_translate'),
				'std' => 'close info',
				'type' => 'text'
			),
			array(
				'id' => '_slider_slideshow',
				'label' => __('Slider slideshow', 'pk_translate'),
				'desc' => __('Select an option to enable or not the slider slideshow.', 'pk_translate'),
				'std' => 'true',
				'type' => 'radio',
				'choices' => array(
					array(
						'value' => 'true',
						'label' => __('Yes', 'pk_translate')
					),
					array(
						'value' => 'false',
						'label' => __('No', 'pk_translate')
					)
				)
			),
			array(
				'id' => '_slider_slideshow_auto_start',
				'label' => __('Slider slideshow auto start', 'pk_translate'),
				'desc' => __('Select an option.', 'pk_translate'),
				'std' => 'true',
				'type' => 'radio',
				'choices' => array(
					array(
						'value' => 'true',
						'label' => __('Yes', 'pk_translate')
					),
					array(
						'value' => 'false',
						'label' => __('No', 'pk_translate')
					)
				)
			),
			array(
				'id' => '_slider_slideshow_interval',
				'label' => __('Slider slideshow interval', 'pk_translate'),
				'desc' => __('Enter the number of seconds.', 'pk_translate'),
				'std' => '5',
				'type' => 'text'
			)
		)
	);
	
	$blog_posts_sidebar = array(
		'id' => 'blog_posts_sidebar',
		'title' => __('Options', 'pk_translate'),
		'desc' => '',
		'pages' => array('post'),
		'context' => 'normal',
		'priority' => 'high',
		'fields' => array(
			array(
				'id' => '_sidebar',
				'label' => __('Sidebar', 'pk_translate'),
				'desc' => __('Select "Hide" for a full width layout.', 'pk_translate'),
				'std' => 'show',
				'type' => 'radio',
				'choices' => array(
					array(
						'value' => 'show',
						'label' => __('Show', 'pk_translate')
					),
					array(
						'value' => 'hide',
						'label' => __('Hide', 'pk_translate')
					)
				)
			)
		)
	);
	
	$portfolio_posts_gallery = array(
		'id' => 'portfolio_posts_gallery',
		'title' => __('Options', 'pk_translate'),
		'desc' => '',
		'pages' => array('portfolio'),
		'context' => 'normal',
		'priority' => 'high',
		'fields' => array(
			array(
				'id' => '_sidebar',
				'label' => __('Sidebar', 'pk_translate'),
				'desc' => __('Select "Hide" for a full width layout.', 'pk_translate'),
				'std' => 'show',
				'type' => 'radio',
				'choices' => array(
					array(
						'value' => 'show',
						'label' => __('Show', 'pk_translate')
					),
					array(
						'value' => 'hide',
						'label' => __('Hide', 'pk_translate')
					)
				)
				
			),
			array(
				'id' => '_slider_info_button_open_label',
				'label' => __('Slider info button (open label)', 'pk_translate'),
				'desc' => __('Enter a label.', 'pk_translate'),
				'std' => 'info',
				'type' => 'text'
			),
			array(
				'id' => '_slider_info_button_close_label',
				'label' => __('Slider info button (close label)', 'pk_translate'),
				'desc' => __('Enter a label.', 'pk_translate'),
				'std' => 'close info',
				'type' => 'text'
			),
			array(
				'id' => '_slider_slideshow',
				'label' => __('Slider slideshow', 'pk_translate'),
				'desc' => __('Select an option to enable or not the slider slideshow.', 'pk_translate'),
				'std' => 'true',
				'type' => 'radio',
				'choices' => array(
					array(
						'value' => 'true',
						'label' => __('Yes', 'pk_translate')
					),
					array(
						'value' => 'false',
						'label' => __('No', 'pk_translate')
					)
				)
			),
			array(
				'id' => '_slider_slideshow_auto_start',
				'label' => __('Slider slideshow auto start', 'pk_translate'),
				'desc' => __('Select an option.', 'pk_translate'),
				'std' => 'true',
				'type' => 'radio',
				'choices' => array(
					array(
						'value' => 'true',
						'label' => __('Yes', 'pk_translate')
					),
					array(
						'value' => 'false',
						'label' => __('No', 'pk_translate')
					)
				)
			),
			array(
				'id' => '_slider_slideshow_interval',
				'label' => __('Slider slideshow interval', 'pk_translate'),
				'desc' => __('Enter the number of seconds.', 'pk_translate'),
				'std' => '5',
				'type' => 'text'
			)
		)
	);
	
	$portfolio_posts_sidebar = array(
		'id' => 'portfolio_posts_sidebar',
		'title' => __('Options', 'pk_translate'),
		'desc' => '',
		'pages' => array('portfolio'),
		'context' => 'normal',
		'priority' => 'high',
		'fields' => array(
			array(
				'id' => '_sidebar',
				'label' => __('Sidebar', 'pk_translate'),
				'desc' => __('Select "Hide" for a full width layout.', 'pk_translate'),
				'std' => 'show',
				'type' => 'radio',
				'choices' => array(
					array(
						'value' => 'show',
						'label' => __('Show', 'pk_translate')
					),
					array(
						'value' => 'hide',
						'label' => __('Hide', 'pk_translate')
					)
				)
			)
		)
	);
	
	$product_posts_sidebar = array(
		'id' => 'product_posts_sidebar',
		'title' => __('Options', 'pk_translate'),
		'desc' => '',
		'pages' => array('product'),
		'context' => 'normal',
		'priority' => 'high',
		'fields' => array(
			array(
				'id' => '_sidebar',
				'label' => __('Sidebar', 'pk_translate'),
				'desc' => __('Select "Hide" for a full width layout.', 'pk_translate'),
				'std' => 'show',
				'type' => 'radio',
				'choices' => array(
					array(
						'value' => 'show',
						'label' => __('Show', 'pk_translate')
					),
					array(
						'value' => 'hide',
						'label' => __('Hide', 'pk_translate')
					)
				)
			)
		)
	);
	
	$pages_gallery = array(
		'id' => 'pages_gallery',
		'title' => __('Options', 'pk_translate'),
		'desc' => '',
		'pages' => array('page'),
		'context' => 'normal',
		'priority' => 'high',
		'fields' => array(
			array(
				'id' => '_slider_info_button_open_label',
				'label' => __('Slider info button (open label)', 'pk_translate'),
				'desc' => __('Enter a label.', 'pk_translate'),
				'std' => 'info',
				'type' => 'text'
			),
			array(
				'id' => '_slider_info_button_close_label',
				'label' => __('Slider info button (close label)', 'pk_translate'),
				'desc' => __('Enter a label.', 'pk_translate'),
				'std' => 'close info',
				'type' => 'text'
			),
			array(
				'id' => '_slider_slideshow',
				'label' => __('Slider slideshow', 'pk_translate'),
				'desc' => __('Select an option to enable or not the slider slideshow.', 'pk_translate'),
				'std' => 'true',
				'type' => 'radio',
				'choices' => array(
					array(
						'value' => 'true',
						'label' => __('Yes', 'pk_translate')
					),
					array(
						'value' => 'false',
						'label' => __('No', 'pk_translate')
					)
				)
			),
			array(
				'id' => '_slider_slideshow_auto_start',
				'label' => __('Slider slideshow auto start', 'pk_translate'),
				'desc' => __('Select an option.', 'pk_translate'),
				'std' => 'true',
				'type' => 'radio',
				'choices' => array(
					array(
						'value' => 'true',
						'label' => __('Yes', 'pk_translate')
					),
					array(
						'value' => 'false',
						'label' => __('No', 'pk_translate')
					)
				)
			),
			array(
				'id' => '_slider_slideshow_interval',
				'label' => __('Slider slideshow interval', 'pk_translate'),
				'desc' => __('Enter the number of seconds.', 'pk_translate'),
				'std' => '5',
				'type' => 'text'
			)
		)
	);
	
	if (!function_exists('ot_register_meta_box')) return;
	if ($post_type == 'page' && $post_format == 'page-faq.php') : ot_register_meta_box($page_faq); return; endif;
	if ($post_type == 'page' && $post_format == 'page-blog.php') : ot_register_meta_box($page_blog); return; endif;
	if ($post_type == 'page' && $post_format == 'page-portfolio.php') : ot_register_meta_box($page_portfolio); return; endif;
	if ($post_type == 'page' && $post_format == 'page-home-agency.php') : ot_register_meta_box($page_home_agency); return; endif;
	if ($post_type == 'page' && $post_format == 'page-home-business.php') : ot_register_meta_box($page_home_business); return; endif;
	if ($post_type == 'page' && $post_format == 'page-home-freelance.php') : ot_register_meta_box($page_home_freelance); return; endif;
	if ($post_type == 'page' && $post_format == 'page-home-standard.php') : ot_register_meta_box($page_home_standard); return; endif;
	if ($post_type == 'page' && $post_format == 'page-home-jigoshop.php') : ot_register_meta_box($page_home_jigoshop); return; endif;
	if ($post_type == 'page' && $post_format == 'page-home-woocommerce.php') : ot_register_meta_box($page_home_woocommerce); return; endif;
	if ($post_type == 'page' && $post_format == 'slider') : ot_register_meta_box($pages_gallery); return; endif;
	if ($post_type == 'post' && $post_format == 'link') : ot_register_meta_box($blog_posts_link); return; endif;
	if ($post_type == 'post' && $post_format == 'quote') : ot_register_meta_box($blog_posts_quote); return; endif;
	if ($post_type == 'post' && $post_format == 'gallery') : ot_register_meta_box($blog_posts_gallery); return; endif;
	if ($post_type == 'post' && !in_array($post_format, array('link', 'quote', 'gallery'))) : ot_register_meta_box($blog_posts_sidebar); return; endif;
	if ($post_type == 'product') : ot_register_meta_box($product_posts_sidebar); return; endif;
	if ($post_type == 'portfolio' && $post_format != 'slider') : ot_register_meta_box($portfolio_posts_sidebar); return; else : ot_register_meta_box($portfolio_posts_gallery); return; endif;
	
}

add_action('admin_init', 'custom_meta_boxes');

?>