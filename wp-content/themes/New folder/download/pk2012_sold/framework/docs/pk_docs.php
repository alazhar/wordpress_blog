<?php

function pk_create_theme_docs_page() {
	
	?>
<style>
.pk_help {
	font-size:14px;
	line-height:24px;
}
.pk_help ol li {
	margin-bottom:30px;
}
small {
	font-weight:bold;
}
pre {
	 color:#444;
	 background-color:#eee;
	 padding:20px;
	 padding-bottom:0px;
}
</style>
<div class="wrap">
	<div id="icon-options-general" class="icon32"><br /></div>
	<h2>Theme Docs</h2>
	<br />
	<div class="pk_help">
	<h1 id="docs_table_of_contents">Table of contents:</h1>
	<ul>
		<li><a href="#demo_contents_installation">Demo Contents Installation</a></li>
		<li><a href="#important_infos">Important Infos</a></li>
		<li><a href="#supported_plugins">Supported Plugins</a></li>
		<li><a href="#theme_options">Theme Options</a></li>
		<li><a href="#navigation_menus_setup">Navigation Menus Setup</a></li>
		<li><a href="#home_page_setup">Home Page Setup</a></li>
		<li><a href="#blog_setup">Blog Setup</a></li>
		<li><a href="#portfolio_setup">Portfolio Setup</a></li>
		<li><a href="#creating_pages">Creating Pages</a></li>
		<li><a href="#creating_blog_posts">Creating Blog Posts</a></li>
		<li><a href="#creating_works">Creating Works</a></li>
		<li><a href="#page_templates">Page Templates</a></li>
		<li><a href="#formats">Formats</a></li>
		<li><a href="#the_home_page_templates_slider">The Home Page Templates Slider</a></li>
		<li><a href="#sidebars_and_widgets_areas">Sidebars and Widgets Areas</a></li>
		<li><a href="#the_html_generator">The HTML Generator</a></li>
		<li><a href="#theme_translation">Theme Translation</a></li>
	</ul>
	
	
	<br />
	<small><a href="#docs_table_of_contents">^ Back to menu ^</a></small><span id="demo_contents_installation"></span>
	<h1>Demo Contents Installation</h1>
	<p>The demo contents installation explained in this video refers to the official theme's preview that you can see here: <a href="http://sold.parkerandkent.com/sold/" target="_blank">WordPress Sold! Responsive/E-Commerce Theme </a>.</p>
	<p>For the official preview we have used <strong>JigoShop</strong> to build the store. The support of the <strong>WooCommerce</strong> plugin has been added later, so if you intend to use <strong>WooCommerce</strong> just follow the same steps explained for <strong>JigoShop</strong>. Only for WooCommerce: once you've set up the plugin and the demo contents you will need to update each product post in order to add them to the shop. This step is necessary because, for the theme's preview, the products have been created with JigoShop and so they need to be saved for WooCommerce. Also the home page for the ecommerce needs to be updated: instead of the <strong>Home Page JigoShop Template</strong> you need to assign the <strong>Home Page WooCommerce Template</strong> to it and save/update.</p>
	<p>The <strong>sold.xml</strong> file of the demo contents is pretty huge, considering the amount of contents showcased on the preview. In some cases the import of this file can take several minutes, depending on your server performances. <strong>We suggest to import the demo contents only to localhost installations (using MAMP, XAMP or similar applications).</strong></p>
	<p><iframe src="http://www.screenr.com/embed/DTe8" width="650" height="396" frameborder="0"></iframe></p>

	
	<br />
	<small><a href="#docs_table_of_contents">^ Back to menu ^</a></small><span id="important_infos"></span>
	<h1>Important Infos</h1>
	<ol>
		<li><strong>Read the docs</strong> and, if you've never used WordPress before, this must be your <a href="http://codex.wordpress.org/Main_Page" target="_blank">starting point</a>.</li>
		<li>Every time you save a page, post or work <small>(depending on the selected format or page template)</small> an <strong>options panel</strong> will appear below the text editor of the post. <strong>So, first select the format or template, enter a title and then save. Only then the options panel will appear.</strong></li>
		<li>Every page, post or work featuring a <strong>gallery</strong>, a <strong>slider</strong> or an <strong>image</strong> uses post's <a href="http://codex.wordpress.org/Inserting_Images_into_Posts_and_Pages" target="_blank">attachments</a>. So, for these post formats, just upload your images as post's attachments and enjoy. For more detailed info don't miss the next sections about <a href="#page_templates">page templates</a> and <a href="#formats">formats</a>.</li>
		<li>Below the text editor of each page, post or work there is the <strong>HTML Generator</strong>, to generate contents like buttons, pricing tables, boxes, accordions, toggles, etc. Remember to use the generator with the <strong>HTML</strong> tab of the post editor, not the visual one.</li>
		<li>Is a good practice to set a <strong>featured image</strong> to every page, post or work, also if it's not needed. The fetured image is very useful to better visualize the contents, especially within the grid layouts and sidebar widgets.</li>
		<li>This theme supports <strong>oEmbed</strong> to include videos. So, to add a video to your page, post or work just paste the video URL in the editor. More info <a href="http://codex.wordpress.org/Embeds" target="_blank">here</a>.</li>
		<li>The <strong>"1 Column"</strong> layout of blogs and portfolios allows you to display, for each item in the list, also media contents like videos and music players other than standard images. The other <strong>"Grid"</strong> layouts will only show the featured image of the posts. Obviously, the single page of each post will show the full featured content, whichever layout you use.</li>
		<li>If you feel that some options or settings panel described here are missing, check the top right corner of the admin. If the <strong>"Screen Options"</strong> button is there, click on it and select the panels you want to activate.</li>
		<li>Go to the <a href="options-permalink.php">Permalinks</a> page and select a permalinks structure different than the default one, eg. <strong>"Month and name"</strong>.</li>
		<li>Probably you don't want to <strong>enable comments</strong> for every page, post or work automatically... If so, go to the <a href="options-discussion.php">Discussion</a> page and uncheck the first 3 checkboxes.</li>
		<li>To enable user registration go to the <a href="options-general.php">General</a> page and check the <strong>"Membership"</strong> option. You will need to enable user registration in case you want to use an <strong>e-commerce</strong> plugin or just to keep the comments open to members only.</li>
		<li>If you use the <strong>JigoShop</strong> plugin go to its <strong>"Settings"</strong> page.<br />In the <strong>"General"</strong> tab <strong>check</strong> the <strong>"Disable Jigoshop frontend.css"</strong> and the <strong>"Disable bundled Fancybox"</strong> option.<br />In the <strong>"Images"</strong> tab check the first 3 checkboxes of the <strong>"Cropping Options"</strong> and then set the right image sizes for this theme: set <strong>"Tiny Image"</strong> to <strong>40*30</strong>, set <strong>"Thumbnail Image"</strong> to <strong>90*67</strong>, set <strong>"Catalog Image"</strong> to <strong>180*135</strong> and set <strong>"Large Image"</strong> to <strong>580*435</strong>.</li>
		<li>If you use the <strong>WooCommerce</strong> plugin go to its <strong>"Settings"</strong> page.<br />In the <strong>"General"</strong> tab <strong>uncheck</strong> the <strong>"Enable WooCommerce CSS styles"</strong>, the <strong>"Enable WooCommerce lightbox on the product page"</strong> and the <strong>"Enable AJAX add to cart buttons on product archives"</strong> options.<br />In the <strong>"Catalog"</strong> tab set the right image sizes for this theme: set <strong>"Catalog Images"</strong> to <strong>180*135</strong>, set <strong>"Single Product Image"</strong> to <strong>580*435</strong> and set <strong>"Product Thumbnails"</strong> to <strong>90*67</strong>.<br />Then, in the <strong>"Pages"</strong> tab, <strong>uncheck</strong> the <strong>"Append a logout link to menus containing My Account"</strong>.</li>
		<li>The theme automatically defines all the media sizes needed by the different templates. All the settings in the <a href="options-media.php">Media</a> page will be ignored by the theme, but they will be used by plugins and WordPress itself. So, for example, if you intend to use the built in <strong>[gallery] shortocde</strong> in your contents<strong></strong> you will have to set the sizes of the images that better fit your needs.</li>
		<li>If you add to the gallery shortcode the attribute <strong>link="file"</strong>, so the shortcode will be <strong>[gallery link="file"]</strong>, the gallery thumbnails will open the large images using the built in lightbox of the theme.</li>
		<li>So, we have explored just some of the WordPress <strong>Settings</strong> <small>(Permalinks, Discussion, General, Media)</small> and later we will check many more of them for sure. We suggest you to carefully explore all the WordPress settings, especially if you're not very familiar with this platform. In the <strong>Settings</strong> pages you will probably find most of the settings you will need during the website setup, so it's very useful to know where the important things are located.</li>
	</ol>
	
	
	<br />
	<small><a href="#docs_table_of_contents">^ Back to menu ^</a></small><span id="supported_plugins"></span>
	<h1>Supported Plugins</h1>
	<p>This theme supports 6 of the best WordPress plugins, that will extend the functionalities of the theme depending on your needs. During the first activation of the theme you should have noticed a yellow alert box informing you about the supported plugins. Through that alert box you can easily install all the plugins with a few clciks, just follow the instructions... or just <a href="themes.php?page=install-required-plugins" target="_self">click here</a>.</p>
	<p>The supportd plugins are the following:</p>
	<ul>
		<li><a href="http://wordpress.org/extend/plugins/background-manager/" title="Background Manager" target="_blank">Background Manager</a></li>
		<li><a href="http://wordpress.org/extend/plugins/breadcrumb-navxt/" title="Breadcrumb NavXT" target="_blank">Breadcrumb NavXT</a></li>
		<li><a href="http://wordpress.org/extend/plugins/contact-form-7/" title="Contact Form 7" target="_blank">Contact Form 7</a></li>
		<li><a href="http://wordpress.org/extend/plugins/qtranslate/" title="qTranslate" target="_blank">qTranslate</a></li>
		<li><a href="http://www.wpml.org/" title="WPML Multilingual CMS" target="_blank">WPML Multilingual CMS</a></li>
		<li><a href="http://wordpress.org/extend/plugins/jigoshop/" title="JigoShop" target="_blank">JigoShop</a></li>
		<li><a href="http://www.woothemes.com/woocommerce/" title="WooCommerce" target="_blank">WooCommerce</a></li>
		<li><a href="http://wordpress.org/extend/plugins/bbpress/" title="bbPress" target="_blank">bbPress</a></li>
	</ul>
	<p>The <a href="http://wordpress.org/extend/plugins/background-manager/" title="Background Manager" target="_blank">Background Manager</a> plugin is a fantastic tool to assign a background image or pattern to any section of your site, with outstanding transitions and effects. Check out the plugin link for more information and to learn how to properly use it.</p>
	<p>The <a href="http://wordpress.org/extend/plugins/breadcrumb-navxt/" title="Breadcrumb NavXT" target="_blank">Breadcrumb NavXT</a> is a very useful plugin to display a breadcrumbs navigation in the header area of the theme. This kind of plugins are very important to improve the user experience of your website. Check out the plugin link for more information and to learn how to properly use it.</p>
	<p>The <a href="http://wordpress.org/extend/plugins/contact-form-7/" title="Contact Form 7" target="_blank">Contact Form 7</a> plugin is an impressive tool to create any type of form. You can easily create a common and simple contact form, as well a complex and detailed form to collect any type of information from your visitors <small>(eg: a booking form for a restaurant)</small>. Check out the plugin link for more information and to learn how to properly use it.</p>
	<p>The <a href="http://www.wpml.org/" title="WPML Multilingual CMS" target="_blank">WPML Multilingual CMS</a> and the <a href="http://wordpress.org/extend/plugins/qtranslate/" title="qTranslate" target="_blank">qTranslate</a> plugins are necessay if you intend to make your website multilingial. Select the one that works better for you. WPML is commercial, more complex and absolutely complete. qTranslate is free and simpler than WPML. Check out the links for more information and to learn how to properly use these plugins.</p>
	<p><strong style="color:#ff0000;">IMPORTANT</strong> - Please understand that with this theme we only offer the best compatibility with these plugins. We are not the creators of these tools and for this reason in most of the cases we can't answer to any question about them. In order to obtain the best support we suggest to always refer to the official websites and documentation of each plugin.</p>
	<p>The <a href="http://wordpress.org/extend/plugins/jigoshop/" title="JigoShop" target="_blank">JigoShop</a> plugin is probably one of the best free plugins to create a fully functional store for your products. Sell your goods online and take your business to the next level. This plugin is very complete and for this reason it could result a little complex at the beginning. We suggest to visit the <a href="http://jigoshop.com/" title="JigoShop" target="_blank">JigoShop</a> official website and its support forum to get started.</p>
	<p>The <a href="http://www.woothemes.com/woocommerce/" title="WooCommerce" target="_blank">WooCommerce</a> plugin is probably one of the best free plugins to create a fully functional store for your products. Sell your goods online and take your business to the next level. This plugin is very complete and for this reason it could result a little complex at the beginning. We suggest to visit the <a href="http://www.woothemes.com/woocommerce/" title="WooCommerce" target="_blank">WooCommerce</a> official website and its support forum to get started.</p>
	<p>The <a href="http://wordpress.org/extend/plugins/bbpress/" title="bbPress" target="_blank">bbPress</a> plugin is probably one of the best free plugins to create a fully functional forum. We suggest to visit the <a href="http://bbpress.org/" title="bbPress" target="_blank">bbPress</a> official website and its support forum to get started.</p>
	
	
	<br />
	<small><a href="#docs_table_of_contents">^ Back to menu ^</a></small><span id="theme_options"></span>
	<h1>Theme Options</h1>
	<p>The theme options are located <a href="themes.php?page=ot-theme-options" target="_self">here</a>, under the Appearance menu. The options are organized in tabs:</p>
	<h3>General</h3>
	<p>In this tab you can set: logo, logo size, favicon, etc.</p>
	<h3>Sidebar</h3>
	<p>In this tab you can set the global sidebar position <small>(left, right or none)</small> and set the sidebar visibility for some specific site contents <small>(main blog, archives, portfolio, shop, forum, search, 404 error page)</small>. Page, posts, works and any other single content have their own sidebar setting <small>(available in the post's metaboxes, not in the main theme options)</small>.</p>
	<h3>Custom sidebars</h3>
	<p>In this tab you can create custom sidebars and widgets areas for any page you have. By creating a custom sidebar for a page, you will also have the option to hide the default sidebars that would be normally loaded by the page. In this way, you can have pages with unique widgets and additional contents.</p>
	<h3>Blog and blog archives</h3>
	<p>In this tab you have to set some options for the main blog and for the blog archives. Note that this theme comes with a blog <a href="#page_templates">page template</a>, but you must not make confusion between the main WordPress blog and the custom blog that you can create with the <a href="#page_templates">page template</a>. Next in the docs we'll see how to <a href="#blog_setup">set up the blog</a> more in detail and we'll make some important considerations about this topic, so be sure to read it carefully.</p>
	<h3>Portfolio archives</h3>
	<p>In this tab you have to set some options for the portfolio archives <small>(portfolio archives are all the works posts displayed by author, category, etc)</small>. Note that this theme comes with a portfolio <a href="#page_templates">page template</a>, but you must not make confusion between the portfolio archives and the custom portfolio that you can create with the <a href="#page_templates">page template</a>. Next in the docs we'll see how to <a href="#portfolio_setup">set up the portfolio</a> more in detail and we'll make some important considerations about this topic, so be sure to read it carefully.</p>
	<h3>Footer</h3>
	<p>In this tab you can set the footer layout <small>(1 to 6 columns, each one is a widgets area)</small>, the footer logo and a copyright text.</p>
	<h3>Footer social networks</h3>
	<p>In this tab you will find a very long list of social networks. Find the ones you have joined and enter the URL of your profiles. Once done small icons with the social networks logos will be displayed automatically in the footer and they will automatically link to your profile pages.</p>
	<h3>Skin</h3>
	<p>In this tab you can choose between the 2 default skins <small>(light and dark)</small> or create a custom one thanks to a very long and complete list of color options that will allow you to change any color of the theme with just a few clicks.</p>
	<h3>Typography</h3>
	<p>In this tab you can enter the CSS styles for your typography. No need to edit any CSS file, just type your styles here and save.</p>
	<p>This is how we have set up the <strong>Google Font</strong> for the theme's preview, just add to the <strong>Typography CSS</strong> field:</p>
	<pre>
@import url(http://fonts.googleapis.com/css?family=Source+Sans+Pro);
h1,h2,h3,h4,h5,h6,.pk_pricing_table header .pk_price{ font-family:'Source Sans Pro'; }
	</pre>
	<h3>Custom CSS</h3>
	<p>In this tab you can enter any additional CSS you want. Your custom CSS saved in this field is very safe, because if you add your styles to a theme's CSS file you risk to lose your changes when updating the theme to newer releases.</p>
	<h3>Custom JS</h3>
	<p>In this tab you can enter any additional JS script you want.</p>
	<h3>Advanced</h3>
	<p>In this tab you can simply turn on the site's performances optimization. Thanks to this option all the CSS and JS files will be minified and combined automatically, without the need of any specific plugin. Thanks to this option your web pages will load faster.</p>
	<h3>Additional options</h3>
	<p>In this tab are grouped all the new options requested by customers, that don't belong to the original theme's options.</p>
	
	
	<br />
	<small><a href="#docs_table_of_contents">^ Back to menu ^</a></small><span id="navigation_menus_setup"></span>
	<h1>Navigation Menus Setup</h1>
	<p>This theme supports 2 navigation menus: the main one for the header and a secondary one for the footer. Both these menus can be configured for <strong>logged in</strong> and <strong>logged out</strong> users, in case you have enabled the users registration and want to differentiate the visible contents for members and visitors.</p>
	<p>To set up the navigation menus go to the <a href="nav-menus.php" target="_self">Menus</a> page. If you've never used the WordPress menu system, we suggest to check for reference the official guide <a href="http://codex.wordpress.org/Appearance_Menus_SubPanel" target="_blank">here</a>.</p>
	
	
	<br />
	<small><a href="#docs_table_of_contents">^ Back to menu ^</a></small><span id="home_page_setup"></span>
	<h1>Home Page Setup</h1>
	<p>WordPress gives you 2 options for the home page <small>(front page)</small> of your site, you can:</p>
	<ol>
		<li>Display your latest <strong>blog posts <small>(your blog)</small>.</strong></li>
		<li>Display a custom <strong>page</strong>.</li>
	</ol>
	<p>If you want to go with the first option basically you don't have to do anything, because WordPress shows by default the latest posts as front page. In fact, in the <a href="options-reading.php" target="_self">Reading</a> page of the <strong>Settings</strong>, the first option <strong>"Front page displays"</strong> is set by default to the <strong>"Your latest posts"</strong> value.</p>
	<p>If you want to go with the second option, displaying a custom page, in the <a href="options-reading.php" target="_self">Reading</a> page of the <strong>Settings</strong>, the first option <strong>"Front page displays"</strong> should be set to the <strong>"A static page <small>(select below)</small>"</strong> value. Once selected, the 2 select fields below will be activated and so you will be able to assign a custom page for the home <small>(front page)</small> and one for the blog <small>(posts page)</small>. You will need to assign a page for the blog too <small>(if you want to have a blog, otherwise don't make a selection)</small>, because you won't be displaying it anymore as front page.<br /><br />The page for the blog must be a simple default empty page <small>(do not assign a page with any other <a href="#page_templates">page template</a>)</small>, with only the title <small>(used by the navigation menu)</small>. The page for your home <small>(front page)</small> can be everything. <strong>This theme has 6 special home <a href="#page_templates">page templates</a> to present your site in a very professional way.</strong></p>
	
	
	<br />
	<small><a href="#docs_table_of_contents">^ Back to menu ^</a></small><span id="blog_setup"></span>
	<h1>Blog Setup</h1>
	<p>As we've seen in the previous section, WordPress gives you 2 ways to set up your blog <small>(check out the <a href="#home_page_setup">Home Page Setup</a> section)</small>.</p>
	<p>If you've chosen to display it as front page or by using a blank page in a different location, we're in any case talking about the main blog <small>(WordPress is basically a blogging platform, so the blog exists before the egg and the chicken)</small>. The options to manage its layout are within the <strong>"Blog and blog archives"</strong> tab of the <a href="themes.php?page=ot-theme-options" target="_self">Theme Options</a> page.</p>
	<p><strong>With "main blog and blog archives" we intend the default WordPress blog pages <small>(blog archives are all the blog posts displayed by author, date, category, tag, etc)</small>, we're not referring to any <a href="#page_templates">page template</a> that is meant to generate custom or alternative blogs</strong>. This theme has a custom blog <a href="#page_templates">page template</a>, but it doesn't use the global blog options. To avoid any confusion, we'll check the blog <a href="#page_templates">page template</a> later in the docs to see how it works and how to set it up.</p>
	<p><strong style="color:#ff0000;">IMPORTANT 1</strong> - Note that, normally, the number of blog posts displayed per page should be set in the <a href="options-reading.php" target="_self">Reading</a> page of the <strong>Settings</strong> through the <strong>"Blog pages show at most"</strong> option. This theme overrides this setting, so you don't have to set it. Basically, the number of blog posts displayed per page is returned by the <strong>"columns*rows"</strong> multiplication set for the blog layout within the <strong>"Blog and blog archives"</strong> tab of the <a href="themes.php?page=ot-theme-options" target="_self">Theme Options</a> page.</p>
	<p><strong style="color:#ff0000;">IMPORTANT 2</strong> - Note that the <strong>sidebar visibility</strong> option for the <strong>"main blog and blog archives"</strong> is located within the <strong>"Sidebar"</strong> tab of the <a href="themes.php?page=ot-theme-options" target="_self">Theme Options</a> page.</p>
	
	
	<br />
	<small><a href="#docs_table_of_contents">^ Back to menu ^</a></small><span id="portfolio_setup"></span>
	<h1>Portfolio Setup</h1>
	<p>To set up your portfolio you need to:</p>
	<ol>
		<li><a href="post-new.php?post_type=page" target="_self">Create a new page</a>, give it a title, assign the <strong>"Portfolio Template"</strong> and save. Once saved the page options will appear below the editor.</li>
		<li>Through the page options panel you can add specific categories to the portfolio <small>(don't select any to include them all)</small> and most importantly set the layout.</li>
		<li>As last operation, you have to assign the layout for the portfolio archives <small>(portfolio archives are all the works posts displayed by author, category, etc)</small> in the <strong>"Portfolio archives"</strong> tab of the <a href="themes.php?page=ot-theme-options" target="_self">Theme Options</a> page.</li>
	</ol>
	<p>You can add new works <a href="edit.php?post_type=portfolio" target="_self">here</a>. You can drag and drop your works to sort them as you want <a href="edit.php?post_type=portfolio&page=pk_portfolio.php" target="_self">here</a></p>
	
	
	<br />
	<small><a href="#docs_table_of_contents">^ Back to menu ^</a></small><span id="creating_pages"></span>
	<h1>Creating Pages</h1>
	<p>To publish a page you need to <a href="post-new.php?post_type=page" target="_self">create a new page</a>, give it a title, assign a <a href="#page_templates">page template</a> or a <a href="#formats">format</a> and save. Once saved, an options panel will appear below the editor <small>(if available)</small>. Check the <a href="#page_templates">Page Templates</a> section for more info.</p>
	
	
	<br />
	<small><a href="#docs_table_of_contents">^ Back to menu ^</a></small><span id="creating_blog_posts"></span>
	<h1>Creating Blog Posts</h1>
	<p>To publish a blog post you need to <a href="post-new.php" target="_self">create a new post</a>, give it a title, assign a <a href="#formats">format</a> and save. Once saved, an options will appear below the editor <small>(if available)</small>.<br /><br />The supported <a href="#formats">formats</a> are the following:<br /><br /><img src="<?php echo PK_THEME_DIR; ?>/framework/docs/images/post_formats.png" /><br />Check the <a href="#formats">Formats</a> section for detailed instructions about how to implement each format.</p>
	
	
	<br />
	<small><a href="#docs_table_of_contents">^ Back to menu ^</a></small><span id="creating_works"></span>
	<h1>Creating Works</h1>
	<p>To publish a work you need to <a href="post-new.php?post_type=portfolio" target="_self">create a new work</a>, give it a title, assign a <a href="#formats">format</a> and save. Once saved, an options panel will appear below the editor <small>(if available)</small>.<br /><br />The supported <a href="#formats">formats</a> are the following:<br /><br /><img src="<?php echo PK_THEME_DIR; ?>/framework/docs/images/page_work_formats.png" /><br />Check the <a href="#formats">Formats</a> section for detailed instructions about how to implement each format.</p>
	
	
	<br />
	<small><a href="#docs_table_of_contents">^ Back to menu ^</a></small><span id="page_templates"></span>
	<h1>Page Templates</h1>
	<p><strong style="color:#ff0000;">IMPORTANT</strong> - To activate the <strong>options panel</strong> of each page template: <strong>select the page template and save</strong>, only after saving the right options panel will be displayed.</p>
	<p>This theme comes with 10 custom page templates. When you <a href="post-new.php?post_type=page" target="_self">create a new page</a> you can select the one you want from the <strong>"Template"</strong> option within the <strong>"Page Attributes"</strong> box below the save button:</p>
	<img src="<?php echo PK_THEME_DIR; ?>/framework/docs/images/page_templates.png" />
	<ol>
		<li>The <strong>"Default Template"</strong> obviously is the basic standard page template of WordPress and it supports the sidebar. It's the <strong>right one for the main blog</strong> <small>(check the <a href="#blog_setup">Blog Setup</a> section)</small> or for any other non specific content. You can assign to it a <strong>"Format"</strong> and, depending on the selected one, the page will behave differently.<br /><br />The supported <a href="#formats">formats</a> by the <strong>"Default Template"</strong> are the following:<br /><br /><img src="<?php echo PK_THEME_DIR; ?>/framework/docs/images/page_work_formats.png" /><br />Check the <a href="#formats">Formats</a> section for detailed instructions about how to implement each format.</li>
		
		<li>The <strong>"Blog Template"</strong> is a page template that recreates the WordPress blog. This page template should be used only in case you want to have a layout for the main blog and a different one for the blog archives <small>(blog archives are all the blog posts displayed by author, date, category, tag, etc)</small>. To do this you need to:<br /><br />
		<ol>
			<li>Select a static page to use as home page and leave empty the selection for the default blog page as explained in the <a href="#home_page_setup">Home Page Setup</a> section.</li>
			<li><a href="post-new.php?post_type=page" target="_self">Create a new page</a>, give it a title, assign the <strong>"Blog Template"</strong> and save. Once saved the page options will appear as explained at the beginning of this section.</li>
			<li>Through the page options panel you can add specific categories to the blog <small>(don't select any to include them all)</small> and most importantly set the layout.</li>
			<li>As last operation, you have to assign the layout for the blog archives in the <strong>"Blog and blog archives"</strong> tab of the <a href="themes.php?page=ot-theme-options" target="_self">Theme Options</a> page.</li>
		</ol>
		This page template activates this options panel below the editor:<br /><br /><img src="<?php echo PK_THEME_DIR; ?>/framework/docs/images/blog_page_options.png" /></li>
		
		<li>The <strong>"FAQ Template"</strong> is a full width page template that loads within an <strong>"Accordion"</strong> or <strong>"Toggle"</strong> component the <a href="edit.php?post_type=faq" target="_blank">FAQ</a> posts for your frequently asked questions section.<br /><br />This page template activates this options panel below the editor:<br /><br /><img src="<?php echo PK_THEME_DIR; ?>/framework/docs/images/faq_page_options.png" /></li>
		
		<li>The <strong>"Full Width Template"</strong> is exactly like the <strong>"Default Template"</strong>, with the only difference that it doesn't have the sidebar. You can assign to it a <strong>"Format"</strong> and, depending on the selected one, the page will behave differently.<br /><br />The supported <a href="#formats">formats</a> by the <strong>"Full Width Template"</strong> are the following:<br /><br /><img src="<?php echo PK_THEME_DIR; ?>/framework/docs/images/page_work_formats.png" /><br />Check the <a href="#formats">Formats</a> section for detailed instructions about how to implement each format.</li>
		
		<li>The <strong>"Home Page Agency Template"</strong> is a full width home page template that features an <strong>image slider</strong> with a <strong>widgets area</strong> on its right, <strong>3 additional widgets areas</strong> and, after the page contents area, it loads some of your <strong>recent works</strong>. You can preview this page <a href="http://sold.parkerandkent.com/sold/homepage-agency/" target="_blank">here</a>. To create this kind of page you have to <a href="post-new.php?post_type=page" target="_self">create a new page</a>, give it a title, assign the <strong>"Home Page Agency Template"</strong> and save. Once saved the page options will appear below the editor as explained at the beginning of this section.<br /><br />All the <strong>widgets areas</strong> must be set in the <a href="widgets.php" target="_blank">Widgets</a> page of the admin. For more info about sidebars and widgets read <a href="#sidebars_and_widgets_areas">here</a>.<br /><br />The <strong>recent works</strong> must be set within the options panel of the page.<br /><br />Setting up the slider is very easy. Just upload your images <small>(width: 610px, height: any)</small> by clicking on the <strong>"Upload/Insert"</strong> button on the top left corner of the editor, so they will result as page attachments <small>(WordPress post's gallery feature)</small>:<br /><br /><img src="<?php echo PK_THEME_DIR; ?>/framework/docs/images/add_media.png" /><br /><br />When you access the media manager <small>(by clicking on the <strong>"Upload/Insert"</strong> button)</small> you will find a <strong>"Gallery"</strong> tab that will list all the uploaded images.<br /><br /><img src="<?php echo PK_THEME_DIR; ?>/framework/docs/images/drop_media.png" /><br /><br />In this tab you can reorder the images and set the titles and descriptions, that will be displayed by the slider. For more detailed information about all the functionalities of the home page slider <a href="#the_home_page_templates_slider">read this section</a>.<br /><br />This page template activates this options panel below the editor:<br /><br /><img src="<?php echo PK_THEME_DIR; ?>/framework/docs/images/home_agency_options.png" /></li>
		
		<li>The <strong>"Home Page Business Template"</strong> is a full width home page template that features an <strong>image slider</strong>, an <strong>RSS feed reader</strong> below the slider, a <strong>tabs component</strong>, <strong>3 widgets areas</strong> and the featured image <small>(<a href="http://codex.wordpress.org/Post_Thumbnails#Setting_a_Post_Thumbnail" target="_blank">how to set a featured image</a>)</small> just before the footer. You can preview this page <a href="http://sold.parkerandkent.com/sold/homepage-business/" target="_blank">here</a>. To create this kind of page you have to <a href="post-new.php?post_type=page" target="_self">create a new page</a>, give it a title, assign the <strong>"Home Page Business Template"</strong> and save. Once saved the page options will appear below the editor as explained at the beginning of this section.<br /><br />The <strong>RSS feed reader</strong> must be set within the options panel of the page.<br /><br />The <strong>tabs component</strong> is populated by the contents of the sub pages. Once you have created the home page with this template, create some new child pages. <strong>Every child page will be added to the tabs.</strong> To create a child page just create a new page with the <strong>"Default Template"</strong>, enter the title and the contents and then, in the <strong>"Page Attributes"</strong> panel, select the main home page from the <strong>"Parent"</strong> select option:<br /><br /><img src="<?php echo PK_THEME_DIR; ?>/framework/docs/images/page_templates.png" /><br /><br />All the <strong>widgets areas</strong> must be set in the <a href="widgets.php" target="_blank">Widgets</a> page of the admin. For more info about sidebars and widgets read <a href="#sidebars_and_widgets_areas">here</a>.<br /><br />The <strong>featured image</strong> <small>(<a href="http://codex.wordpress.org/Post_Thumbnails#Setting_a_Post_Thumbnail" target="_blank">how to set a featured image</a>)</small> of the home page, as you can see in the page preview linked before, is loaded just before the footer. We have used it to showcase some logos. You can do the same with the logos of your best clients.<br /><br />Setting up the slider is very easy. Just upload your images <small>(width: 980px, height: any)</small> by clicking on the <strong>"Upload/Insert"</strong> button on the top left corner of the editor, so they will result as page attachments <small>(WordPress post's gallery feature)</small>:<br /><br /><img src="<?php echo PK_THEME_DIR; ?>/framework/docs/images/add_media.png" /><br /><br />When you access the media manager <small>(by clicking on the <strong>"Upload/Insert"</strong> button)</small> you will find a <strong>"Gallery"</strong> tab that will list all the uploaded images.<br /><br /><img src="<?php echo PK_THEME_DIR; ?>/framework/docs/images/drop_media.png" /><br /><br />In this tab you can reorder the images and set the titles and descriptions, that will be displayed by the slider. For more detailed information about all the functionalities of the home page slider <a href="#the_home_page_templates_slider">read this section</a>.<br /><br />This page template activates this options panel below the editor:<br /><br /><img src="<?php echo PK_THEME_DIR; ?>/framework/docs/images/home_business_options.png" /></li>
		
		<li>The <strong>"Home Page Freelance Template"</strong> is a full width home page template that features an <strong>image slider</strong> with a <strong>call to action</strong> text, <strong>3 widgets areas</strong> and, after the page contents area, it loads some of your <strong>recent works</strong>. You can preview this page <a href="http://sold.parkerandkent.com/sold/homepage-freelance/" target="_blank">here</a>. To create this kind of page you have to <a href="post-new.php?post_type=page" target="_self">create a new page</a>, give it a title, assign the <strong>"Home Page Freelance Template"</strong> and save. Once saved the page options will appear below the editor as explained at the beginning of this section.<br /><br />The <strong>call to action</strong> text must be set within the options panel of the page.<br /><br />All the <strong>widgets areas</strong> must be set in the <a href="widgets.php" target="_blank">Widgets</a> page of the admin. For more info about sidebars and widgets read <a href="#sidebars_and_widgets_areas">here</a>.<br /><br />The <strong>recent works</strong> must be set within the options panel of the page.<br /><br />Setting up the slider is very easy. Just upload your images <small>(width: 980px, height: any)</small> by clicking on the <strong>"Upload/Insert"</strong> button on the top left corner of the editor, so they will result as page attachments <small>(WordPress post's gallery feature)</small>:<br /><br /><img src="<?php echo PK_THEME_DIR; ?>/framework/docs/images/add_media.png" /><br /><br />When you access the media manager <small>(by clicking on the <strong>"Upload/Insert"</strong> button)</small> you will find a <strong>"Gallery"</strong> tab that will list all the uploaded images.<br /><br /><img src="<?php echo PK_THEME_DIR; ?>/framework/docs/images/drop_media.png" /><br /><br />In this tab you can reorder the images and set the titles and descriptions, that will be displayed by the slider. For more detailed information about all the functionalities of the home page slider <a href="#the_home_page_templates_slider">read this section</a>.<br /><br />This page template activates this options panel below the editor:<br /><br /><img src="<?php echo PK_THEME_DIR; ?>/framework/docs/images/home_freelance_options.png" /></li>
		
		<li>The <strong>"Home Page JigoShop Template"</strong> is an home page template with sidebar that features an <strong>image slider</strong> with a <strong>call to action</strong> text, <strong>2 additional widgets areas</strong> and, after the page contents area, it loads some of your <strong>featured and recent products</strong>. You can preview this page <a href="http://sold.parkerandkent.com/sold/homepage-jigoshop/" target="_blank">here</a>. To create this kind of page you have to <a href="post-new.php?post_type=page" target="_self">create a new page</a>, give it a title, assign the <strong>"Home Page JigoShop Template"</strong> and save. Once saved the page options will appear below the editor as explained at the beginning of this section.<br /><br />The <strong>call to action</strong> text must be set within the options panel of the page.<br /><br />All the <strong>widgets areas</strong> and the main <strong>sidebar</strong> must be set in the <a href="widgets.php" target="_blank">Widgets</a> page of the admin. For more info about sidebars and widgets read <a href="#sidebars_and_widgets_areas">here</a>.<br /><br />The <strong>featured and recent products</strong> must be set within the options panel of the page.<br /><br />Setting up the slider is very easy. Just upload your images <small>(width: 980px, height: any)</small> by clicking on the <strong>"Upload/Insert"</strong> button on the top left corner of the editor, so they will result as page attachments <small>(WordPress post's gallery feature)</small>:<br /><br /><img src="<?php echo PK_THEME_DIR; ?>/framework/docs/images/add_media.png" /><br /><br />When you access the media manager <small>(by clicking on the <strong>"Upload/Insert"</strong> button)</small> you will find a <strong>"Gallery"</strong> tab that will list all the uploaded images.<br /><br /><img src="<?php echo PK_THEME_DIR; ?>/framework/docs/images/drop_media.png" /><br /><br />In this tab you can reorder the images and set the titles and descriptions, that will be displayed by the slider. For more detailed information about all the functionalities of the home page slider <a href="#the_home_page_templates_slider">read this section</a>.<br /><br />This page template activates this options panel below the editor:<br /><br /><img src="<?php echo PK_THEME_DIR; ?>/framework/docs/images/home_jigoshop_woocommerce_options.png" /></li>
		
		<li>The <strong>"Home Page Standard Template"</strong> is an home page template with sidebar that features an <strong>image slider</strong>, an <strong>active featured image</strong>, a <strong>call to action</strong> text, <strong>2 additional widgets areas</strong> and, after the page contents area, it loads some of your <strong>recent works and blog posts</strong>. You can preview this page <a href="http://sold.parkerandkent.com/sold/homepage-standard/" target="_blank">here</a>. To create this kind of page you have to <a href="post-new.php?post_type=page" target="_self">create a new page</a>, give it a title, assign the <strong>"Home Page Standard Template"</strong> and save. Once saved the page options will appear below the editor as explained at the beginning of this section.<br /><br />The <strong>active featured image</strong> <small>(<a href="http://codex.wordpress.org/Post_Thumbnails#Setting_a_Post_Thumbnail" target="_blank">how to set a featured image</a>)</small> supports the <strong>"snap points"</strong> like the slider. To know how to set up the <strong>"snap points"</strong> just check out <a href="#the_home_page_templates_slider">this section</a><br /><br />The <strong>call to action</strong> text must be set within the options panel of the page.<br /><br />All the <strong>widgets areas</strong> and the main <strong>sidebar</strong> must be set in the <a href="widgets.php" target="_blank">Widgets</a> page of the admin. For more info about sidebars and widgets read <a href="#sidebars_and_widgets_areas">here</a>.<br /><br />The <strong>recent works and blog posts</strong> must be set within the options panel of the page.<br /><br />Setting up the slider is very easy. Just upload your images <small>(width: 980px, height: any)</small> by clicking on the <strong>"Upload/Insert"</strong> button on the top left corner of the editor, so they will result as page attachments <small>(WordPress post's gallery feature)</small>:<br /><br /><img src="<?php echo PK_THEME_DIR; ?>/framework/docs/images/add_media.png" /><br /><br />When you access the media manager <small>(by clicking on the <strong>"Upload/Insert"</strong> button)</small> you will find a <strong>"Gallery"</strong> tab that will list all the uploaded images.<br /><br /><img src="<?php echo PK_THEME_DIR; ?>/framework/docs/images/drop_media.png" /><br /><br />In this tab you can reorder the images and set the titles and descriptions, that will be displayed by the slider. For more detailed information about all the functionalities of the home page slider <a href="#the_home_page_templates_slider">read this section</a>.<br /><br />This page template activates this options panel below the editor:<br /><br /><img src="<?php echo PK_THEME_DIR; ?>/framework/docs/images/home_standard_options.png" /></li>
		
		<li>The <strong>"Home Page WooCommerce Template"</strong> is an home page template with sidebar that features an <strong>image slider</strong> with a <strong>call to action</strong> text, <strong>2 additional widgets areas</strong> and, after the page contents area, it loads some of your <strong>featured and recent products</strong>. You can preview this page <a href="http://sold.parkerandkent.com/sold/homepage-jigoshop/" target="_blank">here</a>. To create this kind of page you have to <a href="post-new.php?post_type=page" target="_self">create a new page</a>, give it a title, assign the <strong>"Home Page WooCommerce Template"</strong> and save. Once saved the page options will appear below the editor as explained at the beginning of this section.<br /><br />The <strong>call to action</strong> text must be set within the options panel of the page.<br /><br />All the <strong>widgets areas</strong> and the main <strong>sidebar</strong> must be set in the <a href="widgets.php" target="_blank">Widgets</a> page of the admin. For more info about sidebars and widgets read <a href="#sidebars_and_widgets_areas">here</a>.<br /><br />The <strong>featured and recent products</strong> must be set within the options panel of the page.<br /><br />Setting up the slider is very easy. Just upload your images <small>(width: 980px, height: any)</small> by clicking on the <strong>"Upload/Insert"</strong> button on the top left corner of the editor, so they will result as page attachments <small>(WordPress post's gallery feature)</small>:<br /><br /><img src="<?php echo PK_THEME_DIR; ?>/framework/docs/images/add_media.png" /><br /><br />When you access the media manager <small>(by clicking on the <strong>"Upload/Insert"</strong> button)</small> you will find a <strong>"Gallery"</strong> tab that will list all the uploaded images.<br /><br /><img src="<?php echo PK_THEME_DIR; ?>/framework/docs/images/drop_media.png" /><br /><br />In this tab you can reorder the images and set the titles and descriptions, that will be displayed by the slider. For more detailed information about all the functionalities of the home page slider <a href="#the_home_page_templates_slider">read this section</a>.<br /><br />This page template activates this options panel below the editor:<br /><br /><img src="<?php echo PK_THEME_DIR; ?>/framework/docs/images/home_jigoshop_woocommerce_options.png" /></li>
		
		<li>The <strong>"Portfolio Template"</strong> is the page template to load your <a href="edit.php?post_type=portfolio" target="_self">works</a>. To do this you need to:<br /><br />
		<ol>
			<li><a href="post-new.php?post_type=page" target="_self">Create a new page</a>, give it a title, assign the <strong>"Portfolio Template"</strong> and save. Once saved the page options will appear below the editor as explained at the beginning of this section.</li>
			<li>Through the page options panel you can add specific categories to the portfolio <small>(don't select any to include them all)</small> and most importantly set the layout.</li>
			<li>As last operation, you have to assign the layout for the portfolio archives <small>(portfolio archives are all the works posts displayed by author, category, etc)</small> in the <strong>"Portfolio archives"</strong> tab of the <a href="themes.php?page=ot-theme-options" target="_self">Theme Options</a> page.</li>
		</ol>
		This page template activates this options panel below the editor:<br /><br /><img src="<?php echo PK_THEME_DIR; ?>/framework/docs/images/portfolio_page_options.png" /></li>
	</ol>
	
	
	<br />
	<small><a href="#docs_table_of_contents">^ Back to menu ^</a></small><span id="formats"></span>
	<h1>Formats</h1>
	<p><strong style="color:#ff0000;">IMPORTANT 1</strong> - To activate the options of each format: <strong>select the format and save</strong>, only after saving the right options panel will be displayed.</p>
	<p><strong style="color:#ff0000;">IMPORTANT 2</strong> - <span style="text-decoration:underline;">Because the blog posts formats are a default feature of WordPress and cannot be edited, while the formats for portfolio posts (works) and pages are something we've created from scratch, we need to make a clarification about format names and capabilities:</span></p>
	<ol>
		<li>The <strong>"Standard"</strong> format of blog posts <strong>has the capabilities of both</strong> the <strong>"Standard"</strong> and <strong>"Image"</strong> formats explained next in this section.</li>
		<li>The <strong>"Image"</strong> format of blog posts <strong>is the equivalent of</strong> the <strong>"Lightbox Gallery"</strong> format explained next in this section.</li>
		<li>The <strong>"Gallery"</strong> format of blog posts <strong>is the equivalent of</strong> the <strong>"Slider"</strong> format explained next in this section.</li>
	</ol>
	<p>After this clarification, let's see how each format works:</p>
	<ol>
		<li>The <strong>"Standard"</strong> format as you can imagine doesn't have any talent, it's plain. We suggest to set a <strong>"Featured Image"</strong> <small>(<a href="http://codex.wordpress.org/Post_Thumbnails#Setting_a_Post_Thumbnail" target="_blank">how to set a featured image</a>)</small>, because it will be used as thumbnail where needed <small>(widgets, lists, grid layouts, etc.)</small>.<br /><br />This format activates this options panel below the editor (only for blog posts and portfolio posts):<br /><br /><img src="<?php echo PK_THEME_DIR; ?>/framework/docs/images/sidebar_options.png" /></li>
		<li>The <strong>"Aside"</strong> format is meant to display a content without a title, similar to a Facebook note update. We suggest anyway to set a <strong>"Featured Image"</strong> <small>(<a href="http://codex.wordpress.org/Post_Thumbnails#Setting_a_Post_Thumbnail" target="_blank">how to set a featured image</a>)</small>, because it will be used as thumbnail where needed <small>(widgets, lists, grid layouts, etc.)</small>.<br /><br />This format activates this options panel below the editor:<br /><br /><img src="<?php echo PK_THEME_DIR; ?>/framework/docs/images/sidebar_options.png" /></li>
		<li>The <strong>"Status"</strong> format is meant to display a short status update, similar to a Twitter status update. We suggest anyway to set a <strong>"Featured Image"</strong> <small>(<a href="http://codex.wordpress.org/Post_Thumbnails#Setting_a_Post_Thumbnail" target="_blank">how to set a featured image</a>)</small>, because it will be used as thumbnail where needed <small>(widgets, lists, grid layouts, etc.)</small>.<br /><br />This format activates this options panel below the editor:<br /><br /><img src="<?php echo PK_THEME_DIR; ?>/framework/docs/images/sidebar_options.png" /></li>
		<li>The <strong>"Chat"</strong> format is meant to display a chat transcript. We suggest anyway to set a <strong>"Featured Image"</strong> <small>(<a href="http://codex.wordpress.org/Post_Thumbnails#Setting_a_Post_Thumbnail" target="_blank">how to set a featured image</a>)</small>, because it will be used as thumbnail where needed <small>(widgets, lists, grid layouts, etc.)</small>.<br /><br />This format activates this options panel below the editor:<br /><br /><img src="<?php echo PK_THEME_DIR; ?>/framework/docs/images/sidebar_options.png" /></li>
		<li>The <strong>"Link"</strong> format is meant to display an external link instead of the title and a little description. We suggest anyway to set a <strong>"Featured Image"</strong> <small>(<a href="http://codex.wordpress.org/Post_Thumbnails#Setting_a_Post_Thumbnail" target="_blank">how to set a featured image</a>)</small>, because it will be used as thumbnail where needed <small>(widgets, lists, grid layouts, etc.)</small>.<br /><br />This format activates this options panel below the editor:<br /><br /><img src="<?php echo PK_THEME_DIR; ?>/framework/docs/images/post_link_options.png" /></li>
		<li>The <strong>"Quote"</strong> format is meant to display a quotation. We suggest anyway to set a <strong>"Featured Image"</strong> <small>(<a href="http://codex.wordpress.org/Post_Thumbnails#Setting_a_Post_Thumbnail" target="_blank">how to set a featured image</a>)</small>, because it will be used as thumbnail where needed <small>(widgets, lists, grid layouts, etc.)</small>.<br /><br />This format activates this options panel below the editor:<br /><br /><img src="<?php echo PK_THEME_DIR; ?>/framework/docs/images/post_quote_options.png" /></li>
		<li>The <strong>"Image"</strong> format will automatically display the <strong>"Featured Image"</strong> <small>(<a href="http://codex.wordpress.org/Post_Thumbnails#Setting_a_Post_Thumbnail" target="_blank">how to set a featured image</a>)</small> before any other content. <br /><br />The <strong>"Featured Image"</strong> of this page format can have a link, internal or external with _self or _blank window target. To set up the link and the target you need to access the image properties panel <small>(through the main <a href="upload.php" target="_self">Media</a> library or directly through the attachment)</small>, enter the data in the dedicated fields and save the changes:<br /><img src="<?php echo PK_THEME_DIR; ?>/framework/docs/images/image_options_links.png" /><br /><br />This format activates this options panel below the editor (only for blog posts and portfolio posts):<br /><br /><img src="<?php echo PK_THEME_DIR; ?>/framework/docs/images/sidebar_options.png" /></li>
		<li>The <strong>"Video"</strong> format will display a video <small>(YouTube, Vimeo, self hosted, etc.)</small> before any other content of the page. To set up a video you first need to set up a <strong>"Featured Image"</strong> <small>(<a href="http://codex.wordpress.org/Post_Thumbnails#Setting_a_Post_Thumbnail" target="_blank">how to set a featured image</a>)</small> and then you need to access the image properties panel <small>(through the main <a href="upload.php" target="_self">Media</a> library or directly through the attachment)</small>, enter the data in the dedicated fields and save the changes:
		<br /><img src="<?php echo PK_THEME_DIR; ?>/framework/docs/images/image_options_video.png" /><br />
		The <strong>"Featured Image"</strong> is required to hold the video information and it will be used as cover where needed <small>(widgets, lists, grid layouts, etc.)</small>. If you don't have a specific image for the video you can just upload a screenshot or a standard symbolic cover that you will use for every video. <strong>Depending on the video type you're going to use, you will need to fill 1 of the 2 dedicated fields</strong>.<br /><br />The <strong>"Video URL"</strong> field should be filled with the video URL of services like <strong>YouTube, Vimeo, DailyMotion</strong> or any other supported by the WordPress embedding system <small>(more info <a href="http://codex.wordpress.org/Embeds" target="_blank">here</a>)</small>.<br /><br />The <strong>"Video File/s"</strong> field should be filled with a comma separated list of different encodings of a self hosted video file, that will be played with the built in html5 media player. It's a good practice to enter more than one version of the file <small>(m4v and ogv at least)</small> for a better cross browser compatibility. This is an example of comma separated string to enter: <strong>http://www.your-site.com/your_video.m4v,http://www.your-site.com/your_video.ogv</strong><br /><br /><strong style="color:#ff0000;">IMPORTANT</strong> - Note that if you just want to include a video from <strong>YouTube, Vimeo, DailyMotion</strong> or from any other supported provider by the WordPress embedding system <small>(more info <a href="http://codex.wordpress.org/Embeds" target="_blank">here</a>)</small>, you can also avoid to use the <strong>"Video"</strong> format and just paste the Video URL within your page content, in any position. Of course, for self hosted videos, you need to follow the previous instructions.<br /><br />This format activates this options panel below the editor (only for blog posts and portfolio posts):<br /><br /><img src="<?php echo PK_THEME_DIR; ?>/framework/docs/images/sidebar_options.png" /></li>
		<li>The <strong>"Audio"</strong> format will display an audio player before any other content of the page. To set up an audio player you first need to set up a <strong>"Featured Image"</strong> <small>(<a href="http://codex.wordpress.org/Post_Thumbnails#Setting_a_Post_Thumbnail" target="_blank">how to set a featured image</a>)</small> and then you need to access the image properties panel <small>(through the main <a href="upload.php" target="_self">Media</a> library or directly through the attachment)</small>, enter the data in the dedicated fields and save the changes:<br /><img src="<?php echo PK_THEME_DIR; ?>/framework/docs/images/image_options_audio.png" /><br />The <strong>"Featured Image"</strong> is required to hold the audio information and it will be used as cover where needed <small>(widgets, lists, grid layouts, etc.)</small>. If you don't have a specific image for the audio file you're loading you can just upload a standard symbolic cover that you will use for every audio file. <strong>To set up the audio player you just need to fill 1 field</strong>.<br /><br />The <strong>"Audio File/s"</strong> field should be filled with a comma separated list of different encodings of a self hosted audio file, that will be played with the built in html5 media player. It's a good practice to enter more than one version of the file <small>(mp3 and m4a at least)</small> for a better cross browser compatibility. This is an example of comma separated string to enter: <strong>http://www.your-site.com/your_audio.mp3,http://www.your-site.com/your_audio.m4a</strong>.<br /><br />This format activates this options panel below the editor (only for blog posts and portfolio posts):<br /><br /><img src="<?php echo PK_THEME_DIR; ?>/framework/docs/images/sidebar_options.png" /></li>
		<li>The <strong>"Slider"</strong> format will automatically display an image slider before any other content. We suggest to set up a <strong>"Featured Image"</strong> <small>(<a href="http://codex.wordpress.org/Post_Thumbnails#Setting_a_Post_Thumbnail" target="_blank">how to set a featured image</a>)</small>, because it will be used as thumbnail where needed <small>(widgets, lists, grid layouts, etc.)</small>.<br /><br /><strong>You can add an unlimited number of images of any size to the slider</strong>. Just upload your images by clicking on the <strong>"Upload/Insert"</strong> button on the top left corner of the editor, so they will result as page attachments <small>(WordPress post's gallery feature)</small>:<br /><br /><img src="<?php echo PK_THEME_DIR; ?>/framework/docs/images/add_media.png" /><br /><br />When you access the media manager <small>(by clicking on the <strong>"Upload/Insert"</strong> button)</small> you will find a <strong>"Gallery"</strong> tab that will list all the uploaded images.<br /><br /><img src="<?php echo PK_THEME_DIR; ?>/framework/docs/images/drop_media.png" /><br /><br />In this tab you can reorder the images and access their properties panel. Additionally, every image of the slider will use the lightbox to open the large/original size of the same image.<br /><br />You can also load a video instead of the large/original version, simply filling the <strong>"Video URL"</strong> field in the properties panel of the image <small>(as seen before for the "Video" format)</small>. Note that the built in lighbox supports specific video providers <small>(YouTube, Vimeo, DailyMotion)</small> and formats <small>(swf, mov)</small>. For more info check the official <a href="http://www.no-margin-for-errors.com/projects/prettyphoto-jquery-lightbox-clone/" target="_blank">PrettyPhoto</a> website.<br /><br />This format activates this options panel below the editor:</small><br /><br /><img src="<?php echo PK_THEME_DIR; ?>/framework/docs/images/post_work_slider_options.png" /></li>
		<li>The <strong>"Gallery"</strong> format of blog posts is the equivalent of the <strong>"Slider"</strong> format of the portfolio posts (works) and pages, please refer to it.</li>
		<li>The <strong>"Lightbox Gallery"</strong> format will automatically add a cover image linked to a lightbox gallery before any other content. You first need to set up a <strong>"Featured Image"</strong> <small>(<a href="http://codex.wordpress.org/Post_Thumbnails#Setting_a_Post_Thumbnail" target="_blank">how to set a featured image</a>)</small>, because it will be used as thumbnail where needed <small>(widgets, lists, grid layouts, etc.)</small> and it will be used as cover image to open the lightbox gallery by simply clicking on it.<br /><br /><strong>You can add an unlimited number of images of any size to the lightbox gallery</strong>. Just upload your images by clicking on the <strong>"Upload/Insert"</strong> button on the top left corner of the editor, so they will result as page attachments <small>(WordPress post's gallery feature)</small>:<br /><br /><img src="<?php echo PK_THEME_DIR; ?>/framework/docs/images/add_media.png" /><br /><br />When you access the media manager <small>(by clicking on the <strong>"Upload/Insert"</strong> button)</small> you will find a <strong>"Gallery"</strong> tab that will list all the uploaded images.<br /><br /><img src="<?php echo PK_THEME_DIR; ?>/framework/docs/images/drop_media.png" /><br /><br />In this tab you can reorder the images and access their properties panel.<br /><br />You can also load videos instead of the images <small>(symbolic covers, they won't be displayed, they just hold the videos information)</small>, simply filling the <strong>"Video URL"</strong> field in the properties panel of the image <small>(as seen before for the "Video" format)</small>. Note that the built in lighbox supports specific video providers <small>(YouTube, Vimeo, DailyMotion)</small> and formats <small>(swf, mov)</small>. For more info check the official <a href="http://www.no-margin-for-errors.com/projects/prettyphoto-jquery-lightbox-clone/" target="_blank">PrettyPhoto</a> website.<br /><br />This format activates this options panel below the editor:<br /><br /><img src="<?php echo PK_THEME_DIR; ?>/framework/docs/images/sidebar_options.png" /></li>
	</ol>
	
	
	<br />
	<small><a href="#docs_table_of_contents">^ Back to menu ^</a></small><span id="the_home_page_templates_slider"></span>
	<h1>The Home Page Templates Slider</h1>
	<p>As we've seen before for the home page templates in the <a href="#page_templates">Page Templates</a> section, setting up the slider is very easy. Just upload your images by clicking on the <strong>"Upload/Insert"</strong> button on the top left corner of the editor, so they will result as page attachments <small>(WordPress post's gallery feature)</small>:<br /><br /><img src="<?php echo PK_THEME_DIR; ?>/framework/docs/images/add_media.png" /><br /><br />When you access the media manager <small>(by clicking on the <strong>"Upload/Insert"</strong> button)</small> you will find a <strong>"Gallery"</strong> tab that will list all the uploaded images.<br /><br /><img src="<?php echo PK_THEME_DIR; ?>/framework/docs/images/drop_media.png" /><br /><br />In this tab <strong>you can reorder the images and set the titles and descriptions</strong>, that will be displayed by the slider.</p>
	<p>In this section we want to describe some cool features of the home page slider, we'll see how to: manage the positioning/aspect of <strong>titles and descriptions</strong>, create the <strong>"snap points"</strong> and how to associate a <strong>link</strong> or a <strong>video</strong> to any image of the slider.</p>
	<p>As just seen, all the images uploaded through the media manager of your home page will be listed under the <strong>"Gallery"</strong> tab. If you access the properties panel of each image you will see this screen:</p>
	<p><img src="<?php echo PK_THEME_DIR; ?>/framework/docs/images/image_options.png" /></p>
	<p>The <strong>positioning/aspect</strong> of the titles and descriptions of each slide is indipendent and thanks to the <strong>"Title Properties"</strong> and <strong>"Description Properties"</strong> fields you can edit them easily. As you can see below each field there is an example of string to enter. For the title we have the sample string: <strong>{'x':'100', 'y':'100', 'width':'350', 'color':'#ffffff', 'bg_color':'#000000'}</strong>.</p>
	<p>Basically, this string contains some information that the slider uses to set the position of the title, the width of the text, the text color and the background color. Edit this string as you need, be just sure to <strong>respect the quotes around the properties names and their values</strong>, otherwise you could have some problems. The same properties are available for the description, so just follow the instructions for the title.</p>
	<p>The <strong>"snap points"</strong> are active buttons that you can place on your slides, to feature some details or to add additional informations. You can add as many <strong>"snap points"</strong> as you want, position them anywhere on the images and open any inline HTML content when the user makes a roll over with the mouse. Basically the <strong>"snap points"</strong> are contained by an unordered list <strong>&lt;ul&gt;&lt;/ul&gt;</strong> and each of them is a <strong>&lt;li&gt;&lt;/li&gt;</strong> element, containing the HTML markup to visualize on roll over. The following is an example of 2 <strong>"snap points"</strong> we've added to a slide of the theme's preview. Check it carefully, trying to understand the role of each line:
	<pre>
&lt;ul&gt;
	&lt;li class=&quot;pk_snap_point&quot; data-properties=&quot;properties={'x':'310','y':'90','tooltip_width':'230','color':'blue'}&quot;&gt;
		&lt;img src=&quot;http://sold.parkerandkent.com/sold/wp-content/uploads/2012/09/jigoshop_slides_01_d_01.jpg&quot; alt=&quot;Detail 01&quot; /&gt;
		&lt;p&gt;Easy controller...&lt;/p&gt;
	&lt;/li&gt;
	&lt;li class=&quot;pk_snap_point&quot; data-properties=&quot;properties={'x':'430','y':'380','tooltip_width':'245','color':'blue'}&quot;&gt;
		&lt;img src=&quot;http://sold.parkerandkent.com/sold/wp-content/uploads/2012/09/jigoshop_slides_01_d_02.jpg&quot; alt=&quot;Detail 02&quot; /&gt;
		&lt;h4&gt;18x Wide Optic&lt;/h4&gt;
		&lt;p&gt;Quat vel illum eu feugiat facil...&lt;/p&gt;
	&lt;/li&gt;
&lt;/ul&gt;
	</pre>
	<p>Obviously, the code above goes into the <strong>"Snap Points"</strong> field within the properties panel of each slide image.</p>
	<p>To associate a <strong>link</strong> to a slide, internal or external, you simply have to fill the <strong>"Image Custom Link"</strong> and <strong>"Image Custom Link Target"</strong> fields, always in the properties panel of the slides.</p>
	<p>To associate a <strong>video</strong> to a slide you simply have to fill the <strong>"Video URL"</strong> field. Note that the built in lighbox supports specific video providers <small>(YouTube, Vimeo, DailyMotion)</small> and formats <small>(swf, mov)</small>. For more info check the official <a href="http://www.no-margin-for-errors.com/projects/prettyphoto-jquery-lightbox-clone/" target="_blank">PrettyPhoto</a> website.</p>
	
	
	<br />
	<small><a href="#docs_table_of_contents">^ Back to menu ^</a></small><span id="sidebars_and_widgets_areas"></span>
	<h1>Sidebars and Widgets Areas</h1>
	<p>This theme comes with <strong>31 default sidebars and widgets areas</strong> and almost 60 custom widgets <small>(including the customized widgets of the supported plugins)</small>.</p>
	<p>In the <a href="widgets.php" target="_blank">Widgets</a> page you will find all the available widgets and, on the right, all the registered sidebars. To add a widget to a sidebar, expand the selected sidebar panel and then drag and drop the widget over it. Once dropped, fill in the required fields and save.</p>
	<p>This is the list of the sidebars and widgets areas available for this theme:
	<ol>
		<li><strong>Sidebar - All</strong> - This is a global sidebar that will be loaded in every page. So, if you want to have some widgets always visible, you need to add them to this sidebar.</li>
		<li><strong>Sidebar - Front Page </strong> - This sidebar will be displayed only within the home page, if you have set for the home a static page using a template that support a sidebar. More info <a href="#home_page_setup">here</a>.</li>
		<li><strong>Sidebar - JigoShop </strong> - This sidebar will be loaded in every page belonging to the <strong>JigoShop</strong> plugin <small>(shop, cart, products, product categories, etc.)</small>. If you have set the <strong>"Home Page JigoShop Template"</strong> to your home remember that, in any case, it will load the <strong>Sidebar - Front Page </strong>.</li>
		<li><strong>Sidebar - WooCommerce </strong> - This sidebar will be loaded in every page belonging to the <strong>WooCommerce</strong> plugin <small>(shop, cart, products, product categories, etc.)</small>. If you have set the <strong>"Home Page WooCommerce Template"</strong> to your home remember that, in any case, it will load the <strong>Sidebar - Front Page </strong>.</li>
		<li><strong>Sidebar - Pages </strong> - This sidebar will be loaded in every page.</li>
		<li><strong>After Page Content</strong> - This widgets area will be loaded after the content of each page.</li>
		<li><strong>Sidebar - Blog</strong> - This sidebar will be loaded by every page belonging to the main blog, blog archives and in every blog post detail page.</li>
		<li><strong>After Blog Post Content</strong> - This widgets area will be loaded after the content of each blog post.</li>
		<li><strong>Sidebar - Portfolio</strong> - This sidebar will be loaded in every portfolio page and work post detail page.</li>
		<li><strong>After Work Content</strong> - This widgets area will be loaded after the content of each work post.</li>
		<li><strong>Sidebar - Search</strong> - This sidebar will be loaded only in the <strong>search results</strong> pages.</li>
		<li><strong>Sidebar - 404</strong> - This sidebar will be loaded only in the <strong>404 not found</strong> error page.</li>
		<li><strong>Sidebar - Home Agency (Slider - Left - Middle - Right)</strong> - These widgets areas are all dedicated to the <strong>"Home Page Agency Template"</strong></li>
		<li><strong>Sidebar - Home Business (Left - Middle - Right)</strong> - These widgets areas are all dedicated to the <strong>"Home Page Business Template"</strong></li>
		<li><strong>Sidebar - Home Freelance (Left - Middle - Right)</strong> - These widgets areas are all dedicated to the <strong>"Home Page Freelance Template"</strong></li>
		<li><strong>Sidebar - Home Standard (Left - Right)</strong> - These widgets areas are all dedicated to the <strong>"Home Page Standard Template"</strong></li>
		<li><strong>Sidebar - Home JigoShop (Left - Right)</strong> - These widgets areas are all dedicated to the <strong>"Home Page JigoShop Template"</strong></li>
		<li><strong>Sidebar - Home WooCommerce (Left - Right)</strong> - These widgets areas are all dedicated to the <strong>"Home Page WooCommerce Template"</strong></li>
		<li><strong>Footer Column (1, 2, 3, 4, 5, 6)</strong> - These widgets areas are all dedicated to the footer. They will be loaded depending on the footer layout that you set in the <strong>"Footer"</strong> tab of the <a href="themes.php?page=ot-theme-options" target="_self">Theme Options</a>.</li>
	</ol>
	</p>
	<p>For each page you can create an unique custom sidebar and widgets area that will be loaded and displayed only that page. You can also disable the default sidebars for the selected poage. Check the <strong>"Custom sidebars"</strong> tab of the <a href="themes.php?page=ot-theme-options" target="_self">Theme Options</a>.</p>
	
	
	<br />
	<small><a href="#docs_table_of_contents">^ Back to menu ^</a></small><span id="the_html_generator"></span>
	<h1>The HTML Generator</h1>
	<p>Below the editor of each page/post/work you will find the <strong>HTML Generator</strong>:<br /><br /><img src="<?php echo PK_THEME_DIR; ?>/framework/docs/images/html_generator.png" /></p>
	<p>You can use this tool to generate the necessary HTML code to recreate the components and elements listed under the <strong>"Shortcodes"</strong> section of the <a href="http://sold.parkerandkent.com/sold/" target="_blank">theme's preview</a>.</p>
	<p>Remember to generate the HTML code of each component in this way:
	<ol>
		<li>Switch to the <strong>"HTML Tab"</strong> <small>(not the visual one)</small> of the page/post/work editor.</li>
		<li>Put the cursor in the position where you want the generated code to be placed within your contents.</li>
		<li>Select the element/component you want from the <strong>HTML Generator</strong> list.</li>
		<li>Fill in the required fields and click on the "Generate HTML" button and you'll see the code added to the editor.</li>
	</ol>
	</p>
	
	
	<br />
	<small><a href="#docs_table_of_contents">^ Back to menu ^</a></small><span id="theme_translation"></span>
	<h1>Theme Translation</h1>
	<p>This theme is localized. This means that it is translation-ready. To translate, follow these instructions:</p>
	<ol>
		<li>If not already done, convert your WordPress install to the preferred language. Check this <a href="http://codex.wordpress.org/WordPress_in_Your_Language" target="_blank">link</a> for more info.</li>
		<li>Download <a href="http://www.poedit.net/download.php" target="_blank">PoEdit</a> or a similar software.</li>
		<li>Open the file pk_translate.po located in the pk2012_sold/translate/ folder using PoEdit.</li>
		<li>Enter a translation for any string you need.</li>
		<li>Once you have finished, save the file twice, once as .po and once as .mo, the filename should be your language code. Example: it_IT.po and it_IT.mo for Italian. See the language codes <a href="http://codex.wordpress.org/WordPress_in_Your_Language" target="_blank">here</a>.</li>
		<li>Save and upload these files in the same folder where the original pk_translate.po is located. Your theme will be localized.</li>
	</ol>
	</div>
</div>
	<?php
	
}

function pk_add_theme_docs_page() {

	add_theme_page(__('Theme Docs', 'pk_translate'), __('Theme Docs', 'pk_translate'), 'edit_posts', basename(__FILE__), 'pk_create_theme_docs_page');

}

add_action('admin_menu', 'pk_add_theme_docs_page');

?>