<?php

function pk_remove_bbp_stylesheets(){
	
	wp_deregister_style('bbp-default-bbpress');
	wp_deregister_style('bbp-twentyten-bbpress');
	
}

add_action('wp_enqueue_scripts', 'pk_remove_bbp_stylesheets');

function pk_bbp_no_breadcrumb($param) {
	
	return true;
	
}

add_filter ('bbp_no_breadcrumb', 'pk_bbp_no_breadcrumb');

?>