<?php

if (!class_exists('pk_shortcodes_manager_generator')) {
	
	class pk_shortcodes_manager_generator {
		
		private $options;
		private $type;
		private $suffix;
		
		function pk_shortcodes_manager_generator($options = array(), $suffix = '') {
			
			$this -> options = $options;
			$this -> suffix = $suffix;
			
			add_action('admin_init', array(&$this, 'pk_init_metabox'));
			
		}
		
		function pk_init_metabox() {
			
			if (function_exists('add_meta_box')) {
				
				add_meta_box(strtolower($this -> suffix.str_replace(' ', '_', $this -> options['title'])), $this -> options['title'], array(&$this, 'pk_create_metabox'), 'page', 'normal', 'high');
				add_meta_box(strtolower($this -> suffix.str_replace(' ', '_', $this -> options['title'])), $this -> options['title'], array(&$this, 'pk_create_metabox'), 'post', 'normal', 'high');
				add_meta_box(strtolower($this -> suffix.str_replace(' ', '_', $this -> options['title'])), $this -> options['title'], array(&$this, 'pk_create_metabox'), 'portfolio', 'normal', 'high');
				add_meta_box(strtolower($this -> suffix.str_replace(' ', '_', $this -> options['title'])), $this -> options['title'], array(&$this, 'pk_create_metabox'), 'product', 'normal', 'high');
				add_meta_box(strtolower($this -> suffix.str_replace(' ', '_', $this -> options['title'])), $this -> options['title'], array(&$this, 'pk_create_metabox'), 'faq', 'normal', 'high');
			
			}
			
		}
		
		function pk_create_metabox() {
			
			global $pk_shortcodes_manager_instance;
			
			$pk_shortcodes_manager_instance -> pk_open_div();
			
			for ($i = 0; $i < count($this -> options['options']); $i++) {
				
				$pk_shortcodes_manager_instance -> pk_open_shortcodes_manager_div($this -> suffix.$this -> options['type'][$i]);
				
				if (count($this -> options['options'][$i]) > 0) {
					
					$pk_shortcodes_manager_instance -> pk_open_table($this -> options['type'][$i]);
						
					$count = 0;
				
					foreach ($this -> options['options'][$i] as $k => $v) {
					
						if ($this -> options['options'][$i][$k]['title'] != '') {
										
							$pk_shortcodes_manager_instance -> pk_add_title($this -> options['options'][$i][$k]['title'], ($count % 2) ? true : false, ($this -> options['options'][$i][$k]['type'] == 'metabox_selector') ? true : false);
										
						}
						
						$default = $this -> options['values'][$i][$k];
						$helper = $this -> options['options'][$i][$k]['helper'];
				
						switch ($this -> options['options'][$i][$k]['type']) {
					
							case 'text':
								
								$pk_shortcodes_manager_instance -> pk_add_input_text_field($this -> suffix.$k, $default, $helper);
								break;
								
							case 'text_area':
								
								$pk_shortcodes_manager_instance -> pk_add_input_text_area($this -> suffix.$k, $default, $this -> options['options'][$i][$k]['rows'], $helper);
								break;
								
							case 'radio_group':
								
								$pk_shortcodes_manager_instance -> pk_add_input_radio_group($this -> suffix.$k, $this -> options['options'][$i][$k]['values'], $this -> options['options'][$i][$k]['labels'], $default, $helper);
								break;
								
							case 'select':
								
								$pk_shortcodes_manager_instance -> pk_add_input_select($this -> suffix.$k, false, $this -> options['options'][$i][$k]['values'], $this -> options['options'][$i][$k]['labels'], $default, $helper);
								break;
								
							case 'metabox_selector':
								
								$pk_shortcodes_manager_instance -> pk_add_input_select($this -> suffix.$k, true, $this -> options['options'][$i][$k]['values'], $this -> options['options'][$i][$k]['labels'], $default, $helper);
								break;
								
							case 'color':
								
								$pk_shortcodes_manager_instance -> pk_add_input_color($this -> suffix.$k, $default, $helper);
								break;
								
							case 'slider':
								
								$pk_shortcodes_manager_instance -> pk_add_input_slider($this -> suffix.$k, $default, $this -> options['options'][$i][$k]['min'], $this -> options['options'][$i][$k]['max'], $this -> options['options'][$i][$k]['uom'], $helper);
								break;
								
							case 'category':
						
								$pk_shortcodes_manager_instance -> pk_add_input_category($this -> suffix.$k, $default, $this -> options['options'][$i][$k]['taxonomy'], $helper);
								break;
								
							case 'categories':
						
								$pk_shortcodes_manager_instance -> pk_add_input_categories($this -> suffix.$k, $default, $this -> options['options'][$i][$k]['taxonomy'], $helper);
								break;
								
							case 'page':
						
								$pk_shortcodes_manager_instance -> pk_add_input_page($this -> suffix.$k, $default, $this -> options['options'][$i][$k]['post_type'], $helper);
								break;
								
							case 'pages':
						
								$pk_shortcodes_manager_instance -> pk_add_input_pages($this -> suffix.$k, $default, $this -> options['options'][$i][$k]['post_type'], $helper);
								break;
								
							case 'post':
						
								$pk_shortcodes_manager_instance -> pk_add_input_post($this -> suffix.$k, $default, $this -> options['options'][$i][$k]['post_type'], $helper);
								break;
								
							case 'posts':
						
								$pk_shortcodes_manager_instance -> pk_add_input_posts($this -> suffix.$k, $default, $this -> options['options'][$i][$k]['post_type'], $helper);
								break;
								
							case 'tag':
						
								$pk_shortcodes_manager_instance -> pk_add_input_tag($this -> suffix.$k, $default, $helper);
								break;
								
							case 'tags':
						
								$pk_shortcodes_manager_instance -> pk_add_input_tags($this -> suffix.$k, $default, $helper);
								break;
								
						}
					
						$count++;
				
					}
					
					$pk_shortcodes_manager_instance -> pk_close_table();
					
				}
				
				$pk_shortcodes_manager_instance -> pk_add_buttons($this -> options['type'][$i]);
				
				$pk_shortcodes_manager_instance -> pk_close_shortcodes_manager_div();
				
			}
			
			$pk_shortcodes_manager_instance -> pk_close_div();
			
		}
		
	}
	
}

?>