var $pk_aj = jQuery.noConflict();
$pk_aj(document).ready(function() {
	
	$pk_aj('#pk_sc_table_selector_generate_shortcode_button').hide();
	
	$pk_aj('.pk_admin_input_slider').rangeinput();
	$pk_aj('.pk_admin_input_slider').change(function(event) {
		
		$pk_aj(this).data("rangeinput").setValue($pk_aj(this).val());
		
	});
	
	$pk_aj('.pk_admin_input_select_box').change(function(event) {
		
		var count = 0;
		
		$pk_aj(this).find('option').each(function() {
			
			if (count > 0) {
				
				$pk_aj('.' + $pk_aj(this).val().split(',')[0]).hide();
				$pk_aj('#' + $pk_aj(this).val().split(',')[0]).hide();
				
			}
			
			count++;
			
		});
		
		$pk_aj('.' + $pk_aj(this).val().split(',')[0]).show();
		$pk_aj('#' + $pk_aj(this).val().split(',')[0]).show();
		
	});
	
	$pk_aj('.pk_admin_input_select_box').change();
	
	$pk_aj('.pk_admin_multiple_checkbox').change(function(event) {
		
		pkCheckMultipleFields();
		
	});
	
	$pk_aj('.pk_admin_input_select_multiple').change(function(event) {
		
		pkCheckMultipleFields();
		
	});
	
	function pkCheckMultipleFields() {
	
		$pk_aj('.pk_admin_multiple_field').each(function() {
				
			var main = $pk_aj(this).attr('name');
			var result = '';
			
			$pk_aj('.pk_admin_multiple_checkbox[data-main="' + main + '"]').each(function() {
				
				if ($pk_aj(this).is(':checked')) {
					
					(result == '') ? result += $pk_aj(this).attr('value') : result += ',' + $pk_aj(this).attr('value');
					
				}
				
			});
			
			$pk_aj('.pk_admin_input_select_multiple[data-main="' + main + '"]').find('option').each(function() {
				
				if ($pk_aj(this).is(':selected')) {
					
					(result == '') ? result += $pk_aj(this).val() : result += ',' + $pk_aj(this).val();
					
				}
				
			});
			
			$pk_aj(this).val(result);
				
		});
	
	}
	
	function pk_sc_create_generate_shortcode_button() {
		
		$pk_aj('.pk_admin_sc_div').each(function() {
			
			var shortcode_type = $pk_aj(this).attr('class').split(' ')[2].split('pk_sc_')[1];
			
			$pk_aj('#pk_sc_table_' + shortcode_type + '_generate_shortcode_button').click(function(event) {
				
				pk_sc_send_to_editor(shortcode_type);
				
				return false;
				
			});
			
		 });
		 
	}
	
	pk_sc_create_generate_shortcode_button();
	
	function pk_sc_send_to_editor(type) {
		
		switch (type) {
			
			case 'box':
				
				var box_width = $pk_aj('.pk_sc_box').find('[name="pk_sc_box_width"]').val();
				var box_content = $pk_aj('.pk_sc_box').find('[name="pk_sc_box_content"]').val();
				
				box_width = (box_width == '0') ? box_width = 'width:100%;' : box_width = 'width:' + box_width + 'px;';
				
				window.send_to_editor('\n<div class="pk_box" style="' + box_width + '">\n<div class="pk_box_content_wrapper">\n<div class="pk_box_content">\n' + box_content + '\n</div>\n</div>\n</div>\n');
				break;
			
			case 'message_box':
				
				var message_box_width = $pk_aj('.pk_sc_message_box').find('[name="pk_sc_message_box_width"]').val();
				var message_box_type = $pk_aj('.pk_sc_message_box').find('[name="pk_sc_message_box_type"]').val();
				var message_box_content = $pk_aj('.pk_sc_message_box').find('[name="pk_sc_message_box_content"]').val();
				
				message_box_width = (message_box_width == '0') ? message_box_width = 'width:100%;' : message_box_width = 'width:' + message_box_width + 'px;';
				message_box_type = 'pk_' + message_box_type + '_box';
				
				window.send_to_editor('\n<div class="pk_message_box ' + message_box_type + '" style="' + message_box_width + '">\n<div class="pk_message_box_content_wrapper">\n<div class="pk_message_box_content">\n' + message_box_content + '\n</div>\n</div>\n</div>\n');
				break;
			
			case 'buttons':
				
				var button_label = $pk_aj('.pk_sc_buttons').find('[name="pk_sc_buttons_label"]').val();
				var button_size = $pk_aj('.pk_sc_buttons').find('[name="pk_sc_buttons_size"]').val();
				var button_color = $pk_aj('.pk_sc_buttons').find('[name="pk_sc_buttons_color"]').val();
				var button_icon = $pk_aj('.pk_sc_buttons').find('[name="pk_sc_buttons_icon"]').val();
				var button_link = $pk_aj('.pk_sc_buttons').find('[name="pk_sc_buttons_link"]').val();
				var button_link_target = $pk_aj('.pk_sc_buttons').find('[name="pk_sc_buttons_link_target"]').val();
				var button_title = $pk_aj('.pk_sc_buttons').find('[name="pk_sc_buttons_title"]').val();
				
				button_color = (button_color != '') ? ' pk_button_' + button_color : '';
				button_icon = (button_icon != '') ? ' pk_button_with_icon pk_' + button_icon + '_icon' : '';
				
				window.send_to_editor('\n<a href="' + button_link + '" class="pk_button_' + button_size + button_color + button_icon + '" target="' + button_link_target + '" title="' + button_title + '"><span>' + button_label + '</span></a>\n');
				break;
			
			case 'columns':
				
				var column = $pk_aj('.pk_sc_columns').find('[name="pk_sc_columns_size"]').val();
				var column_content = $pk_aj('.pk_sc_columns').find('[name="pk_sc_columns_content"]').val();
				
				window.send_to_editor('\n<div class="' + column + '">\n\t' + column_content + '\n</div>\n');
				break;
			
			case 'dividers':
				
				var divider = '';
				var dividers_type = $pk_aj('.pk_sc_dividers').find('[name="pk_sc_dividers_type"]').val();
				var dividers_height = '';
		
				if ($pk_aj('.pk_sc_dividers').find('[name="pk_sc_dividers_type"]').val() == 'pk_empty_space') {
			
					dividers_height = ' style="height:' + $pk_aj('.pk_sc_dividers').find('[name="pk_sc_dividers_height"]').val() + 'px;"';
			
				}
				
				if (dividers_type == 'pk_clear_both') divider = '<span class="pk_clear_both"></span>';
				if (dividers_type == 'pk_empty_space') divider = '<div class="pk_empty_space"' + dividers_height + '></div>';
				if (dividers_type == 'pk_divider') divider = '<div class="pk_divider"><hr /></div>';
				if (dividers_type == 'pk_divider_top') divider = '<div class="pk_divider pk_top"><hr /><a href="#" title="top">top</a></div>';
				
				window.send_to_editor('\n' + divider + '\n');
				break;
			
			case 'google_maps':
				
				var map_height = $pk_aj('.pk_sc_google_maps').find('[name="pk_sc_google_maps_map_height"]').val();
				var map_url = $pk_aj('.pk_sc_google_maps').find('[name="pk_sc_google_maps_map_url"]').val().replace('#038;', '&amp;');
				
				window.send_to_editor('\n<iframe src="' + map_url + '&amp;output=embed" style="width:100%; height:' + map_height + 'px;"></iframe>\n');
				break;
			
			case 'pricing_tables':
				
				var layout_columns = Number($pk_aj('.pk_sc_pricing_tables').find('[name="pk_sc_pricing_tables_layout_columns"]').val());
				var layout_rows = Number($pk_aj('.pk_sc_pricing_tables').find('[name="pk_sc_pricing_tables_layout_rows"]').val());
				var highlight = Number($pk_aj('.pk_sc_pricing_tables').find('[name="pk_sc_pricing_tables_highlight"]').val());
				var highlight_color = $pk_aj('.pk_sc_pricing_tables').find('[name="pk_sc_pricing_tables_highlight_color"]').val();
				
				var table_output = '\n<div class="pk_pricing_table pk_pricing_' + ((layout_columns == 2) ? 'two' : ((layout_columns == 3) ? 'three' : ((layout_columns == 4) ? 'four' : 'five'))) + '_columns' + ((highlight != 0) ? ' pk_pricing_with_highlight' + highlight_color : '') + ' pk_clearfix">\n';
				
				for (var i = 1; i <= layout_columns; i++) {
					
					table_output += '\n<div class="pk_column' + ((layout_columns == 2) ? ' pk_one_half' : ((layout_columns == 3) ? ' pk_one_third' : ((layout_columns == 4) ? ' pk_one_fourth' : ' pk_one_fifth'))) + ((i == highlight) ? ' pk_highlight_column' : '') + ((i == 1) ? ' pk_first' : ((i == layout_columns) ? ' pk_last' : '')) + '">\n';
					
					table_output += '<header>';
					table_output += '\n<p class="pk_type"><span>Plan</span></p>';
					table_output += '\n<p class="pk_price">$99.<sup>99</sup><span>per month</span></p>';
					table_output += '\n</header>\n<ul>\n';
					
					for (var l = 0; l < layout_rows; l++) {
						
						table_output += '<li' + ((l % 2) ? ' class="pk_alternate_color"' : '') + '>Row text here</li>\n';
						
					}
					
					table_output += '</ul>\n<footer>';
					table_output += '\n<p class="pk_button_wrapper"><a href="#" class="pk_button_small pk_button_' + ((i == highlight) ? 'grey' : 'white') + '"><span>Sign Up</span></a></p>';
					table_output += '\n</footer>\n';
					
					table_output += '</div>\n';
					
				}
				
				table_output += '\n</div>\n';
				
				window.send_to_editor(table_output);
				break;
			
			case 'tabs':
				
				var tabs_title = $pk_aj('.pk_sc_tabs').find('[name="pk_sc_tabs_title"]').val();
				var tabs_number = Number($pk_aj('.pk_sc_tabs').find('[name="pk_sc_tabs_number"]').val());
				var tabs_width = $pk_aj('.pk_sc_tabs').find('[name="pk_sc_tabs_width"]').val();
				
				tabs_width = (tabs_width == '0') ? tabs_width = 'width:100%;' : tabs_width = 'width:' + tabs_width + 'px;';
				
				var tabs_output = '\n<div class="pk_tabs" style="' + tabs_width + '">\n<h4 class="pk_tabs_label">' + tabs_title + '</h4>\n<ul class="pk_tabs_navigation">\n';
				
				for (var i = 0; i < tabs_number; i++) {
					
					tabs_output += '<li><a title="Tab ' + (i + 1) + '" href="#">Tab ' + (i + 1) + '</a></li>\n';
					
				}
				
				tabs_output += '</ul>\n<div class="pk_tabs_content">\n';
				
				for (var i = 0; i < tabs_number; i++) {
					
					tabs_output += '<div class="pk_tab">\n<p>Tab ' + (i + 1) + ' content here...</p>\n</div>\n';
					
				}
				
				tabs_output += '</div>\n</div>\n';
				
				window.send_to_editor(tabs_output);
				break;
			
			case 'toggles':
				
				var toggles_width = $pk_aj('.pk_sc_toggles').find('[name="pk_sc_toggles_width"]').val();
				var toggles_accordion = $pk_aj('.pk_sc_toggles').find('[name="pk_sc_toggles_accordion"]').val();
				var toggles_number = Number($pk_aj('.pk_sc_toggles').find('[name="pk_sc_toggles_number"]').val());
				
				toggles_width = (toggles_width == '0') ? toggles_width = 'width:100%;' : toggles_width = 'width:' + toggles_width + 'px;';
				
				var toggles_output = '\n<div class="pk_toggles' + toggles_accordion + '" style="' + toggles_width + '">\n';
				
				for (var i = 0; i < toggles_number; i++) {
					
					toggles_output += '<div class="pk_toggle">\n<p class="pk_toggle_button">Toggle ' + (i + 1) + '</p>\n<div class="pk_toggle_content_wrapper">\n<div class="pk_toggle_content">\n<p>Toggle ' + (i + 1) + ' content here...</p>\n</div>\n</div>\n</div>\n';
					
				}
				
				toggles_output += '</div>\n';
				
				window.send_to_editor(toggles_output);
				break;
			
			case 'typography_hu':
				
				var size = $pk_aj('.pk_sc_typography_hu').find('[name="pk_sc_typography_hu_size"]').val();
				var heading = $pk_aj('.pk_sc_typography_hu').find('[name="pk_sc_typography_hu_heading"]').val();
				
				window.send_to_editor('\n<' + size + ' class="pk_heading_underline">' + heading + '</' + size + '>\n');
				break;
			
			case 'typography_hb':
				
				var color = $pk_aj('.pk_sc_typography_hb').find('[name="pk_sc_typography_hb_color"]').val();
				var background_color = $pk_aj('.pk_sc_typography_hb').find('[name="pk_sc_typography_hb_background_color"]').val();
				var rounded = $pk_aj('.pk_sc_typography_hb').find('[name="pk_sc_typography_hb_rounded"]').val();
				var size = $pk_aj('.pk_sc_typography_hb').find('[name="pk_sc_typography_hb_size"]').val();
				var heading = $pk_aj('.pk_sc_typography_hb').find('[name="pk_sc_typography_hb_heading"]').val();
				
				window.send_to_editor('\n<' + size + ' class="pk_heading_underline' + rounded + '" style="color:' + color + '; background-color:' + background_color + ';">' + heading + '</' + size + '>\n');
				break;
			
			case 'typography_quote':
				
				var width = $pk_aj('.pk_sc_typography_quote').find('[name="pk_sc_typography_quote_width"]').val();
				var quote = $pk_aj('.pk_sc_typography_quote').find('[name="pk_sc_typography_quote_quote"]').val();
				var cite =  $pk_aj('.pk_sc_typography_quote').find('[name="pk_sc_typography_quote_cite"]').val();
				
				window.send_to_editor('\n<blockquote' + ((width == 0) ? '' : ' style="width:' + width + 'px;"' ) + '><p>' + quote + '</p><cite>' + cite + '</cite></blockquote>\n');
				break;
			
			case 'typography_lists':
				
				var style = $pk_aj('.pk_sc_typography_lists').find('[name="pk_sc_typography_lists_style"]').val();
				var list = $pk_aj('.pk_sc_typography_lists').find('[name="pk_sc_typography_lists_list"]').val();
				
				window.send_to_editor('\n' + list.replace('<ul>', '<ul class="' + style + '">') + ']\n');
				break;
			
			case 'typography_hl':
				
				var color = $pk_aj('.pk_sc_typography_hl').find('[name="pk_sc_typography_hl_color"]').val();
				var background_color = $pk_aj('.pk_sc_typography_hl').find('[name="pk_sc_typography_hl_background_color"]').val();
				var rounded = $pk_aj('.pk_sc_typography_hl').find('[name="pk_sc_typography_hl_rounded"]').val();
				var content = $pk_aj('.pk_sc_typography_hl').find('[name="pk_sc_typography_hl_content"]').val();
				
				window.send_to_editor('\n<span class="pk_highlight' + rounded + '" style="color:' + color + '; background-color:' + background_color + ';">' + content + '</span>\n');
				break;
			
			case 'typography_dc':
				
				var type = $pk_aj('.pk_sc_typography_dc').find('[name="pk_sc_typography_dc_type"]').val();
				var color = $pk_aj('.pk_sc_typography_dc').find('[name="pk_sc_typography_dc_color"]').val();
				var text = $pk_aj('.pk_sc_typography_dc').find('[name="pk_sc_typography_dc_text"]').val();
				
				window.send_to_editor('\n<span class="' + type + color + '">' + text + '</span>\n');
				break;
			
		}
		
		return false;
		
	}
	
});