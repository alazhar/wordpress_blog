<?php

function pk_enqueue_theme_scripts() {
	
	if (is_admin()) {
		
		return;
		
	}
		
	wp_enqueue_script('jquery');
	
	if (is_singular() && get_option('thread_comments')) wp_enqueue_script('comment-reply', false, array(), false, true);
	
	if (pk_get_option('optimize', 'false') == 'false') : 
		
		wp_deregister_script('jquery-easing');
		wp_register_script('jquery-easing', PK_THEME_DIR.'/js/jquery.easing.js', array('jquery'), false, true);
		wp_enqueue_script('jquery-easing');
		
		wp_deregister_script('pk-jplayer');
		wp_register_script('pk-jplayer', PK_THEME_DIR.'/js/jquery.jplayer.min.js', array('jquery'), false, true);
		wp_enqueue_script('pk-jplayer');
		
		wp_deregister_script('pretty-photo');
		wp_register_script('pretty-photo', PK_THEME_DIR.'/prettyPhoto/js/jquery.prettyPhoto.js', array('jquery'), false, true);
		wp_enqueue_script('pretty-photo');
		
		wp_deregister_script('pk-slider');
		wp_register_script('pk-slider', PK_THEME_DIR.'/js/pk_slider.js', array('jquery'), false, true);
		wp_enqueue_script('pk-slider');
		
		wp_deregister_script('pk-content-slider');
		wp_register_script('pk-content-slider', PK_THEME_DIR.'/js/pk_content_slider.js', array('jquery'), false, true);
		wp_enqueue_script('pk-content-slider');
		
		wp_deregister_script('pk');
		wp_register_script('pk', PK_THEME_DIR.'/js/pk.js', array('jquery'), false, true);
		wp_enqueue_script('pk');
		
	else : 
		
		wp_register_script('pk', PK_THEME_DIR.'/js/pk_allinone_js.php', array('jquery'), false, true);
		wp_enqueue_script('pk');
		
	endif;
	
}

add_action('wp_enqueue_scripts', 'pk_enqueue_theme_scripts');

function pk_enqueue_theme_styles() {
	
	if (is_admin()) {
		
		return;
		
	}
	
	if (pk_get_option('optimize', 'false') == 'false' || (function_exists('is_jigoshop') && pk_get_option('selected_skin', 'light') == 'custom')) : 
		
		wp_deregister_style('pk_reset');
		wp_register_style('pk_reset', PK_THEME_DIR.'/css/pk_reset.css');
		wp_enqueue_style('pk_reset');
		
		wp_deregister_style('pk_jigoshop');
		wp_register_style('pk_jigoshop', PK_THEME_DIR.'/css/pk_jigoshop.css');
		if (function_exists('is_jigoshop')) wp_enqueue_style('pk_jigoshop');
		
		wp_deregister_style('pk_jigoshop_widgets');
		wp_register_style('pk_jigoshop_widgets', PK_THEME_DIR.'/css/pk_jigoshop_widgets.css');
		if (function_exists('is_jigoshop')) wp_enqueue_style('pk_jigoshop_widgets');
		
		wp_deregister_style('pk_woocommerce');
		wp_register_style('pk_woocommerce', PK_THEME_DIR.'/css/pk_woocommerce.css');
		if (function_exists('is_woocommerce')) wp_enqueue_style('pk_woocommerce');
		
		wp_deregister_style('pk_woocommerce_widgets');
		wp_register_style('pk_woocommerce_widgets', PK_THEME_DIR.'/css/pk_woocommerce_widgets.css');
		if (function_exists('is_woocommerce')) wp_enqueue_style('pk_woocommerce_widgets');
		
		wp_deregister_style('pk_bbpress');
		wp_register_style('pk_bbpress', PK_THEME_DIR.'/css/pk_bbpress.css');
		if (function_exists('is_bbpress')) wp_enqueue_style('pk_bbpress');
		
		wp_deregister_style('pk_bbpress_widgets');
		wp_register_style('pk_bbpress_widgets', PK_THEME_DIR.'/css/pk_bbpress_widgets.css');
		if (function_exists('is_bbpress')) wp_enqueue_style('pk_bbpress_widgets');
		
		wp_deregister_style('pk_shortcodes');
		wp_register_style('pk_shortcodes', PK_THEME_DIR.'/css/pk_shortcodes.css');
		wp_enqueue_style('pk_shortcodes');
		
		wp_deregister_style('pk_widgets');
		wp_register_style('pk_widgets', PK_THEME_DIR.'/css/pk_widgets.css');
		wp_enqueue_style('pk_widgets');
		
		wp_deregister_style('pk_style');
		wp_register_style('pk_style', PK_THEME_DIR.'/css/pk_style.css');
		wp_enqueue_style('pk_style');
		
		wp_deregister_style('pk_skin');
		wp_register_style('pk_skin', PK_THEME_DIR.'/css/pk_skin_'.pk_get_option('selected_skin', 'light').((pk_get_option('selected_skin', 'light') == 'custom') ? '.php?path='.urlencode(ABSPATH."wp-load.php") : '.css'));
		wp_enqueue_style('pk_skin');
		
		wp_deregister_style('pretty-photo');
		wp_register_style('pretty-photo', PK_THEME_DIR.'/prettyPhoto/css/prettyPhoto.css');
		wp_enqueue_style('pretty-photo');
		
		wp_deregister_style('pk_main_style');
		wp_register_style('pk_main_style', get_stylesheet_uri());
		wp_enqueue_style('pk_main_style');
		
	else : 
		
		wp_register_style('pk', PK_THEME_DIR.'/css/pk_allinone_css.php?skin='.pk_get_option('selected_skin', 'light').'&path='.urlencode(ABSPATH."wp-load.php").'&ecommerce='.((function_exists('is_jigoshop')) ? 'jigoshop' : ((function_exists('is_woocommerce')) ? 'woocommerce' : '')).'&bbpress='.((function_exists('is_bbpress')) ? 'yes' : 'no'));
		wp_enqueue_style('pk');
		
	endif;
	
}

add_action('wp_enqueue_scripts', 'pk_enqueue_theme_styles');

?>