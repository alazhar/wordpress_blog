<?php

if (!is_admin()) define('ICL_DONT_LOAD_NAVIGATION_CSS', true);
if (!is_admin()) define('ICL_DONT_LOAD_LANGUAGE_SELECTOR_CSS', true);

function pk_qtranslate_edit_taxonomies() {
	$args = array('public' => true, '_builtin' => false);
	
	$output = 'object';
	$operator = 'and';
	$taxonomies = get_taxonomies($args, $output, $operator);
	
	if ($taxonomies) {
		
		foreach($taxonomies as $taxonomy) {
			
			add_action($taxonomy -> name.'_add_form', 'qtrans_modifyTermFormFor');
			add_action($taxonomy -> name.'_edit_form', 'qtrans_modifyTermFormFor');
			
		}
		
	}
	
}

if (function_exists('qtrans_modifyTermFormFor')) add_action('admin_init', 'pk_qtranslate_edit_taxonomies');

function pk_load_theme_translation() {

	load_theme_textdomain('pk_translate', get_template_directory().'/translate');

}

add_action('after_setup_theme', 'pk_load_theme_translation');

function pk_lang_object_ids($ids_array, $type) {
	
	if (function_exists('icl_object_id')) {
		
		$res = array();
		
		if (is_array($ids_array)) foreach ($ids_array as $id) {
			
			$xlat = icl_object_id($id, $type, true);
			
			if(!is_null($xlat)) $res[] = $xlat;
			
		}
		
		return $res;
		
	} else {
		
		return $ids_array;
		
	}
	
}

?>