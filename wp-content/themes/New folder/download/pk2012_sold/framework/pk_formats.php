<?php

add_action('admin_init', 'pk_init_metaboxes');
add_action('save_post', 'pk_save_metaboxes_values');

function pk_init_metaboxes() {
	
	if (function_exists('add_meta_box')) {
		
		add_meta_box('pagesformatdiv', __('Format', 'pk_translate'), 'pk_create_pages_format_metabox', 'page', 'side', 'default');
		add_meta_box('worksformatdiv', __('Format', 'pk_translate'), 'pk_create_works_format_metabox', 'portfolio', 'side', 'default');
		
	}
	
}

function pk_create_pages_format_metabox() {
	
	global $post;
	
	$post_meta = get_post_custom($post -> ID);
	$selected = (isset($post_meta['_page_format'][0])) ? $post_meta['_page_format'][0] : 'standard';
	
	echo '
<div id="page-formats-select">
	<input type="radio" name="_page_format" class="post-format" id="page-format-standard" value="standard"'.(($selected == 'standard') ? ' checked="checked" ' : ' ').'/> <label for="page-format-standard">'.__('Standard', 'pk_translate').'</label><br />
	<input type="radio" name="_page_format" class="post-format" id="page-format-image" value="image"'.(($selected == 'image') ? ' checked="checked" ' : ' ').'/> <label for="page-format-image">'.__('Image', 'pk_translate').'</label><br />
	<input type="radio" name="_page_format" class="post-format" id="page-format-video" value="video"'.(($selected == 'video') ? ' checked="checked" ' : ' ').'/> <label for="page-format-video">'.__('Video', 'pk_translate').'</label><br />
	<input type="radio" name="_page_format" class="post-format" id="page-format-audio" value="audio"'.(($selected == 'audio') ? ' checked="checked" ' : ' ').'/> <label for="page-format-audio">'.__('Audio', 'pk_translate').'</label><br />
	<input type="radio" name="_page_format" class="post-format" id="page-format-slider" value="slider"'.(($selected == 'slider') ? ' checked="checked" ' : ' ').'/> <label for="page-format-slider">'.__('Slider', 'pk_translate').'</label><br />
	<input type="radio" name="_page_format" class="post-format" id="page-format-gallery" value="gallery"'.(($selected == 'gallery') ? ' checked="checked" ' : ' ').'/> <label for="page-format-gallery">'.__('Lightbox gallery', 'pk_translate').'</label><br />
</div>
';
	
}

function pk_create_works_format_metabox() {
	
	global $post;
	
	$post_meta = get_post_custom($post -> ID);
	$selected = (isset($post_meta['_work_format'][0])) ? $post_meta['_work_format'][0] : 'standard';
	
	echo '
<div id="work-formats-select">
	<input type="radio" name="_work_format" class="post-format" id="work-format-standard" value="standard"'.(($selected == 'standard') ? ' checked="checked" ' : ' ').'/> <label for="work-format-standard">'.__('Standard', 'pk_translate').'</label><br />
	<input type="radio" name="_work_format" class="post-format" id="work-format-image" value="image"'.(($selected == 'image') ? ' checked="checked" ' : ' ').'/> <label for="work-format-image">'.__('Image', 'pk_translate').'</label><br />
	<input type="radio" name="_work_format" class="post-format" id="work-format-video" value="video"'.(($selected == 'video') ? ' checked="checked" ' : ' ').'/> <label for="work-format-video">'.__('Video', 'pk_translate').'</label><br />
	<input type="radio" name="_work_format" class="post-format" id="work-format-audio" value="audio"'.(($selected == 'audio') ? ' checked="checked" ' : ' ').'/> <label for="work-format-audio">'.__('Audio', 'pk_translate').'</label><br />
	<input type="radio" name="_work_format" class="post-format" id="work-format-slider" value="slider"'.(($selected == 'slider') ? ' checked="checked" ' : ' ').'/> <label for="work-format-slider">'.__('Slider', 'pk_translate').'</label><br />
	<input type="radio" name="_work_format" class="post-format" id="work-format-gallery" value="gallery"'.(($selected == 'gallery') ? ' checked="checked" ' : ' ').'/> <label for="work-format-gallery">'.__('Lightbox gallery', 'pk_translate').'</label><br />
</div>
';
	
}

function pk_save_metaboxes_values($post_id) {
	
	if (isset($_POST['_page_format'])) {
		
		update_post_meta($post_id, '_page_format', $_POST['_page_format']);
		
	}
	
	if (isset($_POST['_work_format'])) {
		
		update_post_meta($post_id, '_work_format', $_POST['_work_format']);
		
	}

}

?>