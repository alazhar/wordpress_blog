<?php

if (!class_exists('pk_theme')) {
	
	class pk_theme {
		
		function pk_theme() {
			
			global $content_width;
			
			if (!isset($content_width)) $content_width = 580;
			
			require_once(PK_FRAMEWORK.'/widgets/pk_widgets.php');
			require_once(PK_FRAMEWORK.'/pk_ecommerce.php');
			require_once(PK_FRAMEWORK.'/pk_faq.php');
			require_once(PK_FRAMEWORK.'/pk_filters.php');
			require_once(PK_FRAMEWORK.'/pk_formats.php');
			require_once(PK_FRAMEWORK.'/pk_forums.php');
			require_once(PK_FRAMEWORK.'/pk_functions.php');
			require_once(PK_FRAMEWORK.'/pk_gallery_extended.php');
			require_once(PK_FRAMEWORK.'/pk_images.php');
			require_once(PK_FRAMEWORK.'/pk_multi_language.php');
			require_once(PK_FRAMEWORK.'/pk_portfolio.php');
			require_once(PK_FRAMEWORK.'/pk_setup.php');
			require_once(PK_FRAMEWORK.'/pk_sidebars.php');
			
			if (is_admin()) {
				
				require_once(PK_FRAMEWORK.'/pk_admin.php');
				
			}
			
			add_action('after_setup_theme', array(&$this, 'pk_add_theme_support'));

		}
		
		function pk_add_theme_support() {
	
			if (function_exists('add_theme_support')) {
				
				add_theme_support('post-formats', array(
					'aside',
					'chat',
					'gallery',
					'image',
					'link',
					'quote',
					'status',
					'video',
					'audio'
				));
				
				add_theme_support('custom-background');
				
				add_theme_support('custom-header');
				
				add_theme_support('automatic-feed-links');
				
				add_theme_support('post-thumbnails', array(
					'post',
					'page',
					'portfolio',
					'product'
				));
				
				add_theme_support('menus');
				
				add_editor_style();
				
			}
			
			if (function_exists('add_image_size')) { 
				
				add_image_size('thumb', 50, 50, true);
				add_image_size('thumb-latest-posts', 80, 80, true);
				add_image_size('thumb-latest-works', 180, 135, true);
				add_image_size('big', 580, 0);
				add_image_size('full-width', 940, 0);
				add_image_size('grid-normal', 460, 345, true);
				add_image_size('grid-special', 460, 0);
				
			}
			
			if (function_exists('register_nav_menu')) {
				
				register_nav_menu('pk_main_menu_li', __('Main Menu (For logged in users)', 'pk_translate'));
				register_nav_menu('pk_footer_menu_li', __('Footer Menu (For logged in users)', 'pk_translate'));
				
				register_nav_menu('pk_main_menu_lo', __('Main Menu (For logged out users)', 'pk_translate'));
				register_nav_menu('pk_footer_menu_lo', __('Footer Menu (For logged out users)', 'pk_translate'));
				
			}
	
		}
		
	}
	
}

if (class_exists('pk_theme') && !isset($theme_instance)) {
	
	$theme_instance = new pk_theme();
	
}

?>