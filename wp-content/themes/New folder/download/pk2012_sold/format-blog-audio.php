
					<article id="post-<?php the_ID(); ?>" <?php post_class('pk_entry pk_entry_blog pk_entry_audio pk_boxed pk_full_content'.((is_singular()) ? ' pk_entry_single' : '')); ?>>
<?php
	$post_meta = get_post_custom(get_the_ID());
	$audio_urls = explode(',', get_post_meta(get_post_thumbnail_id(), '_audio_urls', true));
	
	if (is_array($audio_urls) && count($audio_urls) > 0 && $audio_urls[0] != '') : 
		
		$supplied = '';
		$media = '';
		
		for ($i = 0; $i < count($audio_urls); $i++) :
			
			$supplied .= str_replace('.', '', strrchr(trim($audio_urls[$i]), '.')).(($i < count($audio_urls) - 1) ? ',' : '');
			$media .= str_replace(array('.'), array(''), strrchr(trim($audio_urls[$i]), '.')).':"'.trim($audio_urls[$i]).'"'.(($i < count($audio_urls) - 1) ? ',' : '');
			
		endfor;
?>
						<div id="jp_container_<?php the_ID(); ?>" class="jp-video jp-video-normal-width">
							<div id="jquery_jplayer_<?php the_ID(); ?>" class="jp-jplayer"></div>
							<?php echo PK_JPLAYER_GUI; ?>

						</div>
						<script type="text/javascript">
							jQuery(document).ready(function(){
								jQuery("#jquery_jplayer_<?php the_ID(); ?>").jPlayer({
									ready: function () {
										jQuery(this).jPlayer("setMedia", {
											<?php echo $media; ?>

										});
									},
									play: function() {
										jQuery(this).jPlayer("pauseOthers");
									},
									swfPath:"<?php echo PK_THEME_DIR; ?>/js",
									solution:"flash,html",
									supplied:"<?php echo $supplied; ?>",
									cssSelectorAncestor:"#jp_container_<?php the_ID(); ?>",
									sizeFull: {
										width:"100%",
										height:"100%",
										cssClass:"jp-video-full"
									}
								});
							});
						</script>
<?php
	endif;
?>
						<div class="pk_entry_content">
							<header>
<?php
	if (!is_singular()) : 
?>
								<h3><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h3>
<?php
	else : 
?>
								<h3><?php the_title(); ?></h3>
<?php
	endif;
?>
								<ul class="pk_entry_meta">
									<li class="pk_entry_author"><?php _e('By:', 'pk_translate'); ?> <?php the_author_posts_link(); ?></li>
									<li class="pk_entry_date"><?php echo get_the_date(); ?></li>
									<li class="pk_entry_categories"><?php the_category(', '); ?></li>
									<li class="pk_comments_count"><?php comments_popup_link(__('0', 'pk_translate'), __('1', 'pk_translate'), __('%', 'pk_translate'), 'pk_meta_tot_comments', __('Off', 'pk_translate')); ?></li>
								</ul>
							</header>
							<?php if (is_singular()) : add_filter('embed_defaults', 'pk_post_embed_defaults'); the_content(); else : add_filter('excerpt_length', 'pk_blog_excerpt_filter'); the_excerpt(); remove_filter('excerpt_length', 'pk_blog_excerpt_filter'); endif; ?>
							<footer>
<?php
	if (!is_singular()) : 
?>
								<a href="<?php the_permalink(); ?>" title="<?php _e('Read more', 'pk_translate'); ?>" class="pk_button_read_more"><?php _e('Read more', 'pk_translate'); ?></a>
<?php
	else : 
?>
								<?php the_tags('<p>'.__('tags:', 'pk_translate').' </p><ul class="pk_entry_tags"><li>','</li><li>','</li></ul>'); ?>
<?php
	endif;
?>

							</footer>
						</div>
					</article>
