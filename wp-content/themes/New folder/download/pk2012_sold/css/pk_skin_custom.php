<?php
header("Content-type: text/css; charset: UTF-8");
header("Cache-Control: max-age=".(60 * 60 * 24 * 7).", must-revalidate");
header("Expires: ".gmdate("D, d M Y H:i:s", time() + (60 * 60 * 24 * 7))." GMT");

$path = $_GET['path'];

if (empty($path)) return;

$path = urldecode($path);

if (file_exists($path)) {
	require_once($path);
} else {
	return;
}

$options = get_option('option_tree');
?>


/*
	GLOBAL
-------------------------------------------------------------------------------------------------------------------------------------*/


body {
	color: <?php echo $options['main_primary_text_color']; ?>;
	background: <?php echo $options['site_bg_color']; ?>;
}
::selection {
	color: <?php echo $options['site_special_primary_text_color']; ?>;
	background: <?php echo $options['site_special_color']; ?>;
}
::-moz-selection {
	color: <?php echo $options['site_special_primary_text_color']; ?>;
	background: <?php echo $options['site_special_color']; ?>;
}
::-webkit-selection {
	color: <?php echo $options['site_special_primary_text_color']; ?>;
	background: <?php echo $options['site_special_color']; ?>;
}


/*
	TYPOGRAPHY 
-------------------------------------------------------------------------------------------------------------------------------------*/


h1, h2, h3, h4, h5, h6, mark { 
	color: <?php echo $options['main_titles_color']; ?>;
}
a, a span {
	color: <?php echo $options['main_link_color']; ?>;
}
a:hover, a:hover span {
	color: <?php echo $options['main_link_rollover_color']; ?>;
}
ins, abbr, dfn {
	border-bottom: solid 1px <?php echo $options['main_borders_color']; ?>;
}
pre {
	background: <?php echo $options['boxes_main_bg_color']; ?> url('../images/back_grounds/<?php echo $options['skin_type']; ?>/code_bg.png') repeat-y 0 0;
}
blockquote {
	color: <?php echo $options['main_titles_color']; ?>;
	background: url('../images/back_grounds/<?php echo $options['skin_type']; ?>/quote_bg.png') no-repeat 0 0;
}
blockquote cite, del, dt {
	color: <?php echo $options['main_secondary_text_color']; ?>;
}
mark,
fieldset,
select[multiple=multiple],
.pk_boxed pre,
.pk_boxed code, {
	border: solid 1px <?php echo $options['main_borders_color']; ?>;
}
label {
	color: <?php echo $options['main_primary_text_color']; ?>;
}
input[type=submit], button {
	color: <?php echo $options['main_titles_color']; ?>;
	background: url('../images/button_icons/<?php echo $options['skin_type']; ?>/default_buttons.png') 0 -50px;
}
input[type=text],
input[type=password],
input[type=email],
input[type=file],
textarea {
	color: <?php echo $options['main_input_text_color']; ?>;
	background-color: <?php echo $options['main_input_bg_color']; ?>;
}
.ie8 pre {
	border: solid 1px <?php echo $options['main_borders_color']; ?>;
}
.ie8 input, .ie8 textarea {
	border: solid 1px <?php echo $options['main_borders_color']; ?> !important;
}

/* Widgets Area */

#pk_widgets_area h1,
#pk_widgets_area h2,
#pk_widgets_area h3,
#pk_widgets_area h4,
#pk_widgets_area h5,
#pk_widgets_area h6 {
	color: <?php echo $options['footer_wa_titles_color']; ?>;
}
#pk_widgets_area a,
#pk_widgets_area a span {
	color: <?php echo $options['footer_wa_link_color']; ?>;
}
#pk_widgets_area a:hover,
#pk_widgets_area a:hover span {
	color: <?php echo $options['footer_wa_link_rollover_color']; ?>;
}
#pk_widgets_area ins, 
#pk_widgets_area abbr, 
#pk_widgets_area dfn {
	border-bottom: solid 1px <?php echo $options['footer_wa_borders_color']; ?>;
}
#pk_widgets_area pre {
	background: <?php echo $options['boxes_footer_wa_bg_color']; ?> url('../images/back_grounds/<?php echo $options['skin_type']; ?>/wa_code_bg.png') repeat-y 0 0;
}
#pk_widgets_area blockquote {
	color: <?php echo $options['footer_wa_titles_color']; ?>;
	background: url('../images/back_grounds/<?php echo $options['skin_type']; ?>/wa_quote_bg.png') no-repeat 0 0;
}
#pk_widgets_area blockquote cite {
	color: <?php echo $options['footer_wa_primary_text_color']; ?>;
} 
#pk_widgets_area del, 
#pk_widgets_area dt {
	color: <?php echo $options['footer_wa_secondary_text_color']; ?>;
}
#pk_widgets_area mark,
#pk_widgets_area fieldset,
#pk_widgets_area select[multiple=multiple] {
	border: solid 1px <?php echo $options['footer_wa_borders_color']; ?>;
}
#pk_widgets_area label {
	color: <?php echo $options['footer_wa_primary_text_color']; ?>;
}
#pk_widgets_area input[type=text],
#pk_widgets_area input[type=password],
#pk_widgets_area input[type=email],
#pk_widgets_area textarea {
	color: <?php echo $options['footer_wa_input_text_color']; ?>;
	background-color: <?php echo $options['footer_wa_input_bg_color']; ?>;
}
.ie8 #pk_widgets_area pre {
	border: solid 1px <?php echo $options['footer_wa_borders_color']; ?>;
}
.ie8 #pk_widgets_area input, .ie8 #pk_widgets_area textarea {
	border: solid 1px <?php echo $options['footer_wa_borders_color']; ?> !important;
}


/*
	TABLES
-------------------------------------------------------------------------------------------------------------------------------------*/


table, table caption {
	background-color: <?php echo $options['boxes_main_bg_color']; ?>;
}
table th {
	color: <?php echo $options['main_titles_color']; ?>;
	border-left: solid 1px <?php echo $options['boxes_main_borders_color']; ?>;
	border-right: solid 1px <?php echo $options['boxes_main_borders_color']; ?>;
	background-color: <?php echo $options['main_titles_bg_color']; ?>;
}
table tr{
	border-top: solid 1px <?php echo $options['boxes_main_borders_color']; ?>;	
	border-bottom: solid 1px <?php echo $options['boxes_main_borders_color']; ?>;	
}
table td{
	border-left: solid 1px <?php echo $options['boxes_main_borders_color']; ?>;
	border-right: solid 1px <?php echo $options['boxes_main_borders_color']; ?>;
}
table tfoot td{ background-color: <?php echo $options['main_titles_bg_color']; ?>; }


/*
	HEADER
-------------------------------------------------------------------------------------------------------------------------------------*/


.pk_header {
	color: <?php echo $options['header_text_color']; ?>;
}
.pk_header .pk_right_info a {
	color: <?php echo $options['header_link_color']; ?>;
}
.pk_header .pk_right_info a:hover {
	color: <?php echo $options['header_link_rollover_color']; ?>;
}


/*
	NAVIGATION
-------------------------------------------------------------------------------------------------------------------------------------*/


.pk_navigation_wrapper {
	background: <?php echo $options['menu_bg_color']; ?> url('../images/back_grounds/menu_light.png') repeat-x;
}
.pk_main_menu {
	border: solid 1px rgba(255, 255, 255, .12);
}
.pk_navigation_wrapper .pk_button_menu {
	color: <?php echo $options['menu_text_rollover_color']; ?>;
	background-color: <?php echo $options['menu_text_rollover_bg_color']; ?>;
}
.pk_navigation_wrapper .pk_button_menu.pk_current {
	color: <?php echo $options['mobile_menu_text_color']; ?>;
	background-color: <?php echo $options['mobile_menu_bg_color']; ?>;
}

/* Top Level */

.pk_navigation_wrapper nav a {
	color: <?php echo $options['menu_text_color']; ?>;
}
.pk_navigation_wrapper nav li:hover {
	background-color: <?php echo $options['menu_text_rollover_bg_color']; ?>;
}
.pk_navigation_wrapper nav li:hover > a {
	color: <?php echo $options['menu_text_rollover_color']; ?>;
}
.pk_navigation_wrapper nav ul .current-menu-item > a,
.pk_navigation_wrapper nav ul .current-menu-ancestor > a {
	color: <?php echo $options['menu_text_rollover_color']; ?>;
	background-color: <?php echo $options['menu_text_rollover_bg_color']; ?>;
}

/* Sub Levels */

.pk_navigation_wrapper nav ul ul {
	background-color: <?php echo $options['sub_menu_bg_color']; ?>;
}
.pk_navigation_wrapper nav ul ul li:hover{ background: none; }
.pk_navigation_wrapper nav ul ul a {
	color: <?php echo $options['sub_menu_text_color']; ?>;
}
.pk_navigation_wrapper nav ul ul a:hover {
	color: <?php echo $options['sub_menu_text_rollover_color']; ?>;
}
.pk_navigation_wrapper nav ul ul .current-menu-item > a,
.pk_navigation_wrapper nav ul ul .current-menu-ancestor > a {
	color: <?php echo $options['sub_menu_text_color']; ?>;
	background: none;
}

/* Option Nav */

.pk_option_nav {
	background: url('../images/back_grounds/<?php echo $options['skin_type']; ?>/option_nav_bg.png');
}
.pk_option_item a span {
	color: <?php echo $options['option_nav_text_color']; ?>;
}
.pk_option_item .pk_option_item_divider {
	background: url('../images/back_grounds/<?php echo $options['skin_type']; ?>/option_nav_divider.png') repeat-x 0 0;
}
#pk_search_form input[type=text] {
	color: <?php echo $options['option_nav_search_text_color']; ?>;
	border-left: solid 1px <?php echo $options['main_borders_color']; ?>;
	background-color: <?php echo $options['option_nav_search_bg_color']; ?>;
}
#pk_search_form #pk_search_submit {
	background: url('../images/button_icons/<?php echo $options['skin_type']; ?>/option_nav_button_search.png') no-repeat 0 0;
}
.ie8 #pk_search_form input{ border: none !important; }
.pk_login {
	background: url('../images/button_icons/<?php echo $options['skin_type']; ?>/option_nav_button_login.png') no-repeat 9px 0;
}


/*
	BREADCRUMBS - FEATURED - CALL TO ACTION
-------------------------------------------------------------------------------------------------------------------------------------*/


.breadcrumbs span {
	color: <?php echo $options['main_secondary_text_color']; ?>;
}
.pk_featured_content,
#pk_featured_slider,
#pk_featured_text,
.pk_call_to_action {
	background-color: <?php echo $options['main_bg_color']; ?>;
}
.pk_featured_content,
#pk_featured_text {
	border-bottom: solid 1px <?php echo $options['main_borders_color']; ?>;
}


/*
	PAGE
-------------------------------------------------------------------------------------------------------------------------------------*/


.pk_page_top_shadow, .pk_page {
	background-color: <?php echo $options['main_bg_color']; ?>;
}
.pk_right_sidebar #pk_sidebar {
	background: <?php echo $options['main_bg_color']; ?> url('../images/shadows/<?php echo $options['skin_type']; ?>/right_sidebar_middle_shadow.png') repeat-y 0 0;
}
.pk_right_sidebar #pk_sidebar_top_shadow {
	background: <?php echo $options['main_bg_color']; ?> url('../images/shadows/<?php echo $options['skin_type']; ?>/right_sidebar_top_shadow.png') no-repeat 0 0;
}
.pk_right_sidebar #pk_sidebar_bottom_shadow {
	background: <?php echo $options['main_bg_color']; ?> url('../images/shadows/<?php echo $options['skin_type']; ?>/right_sidebar_bottom_shadow.png') no-repeat 0 bottom;
}
.pk_left_sidebar #pk_sidebar {
	background: <?php echo $options['main_bg_color']; ?> url('../images/shadows/<?php echo $options['skin_type']; ?>/left_sidebar_middle_shadow.png') repeat-y right 0;
}
.pk_left_sidebar #pk_sidebar_top_shadow {
	background: <?php echo $options['main_bg_color']; ?> url('../images/shadows/<?php echo $options['skin_type']; ?>/left_sidebar_top_shadow.png') no-repeat 0 0;
}
.pk_left_sidebar #pk_sidebar_bottom_shadow {
	background: <?php echo $options['main_bg_color']; ?> url('../images/shadows/<?php echo $options['skin_type']; ?>/left_sidebar_bottom_shadow.png') no-repeat 0 bottom;
}
.pk_page #pk_widgets_area {
	color: <?php echo $options['footer_wa_primary_text_color']; ?>;
	background-color: <?php echo $options['footer_wa_bg_color']; ?>;
}


/*
	FOOTER
-------------------------------------------------------------------------------------------------------------------------------------*/


.pk_footer {
	border-top: solid 1px <?php echo $options['footer_wa_borders_color']; ?>;
	background-color: <?php echo $options['footer_bg_color']; ?>;
}
.pk_footer_menu,
.pk_footer_menu a,
.pk_copyright {
	color: <?php echo $options['footer_text_color']; ?>;
}
.pk_footer_menu a:hover {
	color: <?php echo $options['footer_link_rollover_color']; ?>;
}
.pk_footer_menu .pk_button_top {
	background: url('../images/button_icons/<?php echo $options['skin_type']; ?>/top_button_icon.png') no-repeat right 6px;
}
.pk_footer_sn {
	border-right: solid 1px <?php echo $options['footer_text_color']; ?>;
}


/*
	HOMEPAGES
-------------------------------------------------------------------------------------------------------------------------------------*/


.pk_business_tabs .pk_tabs_navigation {
	background-color: <?php echo $options['bh_tabs_bg_color']; ?>;
}
.pk_business_tabs .pk_tabs_navigation ul li {
	border-left: solid 1px <?php echo $options['bh_tabs_borders_color']; ?>;
	border-bottom: solid 1px <?php echo $options['bh_tabs_borders_color']; ?>;
	background-color: <?php echo $options['bh_tabs_text_bg_color']; ?>;
}
.pk_business_tabs .pk_tabs_navigation ul li a {
	color: <?php echo $options['bh_tabs_text_color']; ?>;
}
.pk_business_tabs .pk_tabs_navigation ul li.pk_active_tab {
	background: <?php echo $options['main_bg_color']; ?> url('../images/back_grounds/<?php echo $options['skin_type']; ?>/business_home_selected_tab_bg.png') no-repeat center 65px;
}
.pk_business_tabs .pk_tabs_navigation ul li.pk_active_tab a {
	color: <?php echo $options['bh_tabs_selected_text_color']; ?>;
}
.pk_business_tabs .pk_tabs {
	border-bottom: solid 1px <?php echo $options['main_borders_color']; ?>;
}
.pk_business_home #pk_home_business_featured_image {
	border-top: solid 1px <?php echo $options['main_borders_color']; ?>;
}


/*
	ENTRIES
-------------------------------------------------------------------------------------------------------------------------------------*/


.pk_boxed {
	color: <?php echo $options['main_primary_text_color']; ?>;
	background-color: <?php echo $options['boxes_main_bg_color']; ?>;
}
.pk_boxed .pk_entry_content form input[type=password] {
	color: <?php echo $options['boxes_main_input_text_color']; ?>;
	background-color: <?php echo $options['boxes_main_input_bg_color']; ?>;
}
.pk_boxed p.icl_post_in_other_langs {
	border: solid 1px <?php echo $options['boxes_main_borders_color']; ?>;
	background-color: <?php echo $options['boxes_main_input_bg_color']; ?>;
}
#pk_widgets_area .pk_boxed {
	background-color: <?php echo $options['boxes_footer_wa_bg_color']; ?>;
}
	
/* Entry Product */
	
.pk_entry_product header {
	border-top: solid 1px <?php echo $options['boxes_main_borders_color']; ?>;
	border-bottom: solid 1px <?php echo $options['boxes_main_borders_color']; ?>;
}
.pk_entry_product header .pk_entry_price {
	color: <?php echo $options['ec_products_price_color']; ?>;
}
#pk_featured_products .pk_entry_product footer {
	border-top: solid 1px <?php echo $options['boxes_main_borders_color']; ?>;
}

/* Entry Blog */

.pk_entry_blog .pk_entry_meta {
	border-bottom: solid 1px <?php echo $options['boxes_main_borders_color']; ?>;
}
.pk_entry_blog .pk_entry_meta li {
	border-left: solid 1px <?php echo $options['boxes_main_borders_color']; ?>;
}
.pk_entry_blog .pk_entry_meta li.pk_comments_count a {
	background: url('../images/back_grounds/<?php echo $options['skin_type']; ?>/comments_icon.png') no-repeat 0 0;
}
.pk_entry_standard .pk_entry_content {
	background: url('../images/post_format_icons/<?php echo $options['skin_type']; ?>/standard.png') no-repeat 15px 19px;
}
.pk_entry_aside .pk_entry_content {
	background: url('../images/post_format_icons/<?php echo $options['skin_type']; ?>/aside.png') no-repeat 15px 15px;
}
.pk_entry_video .pk_entry_content {
	background: url('../images/post_format_icons/<?php echo $options['skin_type']; ?>/video.png') no-repeat 15px 20px;
}
.pk_entry_audio .pk_entry_content {
	background: url('../images/post_format_icons/<?php echo $options['skin_type']; ?>/audio.png') no-repeat 15px 22px;
}
.pk_entry_image .pk_entry_content {
	background: url('../images/post_format_icons/<?php echo $options['skin_type']; ?>/image.png') no-repeat 15px 22px;
}
.pk_entry_gallery .pk_entry_content {
	background: url('../images/post_format_icons/<?php echo $options['skin_type']; ?>/gallery.png') no-repeat 15px 21px;
}
.pk_entry_gallery .pk_slider {
	border-bottom: solid 1px <?php echo $options['boxes_main_borders_color']; ?>;
}
.pk_entry_quote .pk_quote {
	border-bottom: solid 1px <?php echo $options['boxes_main_borders_color']; ?>;
	background: url('../images/post_format_icons/<?php echo $options['skin_type']; ?>/quote.png') no-repeat 15px 18px;
}
.pk_entry_link .pk_entry_content {
	background: url('../images/post_format_icons/<?php echo $options['skin_type']; ?>/link.png') no-repeat 15px 18px;
}
.pk_entry_chat .pk_entry_content {
	background: url('../images/post_format_icons/<?php echo $options['skin_type']; ?>/chat.png') no-repeat 15px 20px;
}
.pk_entry_status .pk_entry_content {
	background: url('../images/post_format_icons/<?php echo $options['skin_type']; ?>/status.png') no-repeat 15px 15px;
}


/*
	COMMENTS
-------------------------------------------------------------------------------------------------------------------------------------*/


.pk_comments .pk_comments_rss {
	background: url('../images/back_grounds/<?php echo $options['skin_type']; ?>/feed_icon.png') no-repeat center center;
}
.pk_comments .pk_message {
	background-color: <?php echo $options['boxes_main_bg_color']; ?>;
}
.bypostauthor > .pk_message .pk_gravatar {
	border: solid 3px <?php echo $options['site_special_color']; ?>;
	background-color: <?php echo $options['site_special_color']; ?>;
}
.pk_comments .pk_message .pk_comment_meta {
	border-left: solid 1px <?php echo $options['boxes_main_borders_color']; ?>;
}
.pk_comments .pk_message .pk_comment_meta p.pk_comment_author {
	color: <?php echo $options['main_titles_color']; ?>;
}
.pk_comments .pk_message .pk_comment_meta p.pk_comment_date span {
	color: <?php echo $options['main_secondary_text_color']; ?>;
}
.nocomments {
	background-color: <?php echo $options['boxes_main_bg_color']; ?>;
}
#respond h3 small a {
	border-left: solid 1px <?php echo $options['main_borders_color']; ?>;
}
.required {
	color: <?php echo $options['site_special_color']; ?>;
}


/*
	CATEGORIES FILTER
-------------------------------------------------------------------------------------------------------------------------------------*/


.pk_categories_filter li {
	border-left: solid 1px <?php echo $options['main_borders_color']; ?>;
}
.pk_categories_filter a.pk_current_category {
	color: <?php echo $options['main_secondary_text_color']; ?>;
}


/*
	PAGINATION
-------------------------------------------------------------------------------------------------------------------------------------*/


.pk_pagination a {
	color: <?php echo $options['main_primary_text_color']; ?>;
	background-color: <?php echo $options['boxes_main_bg_color']; ?>;
}
.pk_pagination a:hover, .pk_posts_link_pages a:hover {
	color: <?php echo $options['main_titles_color']; ?>;
}
.pk_pagination .pk_current_page,
.pk_pagination .pk_current_page:hover {
	color: <?php echo $options['main_titles_color']; ?>;
	background-color: <?php echo $options['main_bg_color']; ?>;
}
.pk_posts_link_pages p {
	color: <?php echo $options['main_secondary_text_color']; ?>;
	background-color: <?php echo $options['boxes_main_bg_color']; ?>;
}
.pk_previous_post_links a {
	background: url('../images/button_icons/<?php echo $options['skin_type']; ?>/prev_post_button_icon.png') no-repeat 0 center;
}
.pk_next_post_links a {
	background: url('../images/button_icons/<?php echo $options['skin_type']; ?>/next_post_button_icon.png') no-repeat right center;
}


/*
	WP GALLERY - WP CAPTION
-------------------------------------------------------------------------------------------------------------------------------------*/


.pk_entry_attachment {
	border: solid 1px <?php echo $options['main_borders_color']; ?>;
	background-color: <?php echo $options['main_titles_bg_color']; ?>;
}
.pk_attachment_caption p {
	border: solid 1px <?php echo $options['main_borders_color']; ?>;
	background-color: <?php echo $options['main_titles_bg_color']; ?>;
}
.pk_prev_atachment a,
.pk_next_atachment a,
.pk_prev_atachment a:hover,
.pk_next_atachment a:hover {
	color: <?php echo $options['main_titles_color']; ?>;
}
.wp-caption {
	border: solid 1px <?php echo $options['main_borders_color']; ?>;
}
.wp-caption-text {
	color: <?php echo $options['main_secondary_text_color']; ?>;
}
.pk_entry_single .wp-caption {
	border: solid 1px <?php echo $options['boxes_main_borders_color']; ?>;
}


/*
	SLIDERS
-------------------------------------------------------------------------------------------------------------------------------------*/


.pk_content_slider .pk_slider_thumbs .pk_slider_navigation_button {
	background: url('../images/button_icons/<?php echo $options['skin_type']; ?>/sliders_buttons.png') no-repeat -120px 0;
}
.pk_content_slider .pk_slider_thumbs .pk_slider_navigation_button.pk_selected_button {
	background-position: -132px 0;
}
.pk_content_slider .pk_button_prev {
	background: url('../images/button_icons/<?php echo $options['skin_type']; ?>/sliders_buttons.png') no-repeat 0 center;
}
.pk_content_slider .pk_button_next { 
	background: url('../images/button_icons/<?php echo $options['skin_type']; ?>/sliders_buttons.png') no-repeat -60px center;
}
.pk_content_slider .pk_overlay {
	background-color: <?php echo $options['sliders_overlay_bg_color']; ?>;
}
.pk_content_slider .pk_loader {
	background: <?php echo $options['main_bg_color']; ?> url('../images/back_grounds/<?php echo $options['skin_type']; ?>/sliders_loader.gif') no-repeat center center;
}
.pk_slider, .pk_slider .pk_slider_content {
	background-color: <?php echo $options['boxes_main_bg_color']; ?>;
}
.pk_slider .pk_button_prev {
	background: url('../images/button_icons/<?php echo $options['skin_type']; ?>/sliders_buttons.png') no-repeat 0 center;
}
.pk_slider .pk_button_next {
	background: url('../images/button_icons/<?php echo $options['skin_type']; ?>/sliders_buttons.png') no-repeat -60px center;
}
.pk_slider .pk_slider_thumbs .pk_slider_navigation_button {
	background: url('../images/button_icons/<?php echo $options['skin_type']; ?>/sliders_buttons.png') no-repeat -120px -12px;
}
.pk_slider .pk_slider_thumbs .pk_slider_navigation_button.pk_selected_button {
	background-position: -132px -12px;
}
.pk_slider .pk_button_slideshow {
	background: url('../images/button_icons/<?php echo $options['skin_type']; ?>/sliders_buttons.png') no-repeat -144px -12px;
}
.pk_slider .pk_button_slideshow.pk_paused {
	background-position: -156px -12px;
}	
.pk_slider .pk_slider_info_button {
	color: <?php echo $options['main_primary_text_color']; ?>;
}
.pk_slider .pk_slider_item_info_content {
	color: <?php echo $options['sliders_info_text_color']; ?>;
}
.pk_slider .pk_slider_item_info_background {
	background-color: <?php echo $options['sliders_info_bg_color']; ?>;
}
.pk_slider .pk_loader {
	background: <?php echo $options['main_bg_color']; ?> url('../images/back_grounds/<?php echo $options['skin_type']; ?>/sliders_loader.gif') no-repeat center center;
}


/*
	IMAGES - VIDEOS
-------------------------------------------------------------------------------------------------------------------------------------*/


.pk_image_wrapper,
.pk_video_wrapper {
	background-color: <?php echo $options['boxes_main_bg_color']; ?>;
}
.pk_video video,
.pk_video object,
.pk_video embed,
.pk_video iframe {
	background-color: #000;
}
#pk_widgets_area .pk_image_wrapper,
#pk_widgets_area .pk_video_wrapper {
	background-color: <?php echo $options['footer_wa_bg_color']; ?>;
}
.pk_zoom_icon .pk_image_button_icon {
	background: url('../images/image_icons/<?php echo $options['skin_type']; ?>/zoom_icon.png') no-repeat center center;
}
.pk_play_icon .pk_image_button_icon {
	background: url('../images/image_icons/<?php echo $options['skin_type']; ?>/play_icon.png') no-repeat center center;
}
.pk_page_icon .pk_image_button_icon {
	background: url('../images/image_icons/<?php echo $options['skin_type']; ?>/page_icon.png') no-repeat center center;
}
.pk_link_icon .pk_image_button_icon {
	background: url('../images/image_icons/<?php echo $options['skin_type']; ?>/link_icon.png') no-repeat center center;
}


/*
	FORMS
-------------------------------------------------------------------------------------------------------------------------------------*/


.wpcf7 .wpcf7-mail-sent-ok {
	color: #44802a;
	border: solid 1px #c9f0b8;
	background-color: #c9f0b8;
}
.wpcf7 .wpcf7-mail-sent-ng {
	color: #af3017;
	border: solid 1px #f1bbb0;
	background-color: #f1bbb0;
}
.wpcf7 .wpcf7-spam-blocked {
	color: #705213;
	border: solid 1px #ffd987;
	background-color: #ffd987;
}
.wpcf7 .wpcf7-validation-errors {
	color: #af3017;
	background-color: #f1bbb0;
}

/* Login / Register (pk form box) */

.pk_form_box {
	background-color: <?php echo $options['boxes_main_bg_color']; ?>;
}
.pk_button_close {
	background: url('../images/button_icons/<?php echo $options['skin_type']; ?>/close_button_icon.png') no-repeat 0 0;
}
.pk_form_box_back_ground {
	background-color: <?php echo $options['pp_overlay_bg_color']; ?>;
}
.pk_user_form {
	background-color: <?php echo $options['pp_bg_color']; ?>;
}
.pk_user_form form {
	border-bottom: solid 1px <?php echo $options['main_borders_color']; ?>;
	background-color: <?php echo $options['boxes_main_bg_color']; ?>;
}
.pk_user_form input[type=text],
.pk_user_form input[type=email],
.pk_user_form input[type=password] {
	color: <?php echo $options['boxes_main_input_text_color']; ?>;
	background-color: <?php echo $options['boxes_main_input_bg_color']; ?>;
}
.pk_user_form p#reg_passmail, .pk_user_form p.message {
	color: <?php echo $options['main_primary_text_color']; ?>;
}
.pk_user_form p.register {
	color: <?php echo $options['main_titles_color']; ?>;
}
.pk_user_form #registerform {
	border-top: solid 1px <?php echo $options['main_borders_color']; ?>;
}


/*
	JPLAYER
-------------------------------------------------------------------------------------------------------------------------------------*/


.jp-video .jp-jplayer {
	background-color: #000;
}
.jp-interface {
	background: url('../images/jplayer/<?php echo $options['skin_type']; ?>/controls_bg.png') repeat-x 0 0;
}
a.jp-play {
	background: url('../images/jplayer/<?php echo $options['skin_type']; ?>/button_icons.png') no-repeat 0 0;
}
a.jp-pause {
	background: url('../images/jplayer/<?php echo $options['skin_type']; ?>/button_icons.png') no-repeat -35px 0;
}
.separator {
	background: url('../images/jplayer/<?php echo $options['skin_type']; ?>/separator.png') no-repeat 0 0;
}
.jp-progress,
.jp-volume-bar {
	background: <?php echo $options['controls_bars_light_bg_color']; ?>;
}
.jp-seeking-bg {
	background: <?php echo $options['controls_seek_bar_bg_color']; ?>;
}
.jp-play-bar {
	background: url('../images/jplayer/<?php echo $options['skin_type']; ?>/bar_bg.png') repeat-x 0 0;
}
.jp-current-time,
.time-sep,
.jp-duration {
	color: <?php echo $options['main_secondary_text_color']; ?>;
}
.jp-mute {
	background: url('../images/jplayer/<?php echo $options['skin_type']; ?>/button_icons.png') no-repeat -70px 0;
}
a.jp-unmute {
	background: url('../images/jplayer/<?php echo $options['skin_type']; ?>/button_icons.png') no-repeat -105px 0;
}
.jp-volume-bar-value {
	background: url('../images/jplayer/<?php echo $options['skin_type']; ?>/bar_bg.png') repeat-x 0 0;
}
.jp-full-screen {
	background: url('../images/jplayer/<?php echo $options['skin_type']; ?>/button_icons.png') no-repeat -140px 0;
}
.jp-restore-screen {
	background: url('../images/jplayer/<?php echo $options['skin_type']; ?>/button_icons.png') no-repeat -175px 0;
}
.jp-no-solution {
	color: #43412a;
	background-color: #fff4c0;
}


/*
	PRETTYPHOTO
-------------------------------------------------------------------------------------------------------------------------------------*/


.pp_content{ background: <?php echo $options['pp_bg_color']; ?> !important; }
.pp_overlay{ background: <?php echo $options['pp_overlay_bg_color']; ?> !important; }


/*
	DEFAULT BUTTONS
-------------------------------------------------------------------------------------------------------------------------------------*/


.pk_button_read_more {
	background: url('../images/button_icons/<?php echo $options['skin_type']; ?>/read_more_button_icon.png') no-repeat 0 center;
}
.pk_point_button {
	background: url('../images/button_icons/snap_point_buttons.png') no-repeat 0 0;
}
.pk_tooltip .pk_tooltip_content {
	background-color: <?php echo $options['boxes_main_bg_color']; ?>;
}
.pk_tooltip .pk_tooltip_arrow {
	background: url('../images/back_grounds/<?php echo $options['skin_type']; ?>/tooltip_arrow.png') no-repeat 0 0;
}


/*
	JIGOSHOP - WOOCOMMERCE
-------------------------------------------------------------------------------------------------------------------------------------*/


.info,
p.demo_store {
	color: #43412a;
	background-color: #fff4c0;
}
.info a,
.info a:hover,
.info a.button,
.info a.button:hover {
	color: #43412a !important;
}
.jigoshop_error,
.woocommerce_error {
	color: #af3017;
	background-color: #f1bbb0;
}
.jigoshop_error a,
.jigoshop_error a:hover,
.jigoshop_error a.button,
.jigoshop_error a.button:hover,
.woocommerce_error a,
.woocommerce_error a:hover,
.woocommerce_error a.button,
.woocommerce_error a.button:hover {
	color: #af3017 !important;
}
.jigoshop_message,
.woocommerce_message {
	color: #44802a;
	background-color: #c9f0b8;
}
.jigoshop_message a,
.jigoshop_message a:hover,
.jigoshop_message a.button,
.jigoshop_message a.button:hover,
.woocommerce_message a,
.woocommerce_message a:hover,
.woocommerce_message a.button,
.woocommerce_message a.button:hover {
	color: #44802a !important;
}

p small,
small.note {
	color: <?php echo $options['main_secondary_text_color']; ?>;
}
.products li,
.product .images,
.product .summary,
table.shop_table,
.cart-collaterals,
form.login,
.checkout #customer_details,
#payment,
.order_details,
.addresses,
.jigoshop-myaccount dl,
.woocommerce-account dl,
.woocommerce-thankyou dl {
	background-color: <?php echo $options['boxes_main_bg_color']; ?>;
}
.products li {
	background-image: url('../images/ecommerce/<?php echo $options['skin_type']; ?>/grid_product_box_bg.png');
	background-position: 0 bottom;
	background-repeat: repeat-x;
}
.products li a h3 { color: <?php echo $options['main_link_color']; ?>; }
.products li a:hover h3 { color: <?php echo $options['main_link_rollover_color']; ?>; }
.products li a img {
	border-bottom: solid 1px <?php echo $options['boxes_main_borders_color']; ?>;
}
.products li .price,
.products li .price ins span,
.products li .price .amount {
	color: <?php echo $options['ec_products_price_color']; ?>;
}
.pk_ordering span.pk_select {
	background: <?php echo $options['boxes_main_bg_color']; ?> url('../images/ecommerce/<?php echo $options['skin_type']; ?>/products_ordering_arrow_down.png') no-repeat right center;
}
.products li .price del,
.products li .price .from,
.products li .price del .amount,
.pk_entry_product del,
.pk_entry_product .from {
	color: <?php echo $options['main_secondary_text_color']; ?>;
}
.products li mark.count {
	color: <?php echo $options['main_primary_text_color']; ?>;
	background-color: <?php echo $options['main_bg_color']; ?>;
}
.product .images .thumbnails {
	border-bottom: solid 1px <?php echo $options['boxes_main_borders_color']; ?>;
}
.product p.price {
	color: <?php echo $options['site_special_primary_text_color']; ?>;
	background-color: <?php echo $options['site_special_color']; ?>;
}
.product p.price del,
.product p.price .from {
	color: <?php echo $options['site_special_secondary_text_color']; ?>;
}
.onsale,
.products li a:hover .onsale {
	color: <?php echo $options['site_special_primary_text_color']; ?>;
	background-color: <?php echo $options['site_special_color']; ?>;
}
.stock {
	color: <?php echo $options['site_special_color']; ?>;
}
.out-of-stock,
.products li .nostock,
#pk_latest_products .pk_entry_product .nostock {
	color: <?php echo $options['ec_product_no_stock_text_color']; ?>;
}
form.cart table td,
form.variations_form fieldset.variations div {
	border-top: solid 1px <?php echo $options['boxes_main_borders_color']; ?>;
	border-bottom: solid 1px <?php echo $options['boxes_main_borders_color']; ?>;
}
form.variations_form .reset_variations {
	border-top: solid 1px <?php echo $options['boxes_main_borders_color']; ?>;
}
form.variations_form .single_variation {
	border-bottom: solid 1px <?php echo $options['boxes_main_borders_color']; ?>;
}
form.variations_form .single_variation .price {
	color: <?php echo $options['ec_products_price_color']; ?>;
}

/* Product Page Navigation */

.navigation .nav-previous a,
.navigation .nav-next a {
	color: <?php echo $options['main_primary_text_color']; ?>;
}
.navigation .nav-previous a:hover,
.navigation .nav-next a:hover {
	color: <?php echo $options['main_titles_color']; ?>;
}

/* Product Page Tabs */

#tabs, .woocommerce_tabs {
	background-color: <?php echo $options['menu_bg_color']; ?>;
	background-image: url('../images/back_grounds/menu_light.png');
	background-repeat: repeat-x;
}
#tabs ul.tabs, .woocommerce_tabs ul.tabs {
	border: solid 1px rgba(255, 255, 255, .12);
}
#tabs ul.tabs a, .woocommerce_tabs ul.tabs a {
	color: <?php echo $options['menu_text_color']; ?>;
}
#tabs ul.tabs li.active,
#tabs ul.tabs li:hover,
.woocommerce_tabs ul.tabs li.active,
.woocommerce_tabs ul.tabs li:hover {
	background-color: <?php echo $options['menu_text_rollover_bg_color']; ?>;
}
#tabs ul.tabs li:hover a,
#tabs ul.tabs li.active a,
.woocommerce_tabs ul.tabs li:hover a,
.woocommerce_tabs ul.tabs li.active a {
	color: <?php echo $options['menu_text_rollover_color']; ?>;
}
#tabs .panel, .woocommerce_tabs .panel {
	background-color: <?php echo $options['boxes_main_bg_color']; ?>;
}
#tabs #tab-customize form input[type=text],
.woocommerce_tabs #tab-customize form input[type=text] {
	color: <?php echo $options['boxes_main_input_text_color']; ?>;
	background-color: <?php echo $options['boxes_main_input_bg_color']; ?>;
}

/* Buttons */

a.button,
button.button,
input[type=submit].button,
.button-alt,
.button.alt,
input[type=submit].button-alt,
input[type=submit].button.alt,
#review_form #submit,
.jigoshop_login_widget input[type=submit],
.widget_login input[type=submit] {
	color: <?php echo $options['main_titles_color']; ?> !important;
	background: url('../images/button_icons/<?php echo $options['skin_type']; ?>/default_buttons.png') 0 -50px;
}
a.button:hover,
button.button:hover,
input[type=submit].button:hover,
.button-alt:hover,
input[type=submit].button-alt:hover,
#review_form #submit:hover,
.jigoshop_login_widget input[type=submit]:hover,
.widget_login input[type=submit]:hover {
	color: <?php echo $options['main_titles_color']; ?> !important;
}
.products li a.button,
.pk_entry_product a.button {
	background: url('../images/button_icons/<?php echo $options['skin_type']; ?>/default_buttons.png') 0 0;
}
td.actions .button-alt,
td.actions .button.alt,
#place_order {
	color: <?php echo $options['site_special_primary_text_color']; ?> !important;
	background: url('../images/ecommerce/<?php echo $options['skin_type']; ?>/button_check_out.png') 0 0;
}
td.actions .coupon input[type=submit].button {
	color: <?php echo $options['main_titles_color']; ?>;
	background: url('../images/button_icons/<?php echo $options['skin_type']; ?>/default_buttons.png') 0 0;
}
table.my_account_orders a.button {
	color: <?php echo $options['main_link_color']; ?> !important;
}
table.my_account_orders a.button:hover {
	color: <?php echo $options['main_link_rollover_color']; ?> !important;
}

/* Reviews / Rating */

#reviews h2 small,
#reviews h2 small a,
#reviews #comments ol.commentlist li .comment-text p.meta {
	color: <?php echo $options['main_secondary_text_color']; ?>;
}
#reviews #comments ol.commentlist li,
#review_form {
	border-top: 1px solid <?php echo $options['boxes_main_borders_color']; ?>;
}
#review_form #respond h3 small a {
	border-left: solid 1px <?php echo $options['boxes_main_borders_color']; ?>;
}
#review_form #respond input[type=text],
#review_form #respond textarea {
	color: <?php echo $options['boxes_main_input_text_color']; ?>;
	background-color: <?php echo $options['boxes_main_input_bg_color']; ?>;
}
.star-rating {
	background: url('../images/ecommerce/star.png') repeat-x left 0;
}
.star-rating span {
	background: url('../images/ecommerce/star.png') repeat-x left -32px;
}
p.stars span {
	background: url('../images/ecommerce/star.png') repeat-x left 0;
}
p.stars span a:hover,
p.stars span a:focus {
	background: url('../images/ecommerce/star.png') repeat-x left -16px;
}
p.stars span a.active {
	background: url('../images/ecommerce/star.png') repeat-x left -32px;
}

/* Tables */

table.shop_table tr {
	border-top: solid 1px <?php echo $options['boxes_main_borders_color']; ?>;
	border-bottom: solid 1px <?php echo $options['boxes_main_borders_color']; ?>;
}
.jigoshop-myaccount table.shop_table tfoot strong,
#order_review table.shop_table tfoot strong,
.woocommerce-account table.shop_table tfoot .amount,
.woocommerce-thankyou table.shop_table tfoot .amount {
	color: <?php echo $options['main_titles_color']; ?>;
}

/* Cart */

a.remove {
	background: <?php echo $options['main_secondary_text_color']; ?> url('../images/ecommerce/<?php echo $options['skin_type']; ?>/button_remove.png') no-repeat 0 0;
}
a.remove:hover {
	background-color: <?php echo $options['main_primary_text_color']; ?>;
}
td.actions .coupon input[type=text].input-text,
.quantity,
.quantity input.qty {
	color: <?php echo $options['boxes_main_input_text_color']; ?>;
	background-color: <?php echo $options['boxes_main_input_bg_color']; ?>;
}
.quantity input.plus,
.quantity input.minus {
	background: url('../images/ecommerce/<?php echo $options['skin_type']; ?>/buttons_quantity.png') no-repeat 0 0;
}
.quantity input.minus{ background-position: 0 -19px; }
.ie8 .quantity {
	border: solid 1px <?php echo $options['boxes_main_borders_color']; ?>;
}
.cart-collaterals .cart_totals table th,
.cart-collaterals .cart_totals table td {
	border-top: solid 1px <?php echo $options['boxes_main_borders_color']; ?>;
	border-bottom: solid 1px <?php echo $options['boxes_main_borders_color']; ?>;
}
.cart-collaterals .cart_totals tr th {
	border-right: solid 1px <?php echo $options['boxes_main_borders_color']; ?>;
}
.cart-collaterals .cart_totals tr td.cart-row-total,
.cart-collaterals .cart_totals tr.total td {
	color: <?php echo $options['main_titles_color']; ?>;
}
.cart-collaterals .cart_totals table small {
	color: <?php echo $options['main_secondary_text_color']; ?>;
}
.cart-collaterals .cart_totals .discount td {
	color: <?php echo $options['main_link_color']; ?>;
}
.cart-collaterals .shipping_calculator h2 span {
	background: url('../images/ecommerce/<?php echo $options['skin_type']; ?>/calculate_shipping_icon.png') no-repeat 0 0;
}
.cart-collaterals .shipping_calculator .col2-set input[type=text],
.cart-collaterals .shipping_calculator .form-row input[type=text], {
	color: <?php echo $options['boxes_main_input_text_color']; ?>;
	background-color: <?php echo $options['boxes_main_input_bg_color']; ?>;
}

/* Login */

form.login input[type=text],
form.login input[type=password] {
	color: <?php echo $options['boxes_main_input_text_color']; ?>;
	background-color: <?php echo $options['boxes_main_input_bg_color']; ?>;
}

/* Checkout */

.checkout .form-row input[type=text],
.checkout .col-2 .notes textarea {
	color: <?php echo $options['boxes_main_input_text_color']; ?>;
	background-color: <?php echo $options['boxes_main_input_bg_color']; ?>;
}
#payment ul.payment_methods {
	border-bottom: solid 1px <?php echo $options['boxes_main_borders_color']; ?>;
}
#payment .payment_box {
	border: 1px solid <?php echo $options['boxes_main_borders_color']; ?>;
	background: <?php echo $options['boxes_main_input_bg_color']; ?>;
}

/* Account - Order Page (Thankyou Page) */

.addresses .title,
.order_details li {
	border-bottom: solid 1px <?php echo $options['boxes_main_borders_color']; ?>;
}
.jigoshop-myaccount dl dt,
.order_details li strong,
.woocommerce-account dl dt,
.woocommerce-thankyou dl dt {
	color: <?php echo $options['main_titles_color']; ?>;
}
.country_select a span{
	color: <?php echo $options['main_primary_text_color']; ?>;
}


/*
	BBPRESS
-------------------------------------------------------------------------------------------------------------------------------------*/


#bbpress-forums ul.even, #bbpress-forums div.even { background: rgba(0, 0, 0, 0.01); }
#bbpress-forums ul.odd, #bbpress-forums div.odd {  }
#bbpress-forums ul.bbp-forums,
#bbpress-forums ul.bbp-topics,
#bbpress-forums ul.bbp-replies,
#bbpress-forums ul.bbp-lead-topic,
#bbpress-forums .bbp-topic-tags p,
.bbp-single-reply,
fieldset.bbp-form code,
#bbpress-forums #entry-author-info,
#bbp-your-profile fieldset.bbp-form,
#bbpress-forums .bbp-template-notice {
	background-color: <?php echo $options['boxes_main_bg_color']; ?>;
}

#bbpress-forums .bbp-forums .bbp-header,
#bbpress-forums .bbp-topics .bbp-header,
#bbpress-forums .bbp-replies .bbp-header {
    color: <?php echo $options['menu_text_color']; ?>;
    background: <?php echo $options['menu_bg_color']; ?> url('../images/back_grounds/menu_light.png') repeat-x;
}
#bbpress-forums .bbp-forums .bbp-header ul.forum-titles,
#bbpress-forums .bbp-topics .bbp-header ul.forum-titles,
#bbpress-forums .bbp-replies .bbp-header .bbp-reply-author,
#bbpress-forums .bbp-replies .bbp-header .bbp-reply-content {
	border: solid 1px rgba(255, 255, 255, 0.12);
}

#bbpress-forums .bbp-forums .bbp-body ul.forum,
#bbpress-forums .bbp-forums .bbp-body ul.topic,
#bbpress-forums .bbp-forums .bbp-footer,
#bbpress-forums .bbp-topics .bbp-body ul.forum,
#bbpress-forums .bbp-topics .bbp-body ul.topic,
#bbpress-forums .bbp-topics .bbp-footer,
#bbpress-forums .bbp-replies .bbp-body ul.forum,
#bbpress-forums .bbp-replies .bbp-body ul.topic,
#bbpress-forums .bbp-replies .bbp-footer {
	border-top: 1px solid <?php echo $options['boxes_main_borders_color']; ?>;
}
#bbpress-forums .bbp-forums .bbp-forums-list li,
#bbpress-forums .bbp-replies .bbp-body .bbp-reply-content .bbp-reply-revision-log li,
.bbp-single-reply .bbp-reply-content .bbp-reply-revision-log li {
	background: url('../images/back_grounds/<?php echo $options['skin_type']; ?>/widgets_list_icon.png') no-repeat 0 8px;
}
#bbpress-forums .bbp-replies .bbp-body .bbp-reply-content .bbp-reply-revision-log li,
.bbp-single-reply .bbp-reply-content .bbp-reply-revision-log li {
	background-position: 0 7px;
}
#bbpress-forums .bbp-topics .bbp-topic-action #subscription-toggle span.is-subscribed a,
#bbpress-forums .bbp-topics .bbp-topic-action #favorite-toggle span.is-favorite a {
	color: #af3017;
	background-color: #f1bbb0;
}
#bbpress-forums .bbp-replies .bbp-header .bbp-reply-content span#subscription-toggle a,
#bbpress-forums .bbp-replies .bbp-header .bbp-reply-content span#favorite-toggle a {
	color: <?php echo $options['menu_text_color']; ?>;
}

#bbpress-forums .bbp-replies .bbp-body .bbp-reply-header,
.bbp-single-reply .bbp-reply-header {
	border-top: 1px solid <?php echo $options['boxes_main_borders_color']; ?>;
    border-bottom: 1px solid <?php echo $options['boxes_main_borders_color']; ?>;
	background-color: rgba(0, 0, 0, 0.05);
}
#bbpress-forums .bbp-replies .bbp-body .bbp-reply-content .bbp-reply-revision-log,
.bbp-single-reply .bbp-reply-content .bbp-reply-revision-log {
	color: <?php echo $options['main_secondary_text_color']; ?>;
	border-top: 1px solid <?php echo $options['boxes_main_borders_color']; ?>;
}

/* Status */

#bbpress-forums .bbp-topics-front ul.super-sticky,
#bbpress-forums .bbp-topics ul.super-sticky {
	background-color: <?php echo $options['bbp_super_sticky_bg_color']; ?> !important;
}
#bbpress-forums .bbp-topics ul.sticky,
#bbpress-forums .bbp-forum-content ul.sticky {
	background-color: <?php echo $options['bbp_sticky_bg_color']; ?> !important;
}
#bbpress-forums .bbp-forum-status-closed .bbp-forum-title {
	background: url('../images/forum/<?php echo $options['skin_type']; ?>/close_icon.png') no-repeat 0 1px;
}
#bbpress-forums .status-closed .bbp-topic-permalink {
	background: url('../images/forum/<?php echo $options['skin_type']; ?>/close_icon.png') no-repeat 0 1px;
}
#bbpress-forums .status-spam .bbp-topic-permalink {
	background: url('../images/forum/<?php echo $options['skin_type']; ?>/spam_icon.png') no-repeat 0 1px;
}
#bbpress-forums .status-trash .bbp-topic-permalink {
	background: url('../images/forum/<?php echo $options['skin_type']; ?>/trash_icon.png') no-repeat 0 1px;
}

/* Forms */

fieldset.bbp-form legend {
	color: <?php echo $options['main_titles_color']; ?>;
}
fieldset.bbp-form .bbp-admin-links {
	border-top: 1px solid <?php echo $options['main_borders_color']; ?>;
    border-bottom: 1px solid <?php echo $options['main_borders_color']; ?>;
}
.bbp-login-form .bbp-template-notice {
    color: <?php echo $options['boxes_main_input_text_color']; ?>;
    background-color: <?php echo $options['boxes_main_input_bg_color']; ?> !important;
}

/* Pagination */

.bbp-pagination {
	color: <?php echo $options['main_secondary_text_color']; ?>;
}
.bbp-topic-pagination a {
	color: <?php echo $options['main_primary_text_color']; ?>;
	background-color: <?php echo $options['main_bg_color']; ?>;
}

/* User Profile Page */

#bbp-author-subscriptions,
#bbp-author-favorites,
#bbp-author-topics-started {
	border-top: 1px solid <?php echo $options['main_borders_color']; ?>;
}

/* Edit User Page */

#bbp-your-profile fieldset.bbp-form input,
#bbp-your-profile fieldset.bbp-form textarea,
#bbp-your-profile fieldset.bbp-form #pass-strength-result {
	color: <?php echo $options['boxes_main_input_text_color']; ?>;
    background-color: <?php echo $options['boxes_main_input_bg_color']; ?>;
}
#bbp-your-profile fieldset.bbp-form span.description {
	color: <?php echo $options['main_secondary_text_color']; ?>;
}
#bbp-your-profile fieldset.bbp-form #pass-strength-result {
	color: <?php echo $options['main_titles_color']; ?>;
}
        
/* Notices */

#bbpress-forums .bbp-template-notice.info {
	color: #43412a;
	background-color: #fff4c0;
}
#bbpress-forums .bbp-template-notice.info a,
#bbpress-forums .bbp-template-notice.info a:hover {
	color: #43412a;
}
#bbpress-forums .bbp-template-notice.important {
	color: #59330d;
	background-color: #f8ac61;
}
#bbpress-forums .bbp-template-notice.important a,
#bbpress-forums .bbp-template-notice.important a:hover {
	color: #59330d;
}
#bbpress-forums .bbp-template-notice.warning {
	color: #705213;
	background-color: #ffd987;
}
#bbpress-forums .bbp-template-notice.warning a,
#bbpress-forums .bbp-template-notice.warning a {
	color: #705213;
}
#bbpress-forums .bbp-template-notice.error {
	color: #af3017;
	background-color: #f1bbb0;
}
#bbpress-forums .bbp-template-notice.error a,
#bbpress-forums .bbp-template-notice.error a {
	color: #af3017;
}


/*
	WIDGETS
-------------------------------------------------------------------------------------------------------------------------------------*/


/* Our Widgets */


.pk_widget li {
	background: url('../images/back_grounds/<?php echo $options['skin_type']; ?>/widgets_list_icon.png') no-repeat 0 8px;
}
#pk_widgets_area .pk_widget li {
	background: url('../images/back_grounds/<?php echo $options['skin_type']; ?>/wa_widgets_list_icon.png') no-repeat 0 8px;
}
.widget_dribbble li,
.widget_featured_posts li,
.widget_featured_works li,
.widget_popular_posts li,
.widget_popular_works li,
.widget_recent_posts li,
.widget_recent_works li,
.widget_related_posts li,
.widget_related_works li,
.widget_advertising_125_125 a,
.pk_testimonials,
.widget_flickr div,
.pk_widget_google_maps,
.widget_search,
#lang_sel li,
#lang_sel_list li,
#lang_sel ul ul li,
#lang_sel_list,
#lang_sel_footer,
.qtrans_language_chooser {
	background-color: <?php echo $options['boxes_main_bg_color']; ?>;
}
#pk_widgets_area .widget_dribbble li,
#pk_widgets_area .widget_featured_posts li,
#pk_widgets_area .widget_featured_works li,
#pk_widgets_area .widget_popular_posts li,
#pk_widgets_area .widget_popular_works li,
#pk_widgets_area .widget_recent_posts li,
#pk_widgets_area .widget_recent_works li,
#pk_widgets_area .widget_related_posts li,
#pk_widgets_area .widget_related_works li,
#pk_widgets_area .widget_advertising_125_125 a,
#pk_widgets_area .pk_testimonials,
#pk_widgets_area .widget_flickr div,
#pk_widgets_area .pk_widget_google_maps,
#pk_widgets_area .widget_search,
#pk_widgets_area #lang_sel li,
#pk_widgets_area #lang_sel_list li,
#pk_widgets_area #lang_sel ul ul li,
#pk_widgets_area #lang_sel_list,
#pk_widgets_area #lang_sel_footer,
#pk_widgets_area .qtrans_language_chooser {
	background-color: <?php echo $options['boxes_footer_wa_bg_color']; ?>;
}
.pk_testimonials blockquote {
	color: <?php echo $options['main_titles_color']; ?>;
}
.pk_testimonials cite {
	color: <?php echo $options['main_primary_text_color']; ?>;
}
.pk_testimonials .pk_arrow {
	border-right: solid 7px <?php echo $options['boxes_main_bg_color']; ?>;
}
#pk_widgets_area .pk_testimonials blockquote {
	color: <?php echo $options['footer_wa_titles_color']; ?>;
}
#pk_widgets_area .pk_testimonials cite {
	color: <?php echo $options['footer_wa_primary_text_color']; ?>;
}
#pk_widgets_area .pk_testimonials .pk_arrow {
	border-right: solid 7px <?php echo $options['boxes_footer_wa_bg_color']; ?>;
}
.widget_dribbble small {
	color: <?php echo $options['main_secondary_text_color']; ?>;
}
.widget_twitter li {
	background: <?php echo $options['boxes_main_bg_color']; ?> url('../images/back_grounds/<?php echo $options['skin_type']; ?>/twitter_icon.png') no-repeat 13px 13px;
}
#pk_widgets_area .widget_twitter li {
	background: <?php echo $options['boxes_footer_wa_bg_color']; ?> url('../images/back_grounds/<?php echo $options['skin_type']; ?>/wa_twitter_icon.png') no-repeat 13px 13px;
}
#searchform {
	background-color: <?php echo $options['boxes_main_input_bg_color']; ?>;
}
#searchform input[type=text] {
	color: <?php echo $options['boxes_main_input_text_color']; ?>;
}
#searchform input[type=submit] {
	background: url('../images/button_icons/<?php echo $options['skin_type']; ?>/default_buttons.png') 0 -50px;
}
.ie8 #searchform {
	border: solid 1px <?php echo $options['main_borders_color']; ?>;
}
#pk_widgets_area #searchform {
	background-color: <?php echo $options['boxes_footer_wa_input_bg_color']; ?>;
}
#pk_widgets_area #searchform input[type=text] {
	color: <?php echo $options['boxes_footer_wa_input_text_color']; ?>;
}
.ie8 #pk_widgets_area #searchform {
	border: solid 1px <?php echo $options['boxes_footer_wa_borders_color']; ?>;
}
#calendar_wrap {
	border: solid 1px <?php echo $options['boxes_main_borders_color']; ?>;
}
#today, #pk_page #today, #today a,
#pk_page #today a, #today a:hover,
#pk_page #today a:hover {
	color: <?php echo $options['site_special_primary_text_color']; ?>;
}
#today, #pk_page #today {
	background-color: <?php echo $options['site_special_color']; ?>;
}
#pk_widgets_area #calendar_wrap {
	border: solid 1px <?php echo $options['boxes_footer_wa_borders_color']; ?>;
}


/* Jigoshop - WooCommerce Widgets */


span.empty,
.pk_widget p.total,
.pk_widget p.buttons,
ul.product_list_widget li,
ul.cart_list,
.widget_layered_nav ul,
.jigoshop_login_widget,
.widget_login,
.jigoshop_price_filter .price_slider_wrapper,
.widget_price_filter .price_slider_wrapper,
.jigoshop_product_search,
.widget_product_search {
	background-color: <?php echo $options['boxes_main_bg_color']; ?>;
}
ul.cart_list .js_widget_product_price del,
ul.product_list_widget .js_widget_product_price del,
ul.product_list_widget del,
ul.product_list_widget .from {
	color: <?php echo $options['main_secondary_text_color']; ?>;
}
ul.cart_list li,
.pk_widget p.total,
.widget_layered_nav ul li {
	border-bottom: solid 1px <?php echo $options['boxes_main_borders_color']; ?>;
}
#pk_widgets_area .pk_widget p.total,
#pk_widgets_area .pk_widget p.buttons,
#pk_widgets_area span.empty,
#pk_widgets_area ul.cart_list,
#pk_widgets_area ul.product_list_widget li,
#pk_widgets_area .widget_layered_nav ul,
#pk_widgets_area .jigoshop_login_widget,
#pk_widgets_area .widget_login,
#pk_widgets_area .jigoshop_price_filter .price_slider_wrapper,
#pk_widgets_area .widget_price_filter .price_slider_wrapper,
#pk_widgets_area .jigoshop_product_search,
#pk_widgets_area .widget_product_search {
	background-color: <?php echo $options['boxes_footer_wa_bg_color']; ?>;
}
#pk_widgets_area ul.cart_list .js_widget_product_price del,
#pk_widgets_area ul.product_list_widget .js_widget_product_price del,
#pk_widgets_area ul.product_list_widget del,
#pk_widgets_area ul.product_list_widget .from {
	color: <?php echo $options['footer_wa_secondary_text_color']; ?>;
}
#pk_widgets_area ul.cart_list li,
#pk_widgets_area .pk_widget p.total,
#pk_widgets_area .widget_layered_nav ul li {
	border-bottom: solid 1px <?php echo $options['boxes_footer_wa_borders_color']; ?>;
}

.jigoshop_login_widget input[type=text],
.jigoshop_login_widget input[type=password],
.widget_login input[type=text],
.widget_login input[type=password] {
	color: <?php echo $options['boxes_main_input_text_color']; ?>;
	background-color: <?php echo $options['boxes_main_input_bg_color']; ?>;
}
#pk_widgets_area .jigoshop_login_widget input[type=text],
#pk_widgets_area .jigoshop_login_widget input[type=password],
#pk_widgets_area .widget_login input[type=text],
#pk_widgets_area .widget_login input[type=password] {
	color: <?php echo $options['boxes_footer_wa_input_text_color']; ?>;
	background-color: <?php echo $options['boxes_footer_wa_input_bg_color']; ?>;
}

/* Layered Nav */

.widget_layered_nav .layerd_nav_clear,
.widget_layered_nav .layerd_nav_clear:hover {
	color: <?php echo $options['site_special_primary_text_color']; ?> !important;
	background-color: <?php echo $options['site_special_color']; ?> !important;
}
.widget_layered_nav ul li.chosen {
	background: url('../images/ecommerce/<?php echo $options['skin_type']; ?>/layered_nav_chosen_icon.png') no-repeat 15px center;
}
.widget_layered_nav ul small.count {
	color: <?php echo $options['main_secondary_text_color']; ?>;
	background-color: <?php echo $options['main_bg_color']; ?>;
}
#pk_widgets_area .widget_layered_nav ul li.chosen {
	background: url('../images/ecommerce/<?php echo $options['skin_type']; ?>/wa_layered_nav_chosen_icon.png') no-repeat 15px center;
}
#pk_widgets_area .widget_layered_nav ul small.count {
	color: <?php echo $options['footer_wa_secondary_text_color']; ?>;
	background-color: <?php echo $options['footer_wa_bg_color']; ?>;
}

/* Price Filter */

.ui-state-default,
.ui-widget-content .ui-state-default,
.ui-widget-header .ui-state-default {
	background: url('../images/ecommerce/<?php echo $options['skin_type']; ?>/price_filter_dragger.png') no-repeat 0 0;
}
.ui-state-hover, 
.ui-widget-content .ui-state-hover,
.ui-widget-header .ui-state-hover,
.ui-state-focus,
.ui-widget-content .ui-state-focus,
.ui-widget-header .ui-state-focus {
	background: url('../images/ecommerce/<?php echo $options['skin_type']; ?>/price_filter_dragger.png') no-repeat 0 -19px;
}
.ui-state-active,
.ui-widget-content .ui-state-active,
.ui-widget-header .ui-state-active {
	background: url('../images/ecommerce/<?php echo $options['skin_type']; ?>/price_filter_dragger.png') no-repeat 0 -19px;
}
.price_slider_wrapper .ui-widget-content {
	background: <?php echo $options['controls_bars_light_bg_color']; ?>;
}
.price_slider_wrapper .ui-widget-header {
	background: url('../images/ecommerce/<?php echo $options['skin_type']; ?>/price_filter_bar.png') repeat-x 0 0;
}
.widget_price_filter input[type="text"] {
	color: <?php echo $options['boxes_main_input_text_color']; ?>;
	background-color: <?php echo $options['boxes_main_input_bg_color']; ?>;
}

#pk_widgets_area .ui-state-default,
#pk_widgets_area .ui-widget-content .ui-state-default,
#pk_widgets_area .ui-widget-header .ui-state-default {
	background: url('../images/ecommerce/<?php echo $options['skin_type']; ?>/price_filter_dragger.png') no-repeat 0 -38px;
}
#pk_widgets_area .ui-state-hover, 
#pk_widgets_area .ui-widget-content .ui-state-hover,
#pk_widgets_area .ui-widget-header .ui-state-hover,
#pk_widgets_area .ui-state-focus,
#pk_widgets_area .ui-widget-content .ui-state-focus,
#pk_widgets_area .ui-widget-header .ui-state-focus {
	background: url('../images/ecommerce/<?php echo $options['skin_type']; ?>/price_filter_dragger.png') no-repeat 0 -57px;
}
#pk_widgets_area .ui-state-active,
#pk_widgets_area .ui-widget-content .ui-state-active,
#pk_widgets_area .ui-widget-header .ui-state-active {
	background: url('../images/ecommerce/<?php echo $options['skin_type']; ?>/price_filter_dragger.png') no-repeat 0 -57px; 
}
#pk_widgets_area .price_slider_wrapper .ui-widget-content {
	background: <?php echo $options['controls_bars_dark_bg_color']; ?>;
}
#pk_widgets_area .widget_price_filter input[type="text"] {
	color: <?php echo $options['boxes_footer_wa_input_text_color']; ?>;
	background-color: <?php echo $options['boxes_footer_wa_input_bg_color']; ?>;
}

/* BBpress */

.bbp_widget_login .bbp-login-form fieldset,
.bbp_widget_login .bbp-logged-in {
	background-color: <?php echo $options['boxes_main_bg_color']; ?>;
}
.bbp_widget_login .bbp-login-form .bbp-username input,
.bbp_widget_login .bbp-login-form .bbp-email input,
.bbp_widget_login .bbp-login-form .bbp-password input {
	color: <?php echo $options['boxes_main_input_text_color']; ?>;
	background-color: <?php echo $options['boxes_main_input_bg_color']; ?>;
}
#pk_widgets_area .bbp_widget_login .bbp-login-form fieldset,
#pk_widgets_area .bbp_widget_login .bbp-logged-in {
	background-color: <?php echo $options['boxes_footer_wa_bg_color']; ?>;
}
#pk_widgets_area .bbp_widget_login .bbp-login-form .bbp-username input,
#pk_widgets_area .bbp_widget_login .bbp-login-form .bbp-email input,
#pk_widgets_area .bbp_widget_login .bbp-login-form .bbp-password input {
	color: <?php echo $options['boxes_footer_wa_input_text_color']; ?>;
	background-color: <?php echo $options['boxes_footer_wa_input_bg_color']; ?>;
}

/* WPML / qTranslate */

#lang_sel ul ul a,
#lang_sel ul ul a:visited,
#lang_sel_list a {
	border-top:1px solid <?php echo $options['boxes_main_borders_color']; ?>;
}
#lang_sel a.lang_sel_sel span,
#lang_sel_list a.lang_sel_sel span {
	color: <?php echo $options['main_primary_text_color']; ?>;
}
#lang_sel a,
#lang_sel a:visited {
	background: url('../images/back_grounds/<?php echo $options['skin_type']; ?>/languages_arrow_down.png') no-repeat right center;
}
#lang_sel_footer ul li,
.qtrans_language_chooser li {
	border-right: solid 1px <?php echo $options['boxes_main_borders_color']; ?>;
}
#pk_widgets_area #lang_sel ul ul a,
#pk_widgets_area #lang_sel ul ul a:visited,
#pk_widgets_area #lang_sel_list a {
	border-top:1px solid <?php echo $options['boxes_footer_wa_borders_color']; ?>;
}
#pk_widgets_area #lang_sel a,
#pk_widgets_area #lang_sel a:visited {
	background: url('../images/back_grounds/<?php echo $options['skin_type']; ?>/wa_languages_arrow_down.png') no-repeat right center;
}
#pk_widgets_area #lang_sel_footer ul li,
#pk_widgets_area .qtrans_language_chooser li {
	border-right: solid 1px <?php echo $options['boxes_footer_wa_borders_color']; ?>; 
}


/*
	SHORTCODES (DAFAULT)
-------------------------------------------------------------------------------------------------------------------------------------*/


/* Headings */

.pk_heading_underline {
	border-bottom: solid 1px <?php echo $options['main_borders_color']; ?>;
}
.pk_heading_background {
	color: <?php echo $options['site_special_primary_text_color']; ?>;
	background-color: <?php echo $options['site_special_color']; ?>;
}
#pk_widgets_area .pk_heading_underline {
	border-bottom: solid 1px <?php echo $options['footer_wa_borders_color']; ?>;
}

/* Lists */

.pk_arrow_list li {
	background: url('../images/shortcodes/lists/list_<?php echo $options['skin_type']; ?>_icons.png') no-repeat -86px -2px;
}
.pk_check_list li {
	background: url('../images/shortcodes/lists/list_<?php echo $options['skin_type']; ?>_icons.png') no-repeat -56px -27px;
}	
#pk_widgets_area .pk_arrow_list li {
	background: url('../images/shortcodes/lists/list_<?php echo $options['skin_type']; ?>_icons.png') no-repeat -36px -52px;
}
#pk_widgets_area .pk_check_list li {
	background: url('../images/shortcodes/lists/list_<?php echo $options['skin_type']; ?>_icons.png') no-repeat -6px -77px;
}

/* Hightlights */
	
.pk_highlight {
	color: <?php echo $options['site_special_primary_text_color']; ?>;
	background-color: <?php echo $options['site_special_color']; ?>;
}

/* Drop Caps */

.pk_drop_cap_1 { 
	color: <?php echo $options['main_titles_color']; ?>;
}

/* Styled Boxes */

.pk_box_content_wrapper {
	background-color: <?php echo $options['boxes_main_bg_color']; ?>;
}
#pk_widgets_area .pk_box_content_wrapper {
	background-color: <?php echo $options['boxes_footer_wa_bg_color']; ?>;
}

/* Pricing Tables */

.pk_pricing_table header {
	background: <?php echo $options['pt_header_bg_color']; ?> url('../images/back_grounds/menu_light.png') repeat-x;
}
.pk_pricing_table header .pk_type {
	color: <?php echo $options['pt_header_title_text_color']; ?>;
	border-color: <?php echo $options['pt_header_title_borders_color']; ?>;
}
.pk_pricing_table header .pk_type span{ border-top-color: rgba(255, 255, 255, .12); }
.pk_pricing_table .pk_first header .pk_type span{ border-left-color: rgba(255, 255, 255, .12); }
.pk_pricing_table .pk_last header .pk_type span{ border-right-color: rgba(255, 255, 255, .12); }
.pk_pricing_table header .pk_price {
	color: <?php echo $options['pt_header_price_primary_text_color']; ?>;
	border-color: <?php echo $options['pt_header_price_borders_color']; ?>;
	background-color: <?php echo $options['pt_header_price_bg_color']; ?>;
}
.pk_pricing_table header .pk_price sup {
	color: <?php echo $options['pt_header_price_primary_text_color']; ?>;
}
.pk_pricing_table header .pk_price span {
	color: <?php echo $options['pt_header_price_secondary_text_color']; ?>;
}
.pk_pricing_table ul {
	color: <?php echo $options['pt_primary_text_color']; ?>;
	border-color: <?php echo $options['pt_borders_color']; ?>;
	background-color: <?php echo $options['pt_bg_color']; ?>;
}
.pk_pricing_table ul li.pk_alternate_color {
	background-color: <?php echo $options['pt_alternate_color']; ?>;
}
.pk_pricing_table ul li del {
	color: <?php echo $options['pt_secondary_text_color']; ?>;
}
.pk_pricing_table footer {
	border-color: <?php echo $options['pt_footer_borders_color']; ?>;
	background-color: <?php echo $options['pt_footer_bg_color']; ?>;
}

/* ie8 Border Fix */

.ie8 .pk_pricing_table .pk_column {
	border-bottom: solid 1px <?php echo $options['main_borders_color']; ?>;
}
.ie8 .pk_pricing_table .pk_first ul,
.ie8 .pk_pricing_table .pk_first footer {
	border-left: solid 1px <?php echo $options['main_borders_color']; ?>;
}
.ie8 .pk_pricing_table .pk_last ul,
.ie8 .pk_pricing_table .pk_last footer {
	border-right: solid 1px <?php echo $options['main_borders_color']; ?>;
}
.ie8 .pk_pricing_table .pk_highlight_column ul,
.ie8 .pk_pricing_table .pk_highlight_column footer {
	border-left: solid 1px <?php echo $options['main_borders_color']; ?>;
	border-right: solid 1px <?php echo $options['main_borders_color']; ?>;
}
.ie8 #pk_widgets_area .pk_pricing_table .pk_column {
	border-bottom: solid 1px <?php echo $options['footer_wa_borders_color']; ?>;
}
.ie8 #pk_widgets_area .pk_pricing_table .pk_first ul,
.ie8 #pk_widgets_area .pk_pricing_table .pk_first footer {
	border-left: solid 1px <?php echo $options['footer_wa_borders_color']; ?>;
}
.ie8 #pk_widgets_area .pk_pricing_table .pk_last ul,
.ie8 #pk_widgets_area .pk_pricing_table .pk_last footer {
	border-right: solid 1px <?php echo $options['footer_wa_borders_color']; ?>;
}
.ie8 #pk_widgets_area .pk_pricing_table .pk_highlight_column ul,
.ie8 #pk_widgets_area .pk_pricing_table .pk_highlight_column footer {
	border-left: solid 1px <?php echo $options['footer_wa_borders_color']; ?>;
	border-right: solid 1px <?php echo $options['footer_wa_borders_color']; ?>;
}

/* Tabs */

.pk_tabs .pk_tabs_navigation li {
	border-top: solid 1px <?php echo $options['main_borders_color']; ?>;
	border-bottom: none;
	background-color: <?php echo $options['main_titles_bg_color']; ?>;
}
.pk_tabs .pk_tabs_navigation a {
	color: <?php echo $options['main_titles_color']; ?>;
}
.pk_tabs .pk_tabs_navigation li.pk_active_tab,
.pk_tabs .pk_tabs_content {
	background-color: <?php echo $options['boxes_main_bg_color']; ?>;
}
.pk_tabs .pk_tabs_navigation li.pk_active_tab a {
	color: <?php echo $options['main_primary_text_color']; ?>;
}
.pk_entry_single .pk_tabs .pk_tabs_content {
	border-top: solid 1px <?php echo $options['boxes_main_borders_color']; ?>;
}
#pk_widgets_area .pk_tabs .pk_tabs_navigation li {
	border-top: solid 1px <?php echo $options['footer_wa_borders_color']; ?>;
	border-bottom: none;
	background-color: <?php echo $options['footer_wa_titles_bg_color']; ?>;
}
#pk_widgets_area .pk_tabs .pk_tabs_navigation a {
	color: <?php echo $options['footer_wa_titles_color']; ?>;
}
#pk_widgets_area .pk_tabs .pk_tabs_navigation li.pk_active_tab,
#pk_widgets_area .pk_tabs .pk_tabs_content {
	background-color: <?php echo $options['boxes_footer_wa_bg_color']; ?>;
}
#pk_widgets_area .pk_tabs .pk_tabs_navigation li.pk_active_tab a {
	color: <?php echo $options['footer_wa_primary_text_color']; ?>;
}

/* Toggles */

.pk_toggles .pk_toggle {
	background-color: <?php echo $options['boxes_main_bg_color']; ?>;
}
.pk_toggles .pk_toggle_button {
	color: <?php echo $options['main_titles_color']; ?>;
	background: url('../images/shortcodes/toggles/open_<?php echo $options['skin_type']; ?>_icon.png') no-repeat 15px center;
}
.pk_toggles .pk_toggle_button.pk_selected {
	background-image: url('../images/shortcodes/toggles/close_<?php echo $options['skin_type']; ?>_icon.png');
}
.pk_toggles .pk_toggle_content_wrapper,
.pk_entry_single .pk_toggles .pk_toggle {
	border-top: solid 1px <?php echo $options['boxes_main_borders_color']; ?>;
}
#pk_widgets_area .pk_toggles .pk_toggle {
	background-color: <?php echo $options['boxes_footer_wa_bg_color']; ?>;
}
#pk_widgets_area .pk_toggles .pk_toggle_button {
	color: <?php echo $options['footer_wa_titles_color']; ?>;
	background: url('../images/shortcodes/toggles/wa_open_<?php echo $options['skin_type']; ?>_icon.png') no-repeat 15px center;
}
#pk_widgets_area .pk_toggles .pk_toggle_button.pk_selected {
	background-image: url('../images/shortcodes/toggles/wa_close_<?php echo $options['skin_type']; ?>_icon.png');
}
#pk_widgets_area .pk_toggles .pk_toggle_content_wrapper {
	border-top: solid 1px <?php echo $options['boxes_footer_wa_borders_color']; ?>;
}

/* Dividers */

body .pk_divider hr,
body .pk_divider.pk_top hr {
	border-top: solid 1px <?php echo $options['main_borders_color']; ?>;
}
body .pk_divider.pk_top a {
	color: <?php echo $options['main_secondary_text_color']; ?>;
}
body #pk_widgets_area .pk_divider hr,
body #pk_widgets_area .pk_divider.pk_top hr {
	border-top: solid 1px <?php echo $options['footer_wa_borders_color']; ?>;
}
body #pk_widgets_area .pk_divider.pk_top a {
	color: <?php echo $options['footer_wa_secondary_text_color']; ?>;
}


/*
	QUERIES
-------------------------------------------------------------------------------------------------------------------------------------*/


@media only screen and (max-width: 959px) {
	.pk_navigation_wrapper nav {
		background-color: <?php echo $options['mobile_menu_bg_color']; ?>;
	}
	.pk_navigation_wrapper nav ul li {
		border-top: solid 1px <?php echo $options['mobile_menu_borders_color']; ?>;
	}
	.pk_navigation_wrapper nav a {
		border-bottom: solid 1px <?php echo $options['mobile_menu_borders_color']; ?>;
	}
	.pk_navigation_wrapper nav ul:first-child > li:first-child {
		border-top: solid 1px <?php echo $options['mobile_menu_border_top_color']; ?>;
	}
	.pk_navigation_wrapper nav ul ul li {
		border-top: solid 1px <?php echo $options['mobile_menu_borders_color']; ?>;
		border-left: solid 1px <?php echo $options['mobile_menu_borders_color']; ?>;
	}
	.pk_navigation_wrapper nav a,
	.pk_navigation_wrapper nav li:hover > a,
	.pk_navigation_wrapper nav ul .current-menu-ancestor > a,
	.pk_navigation_wrapper nav ul ul a,
	.pk_navigation_wrapper nav ul ul a:hover,
	.pk_navigation_wrapper nav ul ul .current-menu-item > a,
	.pk_navigation_wrapper nav ul ul .current-menu-ancestor > a {
		color: <?php echo $options['mobile_menu_text_color']; ?>;
	}
	.pk_navigation_wrapper nav li:hover,
	.pk_navigation_wrapper nav ul .current-menu-item > a,
	.pk_navigation_wrapper nav ul .current-menu-ancestor > a,
	.pk_navigation_wrapper nav ul .current-menu-item:hover > a,
	.pk_navigation_wrapper nav ul .current-menu-ancestor:hover > a,
	.pk_navigation_wrapper nav ul ul{ background: none; }
	.pk_navigation_wrapper nav .current-menu-item.current-menu-parent > a{ background: none !important; }
	.pk_navigation_wrapper nav .current-menu-item > a {
		color: <?php echo $options['mobile_menu_selected_text_color']; ?> !important;
		background-color: <?php echo $options['mobile_menu_selected_bg_color']; ?> !important;
	}
	
	/* Jigoshop */
	
	#tabs ul.tabs li {
		border-top: solid 1px <?php echo $options['mobile_ec_tabs_borders_color']; ?>;
	}
	#tabs ul.tabs li:hover{ background: none; }
	#tabs ul.tabs li.active {
		background-color: <?php echo $options['mobile_ec_tabs_selected_bg_color']; ?>;
	}
	#tabs ul.tabs li:hover a,
	#tabs ul.tabs li a:hover,
	#tabs ul.tabs li.active a {
		color: <?php echo $options['mobile_ec_tabs_selected_text_color']; ?>;
	}
    
    /* BBPress */
    
    #bbpress-forums .bbp-replies .bbp-header .bbp-reply-content {
		border-left: solid 1px rgba(255, 255, 255, 0.12) !important;
	}
	
	/* Footer */
	
	.pk_copyright {
		color: <?php echo $options['mobile_footer_copyright_text_color']; ?>;
		border-top: solid 1px <?php echo $options['mobile_footer_copyright_borders_color']; ?>;
		background-color: <?php echo $options['mobile_footer_copyright_bg_color']; ?>;
	}
}

@media only screen and (min-width: 768px) and (max-width: 959px) {
	.products li strong {
		border-bottom: solid 1px <?php echo $options['boxes_main_borders_color']; ?>;
	}
}

@media screen and (max-width: 767px) {
	.pk_page #pk_sidebar {
		border-top: solid 1px <?php echo $options['main_borders_color']; ?>;
		background-color: <?php echo $options['mobile_sidebar_bg_color']; ?>;
	}
	.pk_business_tabs .pk_tabs_navigation ul li {
		border-left: none;
		border-bottom: solid 1px <?php echo $options['main_borders_color']; ?>;
		background-color: none;
	}
	.pk_business_tabs .pk_tabs_navigation ul li.pk_active_tab {
		border-bottom: solid 1px <?php echo $options['main_borders_color']; ?>;
		background-color: transparent;
		background-position: right center;
	}
	.ie8 .pk_pricing_table ul,
	.ie8 .pk_pricing_table footer {
		border-left: solid 1px <?php echo $options['main_borders_color']; ?> !important;
		border-right: solid 1px <?php echo $options['main_borders_color']; ?> !important;
	}
}

@media only screen and (min-width: 480px) and (max-width: 767px) {
	.products li strong {
		border-bottom: solid 1px <?php echo $options['boxes_main_borders_color']; ?>;
	}
}