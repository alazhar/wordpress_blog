<?php
/*
Template Name: Home Page Business Template
*/
?>
<?php
	get_header();
	
	$post_meta = get_post_custom(get_the_ID());
?>

					<!-- pk start tabs -->
					<p class="pk_prices_button"><a href="<?php if (isset($post_meta['_tabs_button_link'][0])) echo $post_meta['_tabs_button_link'][0]; ?>" title="<?php if (isset($post_meta['_tabs_button_label'][0])) echo $post_meta['_tabs_button_label'][0]; ?>" class="pk_button_big pk_button_<?php if (isset($post_meta['_tabs_button_color'][0])) echo $post_meta['_tabs_button_color'][0]; ?>"><span><?php if (isset($post_meta['_tabs_button_label'][0])) echo $post_meta['_tabs_button_label'][0]; ?></span></a></p>
					<div class="pk_business_tabs">
						<div class="pk_tabs_navigation">
							<ul>
<?php
	$temp_query = $wp_query;
	
	$query_args['post_type'] = 'page';
	$query_args['post_parent'] = get_the_ID();
	$query_args['posts_per_page'] = -1;
	
	$wp_query = new WP_Query($query_args);
	
	$c = 0;
	
	if (have_posts()) : 
		
		while (have_posts()) : 
			
			the_post();
			$c++;
?>
								<li<?php if ($c == 1) echo' class="pk_active_tab"'; ?>><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></li>
<?php
		endwhile;
		
		rewind_posts();
		
	endif;
?>
							</ul>
						</div>
						<div class="pk_tabs">
<?php
	if (have_posts()) : 
		
		while (have_posts()) : 
			
			the_post();
?>
								<div class="pk_tab pk_clearfix"><?php the_content(); ?></div>
<?php
		endwhile;
		
	endif;
?>
						</div>
					</div>
					<!-- pk end tabs -->
<?php
	$wp_query = $temp_query;
	wp_reset_query();
	
	if (is_active_sidebar('sidebar_home_page_business_left') || is_active_sidebar('sidebar_home_page_business_middle') || is_active_sidebar('sidebar_home_page_business_right')) : 
?>

					<!-- pk start widgets areas -->
					<section id="pk_home_business_widgets" class="pk_clearfix">
						<div class="pk_one_third pk_fixed">

<?php if (dynamic_sidebar('sidebar_home_page_business_left')) : endif; ?>
						</div>
						<div class="pk_one_third pk_fixed">

<?php if (dynamic_sidebar('sidebar_home_page_business_middle')) : endif; ?>
						</div>
						<div class="pk_one_third pk_fixed pk_last">

<?php if (dynamic_sidebar('sidebar_home_page_business_right')) : endif; ?>
						</div>
					</section>
					<!-- pk end widgets areas -->
<?php
	endif;
	
	while (have_posts()) : 
		
		the_post();
		add_filter('embed_defaults', 'pk_full_width_page_embed_defaults');
		
		if (get_the_content()) :
?>

					<!-- pk start contents -->
					<section id="pk_page_content" class="pk_clearfix">
						<?php
			the_content();
?>
					</section>
					<!-- pk end contents -->
<?php
		endif;
		
	endwhile;
?>
					
					<!-- pk start featured image -->
					<section id="pk_home_business_featured_image" class="pk_last_section pk_clearfix">
						<?php the_post_thumbnail('full'); ?>

					</section>
					<!-- pk end featured image -->
<?php
	get_footer();
?>