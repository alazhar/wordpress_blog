<?php
	$post_meta = get_post_custom(get_the_ID());
?>

					<article id="post-<?php the_ID(); ?>" <?php post_class('pk_entry pk_entry_blog pk_entry_link pk_boxed pk_full_content'.((is_singular()) ? ' pk_entry_single' : '')); ?>>
						<div class="pk_entry_content">
							<header>
								<h3><a href="<?php echo (isset($post_meta['_link'][0]) && !in_array($post_meta['_link'][0], array('', '#'))) ? $post_meta['_link'][0] : get_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h3>
								<ul class="pk_entry_meta">
									<li class="pk_entry_author"><?php _e('By:', 'pk_translate'); ?> <?php the_author_posts_link(); ?></li>
									<li class="pk_entry_date"><?php echo get_the_date(); ?></li>
									<li class="pk_entry_categories"><?php the_category(', '); ?></li>
									<li class="pk_comments_count"><?php comments_popup_link(__('0', 'pk_translate'), __('1', 'pk_translate'), __('%', 'pk_translate'), 'pk_meta_tot_comments', __('Off', 'pk_translate')); ?></li>
								</ul>
							</header>
							<?php add_filter('embed_defaults', 'pk_post_embed_defaults'); the_content(); ?>
							<footer>
<?php
	if (!is_singular()) : 
?>
								<a href="<?php the_permalink(); ?>" title="<?php _e('Permalink', 'pk_translate'); ?>" class="pk_button_read_more"><?php _e('Permalink', 'pk_translate'); ?></a>
<?php
	else : 
?>
								<?php the_tags('<p>'.__('tags:', 'pk_translate').' </p><ul class="pk_entry_tags"><li>','</li><li>','</li></ul>'); ?>
<?php
	endif;
?>

							</footer>
						</div>
					</article>
