<?php
	$social_networks = array('AIM','Apple','Bebo', 'Behance', 'Blogger','Brightkite','Cargo','Delicious','DesignFloat','Designmoo','DeviantART','Digg','Dopplr','Dribbble','Email','Ember','Evernote','Facebook','Flickr','Forrst','FriendsFeed','GameSpot','Google','GoogleTalk','GoogleVoice','GoogleWave','Gowalla','Grooveshark','iLike','KomodoMedia','LastFm','LinkedIn','Mixx','MobileMe','MyNameIsE','MySpace','Netvibes','Newsvine','OpenID','Orkut','Pandora','PayPal','Picasa','Pinterest', 'Playstation','Plurk','Posterous','Qik','Readernaut','Reddit','Roboto','RSS','ShareThis','Skype', 'Squidoo', 'StumbleUpon','Technorati','Tumblr','Twitter','Viddler','Vimeo','Virb','Windows','WordPress','Xing','Yahoo','YahooBuzz','Yelp','YouTube','Zootool');
?>
			<!-- pk start footer social networks -->
			<ul class="pk_footer_sn">
<?php
	for ($i = 0; $i <count($social_networks); $i++) : 
		
		if (trim(pk_get_option(strtolower($social_networks[$i]).'_link', '') != '')) : 
?>
				<li>
					<a href="<?php echo esc_url(pk_get_option(strtolower($social_networks[$i]).'_link', '')); ?>" title="<?php echo $social_networks[$i]; ?>" target="_blank">
						<img src="<?php echo PK_THEME_DIR.'/images/social_networks_icons/'.strtolower($social_networks[$i]).'.png'; ?>" alt="<?php echo $social_networks[$i]; ?>" />
					</a>
				</li>
<?php
		endif;
		
	endfor;
?>
			</ul>
			<!-- pk end footer social networks -->
 