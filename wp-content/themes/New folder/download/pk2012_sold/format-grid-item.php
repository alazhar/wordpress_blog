<?php
	global $grid;
	
	$post_meta = get_post_custom(get_the_ID());
	$post_images = pk_get_post_gallery_images('full');
?>

						<div class="pk_entry pk_entry_work pk_boxed pk_with_image pk_full_content">
<?php
	if (has_post_thumbnail()) : 
		
		if (!in_array(get_post_type(), array('post', 'portfolio')) || ((get_post_type() == 'post' && get_post_format() != 'image') || (get_post_type() == 'portfolio' && isset($post_meta['_work_format'][0]) && $post_meta['_work_format'][0] != 'gallery'))) : 
?>
							<div class="pk_image">
								<div class="pk_image_wrapper pk_zoom_icon">
									<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
										<?php the_post_thumbnail('grid-'.$grid, array('title' => '')); ?>

										<span class="pk_image_button_back_ground"></span>
										<span class="pk_image_button_icon"></span>
									</a>
								</div>
							</div>
<?php
		else :
?>
							<div class="pk_image">
								<div class="pk_image_wrapper pk_zoom_icon">
									<a class="prettyPhoto<?php the_ID(); ?>" href="<?php echo pk_get_featured_image(); ?>" title="<?php the_title_attribute(); ?>">
										<?php the_post_thumbnail('grid-'.$grid, array('title' => '')); ?>

										<span class="pk_image_button_back_ground"></span>
										<span class="pk_image_button_icon"></span>
									</a>
<?php
			for ($i = 0; $i < count($post_images); $i++) : 
				
				$video_url = get_post_meta($post_images[$i]['id'], '_video_url', true);
?>
									<a class="prettyPhoto<?php the_ID(); ?>" href="<?php echo ($video_url) ? esc_url($video_url) : $post_images[$i]['url']; ?>"></a>
<?php
			endfor;
?>
									<script type="text/javascript">
										jQuery(document).ready(function() {
											jQuery(".prettyPhoto<?php the_ID(); ?>").attr('rel', 'prettyPhoto[gallery_<?php the_ID(); ?>]').prettyPhoto(<?php echo PK_PRETTYPHOTO_PARAMS; ?>);
										});
									</script>
								</div>
							</div>
<?php
		endif;
		
	endif; 
?>
							<div class="pk_entry_content">
								<h4><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h4>
								<?php 
			
			add_filter('excerpt_length', 'pk_grid_excerpt_filter');
			the_excerpt();
			remove_filter('excerpt_length', 'pk_grid_excerpt_filter');
?>
								<a href="<?php the_permalink(); ?>" title="<?php _e('View work', 'pk_translate'); ?>" class="pk_button_read_more"><?php _e('View', 'pk_translate'); ?></a>
							</div>
						</div>
