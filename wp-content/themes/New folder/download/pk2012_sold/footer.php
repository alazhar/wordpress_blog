
				</div>
				<!-- pk end main -->
<?php
	if (pk_sidebar()) : 
?>

				<!-- pk start sidebar -->
				<aside id="pk_sidebar">
					<span id="pk_sidebar_top_shadow"></span>
					<span id="pk_sidebar_bottom_shadow"></span>

<?php get_sidebar(); ?>
				</aside>
				<!-- pk end sidebar -->
<?php
	endif;
?>

			</div>
<?php
	$footer_widgets_areas = pk_get_option('footer_layout', '1/2,1/4,1/4');
	$footer_widgets_areas_replaced = str_replace(array(' ', '1/1', '1/2', '1/3', '1/4', '1/5', '1/6', '2/3', '2/5', '3/4', '3/5', '4/5', '5/6'), array('', 'pk_full_width', 'pk_one_half', 'pk_one_third', 'pk_one_fourth', 'pk_one_fifth', 'pk_one_sixth', 'pk_two_third', 'pk_two_fifth', 'pk_three_fourth', 'pk_three_fifth', 'pk_four_fifth', 'pk_five_sixth'), $footer_widgets_areas);
	$footer_widgets_area_columns = explode(',', $footer_widgets_areas_replaced);
	$footer_widgets_area_active = false;
	
	for ($c = 0; $c < count($footer_widgets_area_columns); $c++) : if (is_active_sidebar('footer_column_'.($c + 1))) : $footer_widgets_area_active = true; break; endif; endfor;
	
	if ($footer_widgets_area_active) : 
?>

			<!-- pk start widgets area -->
			<div id="pk_widgets_area" class="pk_clearfix">
<?php
		for ($c = 0; $c < count($footer_widgets_area_columns); $c++) : 
			
			if (is_active_sidebar('footer_column_'.($c + 1))) : 
?>
				<div class="<?php echo $footer_widgets_area_columns[$c]; ?> pk_fixed<?php if ($c == count($footer_widgets_area_columns) - 1) echo ' pk_last'; ?>">

<?php dynamic_sidebar('footer_column_'.($c + 1)); ?>
				</div>
<?php
			endif;
			
		endfor;
?>

			</div>
			<!-- pk end widgets area -->

<?php
	endif;
?>
		</div>
		<!-- pk end page -->

	</div>
	<!-- pk end wrapper : pk_page -->	

	<!-- pk start footer -->
	<footer class="pk_wrapper pk_bottom_shadow">

		<!-- pk start center box -->
		<div class="pk_center_box pk_footer pk_clearfix">

			<!-- pk start footer navigation -->
			<?php
	$output = wp_nav_menu(array('echo' => false, 'theme_location' => 'pk_footer_menu_'.((is_user_logged_in()) ? 'li' : 'lo'), 'container' => 'nav', 'container_class' => 'pk_footer_menu', 'depth' => 1, 'fallback_cb' => 'pk_set_up_footer_menu_message', 'after' => '<span>|</span>'));
	
	echo str_replace('</ul>', '<li class="pk_last"><a href="#top" title="top" class="pk_button_top">Top</a></li></ul>', $output);
?>

			<!-- pk end footer navigation -->
			
<?php
	get_template_part('footer', 'social-networks');
?>

			<!-- pk start footer copyright -->
            <div class="pk_copyright">
<?php
	if (pk_get_option('logo_footer', '') != '') : 
?>
				<img src="<?php echo stripslashes(pk_get_option('logo_footer', '')); ?>" class="pk_footer_logo" alt="<?php echo get_bloginfo('name'); ?>" />
<?php
	endif;
?>
				<p><?php echo esc_attr(pk_get_option('copyright', '')); ?></p>
			</div>
			<!-- pk end footer copyright -->

		</div>
		<!-- pk end center box -->

	</footer>
	<!-- pk end footer -->

</div>
<!-- pk end wrapper all -->
<?php
	if (get_option('users_can_register') && !is_user_logged_in()) : 
?>

<!-- pk start login form -->
<div id="pk_login_form" class="pk_hide">
    <div class="pk_user_form">
        <form name="loginform" id="loginform" action="<?php echo home_url('/wp-login.php'); ?>" method="post">
            <p>
                <label for="user_login_login_form"><?php _e('Username', 'pk_translate'); ?><br />
                <input type="text" name="log" id="user_login_login_form" class="input" value="" size="20" tabindex="10" /></label>
            </p>
            <p>
                <label for="user_pass_login_form"><?php _e('Password', 'pk_translate'); ?><br />
                <input type="password" name="pwd" id="user_pass_login_form" class="input" value="" size="20" tabindex="20" /></label>
            </p>
            <p class="forgetmenot"><label for="rememberme_login_form"><input name="rememberme" type="checkbox" id="rememberme_login_form" value="forever" tabindex="90" /> <?php _e('Remember me', 'pk_translate'); ?></label></p>
            <p class="submit">
                <input type="submit" name="wp-submit" id="wp-submit_login_form" class="button-primary" value="<?php _e('Log in', 'pk_translate'); ?>" tabindex="100" />
                <input type="hidden" name="redirect_to" value="" />
                <input type="hidden" name="testcookie" value="1" />
            </p>
        </form>
        <a class="pk_lost_password" href="#pk_lost_password_form"><?php _e('Lost your password?', 'pk_translate'); ?></a>
    </div>
    <script type="text/javascript">
		jQuery(document).ready(function() {
			jQuery(".pk_lost_password").pk_form_box();
		});
	</script>
</div>
<!-- pk end login form -->

<!-- pk start register form -->
<div id="pk_register_form" class="pk_hide">
    <div class="pk_user_form">
        <p class="message register"><?php _e('Register for this site', 'pk_translate'); ?></p>
        <form name="registerform" id="registerform" action="<?php echo home_url('/wp-login.php?action=register'); ?>" method="post">
            <p>
                <label for="user_login_register_form"><?php _e('Choose a username', 'pk_translate'); ?><br />
                <input type="text" name="user_login" id="user_login_register_form" class="input" value="" size="20" tabindex="10" /></label>
            </p>
            <p>
                <label for="user_email_register_form"><?php _e('E-mail', 'pk_translate'); ?><br />
                <input type="email" name="user_email" id="user_email_register_form" class="input" value="" size="25" tabindex="20" /></label>
            </p>
            <p id="reg_passmail"><?php _e('A password will be e-mailed to you.', 'pk_translate'); ?></p>
            <br class="clear" />
            <input type="hidden" name="redirect_to" value="" />
            <p class="submit"><input type="submit" name="wp-submit" id="wp-submit_register_form" class="button-primary" value="<?php _e('Register', 'pk_translate'); ?>" tabindex="100" /></p>
        </form>
        <a class="pk_lost_password" href="#pk_lost_password_form"><?php _e('Lost your password?', 'pk_translate'); ?></a>
    </div>
    <script type="text/javascript">
		jQuery(document).ready(function() {
			jQuery(".pk_lost_password").pk_form_box();
		});
	</script>
</div>
<!-- pk end register form -->

<!-- pk start lost password -->
<div id="pk_lost_password_form" class="pk_hide">
    <div class="pk_user_form">
        <form name="lostpasswordform" id="lostpasswordform" action="<?php echo home_url('/wp-login.php?action=lostpassword'); ?>" method="post">
            <p>
                <label for="user_login_lost_password_form" ><?php _e('Username or E-mail:', 'pk_translate'); ?><br />
                <input type="text" name="user_login" id="user_login_lost_password_form" class="input" value="" size="20" tabindex="10" /></label>
            </p>
            <input type="hidden" name="redirect_to" value="" />
            <p class="submit"><input type="submit" name="wp-submit" id="wp-submit_lost_password_form" class="button-primary" value="<?php _e('Get new password', 'pk_translate'); ?>" tabindex="100" /></p>
        </form>
        <p class="message"><?php _e('Please enter your username or email address.', 'pk_translate'); ?><?php _e('You will receive a link to create a new password via email.', 'pk_translate'); ?></p>
    </div>
</div>
<!-- pk end lost password -->
<?php
	endif;
	
	global $welcome_image;
	
	if ($welcome_image == 1) : 
?>
<a id="pk_welcome_image" class="pk_hide" href="<?php echo esc_url(pk_get_option('welcome_image', '')); ?>"></a>
<script type="text/javascript">
	jQuery(document).ready(function() {
		function pkAddLink() {
			jQuery('.pp_content').find('img').css('cursor' , 'pointer').unbind('click').bind('click', function() {
				 window.location = "<?php echo esc_url(pk_get_option('welcome_link', '#')); ?>";
				 return false;
			});
		}
		
		jQuery("#pk_welcome_image").prettyPhoto({
			overlay_gallery:false,
			theme:"pp_default",
			social_tools:"",
			show_title:false,
			deeplinking:false,
			changepicturecallback:pkAddLink
		});
		if (jQuery(window).width() > 450) {
			setTimeout(function() {
				jQuery("#pk_welcome_image").trigger('click');
			}, 500);
		}
	});
</script>

<?php
	endif;
	
	do_action('pk_before_footer');
	
	wp_footer();
	
	if (pk_get_option('js_custom', '') != '') : 
?>

<script type="text/javascript">

<?php
		echo pk_get_option('js_custom', '');
?>


</script>

<?php
	endif;
?>
</body>
</html><a href="http://www.wplocker.com">theme shared on wplocker.com</a>