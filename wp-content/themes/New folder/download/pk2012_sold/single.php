<?php
	get_header();
	
	while (have_posts()) : 
		
		the_post();
		
		do_action('pk_before_post_content');
		
		(!get_post_format()) ? get_template_part('format', 'blog-standard') : get_template_part('format', 'blog-'.get_post_format());
		
		wp_link_pages(array('echo' => 1, 'before' => '<div class="pk_posts_link_pages"><p>'.__('Pages:', 'pk_translate'), 'after' => '</p></div>'));
?>

					<!-- pk start author info box -->
					<div id="pk_author_<?php the_author_meta('ID'); ?>" class="pk_author_box pk_boxed">
						<div class="pk_image">
							<?php echo get_avatar(get_the_author_meta('ID'), '40'); ?>
						</div>
                        <div class="pk_entry_content">
							<h5 class="pk_heading_uppercase"><?php the_author_meta('display_name'); ?></h5>
							<p><?php echo (function_exists('icl_t')) ? icl_t('theme', 'bio-'.get_the_author_meta('nickname'), get_the_author_meta('description')) : get_the_author_meta('description'); ?></p>
                            <a href="<?php echo get_author_posts_url(get_the_author_meta( 'ID' )); ?>" class="pk_more_by_author"><?php _e('Read more articles', 'pk_translate'); ?></a>
                        </div>
					</div>
					<!-- pk end author info box -->

					<div class="pk_previous_next_post_links"><div class="pk_previous_post_links"><?php if (!is_attachment()) : previous_post_link('%link', '%title', false); else : previous_image_link(false); endif; ?></div><div class="pk_next_post_links"><?php if (!is_attachment()) : next_post_link('%link', '%title', false); else : next_image_link(false); endif; ?></div></div>

<?php
		if (is_active_sidebar('sidebar_after_post_content') || is_active_sidebar('custom_sidebar_after_content_'.get_the_ID())) : 
?>
					<!-- pk start widgets area -->
					<div id="sidebar_after_post_content" class="pk_clearfix">

<?php
			if (is_active_sidebar('custom_sidebar_after_content_'.get_the_ID())) : 
				
				dynamic_sidebar('custom_sidebar_after_content_'.get_the_ID());
				if (!in_array(get_the_ID(), pk_get_option('hide_default_sidebars_for', array()))) : dynamic_sidebar('sidebar_after_post_content'); endif;
				
			else : 
				
				dynamic_sidebar('sidebar_after_post_content');
				
			endif;
?>
					</div>
					<!-- pk end widgets area -->

<?php
		endif;
		
		do_action('pk_after_post_content');
		
		comments_template();
		
	endwhile;
	
	get_footer();
?>