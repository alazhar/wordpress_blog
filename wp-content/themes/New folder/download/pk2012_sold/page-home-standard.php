<?php
/*
Template Name: Home Page Standard Template
*/
?>
<?php
	get_header();
	
	while (have_posts()) : 
		
		the_post();
		add_filter('embed_defaults', 'pk_page_embed_defaults');
		
		if (get_the_content()) :
?>

					<!-- pk start contents -->
					<section id="pk_page_content" class="pk_clearfix">
						<?php
		the_content();
?>

					</section>
					<!-- pk end contents -->
<?php
		endif;
		
	endwhile;
	
	$post_meta = get_post_custom(get_the_ID());
	
	if (is_active_sidebar('sidebar_home_page_standard_left') || is_active_sidebar('sidebar_home_page_standard_right')) : 
?>

					<!-- pk start widgets areas -->
					<section id="pk_home_standard_widgets" class="pk_clearfix">
						<div class="pk_one_half">

<?php if (dynamic_sidebar('sidebar_home_page_standard_left')) : endif; ?>
						</div>
						<div class="pk_one_half pk_last">

<?php if (dynamic_sidebar('sidebar_home_page_standard_right')) : endif; ?>
						</div>
					</section>
					<!-- pk end widgets areas -->
<?php
	endif;
	
	if (pk_get_option('homepages_latest_works', 'true') == 'true') : 
?>

					<!-- pk start latest works -->
					<section id="pk_latest_works" class="pk_clearfix">
						<h3 class="pk_heading_uppercase"><?php _e('Latest works', 'pk_translate'); ?></h3>
						<p class="pk_button_view_all"><a href="<?php echo get_permalink((function_exists('icl_object_id')) ? icl_object_id(pk_get_option('portfolio_page_id', ''), 'page', true) : pk_get_option('portfolio_page_id', '')); ?>" title="<?php _e('View all', 'pk_translate'); ?>" class="pk_button_mini pk_button_<?php echo ((isset($post_meta['_view_all_button_color'][0])) ? $post_meta['_view_all_button_color'][0] : 'white'); ?>"><span><?php _e('View all', 'pk_translate'); ?></span></a></p>
<?php
		$total_works = (isset($post_meta['_total_works'][0])) ? (int)trim($post_meta['_total_works'][0]) : 3;
		
		$temp_query = $wp_query;
		
		$query_args['post_type'] = 'portfolio';
		$query_args['posts_per_page'] = $total_works;
		
		$portfolio_categories_array = (isset($post_meta['_portfolio_categories'][0])) ? pk_lang_object_ids(unserialize($post_meta['_portfolio_categories'][0]), 'taxonomy_portfolio') : array();
		
		if (isset($post_meta['_portfolio_categories'][0]) && is_array($portfolio_categories_array) && count($portfolio_categories_array) > 0) $query_args['tax_query'] = array(
			array(
				'taxonomy' => 'taxonomy_portfolio',
				'field' => 'id',
				'terms' => $portfolio_categories_array
			)
		);
		
		$wp_query = new WP_Query($query_args);
		
		$c = 0;
		
		if (have_posts()) : 
			
			while (have_posts()) : 
				
				the_post();
				$c++;
?>
						<div class="pk_one_third pk_fixed<?php if ($c == 3) echo ' pk_last'; ?>">
							<div class="pk_entry pk_boxed pk_full_content">
<?php
				if (has_post_thumbnail()) : 
?>
								<div class="pk_image">
									<div class="pk_image_wrapper pk_zoom_icon">
										<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
											<?php the_post_thumbnail('thumb-latest-works'); ?>

											<span class="pk_image_button_back_ground"></span>
											<span class="pk_image_button_icon"></span>
										</a>
									</div>
								</div>
<?php
				endif;
?>
								<div class="pk_entry_content pk_center_text">
									<h4><?php the_title(); ?></h4>
								</div>
							</div>
						</div>
<?php
				if ($c == 3) $c = 0;
				
			endwhile;
			
		endif;
?>
					</section>
					<!-- pk end latest works -->
<?php
		$wp_query = $temp_query;
		wp_reset_query();
		
		unset($query_args);
		
	endif;
	
	if (pk_get_option('homepages_latest_posts', 'true') == 'true') : 
?>

					<!-- pk start latest news -->
					<section id="pk_latest_news" class="pk_last_section pk_clearfix">
						<h3 class="pk_heading_uppercase"><?php _e('Latest posts', 'pk_translate'); ?></h3>
						<p class="pk_button_view_all"><a href="<?php echo (get_option('show_on_front') == 'page') ? get_permalink((function_exists('icl_object_id')) ? icl_object_id(get_option('page_for_posts'), 'page', true) : get_option('page_for_posts')) : ((function_exists('icl_get_home_url')) ? icl_get_home_url() : home_url());?>" title="<?php _e('View all', 'pk_translate'); ?>" class="pk_button_mini pk_button_<?php echo ((isset($post_meta['_view_all_button_color'][0])) ? $post_meta['_view_all_button_color'][0] : 'white'); ?>"><span><?php _e('View all', 'pk_translate'); ?></span></a></p>
<?php
		$total_posts = (isset($post_meta['_total_posts'][0]) && trim($post_meta['_total_posts'][0]) != '') ? (int)trim($post_meta['_total_posts'][0]) : 3;
		
		$temp_query = $wp_query;
		
		$query_args['post_type'] = 'post';
		$query_args['posts_per_page'] = $total_posts;
		
		$blog_categories_array = (isset($post_meta['_blog_categories'][0])) ? pk_lang_object_ids(unserialize($post_meta['_blog_categories'][0]), 'category') : '';
		
		if (isset($post_meta['_blog_categories'][0]) && is_array($blog_categories_array) && count($blog_categories_array) > 0) $query_args['cat'] = implode(',', $blog_categories_array);
		
		$wp_query = new WP_Query($query_args);
		
		if (have_posts()) : 
			
			while (have_posts()) : 
				
				the_post();
?>
						<div class="pk_entry pk_entry_news pk_boxed<?php echo (has_post_thumbnail()) ? '' : ' pk_no_thumbnail'; ?>">
<?php
				if (has_post_thumbnail()) : 
?>
							<div class="pk_image">
								<div class="pk_image_wrapper pk_zoom_icon">
									<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
										<?php the_post_thumbnail('thumb-latest-posts'); ?>

										<span class="pk_image_button_back_ground"></span>
										<span class="pk_image_button_icon"></span>
									</a>
								</div>
							</div>
<?php
				endif;
?>
							<div class="pk_entry_content">
								<header>
									<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><h4><?php the_title(); ?></h4></a>
									<p class="pk_entry_date"><?php echo get_the_date(); ?></p>
								</header>
								<?php add_filter('excerpt_length', 'pk_blog_excerpt_filter'); the_excerpt(); remove_filter('excerpt_length', 'pk_blog_excerpt_filter'); ?>
								<footer>
									<a href="<?php the_permalink(); ?>" title="<?php _e('Read more', 'pk_translate'); ?>" class="pk_button_read_more"><?php _e('Read more', 'pk_translate'); ?></a>
								</footer>
							</div>
						</div>
<?php
			endwhile;
			
		endif;
?>
					</section>
					<!-- pk end latest news -->
<?php
		$wp_query = $temp_query;
		wp_reset_query();
		
	endif;
	
	get_footer();
?>