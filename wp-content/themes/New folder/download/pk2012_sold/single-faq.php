<?php
	get_header();
	
	while (have_posts()) : 
		
		the_post();
		
		get_template_part('format', 'page-standard');
		
	endwhile;
	
	get_footer();
?>