<?php
/*
Template Name: Full Width Template
*/
?>
<?php
	get_header();
	
	while (have_posts()) : 
		
		the_post();
		
		do_action('pk_before_page_content');
		
		$post_meta = get_post_custom(get_the_ID());
		
		(function_exists('is_jigoshop') && is_jigoshop() || function_exists('is_bbpress') && is_bbpress()) ? get_template_part('format', 'page-standard') : get_template_part('format', 'page-'.((isset($post_meta['_page_format'][0])&& $post_meta['_page_format'][0]) ? $post_meta['_page_format'][0] : 'standard'));
		
		wp_link_pages(array('echo' => 1, 'before' => '<div class="pk_posts_link_pages"><p>'.__('Pages:', 'pk_translate'), 'after' => '</p></div>'));
		
		if (is_active_sidebar('sidebar_after_page_content') || is_active_sidebar('custom_sidebar_after_content_'.get_the_ID())) : 
?>
					<!-- pk start widgets area -->
					<div id="sidebar_after_page_content" class="pk_clearfix">

<?php
			if (is_active_sidebar('custom_sidebar_after_content_'.get_the_ID())) : 
				
				dynamic_sidebar('custom_sidebar_after_content_'.get_the_ID());
				if (!in_array(get_the_ID(), pk_get_option('hide_default_sidebars_for', array()))) : dynamic_sidebar('sidebar_after_page_content'); endif;
				
			else : 
				
				dynamic_sidebar('sidebar_after_page_content');
				
			endif;
?>
					</div>
					<!-- pk end widgets area -->

<?php
		endif;
		
		do_action('pk_after_page_content');
		
		comments_template();
		
	endwhile;
	
	get_footer();
?>