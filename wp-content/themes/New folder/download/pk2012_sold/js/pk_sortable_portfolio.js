var $pk_aj = jQuery.noConflict();
$pk_aj(document).ready(function() {
	
	var sortable_list = $pk_aj('#pk_admin_sortable_list');
 
	sortable_list.sortable({
		
		placeholder:"pk_admin_sortable_placeholder",
		
		update:function() {
			
			$pk_aj('#pk_admin_saving_sorting').show();
			$pk_aj('#pk_admin_success_sorting').hide();
			$pk_aj('#pk_admin_error_sorting').hide();
			
			$pk_aj('body').css('cursor', 'progress');
			
			opts = {
				
				url:ajaxurl,
				type:'POST',
				async:true,
				cache:false,
				dataType:'json',
				data:{
					
					action:'portfolio_sort',
					order:sortable_list.sortable('toArray').toString()
					
				},
				success:function() {
					
					$pk_aj('#pk_admin_success_sorting').show();
					$pk_aj('#pk_admin_saving_sorting').hide();
					$pk_aj('body').css('cursor', 'auto');
					return;
					
				},
				error:function() {
					
					$pk_aj('#pk_admin_error_sorting').show();
					$pk_aj('#pk_admin_saving_sorting').hide();
					$pk_aj('body').css('cursor', 'auto');
					return;
					
				}
				
			};
			
			$pk_aj.ajax(opts);
			
		}
		
	});
	
	sortable_list.disableSelection();
	
});