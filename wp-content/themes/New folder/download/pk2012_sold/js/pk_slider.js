/*
	SLIDER
-------------------------------------------------------------------------------------------------------------------------------------*/


(function($) {
	$.fn.pk_slider = function(options) {
		var defaults = {
			buttonInfoOpenLabel : 'info',
			buttonInfoCloseLabel : 'close info',
			infoTextAlign : 'left',
			infoBackgroundAlpha : 0,
			slideshow : true,
			slideshowAutoStart : false,
			slideshowInterval : 5,
			easing : 'easeOutExpo',
			speed : 800
		};

		var settings = $.extend({}, defaults, options);

		return this.each(function() {
			var root = $(this),
				t_i = $('.pk_slider_item', root).length,
				id = 0;
				is_slideshow = (settings.slideshow == true && settings.slideshowAutoStart == true) ? true : false,
				is_loading = false,
				is_info = false,
				interval = undefined;
				
				
			/*
				GLOBAL
			-------------------------------------------------------------------------------------------------------------------------*/
			
			var methods = {
				init : function() {
					$('.pk_slider_item', root).each(function(i) {
						$(this).css({
							'position' : 'absolute',
							'overflow' : 'hidden',
							'top ': '0px',
							'left ': '0px'
						});
						
						$('.pk_slider_item_info_content', this).css('text-align' , settings.infoTextAlign);
						$('.pk_slider_item_info_background', this).css('opacity' , settings.infoBackgroundAlpha);
						$('.pk_slider_item_info', this).hide();
						
						$(this).hide();
					});
					
					if(!pk_detect_mobile()) {
						root.bind('mouseenter', methods.mouseEnter);
						root.bind('mouseleave', methods.mouseLeave);
					} else {
						root.touchwipe({
							preventDefaultEvents: false,
							wipeLeft: methods.wipeLeft,
							wipeRight: methods.wipeRight,
    						min_move_x: 20
     					});
					}
				
					methods.initNavigation();
					methods.load(true);
				},
					
				resize : function() {
					var c_i = $('.pk_slider_item:eq(' + id + ')', root);
						
					$('.pk_slider_content', root).css('height' , c_i.height() + 'px');
					$('.pk_button_prev', root).css('left' , '-60px');
					$('.pk_button_next', root).css('right' , '-60px');
						
					$('.pk_slider_thumbs', root).css(
						'margin-left' , (root.width() / 2) - ($('.pk_slider_thumbs', root).outerWidth() / 2) + 'px'
					);
				},
				
				load : function(init) {
					is_loading = true;
						
					methods.showLoader();
					methods.stopSlideshow();
					
					$(window).unbind('resize.pkSlider');
						
					var n_i = $('.pk_slider_item_media:eq(' + id + ')', root);
						
					this.img_loader = '';
					this.img_loader = new Image();
					this.img_loader.onload = function() {	
						methods.hideLoader();
						methods.change(init);
					};
			
					this.img_loader.onerror = function() {
						console.log('Image not found. Sorry!');
					};
								
					this.img_loader.src = $('img', n_i).attr('src');
				},
					
				change : function(init) {
					var c_i = $('.pk_slider_item:visible', root);
						n_i = $('.pk_slider_item:eq(' + id + ')', root);
						
					c_i.css({ 'zIndex' : 1 });
					n_i.css({ 'zIndex' : 2 });
						
					n_i.fadeIn(settings.speed, function() {
						$(window).bind('resize.pkSlider', methods.resize);
						is_loading = false;
						
						c_i.hide();
						
						methods.startSlideshow();
					});
						
					if(init) {
						if($.browser.msie && parseInt($.browser.version) < 9) {
							setTimeout(function() {
								methods.resize();
							}, 200);
						} else {
							$('.pk_slider_content', root).css({
								'height' : n_i.height() + 'px'
							});
						}
					} else {
						$('.pk_slider_content', root).animate({
							'height' : n_i.height() + 'px'
						}, (settings.speed / 2), 'easeOutExpo');
					}
					
					if(!$('.pk_slider_item_info', n_i).length) {
						$('.pk_slider_info_button', root).fadeOut(200);
					} else {
						$('.pk_slider_info_button', root).fadeIn(200);
					}
					
					if(is_info) { 
						$('.pk_slider_info_button', root).trigger('click', [true]);
					}
					
					methods.updateNavigation();
				},
				
				
				/*
					NAVIGATION
				---------------------------------------------------------------------------------------------------------------------*/
				
				initNavigation : function() {
					if(t_i <= 1) {
						$('.pk_slider_content', root).css('margin-bottom' , '0');
						return;	
					}
					
					var b_p = '<a href="#" class="pk_button_prev">prev</a>',
						b_n = '<a href="#" class="pk_button_next">next</a>',
						b_i = '<span class="pk_slider_info_button">' + settings.buttonInfoOpenLabel + '</span>',
						b_s = (settings.slideshow == true) ? '<span class="pk_button_slideshow">play/pause</span>' : '';
					
					$('.pk_slider_content', root).append(b_p + b_n);
					$('.pk_slider_content', root).after('<div class="pk_slider_navigation"><div class="pk_slider_thumbs"></div></div>');
					$('.pk_slider_navigation', root).append(b_i + b_s).css('z-index' , t_i + 4);
					
					for(i = 0; i < t_i; i++) {
						$('.pk_slider_thumbs', root).append('<a href="#" title="" class="pk_slider_navigation_button">' + (i + 1) + '</a>');
					}

					$('.pk_slider_thumbs', root).css({
						'margin-left' : (root.width() / 2) - ($('.pk_slider_thumbs', root).outerWidth() / 2) + 'px'
					});
					$('.pk_slider_thumbs a', root).each(function(i) {
						$(this).click(function(e) {
							e.preventDefault();
								
							if(is_loading || i == id) {
								return true;	
							}

							id = i;
							
							methods.load();
						});
					});
					
					$('.pk_button_prev, .pk_button_next', root).css('z-index' , t_i + 2);
					$('.pk_button_prev', root).click(function(e) {
						e.preventDefault();
						
						var index = id;
						(index - 1 < 0) ? index = t_i - 1 : index = index - 1;
						$('.pk_slider_thumbs a:eq(' + index + ')', root).trigger('click');
					});

					$('.pk_button_next', root).click(function(e) {
						e.preventDefault();
						
						var index = id;
						(index + 1 > t_i - 1) ? index = 0 : index = index + 1;
						$('.pk_slider_thumbs a:eq(' + index + ')', root).trigger('click');
					});
					
					$('.pk_slider_info_button', root).click(function() {
						if(!is_info) {
							$(this).text(settings.buttonInfoCloseLabel);
							
							methods.stopSlideshow();
							methods.showInfo();
						} else {
							$(this).text(settings.buttonInfoOpenLabel);
							
							methods.startSlideshow();
							methods.hideInfo();
						}
					});
					
					$('.pk_button_slideshow', root).click(function() {
						if(!is_slideshow) {
							is_slideshow = true;
							
							$(this).addClass('pk_paused');
							methods.startSlideshow();
						} else {
							is_slideshow = false;
							
							$(this).removeClass('pk_paused');
							methods.stopSlideshow();
						}
					}).show();

					if(is_slideshow) { 
						$('.pk_button_slideshow', root).addClass('pk_paused');
					}
				},
				
				updateNavigation : function() {
					var n_i = $('.pk_slider_item:eq(' + id + ')', root);
						
					$('.pk_slider_navigation_button', root).each(function() {
						if($(this).hasClass('pk_selected_button')) {
							$(this).removeClass('pk_selected_button');
						}
					});
					$('.pk_slider_navigation_button:eq(' + id + ')', root).addClass('pk_selected_button');
				},
					
				
				/*
					INFO
				---------------------------------------------------------------------------------------------------------------------*/
					
				showInfo : function() {
					var c_i = $('.pk_slider_item:eq(' + id + ')', root),
						c_i_i = $('.pk_slider_item_info', c_i);
					
					if(!c_i_i.length) {
						return;
					}
					
					is_info = true;
					
					c_i_i.fadeIn(settings.speed);
				},
					
				hideInfo : function() {
					is_info = false;
					$('.pk_slider_item_info', root).fadeOut(settings.speed);		
				},
				
				
				/*
					EVENTS
				---------------------------------------------------------------------------------------------------------------------*/
				
				startSlideshow : function() {
					if(!is_slideshow) {
						return;
					}

					methods.stopSlideshow();
					
					interval = setTimeout(function() {
						$('.pk_button_next', root).trigger('click');
					}, (settings.slideshowInterval * 1000));
				},
				
				stopSlideshow : function() {
					clearTimeout(interval);
				},
					
				showLoader : function() {
					root.append('<span class="pk_loader" />');
					$('.pk_loader', root).stop(true, true).hide().fadeIn(400, 'easeInOutExpo');	
				},
					
				hideLoader : function() {
					$('.pk_loader', root).remove();	
				},
				
				
				/*
					EVENTS
				---------------------------------------------------------------------------------------------------------------------*/
				
				mouseEnter : function() {
					if(root.width() > 395 && !pk_detect_mobile()) {
						$('.pk_button_prev', root).show().stop().animate({
							'left' : '0'
						}, 800, 'easeOutExpo');
						
						$('.pk_button_next', root).show().stop().animate({
							'right' : '0'
						}, 800, 'easeOutExpo');
					}
				},
				
				mouseLeave : function(e) {
					if(root.width() > 395 && !pk_detect_mobile()) {
						$('.pk_button_prev', root).stop().animate({
							'left' : '-60px'
						}, 800, 'easeOutExpo', function() {
							$(this).hide();
						});
						
						$('.pk_button_next', root).stop().animate({
							'right' : '-60px'
						}, 800, 'easeOutExpo', function() {
							$(this).hide();
						});
					}
				},
					
				wipeLeft : function() {
					$('.pk_button_next', root).trigger('click');
				},
					
				wipeRight : function() {
					$('.pk_button_prev', root).trigger('click');
				}
			}
				
			methods.init();
		});
	};
})(jQuery);