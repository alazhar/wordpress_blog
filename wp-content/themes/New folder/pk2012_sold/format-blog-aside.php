
					<article id="post-<?php the_ID(); ?>" <?php post_class('pk_entry pk_entry_blog pk_entry_aside pk_boxed pk_full_content'.((is_singular()) ? ' pk_entry_single' : '')); ?>>
						<div class="pk_entry_content">
							<?php add_filter('embed_defaults', 'pk_post_embed_defaults'); the_content(); ?>
							<footer>
<?php
	if (!is_singular()) : 
?>
								<a href="<?php the_permalink(); ?>" title="<?php _e('Permalink', 'pk_translate'); ?>" class="pk_button_read_more"><?php _e('Permalink', 'pk_translate'); ?></a>
<?php
	else : 
?>
								<?php the_tags('<p>'.__('tags:', 'pk_translate').' </p><ul class="pk_entry_tags"><li>','</li><li>','</li></ul>'); ?>
<?php
	endif;
?>

							</footer>
						</div>
					</article>
