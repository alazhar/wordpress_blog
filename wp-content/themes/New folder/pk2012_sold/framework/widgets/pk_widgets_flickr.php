<?php

class PK_Widget_Flickr extends WP_Widget {

	function PK_Widget_Flickr() {
		
		$widget_ops=array('classname' => 'widget_flickr', 'description' => __('Display your Flickr photos feed', 'pk_translate'));	
		$this -> WP_Widget('pk-flickr', __('Flickr', 'pk_translate'), $widget_ops);
		
	}
	
	function widget($args, $instance) {
		
		extract($args);
		
		$title = apply_filters('widget_title', $instance['title']);
		
		if (empty($title)) $title = false;
		
		$number = absint($instance['number']);
		$open_with_lightbox = (isset($instance['open_with_lightbox']) && $instance['open_with_lightbox']) ? 1 : 0;
		$feed = $instance['feed'];
		
		if (empty($feed)) return;
		if ($feed != '') $feed .= '&format=rss_200';
		
		require_once(ABSPATH.WPINC.'/feed.php');
		
		echo '<!-- pk start pk-flickr widget -->
'.$before_widget.'
	';
		
		if ($title) {
			
			echo $before_title;
			echo $title;
			echo $after_title;
			
		}
		
		$transients = get_option('pk_flickr_transients');
		$transients[] = $args['widget_id'];
		update_option('pk_flickr_transients', array_unique($transients));
		
		$cache = get_transient($args['widget_id']);
		
		if ($cache) {
			
			echo $cache;
			return;
			
		} else {
			
			$feed = fetch_feed($feed);
			
		}
		
		if ($feed != '' && !is_wp_error($feed)) : 
			
			$items = $feed -> get_items(0, $number);
			
			ob_start();
?>

	<div class="flickr_widget">
<?php
			foreach ($items as $item) : $item_media = $item -> get_enclosures();
				
				if ($open_with_lightbox) : 
?>
		<a class="flickr_image_<?php echo $args['widget_id']; ?>" href="<?php echo esc_attr($item_media[0] -> link); ?>" title="<?php echo $item -> get_title(); ?>">
			<img src="<?php echo esc_attr(str_replace(array('_t.','_m.','_z.'), '_s.', $item_media[0] -> thumbnails[0])); ?>" />
		</a>
<?php
				else : 
?>
		<a href="<?php echo esc_attr($item -> get_permalink()); ?>" title="<?php echo $item -> get_title(); ?>">
			<img src="<?php echo esc_attr(str_replace(array('_t.','_m.','_z.'), '_s.', $item_media[0] -> thumbnails[0])); ?>" />
		</a>
<?php
				endif;
				
			endforeach;
?>
	</div>
	<script type="text/javascript">
		jQuery(document).ready(function() {
			jQuery(".flickr_image_<?php echo $args['widget_id']; ?>").attr('rel', 'prettyPhoto[flickr_widget_<?php echo $args['widget_id']; ?>]').prettyPhoto(<?php echo PK_PRETTYPHOTO_PARAMS; ?>);
		});
	</script>
<?php
		else :
			
			echo '<p>'.__('The Flickr feed is either empty or unavailable. Please check back later.', 'pk_translate').'</p>';
			
		endif;
		
		echo $after_widget.'
<!-- pk end pk-flickr widget -->

';
		
		set_transient($args['widget_id'], ob_get_flush(), 3600);
		
	}
	
	function update($new_instance, $old_instance) {
		
		$instance = $old_instance;
		
		$instance['title'] = strip_tags($new_instance['title']);
		$instance['number'] = absint($new_instance['number']);
		$instance['open_with_lightbox'] = $new_instance['open_with_lightbox'] ? 1 : 0;
		$instance['feed'] = $new_instance['feed'];
		
		$transients = get_option('pk_flickr_transients');
		
		foreach ($transients as $transient) {
			
			delete_transient($transient);
			
		}
		
		return $instance;
		
	}
	
	function form($instance) {
		
		$title = isset($instance['title']) ? esc_attr($instance['title']) : '';
		$number = isset($instance['number']) ? absint($instance['number']) : 16;
		$open_with_lightbox = (isset($instance['open_with_lightbox']) && !empty($instance['open_with_lightbox'])) ? 'checked="checked"' : '';
		$feed = isset($instance['feed']) ? esc_attr($instance['feed']) : '';
?>
		<p><label for="<?php echo $this -> get_field_id('title'); ?>"><?php _e('Title:', 'pk_translate'); ?></label>
		<input class="widefat" id="<?php echo $this -> get_field_id('title'); ?>" name="<?php echo $this -> get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" /></p>
		
		<p><label for="<?php echo $this -> get_field_id('number'); ?>"><?php _e('Number of thumbnails to show:', 'pk_translate'); ?></label>
		<input class="widefat" id="<?php echo $this -> get_field_id('number'); ?>" name="<?php echo $this -> get_field_name('number'); ?>" type="text" value="<?php echo $number; ?>" /></p>
		
		<p><input class="checkbox" type="checkbox" <?php echo $open_with_lightbox; ?> id="<?php echo $this -> get_field_id('open_with_lightbox'); ?>" name="<?php echo $this -> get_field_name('open_with_lightbox'); ?>" />
		<label for="<?php echo $this -> get_field_id('open_with_lightbox'); ?>"><?php _e('Open photos with lightbox', 'pk_translate'); ?></label><br />
		
		<p><label for="<?php echo $this -> get_field_id('feed'); ?>"><?php _e('Flickr RSS2 feed URL:', 'pk_translate', 'pk_translate'); ?></label>
		<input class="widefat" id="<?php echo $this -> get_field_id('feed'); ?>" name="<?php echo $this -> get_field_name('feed'); ?>" type="text" value="<?php echo $feed; ?>" /></p>
<?php
	}
	
}

function pk_widgets_flickr() {
	
	register_widget('PK_Widget_Flickr');
	
}

add_action('widgets_init', 'pk_widgets_flickr');

?>