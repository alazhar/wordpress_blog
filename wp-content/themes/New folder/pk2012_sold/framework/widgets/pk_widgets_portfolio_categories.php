<?php

class PK_Widget_Portfolio_Categories extends WP_Widget {
	
	function PK_Widget_Portfolio_Categories() {
		
		$widget_ops = array('classname' => 'widget_portfolio_categories', 'description' => __('A list or dropdown of portfolio categories', 'pk_translate'));
		$this -> WP_Widget('portfolio-categories', __('Portfolio Categories', 'pk_translate'), $widget_ops);
		
	}
	
	function widget($args, $instance) {
		
		extract($args);
		
		$title = apply_filters('widget_title', $instance['title']);
		$c = $instance['count'] ? '1' : '0';
		$h = $instance['hierarchical'] ? '1' : '0';
		$d = $instance['dropdown'] ? '1' : '0';
		
		echo '<!-- pk start portfolio-categories widget -->
'.$before_widget.'
	';
		
		if ($title) {
			
			echo $before_title;
			echo $title;
			echo $after_title;
			
		}
		
		$taxonomies_args = array('orderby' => 'name', 'show_count' => $c, 'hierarchical' => $h, 'taxonomy' => 'taxonomy_portfolio', 'echo' => 0);
		
		if ($d) {
			
			$terms = get_terms($taxonomy, $taxonomies_args);
			
			echo '
	<select id="taxonomy_select_'.$widget_id.'" name="taxonomy_select_'.$widget_id.'" class="postform">';
				echo '
		<option value="">'.__('Select Category', 'pk_translate').'</option>';
				
			foreach ($terms as $term) {
				
				echo '
		<option value="'.get_term_link($term -> slug, $taxonomy).'">'.$term -> name.'</option>';
				
			}
			
			echo '
	</select>
';
?>
	<script type='text/javascript'>
	/* <![CDATA[ */
		document.getElementById("taxonomy_select_<?php echo $widget_id; ?>").onchange = function() {
			location.href = this.options[this.selectedIndex].value;
		}
	/* ]]> */
	</script>
<?php
		} else {
?>

	<ul>
<?php
$taxonomies_args['title_li'] = '';
echo str_replace(array("\n</a>", "\n</li>", "<li"), array("</a>", "</li>", "\t<li"), wp_list_categories(apply_filters('widget_categories_args', $taxonomies_args)));
?>
	</ul>
<?php
		}
		
		echo $after_widget.'
<!-- pk end portfolio-categories widget -->

';
		
	}
	
	function update($new_instance, $old_instance) {
		
		$instance = $old_instance;
		
		$instance['title'] = strip_tags($new_instance['title']);
		$instance['count'] = !empty($new_instance['count']) ? 1 : 0;
		$instance['hierarchical'] = !empty($new_instance['hierarchical']) ? 1 : 0;
		$instance['dropdown'] = !empty($new_instance['dropdown']) ? 1 : 0;
		
		return $instance;
		
	}
	
	function form($instance) {
		
		$instance = wp_parse_args((array) $instance, array('title' => ''));
		$title = esc_attr($instance['title']);
		$count = isset($instance['count']) ? (bool) $instance['count'] : false;
		$hierarchical = isset($instance['hierarchical']) ? (bool) $instance['hierarchical'] : false;
		$dropdown = isset($instance['dropdown']) ? (bool) $instance['dropdown'] : false;
?>
		<p><label for="<?php echo $this -> get_field_id('title'); ?>"><?php _e('Title:', 'pk_translate'); ?></label>
		<input class="widefat" id="<?php echo $this -> get_field_id('title'); ?>" name="<?php echo $this -> get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" /></p>
		
		<p><input type="checkbox" class="checkbox" id="<?php echo $this -> get_field_id('dropdown'); ?>" name="<?php echo $this -> get_field_name('dropdown'); ?>"<?php checked($dropdown); ?> />
		<label for="<?php echo $this -> get_field_id('dropdown'); ?>"><?php _e('Display as dropdown', 'pk_translate'); ?></label><br />
		<input type="checkbox" class="checkbox" id="<?php echo $this -> get_field_id('count'); ?>" name="<?php echo $this -> get_field_name('count'); ?>"<?php checked($count); ?> />
		<label for="<?php echo $this -> get_field_id('count'); ?>"><?php _e('Show post counts', 'pk_translate'); ?></label><br />
		<input type="checkbox" class="checkbox" id="<?php echo $this -> get_field_id('hierarchical', 'pk_translate'); ?>" name="<?php echo $this -> get_field_name('hierarchical'); ?>"<?php checked($hierarchical); ?> />
		<label for="<?php echo $this -> get_field_id('hierarchical'); ?>"><?php _e('Show hierarchy', 'pk_translate'); ?></label></p>
<?php
	}
	
}

function pk_widgets_taxonomies() {
	
	register_widget('PK_Widget_Portfolio_Categories');
	
}

add_action('widgets_init', 'pk_widgets_taxonomies');

?>