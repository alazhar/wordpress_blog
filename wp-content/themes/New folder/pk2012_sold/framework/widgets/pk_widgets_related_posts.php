<?php

class PK_Widget_Related_Posts extends WP_Widget {
	
	function PK_Widget_Related_Posts() {
		
		$widget_ops = array('classname' => 'widget_related_posts', 'description' => __('Displays the related posts', 'pk_translate'));
		$this -> WP_Widget('pk-related-posts', __('Related Posts', 'pk_translate'), $widget_ops);
		$this -> alt_option_name = 'widget_related_posts';

		add_action('save_post', array(&$this, 'flush_widget_cache'));
		add_action('deleted_post', array(&$this, 'flush_widget_cache'));
		add_action('switch_theme', array(&$this, 'flush_widget_cache'));
		
	}
	
	function widget($args, $instance) {
		
		$cache = wp_cache_get('widget_related_posts', 'widget');
		
		$title = apply_filters('widget_title', $instance['title']);
		
		if (empty($title)) $title = false;
		
		$number = absint($instance['number']);

		if (!is_array($cache)) {
			
			$cache = array();
			
		}

		if (isset($cache[$args['widget_id']])) {
			
			echo $cache[$args['widget_id']];
			return;
			
		}
		
		ob_start();
		extract($args);
		
		global $post;
		
		if (isset($post) && $post -> post_type == 'post') {
			
			$tags = wp_get_post_tags($post -> ID);
			$tag_ids = array();
			
		}
		
		if (isset($tags) && $tags) {
			
			$tagcount = count($tags);
			
			for ($i = 0; $i < $tagcount; $i++) {
				
				$tag_ids[$i] = $tags[$i] -> term_id;
				
			}
			
			$r = new WP_Query(array('post_type' => $post -> post_type, 'posts_per_page' => $number, 'tag__in' => $tag_ids, 'post__not_in' => array($post -> ID), 'nopaging' => 0, 'post_status' => 'publish', 'ignore_sticky_posts' => 1, 'orderby' => 'comment_count', 'order' => 'DESC'));
			
			if ($r -> have_posts()) : 
				
				echo '<!-- pk start pk-related-posts widget -->
'.$before_widget.'
	';
			
			if ($title) echo $before_title.$title.$after_title;
?>

	<ul>
<?php
				while ($r -> have_posts()) : 
					
					$r -> the_post();
?>
		<li<?php if (!has_post_thumbnail()) echo ' class="pk_no_thumbnail"'; ?>>
			<?php if (has_post_thumbnail()) the_post_thumbnail('thumb'); ?>

			<div>
				<h5><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h5>
				<?php add_filter('excerpt_length', 'pk_widgets_excerpt_filter'); add_filter('excerpt_more', 'pk_excerpt_more'); the_excerpt(); ?>
			</div>
		</li>
<?php				
				endwhile;
?>
	</ul>
<?php
			echo $after_widget.'
<!-- pk end pk-related-posts widget -->

';
			
			endif;
			
			wp_reset_postdata();
			
			$cache[$args['widget_id']] = ob_get_flush();
			
			wp_cache_set('widget_related_posts', $cache, 'widget');
			
		}
		
	}
	
	function update($new_instance, $old_instance) {
		
		$instance = $old_instance;
		
		$instance['title'] = strip_tags($new_instance['title']);
		$instance['number'] = (int) $new_instance['number'];
		
		$this -> flush_widget_cache();
		
		$alloptions = wp_cache_get('alloptions', 'options');
		
		if (isset($alloptions['widget_related_posts'])) {
			
			delete_option('widget_related_posts');
			
		}
		
		return $instance;
		
	}

	function flush_widget_cache() {
		
		wp_cache_delete('widget_related_posts', 'widget');
		
	}
	
	function form($instance) {
		
		$title = isset($instance['title']) ? esc_attr($instance['title']) : '';
		$number = isset($instance['number']) ? absint($instance['number']) : 5;
		$list_icon = (isset($instance['list_icon']) && !empty($instance['list_icon'])) ? $instance['list_icon'] : '';
?>
		<p><label for="<?php echo $this -> get_field_id('title'); ?>"><?php _e('Title:', 'pk_translate'); ?></label>
		<input class="widefat" id="<?php echo $this -> get_field_id('title'); ?>" name="<?php echo $this -> get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" /></p>
		
		<p><label for="<?php echo $this -> get_field_id('number'); ?>"><?php _e('Number of posts to show:', 'pk_translate'); ?></label>
		<input class="widefat" id="<?php echo $this -> get_field_id('number'); ?>" name="<?php echo $this -> get_field_name('number'); ?>" type="text" value="<?php echo $number; ?>" size="3" /></p>
<?php
	}
	
}

function pk_widgets_related_posts() {
	
	register_widget('PK_Widget_Related_Posts');
	
}

add_action('widgets_init', 'pk_widgets_related_posts');

?>