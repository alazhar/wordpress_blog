<?php

class PK_Widget_Dribbble extends WP_Widget {
	
	function PK_Widget_Dribbble(){
		
		$widget_ops = array('classname' => 'widget_dribbble', 'description' => __('Display your Dribbble updates', 'pk_translate'));
		$this -> WP_Widget('pk-dribbble', __('Dribbble', 'pk_translate'), $widget_ops);
		
	}
	
	function widget($args, $instance) {
		
		extract($args);
		
		$title = apply_filters('widget_title', $instance['title']);
		
		if (empty($title)) $title = false;
		
		$username = (isset($instance['username'])) ? urlencode($instance['username']) : '';
		
		if (empty($username)) return;
		
		$number = (isset($instance['number'])) ? absint($instance['number']) : 3;
		
		$total = 0;
		
		echo '<!-- pk start pk-dribbble widget -->
'.$before_widget.'
	';
		
		if ($title) {
			
			echo $before_title;
			echo $title;
			echo $after_title;
			
		}
		
		require_once(ABSPATH.WPINC.'/feed.php');
		
		$dribbble_feed_url = "http://dribbble.com/$username/shots.rss";
		
		$transients = get_option('pk_dribbble_transients');
		$transients[] = $args['widget_id'];
		update_option('pk_dribbble_transients', array_unique($transients));
		
		$cache = get_transient($args['widget_id']);
		
		if ($cache) {
			
			echo $cache;
			return;
			
		} else {
			
			$feed = fetch_feed($dribbble_feed_url);
			
		}
		
		if (!is_wp_error($feed)) {
			
			$total = $feed -> get_item_quantity($number);
			$items = $feed -> get_items(0, $total);
			
		}
		
		ob_start();
		
		if ($total == 0) : 
			
			echo '<p>'.__('The Dribble feed is either empty or unavailable. Please check back later.', 'pk_translate').'</p>';
			
		else : 
?>

	<ul>
<?php		
			foreach($items as $item) : 
				
				$author = $item -> get_author();
				$text = $item -> get_title();
				$link = $item -> get_permalink();
				$time = $item -> get_date('d F, Y');
				$description = $item -> get_description();
				
				preg_match("/src=\"(http.*(jpg|jpeg|gif|png))/", $description, $image_url);
				$image = $image_url[1];
?>
		<li>
<?php
				if ($image) : 
?>	
			<a href="<?php echo $link; ?>" title="<?php echo $text; ?>" rel="external nofollow">
				<img src="<?php echo $image; ?>" style="width:100%;" />
			</a>
			<h5><?php echo $text; ?></h5>
			<small><?php echo __('By:', 'pk_translate').' '.$username.' . '.$time; ?></small>
<?php
				endif;
?>	
		</li>
<?php
			endforeach;
?>
	</ul>
<?php			
		endif;

		echo $after_widget.'
<!-- pk end pk-dribbble widget -->

';
		
		set_transient($args['widget_id'], ob_get_flush(), 3600);
		
	}
	
	function update($new_instance, $old_instance) {
		
		$instance = $old_instance;
		
		$instance['title'] = strip_tags($new_instance['title']);
		$instance['username'] = $new_instance['username'];
		$instance['number'] = absint($new_instance['number']);
		
		$transients = get_option('pk_dribbble_transients');
		
		foreach ($transients as $transient) {
			
			delete_transient($transient);
			
		}
		
		return $instance;
		
	}
	
	function form($instance) {
		
		$title = isset($instance['title']) ? esc_attr($instance['title']) : '';
		$username = (isset($instance['username'])) ? $instance['username'] : '';
		$number = (isset($instance['number'])) ? absint($instance['number']) : 3;
?>
		<p><label for="<?php echo $this -> get_field_id('title'); ?>"><?php _e('Title:', 'pk_translate'); ?></label>
		<input class="widefat" id="<?php echo $this -> get_field_id('title'); ?>" name="<?php echo $this -> get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" /></p>
		
		<p><label for="<?php echo $this -> get_field_id('username'); ?>"><?php _e('Dribbble username:', 'pk_translate'); ?></label>
		<input class="widefat" id="<?php echo $this -> get_field_id('username'); ?>" name="<?php echo $this -> get_field_name('username'); ?>" type="text" value="<?php echo $username; ?>" /></p>
		
		<p><label for="<?php echo $this -> get_field_id('number'); ?>"><?php _e('Number of updates to show:', 'pk_translate'); ?></label>
		<input class="widefat" id="<?php echo $this -> get_field_id('number'); ?>" name="<?php echo $this -> get_field_name('number'); ?>" type="text" value="<?php echo $number; ?>" /></p>
<?php
	}
	
}

function pk_widgets_dribbble() {
	
	register_widget('PK_Widget_Dribbble');
	
}

add_action('widgets_init', 'pk_widgets_dribbble');

?>