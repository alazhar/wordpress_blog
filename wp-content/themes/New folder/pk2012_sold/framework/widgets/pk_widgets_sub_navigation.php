<?php

class PK_Widget_Sub_Navigation extends WP_Widget {

	function PK_Widget_Sub_Navigation() {
		
		$widget_ops = array('classname' => 'widget_sub_navigation', 'description' => __('Displays sub navigation', 'pk_translate'));
		$this -> WP_Widget('pk-sub-navigation',__('Sub Navigation', 'pk_translate'), $widget_ops);
		
	}
	
	function widget($args, $instance) {
		
		extract($args);
		
		global $post;
		
		if (!isset($post)) return;
		
		$children = wp_list_pages('echo=0&child_of=' . $post -> ID . '&title_li=');
		
		if ($children) {
			
			$parent = $post -> ID;
			
		} else {
			
			$parent = $post -> post_parent;
			
			if (!$parent) {
				
				$parent = $post -> ID;
				
			}
			
		}
		
		$parent_title = get_the_title($parent);
		
		$title = apply_filters('widget_title', empty($instance['title']) ? $parent_title : $instance['title'], $instance, $this -> id_base);
		$sortby = empty($instance['sortby']) ? 'menu_order' : $instance['sortby'];
		$exclude = empty($instance['exclude']) ? '' : $instance['exclude'];
		
		$output = wp_list_pages(array('title_li' => '', 'echo' => 0, 'child_of' => $parent, 'sort_column' => $sortby, 'exclude' => $exclude, 'depth' => 1));
		
		if (!empty($output)) {
			
			echo '<!-- pk start pk-sub-navigation widget -->
'.$before_widget.'
	';
			
			if ($title) {
				
				echo $before_title;
				echo $title;
				echo $after_title;
				
			}
?>

	<ul>
<?php
echo str_replace("<li", "\t\t<li", $output);
?>
	</ul>
<?php
			echo $after_widget.'
<!-- pk end pk-sub-navigation widget -->

';
			
		}
		
	}
	
	function update($new_instance, $old_instance) {
		
		$instance = $old_instance;
		
		$instance['title'] = strip_tags($new_instance['title']);
		
		if (in_array($new_instance['sortby'], array('post_title', 'menu_order', 'ID'))) {
			
			$instance['sortby'] = $new_instance['sortby'];
			
		} else {
			
			$instance['sortby'] = 'menu_order';
			
		}
		
		$instance['exclude'] = strip_tags($new_instance['exclude']);
		
		return $instance;
		
	}
	
	function form($instance) {
		
		$instance = wp_parse_args((array) $instance, array('sortby' => 'menu_order', 'title' => '', 'exclude' => ''));
		$title = esc_attr($instance['title']);
		$exclude = esc_attr($instance['exclude']);
?>
		<p><label for="<?php echo $this -> get_field_id('title'); ?>"><?php _e('Title:', 'pk_translate'); ?></label>
		<input class="widefat" id="<?php echo $this -> get_field_id('title'); ?>" name="<?php echo $this -> get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" /></p>
		
		<p>
			<label for="<?php echo $this -> get_field_id('sortby'); ?>"><?php _e('Sort by:', 'pk_translate'); ?></label>
			<select name="<?php echo $this -> get_field_name('sortby'); ?>" id="<?php echo $this -> get_field_id('sortby'); ?>" class="widefat">
				<option value="menu_order"<?php selected($instance['sortby'], 'menu_order'); ?>><?php _e('Menu order', 'pk_translate'); ?></option>
				<option value="post_title"<?php selected($instance['sortby'], 'post_title'); ?>><?php _e('Page title', 'pk_translate'); ?></option>
				<option value="ID"<?php selected($instance['sortby'], 'ID'); ?>><?php _e('Page ID', 'pk_translate'); ?></option>
			</select>
		</p>
		
		<p>
			<label for="<?php echo $this -> get_field_id('exclude'); ?>"><?php _e('Exclude:', 'pk_translate'); ?></label>
			<input class="widefat" id="<?php echo $this -> get_field_id('exclude'); ?>" name="<?php echo $this -> get_field_name('exclude'); ?>" type="text" value="<?php echo $exclude; ?>" />
			<br /><small><?php _e('Page IDs, separated by commas.' ,'pk_translate'); ?></small>
		</p>
<?php
	}
	
}

function pk_widgets_sub_navigation() {
	
	register_widget('PK_Widget_Sub_Navigation');
	
}

add_action('widgets_init', 'pk_widgets_sub_navigation');