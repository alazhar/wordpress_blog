<?php

class PK_Widget_Twitter extends WP_Widget {
	
	function PK_Widget_Twitter(){
		
		$widget_ops = array('classname' => 'widget_twitter', 'description' => __('Display your Twitter updates', 'pk_translate'));
		$this -> WP_Widget('pk-twitter', __('Twitter', 'pk_translate'), $widget_ops);
		
	}
	
	function widget($args, $instance) {
		
		extract($args);
		
		$title = apply_filters('widget_title', $instance['title']);
		
		if (empty($title)) $title = false;
		
		$username = (isset($instance['username'])) ? urlencode($instance['username']) : '';
		
		if (empty($username)) return;
		
		$number = (isset($instance['number'])) ? absint($instance['number']) : 5;
		
		$show_follow = ($instance['show_follow'] == 1) ? true : false;
		
		$total = 0;
		
		echo '<!-- pk start pk-twitter widget -->
'.$before_widget.'
	';
		
		if ($title) {
			
			echo $before_title;
			echo $title;
			echo $after_title;
			
		}
		
		require_once(ABSPATH.WPINC.'/feed.php');
		
		$twitter_feed_url = "http://api.twitter.com/1/statuses/user_timeline.rss?screen_name=$username";
		
		$transients = get_option('pk_twitter_transients');
		$transients[] = $args['widget_id'];
		update_option('pk_twitter_transients', array_unique($transients));
		
		$cache = get_transient($args['widget_id']);
		
		if ($cache) {
			
			echo $cache;
			return;
			
		} else {
			
			$feed = fetch_feed($twitter_feed_url);
			
		}
		
		if (!is_wp_error($feed)) {
			
			$total = $feed -> get_item_quantity($number);
			$items = $feed -> get_items(0, $total);
			
		}
		
		ob_start();
		
		if ($total == 0) : 
			
			echo '<p>'.__('The Twitter feed is either empty or unavailable. Please check back later.', 'pk_translate').'</p>';
			
		else:
?>

	<ul>
<?php
			foreach ($items as $item) : 
				
				$text = $item -> get_title();
				$link = $item -> get_permalink();
				$time = $item -> get_date();
				
				$text = strstr($text, ': ');
				$text = substr($text, 2);
				$text = make_clickable($text);
				$text = preg_replace_callback('/(^|\s)@(\w+)/', array($this, '_widget_twitter_username'), $text);
				$text = preg_replace_callback('/(^|\s)#(\w+)/', array($this, '_widget_twitter_hashtag'), $text);
				
				$time = str_replace(' ', '&nbsp;', $time);
?>
		<li><?php echo $text; ?> <small><a href="<?php echo $link; ?>" rel="external nofollow"><?php echo $time; ?></a></small></li>
<?php
			endforeach;
		
			if ($show_follow) : 
?>
		<li><a class="follow" href="http://twitter.com/#!/<?php echo $username ?>"><?php printf(__('Follow @%s on Twitter', 'pk_translate'), $username); ?></a></li>
<?php
			endif;
?>
	</ul>
<?php
		endif;
		
		echo $after_widget.'
<!-- pk end pk-twitter widget -->

';
		
		set_transient($args['widget_id'], ob_get_flush(), 3600);
		
	}
	
	function update($new_instance, $old_instance) {
		
		$instance = $old_instance;
		
		$instance['title'] = strip_tags($new_instance['title']);
		$instance['username'] = $new_instance['username'];
		$instance['number'] = $new_instance['number'];
		$instance['show_follow'] = isset($new_instance['show_follow']) ? 1 : 0;
		
		$transients = get_option('pk_twitter_transients');
		
		foreach ($transients as $transient) {
			
			delete_transient($transient);
			
		}
		
		return $instance;
		
	}
	
	function _widget_twitter_username($matches) {
		
		return "$matches[1]@<a href='".esc_url('http://twitter.com/'.urlencode($matches[2]))."'>$matches[2]</a>";
		
	}
	
	function _widget_twitter_hashtag($matches) {
		
		return "$matches[1]<a href='".esc_url('http://search.twitter.com/search?q=%23'.urlencode($matches[2]))."'>#$matches[2]</a>";
		
	}
	
	function form($instance) {
		
		$title = isset($instance['title']) ? esc_attr($instance['title']) : '';
		$username = (isset($instance['username'])) ? $instance['username'] : '';
		$number = (isset($instance['number'])) ? absint($instance['number']) : 5;
		$show_follow = isset($instance['show_follow']) ? 1 : 0;
?>
		<p><label for="<?php echo $this -> get_field_id('title'); ?>"><?php _e('Title:', 'pk_translate'); ?></label>
		<input class="widefat" id="<?php echo $this -> get_field_id('title'); ?>" name="<?php echo $this -> get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" /></p>
		
		<p><label for="<?php echo $this -> get_field_id('username'); ?>"><?php _e('Twitter username:', 'pk_translate'); ?></label>
		<input class="widefat" id="<?php echo $this -> get_field_id('username'); ?>" name="<?php echo $this -> get_field_name('username'); ?>" type="text" value="<?php echo $username; ?>" /></p>
		
		<p><label for="<?php echo $this -> get_field_id('number'); ?>"><?php _e('Number of tweets to show:', 'pk_translate'); ?></label>
		<input class="widefat" id="<?php echo $this -> get_field_id('number'); ?>" name="<?php echo $this -> get_field_name('number'); ?>" type="text" value="<?php echo $number; ?>" /></p>
		
		<p><input class="checkbox" type="checkbox" <?php checked($show_follow, true) ?> id="<?php echo $this -> get_field_id('show_follow'); ?>" name="<?php echo $this -> get_field_name('show_follow'); ?>" />
		<label for="<?php echo $this -> get_field_id('show_follow'); ?>"><?php _e('Show follow link', 'pk_translate'); ?></label></p>
<?php
	}
	
}

function pk_widgets_twitter() {
	
	register_widget('PK_Widget_Twitter');
	
}

add_action('widgets_init', 'pk_widgets_twitter');

?>