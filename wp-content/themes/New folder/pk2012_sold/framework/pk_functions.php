<?php

function pk_get_option($name, $default = false) {
	
	global $option_tree;
	
	$option = (isset($option_tree[$name])) ? $option_tree[$name] : $default;
	
	if (isset($option) && $option != '') {
		
		return (is_array($option)) ? $option : stripslashes($option);
		
	} else {
		
		return $default;
		
	}
	
}

function pk_custom_login_logo() {
	
	echo '
	<style type="text/css">
		.login h1 a { width: 100%; height:'.pk_get_option('logo_height', '60').'px; background-size:'.pk_get_option('logo_width', '130').'px '.pk_get_option('logo_height', '60').'px; background-image:url('.pk_get_option('logo', PK_THEME_DIR.'/images/logo.png').') !important; }
	</style>';
	
}

add_action('login_head', 'pk_custom_login_logo');

function pk_title() {
	
	$separator = stripslashes(pk_get_option('title_separator', '|'));
	
	wp_title($separator, true, 'right');
	
	if (!is_404()) {
		
		if ((int)get_query_var('paged') > 1) { 
		
			echo __('Page', 'pk_translate').' '.get_query_var('paged').' '.$separator.' ';
			
		}
		
		if ((int)get_query_var('page') > 1) { 
		
			echo __('Page', 'pk_translate').' '.get_query_var('page').' '.$separator.' ';
			
		}
		
		global $cpage;
		
		if ($cpage >= 1) { 
		
			echo __('Comments Page', 'pk_translate').' '.$cpage.' '.$separator.' ';
			
		}
		
	}
	
	bloginfo('name');
	
}

function pk_set_up_menu_message() {
	
	if (current_user_can('edit_posts')) : 
?>
<nav><ul class="menu"><li><a class="menu-item" href="<?php echo get_home_url().'/wp-admin/nav-menus.php'; ?>"><?php _e('Set up the menu', 'pk_translate'); ?></a></li></ul></nav>
<?php
	endif;
	
}

function pk_set_up_footer_menu_message() {
	
	if (current_user_can('edit_posts')) : 
?>
<nav class="pk_footer_menu"><ul class="menu"><li><a class="menu-item" href="<?php echo get_home_url().'/wp-admin/nav-menus.php'; ?>"><?php _e('Set up the menu', 'pk_translate'); ?></a></li></ul></nav>
<?php
	endif;
	
}

function pk_pagination($pages = '', $paged = 1, $range = 2) {
	
	$showitems = ($range * 2) + 1;
	
	if ($pages == '') {
		
		global $wp_query;
		
		$pages = $wp_query -> max_num_pages;
		
		if(!$pages) {
			
			$pages = 1;
			
		}
		
	}
	
	if (1 != $pages) {
		
		if ($paged > 1) {
			
			previous_posts_link(__('&laquo;', 'pk_translate'));
			
		} else {
			
			echo '<a>'.__('&laquo;', 'pk_translate').'</a>';
			
		}
		
		$c = 1;
		
		for ($i = 1; $i <= $pages; $i++) {
			
			if (1 != $pages && (!($i >= $paged + $range + 1 || $i <= $paged - $range - 1) || $pages <= $showitems)) {
				
				echo ($paged == $i) ? '<a class="pk_current_page">'.$i.'</a>' : '<a href="'.esc_url(get_pagenum_link($i)).'">'.$i.'</a>';
				
				$c++;
				
			}
			
		}
		
		if ($paged < $pages) {
			
			next_posts_link(__('&raquo;', 'pk_translate'));
			
		} else {
			
			echo '<a>'.__('&raquo;', 'pk_translate').'</a>';
			
		}
		
	}
	
}

function pk_sidebar() {
	
	$force_sidebar = pk_get_option('force_sidebar', 'no forcing');
	if ($force_sidebar != 'no forcing') : return ($force_sidebar == 'show') ? true : false; endif;
	
	if (is_404()) : return (pk_get_option('404_sidebar', 'show') == 'show') ? true : false; endif;
	
	if (is_search()) : return (pk_get_option('search_sidebar', 'show') == 'show') ? true : false; endif;
	
	if (is_singular(array('post', 'portfolio', 'product')) || is_page_template('page-blog.php') || is_page_template('page-portfolio.php')) : 
		
		global $post;
		
		$single_sidebar = get_post_meta($post -> ID, '_sidebar', true);
		if (isset($single_sidebar) && $single_sidebar == 'hide') : return false; else : return true; endif;
		
	endif;
	
	if ((is_home() || is_archive()) && !is_tax() && !is_post_type_archive() && pk_get_option('blog_sidebar', 'show') == 'hide') return false;
	
	if (pk_is_portfolio()) : return (pk_get_option('portfolio_sidebar', 'hide') == 'hide') ? false : true; endif;
	
	if (function_exists('is_bbpress') && is_bbpress()) : return (pk_get_option('bbpress_forum_sidebar', 'hide') == 'hide') ? false : true; endif;
	
	if ((function_exists('is_product_category') || function_exists('is_product_tag') || function_exists('is_shop')) && (is_product_category() || is_product_tag() || is_shop())) return (pk_get_option('shop_sidebar', 'show') == 'hide') ? false : true;
	
	return (is_page_template('page-home-agency.php') || is_page_template('page-home-business.php') || is_page_template('page-home-freelance.php') || is_page_template('page-full-width.php') || is_page_template('page-faq.php') || (function_exists('is_cart') && is_cart())) ? false : true;
	
}

function pk_is_portfolio() {
	
	return (is_post_type_archive('portfolio') || is_tax('taxonomy_portfolio')) ? true : false;
	
}

function pk_feed_lifetime($seconds) {
	
	return 3600;
	
}

function pk_any_feed($feed = '', $total_items = 1) {
	
	if ($feed == '') $feed = get_bloginfo('rss_url');
	
	include_once(ABSPATH.WPINC.'/feed.php');
	
	add_filter('wp_feed_cache_transient_lifetime', 'pk_feed_lifetime');
	$rss = fetch_feed(esc_url($feed));
	remove_filter('wp_feed_cache_transient_lifetime', 'pk_feed_lifetime');
	
	if (!is_wp_error($rss)) : 
		
		$maxitems = $rss -> get_item_quantity($total_items);
		$rss_items = $rss -> get_items(0, $maxitems);
		$output = '<ul>';
		
		foreach ($rss_items as $item) : 
			
			$output .= '<li>'.$item -> get_description().' <a href="'.$item -> get_permalink().'" title="'.__('Read more', 'pk_translate').'" class="pk_button_read_more" target="_blank">'.__('Read more', 'pk_translate').'</a></li>';
			
		endforeach; 
		
		$output .= '</ul>';
		
		return $output;
		
	else : 
	
		return '';
		
	endif;
	
}

function pk_blog_categories_filter() {
	
?>

					<div class="pk_categories_filter">
						<ul>
							<li><a href="<?php echo (get_option('show_on_front') == 'page') ? get_permalink((function_exists('icl_object_id')) ? icl_object_id(get_option('page_for_posts'), 'page', true) : get_option('page_for_posts')) : ((function_exists('icl_get_home_url')) ? icl_get_home_url() : home_url());?>" title="<?php _e('All', 'pk_translate'); ?>"<?php if (is_home()) echo ' class="pk_current_category"'; ?>><?php _e('All', 'pk_translate'); ?></a></li>
<?php
	$categories = get_categories(array('hierarchical' => false, 'taxonomy' => 'category'));
	
	foreach ($categories as $category) : 
?>
							<li><a href="<?php echo get_term_link($category -> slug, 'category'); ?>" title="<?php echo $category -> cat_name; ?>"<?php if (get_query_var('cat') == $category -> term_id) echo ' class="pk_current_category"'; ?>><?php echo $category -> cat_name; ?></a></li>
<?php
	endforeach;
?>
						</ul>
					</div>
<?php
	
}

add_action('pk_blog_categories', 'pk_blog_categories_filter');

function pk_portfolio_categories_filter() {
	
?>

					<div class="pk_categories_filter">
						<ul>
<?php
	if (pk_get_option('portfolio_page_id', '') != '') : 
?>
							<li><a href="<?php echo get_permalink((function_exists('icl_object_id')) ? icl_object_id(pk_get_option('portfolio_page_id', ''), 'page', true) : pk_get_option('portfolio_page_id', '')); ?>" title="<?php _e('All', 'pk_translate'); ?>"<?php if (!is_tax() && get_the_ID() == ((function_exists('icl_object_id')) ? icl_object_id(pk_get_option('portfolio_page_id', ''), 'page', true) : pk_get_option('portfolio_page_id', ''))) echo ' class="pk_current_category"'; ?>><?php _e('All', 'pk_translate'); ?></a></li>
<?php
	endif;
	
	$categories = get_categories(array('hierarchical' => false, 'taxonomy' => 'taxonomy_portfolio'));
	
	foreach ($categories as $category) : 
?>
							<li><a href="<?php echo get_term_link($category -> slug, 'taxonomy_portfolio'); ?>" title="<?php echo $category -> cat_name; ?>"<?php if (get_query_var('term') == $category -> slug) echo ' class="pk_current_category"'; ?>><?php echo $category -> cat_name; ?></a></li>
<?php
	endforeach;
?>
						</ul>
					</div>
<?php
	
}

add_action('pk_portfolio_categories', 'pk_portfolio_categories_filter');

?>