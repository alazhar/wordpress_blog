<?php

require_once(PK_FRAMEWORK.'/shortcodes/pk_shortcodes_manager_generator.php');
require_once(PK_FRAMEWORK.'/shortcodes/pk_shortcodes_manager.php');

function pk_enqueue_shortcodes_scripts() {
	
	wp_enqueue_script('jquery');
	
	wp_deregister_script('pk_rangeinput');
	wp_register_script('pk_rangeinput', PK_THEME_DIR.'/framework/shortcodes/js/pk_rangeinput.js');
	wp_enqueue_script('pk_rangeinput');
	
	wp_deregister_script('jscolor');
	wp_register_script('jscolor', PK_THEME_DIR.'/framework/shortcodes/js/jscolor/jscolor.js');
	wp_enqueue_script('jscolor');
	
	wp_deregister_script('pk_shortcodes_manager');
	wp_register_script('pk_shortcodes_manager', PK_THEME_DIR.'/framework/shortcodes/js/pk_shortcodes_manager.js');
	wp_enqueue_script('pk_shortcodes_manager');
	
	wp_deregister_style('pk_shortcodes_manager');
	wp_register_style('pk_shortcodes_manager', PK_THEME_DIR.'/framework/shortcodes/css/pk_shortcodes_manager.css');
	wp_enqueue_style('pk_shortcodes_manager');
	
}

add_action('admin_enqueue_scripts', 'pk_enqueue_shortcodes_scripts');

$pk_shortcodes = array();

$pk_shortcodes['title'] = __('Generator', 'pk_translate');

$pk_shortcodes['type'][] = 'selector';
$pk_shortcodes['options'][] = array(
										
										'shortcode_type' => array('title' => __('Select:', 'pk_translate'), 'type' => 'metabox_selector', 
									
										'values' => array(
										'pk_sc_selector',
										'pk_sc_box',
										'pk_sc_message_box',
										'pk_sc_buttons',
										'pk_sc_columns',
										'pk_sc_dividers',
										'pk_sc_google_maps',
										'pk_sc_pricing_tables',
										'pk_sc_tabs',
										'pk_sc_toggles',
										'pk_sc_typography_hu',
										'pk_sc_typography_hb',
										'pk_sc_typography_quote',
										'pk_sc_typography_lists',
										'pk_sc_typography_hl',
										'pk_sc_typography_dc'),
									
										'labels' => array(
										__('-- Select --', 'pk_translate'),
										__('Box', 'pk_translate'),
										__('Box (styled)', 'pk_translate'),
										__('Buttons', 'pk_translate'),
										__('Columns', 'pk_translate'),
										__('Dividers', 'pk_translate'),
										__('Google Maps', 'pk_translate'),
										__('Pricing Tables', 'pk_translate'),
										__('Tabs', 'pk_translate'),
										__('Toggles', 'pk_translate'),
										__('Typography (heading underline)', 'pk_translate'),
										__('Typography (heading background)', 'pk_translate'),
										__('Typography (quote)', 'pk_translate'),
										__('Typography (lists)', 'pk_translate'),
										__('Typography (highlight)', 'pk_translate'),
										__('Typography (drop caps)', 'pk_translate')),
									
										'helper' => __('Select the HTML element that you want to set up and generate. <strong>Be sure to generate the HTML elements within the HTML tab of the editor.</strong>', 'pk_translate'))
										
										);
$pk_shortcodes['values'][] = array(
										
										'shortcode_type' => 'pk_shortcodes_table_selector'
										
										);
										
/*
 * Box
*/

$pk_shortcodes['type'][] = 'box';
$pk_shortcodes['options'][] = array(
									
										'box_width' 	=> array('title' => __('Width:', 'pk_translate'), 'type' => 'slider', 'min' => '0', 'max' => '940', 'uom' => 'px', 'helper' => __('Set the box width. Set <i>0</i> for a full width box.', 'pk_translate')),
										'box_content' 	=> array('title' => __('Content:', 'pk_translate'), 'type' => 'text_area', 'rows' => '10', 'helper' => __('Type here the box content. You can leave this field empty and type the content directly in the main editor.', 'pk_translate'))
										
										);
$pk_shortcodes['values'][] = array(
									
										'box_width' 	=> '0',
										'box_content' 	=> ''
										
										);
										
/*
 * Box (message)
*/

$pk_shortcodes['type'][] = 'message_box';
$pk_shortcodes['options'][] = array(
									
										'message_box_width' 	=> array('title' => __('Width:', 'pk_translate'), 'type' => 'slider', 'min' => '0', 'max' => '940', 'uom' => 'px', 'helper' => __('Set the box width. Set <i>0</i> for a full width box.', 'pk_translate')),
										'message_box_type' 		=> array('title' => __('Message type:', 'pk_translate'), 'type' => 'select', 'values' => array('info', 'note', 'success', 'error', 'warning', 'important', 'help'), 'labels' => array(__('Info', 'pk_translate'), __('Note', 'pk_translate'), __('Success', 'pk_translate'), __('Error', 'pk_translate'), __('Warning', 'pk_translate'), __('Important', 'pk_translate'), __('Help', 'pk_translate')), 'helper' => __('Select the message type for the current box. Each message type will output a specific box style.', 'pk_translate')),
										'message_box_content' 	=> array('title' => __('Content:', 'pk_translate'), 'type' => 'text_area', 'rows' => '6', 'helper' => __('Type here the box content. You can leave this field empty and type the content directly in the main editor.', 'pk_translate'))
										
										);
$pk_shortcodes['values'][] = array(
									
										'message_box_width' 	=> '0',
										'message_box_type' 		=> 'info',
										'message_box_content' 	=> ''
										
										);
										
/*
 * Buttons
*/

$pk_shortcodes['type'][] = 'buttons';
$pk_shortcodes['options'][] = array(
									
										'buttons_label' 		=> array('title' => __('Label:', 'pk_translate'), 'type' => 'text', 'helper' => __('Type here the button label.', 'pk_translate')),
										'buttons_size' 			=> array('title' => __('Size:', 'pk_translate'), 'type' => 'select', 'values' => array('mini', 'small', 'medium', 'big'), 'labels' => array(__('Mini', 'pk_translate'), __('Small', 'pk_translate'), __('Medium', 'pk_translate'), __('Big', 'pk_translate')), 'helper' => __('Select a button size.', 'pk_translate')),
										'buttons_color' 		=> array('title' => __('Color:', 'pk_translate'), 'type' => 'select', 'values' => array('white', 'grey', 'black', 'lime', 'orange', 'yellow', 'green', 'blue', 'purple', 'red'), 'labels' => array(__('White', 'pk_translate'), __('Grey', 'pk_translate'), __('Black', 'pk_translate'), __('Lime', 'pk_translate'), __('Orange', 'pk_translate'), __('Yellow', 'pk_translate'), __('Green', 'pk_translate'), __('Blue', 'pk_translate'), __('Purple', 'pk_translate'), __('Red', 'pk_translate')), 'helper' => __('Select the button color.', 'pk_translate')),
										'buttons_icon' 			=> array('title' => __('Icon:', 'pk_translate'), 'type' => 'select', 'values' => array('', 'arrow', 'download', 'home', 'mail', 'mini_arrow', 'zoom'), 'labels' => array(__('No Icon', 'pk_translate'), __('Arrow Icon', 'pk_translate'), __('Download Icon', 'pk_translate'), __('Home Icon', 'pk_translate'), __('Mail Icon', 'pk_translate'), __('Mini Arrow Icon', 'pk_translate'), __('Zoom Icon', 'pk_translate')), 'helper' => __('Select the icon type to add to the button.', 'pk_translate')),
										'buttons_link' 			=> array('title' => __('Link:', 'pk_translate'), 'type' => 'text', 'helper' => __('Type here the link to open by clicking on this button.', 'pk_translate')),
										'buttons_link_target' 	=> array('title' => __('Link target:', 'pk_translate'), 'type' => 'select', 'values' => array('_self', '_blank'), 'labels' => array(__('Self', 'pk_translate'), __('Blank', 'pk_translate')), 'helper' => __('Select the window target for the link. Select <i>Self</i> to open the link in the current browser window, select <i>Blank</i> to open the link in a new browser window.', 'pk_translate')),
										'buttons_title' 		=> array('title' => __('Link tag title:', 'pk_translate'), 'type' => 'text', 'helper' => __('Type here the title attribute for the button link. The title will appear on roll over.', 'pk_translate'))
										
										);
$pk_shortcodes['values'][] = array(
										
										'buttons_label' 		=> '',
										'buttons_size' 			=> 'small',
										'buttons_color' 		=> '',
										'buttons_icon' 			=> '',
										'buttons_link' 			=> '#',
										'buttons_link_target' 	=> '_self',
										'buttons_title' 		=> ''
										
										);
										
/*
 * Columns
*/

$pk_shortcodes['type'][] = 'columns';
$pk_shortcodes['options'][] = array(
									
										'columns_size' 		=> array('title' => __('Column size:', 'pk_translate'), 'type' => 'select', 'values' => array('pk_full_width', 'pk_one_half', 'pk_one_third', 'pk_one_fourth', 'pk_one_fifth', 'pk_one_sixth', 'pk_two_third', 'pk_three_fourth', 'pk_two_fifth', 'pk_three_fifth', 'pk_four_fifth', 'pk_five_sixth', 'pk_one_half pk_last', 'pk_one_third pk_last', 'pk_one_fourth pk_last', 'pk_one_fifth pk_last', 'pk_one_sixth pk_last', 'pk_two_third pk_last', 'pk_three_fourth pk_last', 'pk_two_fifth pk_last', 'pk_three_fifth pk_last', 'pk_four_fifth pk_last', 'pk_five_sixth pk_last'), 'labels' => array('Column Full Width', 'Column 1/2', 'Column 1/3', 'Column 1/4', 'Column 1/5', 'Column 1/6', 'Column 2/3', 'Column 3/4', 'Column 2/5', 'Column 3/5', 'Column 4/5', 'Column 5/6', 'Column 1/2 Last', 'Column 1/3 Last', 'Column 1/4 Last', 'Column 1/5 Last', 'Column 1/6 Last', 'Column 2/3 Last', 'Column 3/4 Last', 'Column 2/5 Last', 'Column 3/5 Last', 'Column 4/5 Last', 'Column 5/6 Last'), 'helper' => __('Select the column size that you want to generate. Remember that the last column of each row must be a <i>Last</i> column.', 'pk_translate')),
										'columns_content' 	=> array('title' => __('Content:', 'pk_translate'), 'type' => 'text_area', 'rows' => '10', 'helper' => __('Enter here the column content. You can leave this field empty and type the content directly in the main editor.', 'pk_translate'))
										
										);
$pk_shortcodes['values'][] = array(
									
										'columns_size' 		=> '',
										'columns_content' 	=> '<p>Column HTML contents go here or leave this field empty and put your contents directly in the main editor.</p>'
										
										);
										
/*
 * Dividers
*/

$pk_shortcodes['type'][] = 'dividers';
$pk_shortcodes['options'][] = array(
										
										'dividers_type' 	=> array('title' => __('Divider type:', 'pk_translate'), 'type' => 'select', 'values' => array('pk_divider', 'pk_divider_top', 'pk_clear_both', 'pk_empty_space'), 'labels' => array('Divider', 'Dvider Top', 'Clear Both', 'Empty Space'), 'helper' => __('Select the divider type that you want to use.', 'pk_translate')),
										'dividers_height' 	=> array('title' => __('Height:', 'pk_translate'), 'type' => 'slider', 'min' => '0', 'max' => '200', 'uom' => 'px', 'helper' => __('The height attribute is used by the <i>Empty Space</i> divider only.', 'pk_translate'))
										
										);
$pk_shortcodes['values'][] = array(
										
										'dividers_type' 	=> '',
										'dividers_height' 	=> '30'
										
										);
										
/*
 * Google Maps
*/

$pk_shortcodes['type'][] = 'google_maps';
$pk_shortcodes['options'][] = array(
										
										'google_maps_map_url' 		=> array('title' => __('Map URL:', 'pk_translate'), 'type' => 'text', 'helper' => __('Type here the URL of the map that you want to load.', 'pk_translate')),
										'google_maps_map_height' 	=> array('title' => __('Map height:', 'pk_translate'), 'type' => 'slider', 'min' => '0', 'max' => '1000', 'uom' => 'px', 'helper' => __('Set the height of the map.', 'pk_translate')),
										
										
										);
$pk_shortcodes['values'][] = array(
										
										'google_maps_map_url' 		=> '',
										'google_maps_map_height' 	=> '400'
										
										);
										
/*
 * Pricing Tables
*/

$pk_shortcodes['type'][] = 'pricing_tables';
$pk_shortcodes['options'][] = array(
										
										'pricing_tables_layout_columns' 	=> array('title' => __('Table layout (columns):', 'pk_translate'), 'type' => 'slider', 'min' => '2', 'max' => '5', 'uom' => 'columns', 'helper' => __('Set the number of columns.', 'pk_translate')),
										'pricing_tables_layout_rows' 		=> array('title' => __('Table layout (rows):', 'pk_translate'), 'type' => 'slider', 'min' => '1', 'max' => '100', 'uom' => 'rows', 'helper' => __('Set the number of rows for each column.', 'pk_translate')),
										'pricing_tables_highlight' 			=> array('title' => __('Highlighted column:', 'pk_translate'), 'type' => 'slider', 'min' => '0', 'max' => '5', 'uom' => '', 'helper' => __('Select the column that you want to highlight. Set 0 if you don\'t want to highlight any column.', 'pk_translate')),
										'pricing_tables_highlight_color' 	=> array('title' => __('Highlight color:', 'pk_translate'), 'type' => 'select', 'values' => array(' pk_highlight_white', ' pk_highlight_grey', ' pk_highlight_black', ' pk_highlight_lime', ' pk_highlight_orange', ' pk_highlight_yellow', ' pk_highlight_green', ' pk_highlight_blue', ' pk_highlight_purple', ' pk_highlight_red'), 'labels' => array(__('White', 'pk_translate'), __('Grey', 'pk_translate'), __('Black', 'pk_translate'), __('Lime', 'pk_translate'), __('Orange', 'pk_translate'), __('Yellow', 'pk_translate'), __('Green', 'pk_translate'), __('Blue', 'pk_translate'), __('Purple', 'pk_translate'), __('Red', 'pk_translate')), 'helper' => __('Select the drop cap color.', 'pk_translate'))
										
										);
$pk_shortcodes['values'][] = array(
										
										'pricing_tables_layout_columns' 	=> '2',
										'pricing_tables_layout_rows' 		=> '5',
										'pricing_tables_highlight' 			=> '0',
										'pricing_tables_highlight_color' 	=> ''
										
										);
										
/*
 * Tabs
*/

$pk_shortcodes['type'][] = 'tabs';
$pk_shortcodes['options'][] = array(
										
										'tabs_title' 	=> array('title' => __('Title:', 'pk_translate'), 'type' => 'text', 'helper' => __('Type here a general title for the tabs.', 'pk_translate')),
										'tabs_width' 	=> array('title' => __('Width:', 'pk_translate'), 'type' => 'slider', 'min' => '0', 'max' => '940', 'uom' => 'px', 'helper' => __('Set the tabs width. Set <i>0</i> for full width tabs.', 'pk_translate')),
										'tabs_number' 	=> array('title' => __('Number of tabs:', 'pk_translate'), 'type' => 'slider', 'min' => '1', 'max' => '30', 'uom' => __('tabs', 'pk_translate'), 'helper' => __('Select the number of tabs that you need. You will create the tabs contents directly in the main editor.', 'pk_translate'))
										
										);
$pk_shortcodes['values'][] = array(
										'tabs_title' 	=> '',
										'tabs_width' 	=> '0',
										'tabs_number' 	=> '3'
										
										);
										
/*
 * Toggles
*/

$pk_shortcodes['type'][] = 'toggles';
$pk_shortcodes['options'][] = array(
										
										'toggles_width' 	=> array('title' => __('Width:', 'pk_translate'), 'type' => 'slider', 'min' => '0', 'max' => '940', 'uom' => 'px', 'helper' => __('Set the toggles width. Set <i>0</i> for full width toggles.', 'pk_translate')),
										'toggles_accordion' => array('title' => __('Accordion style:', 'pk_translate'), 'type' => 'select', 'values' => array(' pk_accordion', ''), 'labels' => array(__('Yes', 'pk_translate'), __('No', 'pk_translate')), 'helper' => __('Select <i>Yes</i> to have the toggles working like an accordion.', 'pk_translate')),
										'toggles_number' 	=> array('title' => __('Number of toggles:', 'pk_translate'), 'type' => 'slider', 'min' => '1', 'max' => '30', 'uom' => __('toggles', 'pk_translate'), 'helper' => __('Select the number of toggles that you need. You will create the toggles contents directly in the main editor.', 'pk_translate'))
										
										);
$pk_shortcodes['values'][] = array(
										
										'toggles_width' 	=> '0',
										'toggles_accordion' => '',
										'toggles_number' 	=> '3'
										
										);
										
/*
 * Typography (heading underline)
*/

$pk_shortcodes['type'][] = 'typography_hu';
$pk_shortcodes['options'][] = array(
										
										'typography_hu_size' 	=> array('title' => __('Heading size:', 'pk_translate'), 'type' => 'select', 'values' => array('h1', 'h2', 'h3', 'h4', 'h5', 'h6'), 'labels' => array('h1', 'h2', 'h3', 'h4', 'h5', 'h6'), 'helper' => __('Select the heading size.', 'pk_translate')),
										'typography_hu_heading' => array('title' => __('Heading text:', 'pk_translate'), 'type' => 'text_area', 'rows' => '3', 'helper' => __('Type here the heading text.', 'pk_translate'))
										
										);
$pk_shortcodes['values'][] = array(
										
										'typography_hu_size' 	=> 'h1',
										'typography_hu_heading' => 'This is an heading!'
										
										);
										
/*
 * Typography (heading background)
*/

$pk_shortcodes['type'][] = 'typography_hb';
$pk_shortcodes['options'][] = array(
										
										'typography_hb_color' 				=> array('title' => __('Text color:', 'pk_translate'), 'type' => 'color', 'helper' => __('Click on the color field to pick a new color.', 'pk_translate')),
										'typography_hb_background_color' 	=> array('title' => __('Background color:', 'pk_translate'), 'type' => 'color', 'helper' => __('Click on the color field to pick a new color.', 'pk_translate')),
										'typography_hb_rounded' 			=> array('title' => __('Rounded background:', 'pk_translate'), 'type' => 'select', 'values' => array(' pk_rounded', ''), 'labels' => array(__('Yes', 'pk_translate'), __('No', 'pk_translate')), 'helper' => __('Select <i>Yes</i> for rounded background.', 'pk_translate')),
										'typography_hb_size' 				=> array('title' => __('Heading size:', 'pk_translate'), 'type' => 'select', 'values' => array('h1', 'h2', 'h3', 'h4', 'h5', 'h6'), 'labels' => array('h1', 'h2', 'h3', 'h4', 'h5', 'h6'), 'helper' => __('Select the heading size.', 'pk_translate')),
										'typography_hb_heading' 			=> array('title' => __('Heading text:', 'pk_translate'), 'type' => 'text_area', 'rows' => '3', 'helper' => __('Type here the heading text.', 'pk_translate'))
										
										);
$pk_shortcodes['values'][] = array(
										
										'typography_hb_color' 				=> '',
										'typography_hb_background_color' 	=> '',
										'typography_hb_rounded' 			=> ' pk_rounded',
										'typography_hb_size' 				=> 'h1',
										'typography_hb_heading' 			=> 'This is an heading!'
										
										);
										
/*
 * Typography (quote)
*/

$pk_shortcodes['type'][] = 'typography_quote';
$pk_shortcodes['options'][] = array(
										
										'typography_quote_width' 	=> array('title' => __('Width:', 'pk_translate'), 'type' => 'slider', 'min' => '0', 'max' => '940', 'uom' => 'px', 'helper' => __('Set the quote width. Set <i>0</i> for full width quote.', 'pk_translate')),
										'typography_quote_cite' 	=> array('title' => __('Cite:', 'pk_translate'), 'type' => 'text_area', 'rows' => '3', 'helper' => __('Type here the citation text.', 'pk_translate')),
										'typography_quote_quote' 	=> array('title' => __('Quote:', 'pk_translate'), 'type' => 'text_area', 'rows' => '6', 'helper' => __('Type here the quote content. You can leave this field empty and type the content directly in the main editor.', 'pk_translate'))
										
										);
$pk_shortcodes['values'][] = array(
										
										'typography_quote_width' 	=> '0',
										'typography_quote_cite' 	=> '',
										'typography_quote_quote' 	=> ''
										
										);
										
/*
 * Typography (lists)
*/

$pk_shortcodes['type'][] = 'typography_lists';
$pk_shortcodes['options'][] = array(
										
										'typography_lists_style' 		=> array('title' => __('Style:', 'pk_translate'), 'type' => 'select', 'values' => array('pk_clear_list', 'pk_arrow_list', 'pk_check_list'), 'labels' => array(__('Clear List', 'pk_translate'), __('Arrow List', 'pk_translate'), __('Check List', 'pk_translate')), 'helper' => __('Select a list style.', 'pk_translate')),
										'typography_lists_list' 		=> array('title' => __('List markup (HTML):', 'pk_translate'), 'type' => 'text_area', 'rows' => '6', 'helper' => __('Type here the HTML markup of the list. You can leave this field empty and type the content directly in the main editor.', 'pk_translate'))
										
										);
$pk_shortcodes['values'][] = array(
										
										'typography_lists_style' 		=> 'pk_clear_list',
										'typography_lists_list' 		=> '
<ul>
<li>Your list item</li>
<li>Your list item</li>
<li>Your list item</li>
</ul>'
										
										);
										
/*
 * Typography (highlight)
*/

$pk_shortcodes['type'][] = 'typography_hl';
$pk_shortcodes['options'][] = array(
										
										'typography_hl_color' 				=> array('title' => __('Text color:', 'pk_translate'), 'type' => 'color', 'helper' => __('Click on the color field to pick a new color.', 'pk_translate')),
										'typography_hl_background_color' 	=> array('title' => __('Background color:', 'pk_translate'), 'type' => 'color', 'helper' => __('Click on the color field to pick a new color.', 'pk_translate')),
										'typography_hl_rounded' 			=> array('title' => __('Rounded background:', 'pk_translate'), 'type' => 'select', 'values' => array(' pk_rounded', ''), 'labels' => array(__('Yes', 'pk_translate'), __('No', 'pk_translate')), 'helper' => __('Select <i>Yes</i> for rounded background.', 'pk_translate')),
										'typography_hl_content' 			=> array('title' => __('Content to highlight:', 'pk_translate'), 'type' => 'text_area', 'rows' => '3', 'helper' => __('Type here the text to highlight.', 'pk_translate'))
										
										);
$pk_shortcodes['values'][] = array(
										
										'typography_hl_color' 				=> '',
										'typography_hl_background_color' 	=> '',
										'typography_hl_rounded' 			=> ' pk_rounded',
										'typography_hl_content' 			=> ''
										
										);
										
/*
 * Typography (drop caps)
*/

$pk_shortcodes['type'][] = 'typography_dc';
$pk_shortcodes['options'][] = array(
										
										'typography_dc_type' 		=> array('title' => __('Type:', 'pk_translate'), 'type' => 'select', 'values' => array('pk_drop_cap_1', 'pk_drop_cap_2'), 'labels' => array(__('Only text', 'pk_translate'), __('Text with background color', 'pk_translate')), 'helper' => __('Select the drop caps type.', 'pk_translate')),
										'typography_dc_color' 		=> array('title' => __('Background color:', 'pk_translate'), 'type' => 'select', 'values' => array(' pk_drop_cap_white', ' pk_drop_cap_grey', ' pk_drop_cap_black', ' pk_drop_cap_lime', ' pk_drop_cap_orange', ' pk_drop_cap_yellow', ' pk_drop_cap_green', ' pk_drop_cap_blue', ' pk_drop_cap_purple', ' pk_drop_cap_red'), 'labels' => array(__('White', 'pk_translate'), __('Grey', 'pk_translate'), __('Black', 'pk_translate'), __('Lime', 'pk_translate'), __('Orange', 'pk_translate'), __('Yellow', 'pk_translate'), __('Green', 'pk_translate'), __('Blue', 'pk_translate'), __('Purple', 'pk_translate'), __('Red', 'pk_translate')), 'helper' => __('Select the drop cap color.', 'pk_translate')),
										'typography_dc_text' 		=> array('title' => __('Text:', 'pk_translate'), 'type' => 'text', 'helper' => __('Type here the character to capitilize.', 'pk_translate'))
										
										);
$pk_shortcodes['values'][] = array(
										
										'typography_dc_type' 		=> 'pk_drop_cap_1',
										'typography_dc_color' 		=> '',
										'typography_dc_text' 		=> ''
										
										);

if (class_exists('pk_shortcodes_manager_generator') && !isset($pk_shortcodes_manager_generator_instance)) {
	
	$pk_shortcodes_manager_generator_instance = new pk_shortcodes_manager_generator($pk_shortcodes, 'pk_sc_');
	
}

?>