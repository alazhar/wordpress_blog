<?php

global $pk_shortcodes_manager_instance;

if (!class_exists('pk_shortcodes_manager')) {
	
	class pk_shortcodes_manager {
		
		function pk_shortcodes_manager() {
			
		}
		
		function pk_add_title($title = '', $color = true, $special_color = false) {
			
			$class = ($color == true) ? 'pk_admin_tr_color' : 'pk_admin_tr';
			$class = ($special_color == true) ? 'pk_admin_tr_special' : $class;
			
			echo '
					<tr class="'.$class.'">
						<th scope="row" class="pk_admin_th">
							<h4>'.$title.'</h4>
						</th>';
			
		}
		
		function pk_add_input_text_field($name = '', $value = '', $helper = '') {
			
			echo '
						<td class="pk_admin_td">
							<p><input class="pk_admin_input_text_field" type="text" name="'.$name.'" value="'.$value.'" /></p>
							<p class="pk_admin_helper"><strong class="pk_admin_helper_strong">? </strong>'.$helper.'</p>
						</td>
					</tr>';
			
		}
		
		function pk_add_input_text_area($name = '', $value = '', $rows = '6', $helper = '') {
			
			echo '
						<td class="pk_admin_td">
							<p><textarea rows="'.$rows.'" cols="60" class="pk_admin_input_text_area" name="'.$name.'">'.$value.'</textarea></p>
							<p class="pk_admin_helper"><strong class="pk_admin_helper_strong">? </strong>'.$helper.'</p>
						</td>
					</tr>';
			
		}
		
		function pk_add_input_radio_group($name = '', $values = array(), $labels = array(), $checked = '', $helper = '') {
			
			echo '
						<td class="pk_admin_td">';
			
			for ($i = 0; $i < count($values); $i++) {
				
				if ($values[$i] == $checked) {
					
					$checked_string = ' checked="checked"';
					
				} else {
					
					$checked_string = '';
					
				}
				
				echo '
							<p><input id="'.$name.'_'.$i.'" class="pk_admin_radio_checkbox" type="radio" name="'.$name.'" value="'.$values[$i].'"'.$checked_string.' /><label for="'.$name.'_'.$i.'" class="pk_admin_radio_label"> '.$labels[$i].'</label></p>';
				
			}
			
			echo '
							<p class="pk_admin_helper"><strong class="pk_admin_helper_strong">? </strong>'.$helper.'</p>
						</td>
					</tr>';
			
		}
		
		function pk_add_input_select($name = '', $select_box = false, $values = array(), $labels = array(), $selected = '', $helper = '') {
			
			($select_box == true) ? $class = 'pk_admin_input_select_box' : $class = 'pk_admin_input_select';
			
			echo '
						<td class="pk_admin_td">
							<p>
								<select class="'.$class.'" name="'.$name.'">';
			
			for ($i = 0; $i < count($values); $i++) {
				
				if ($values[$i] == $selected) {
					
					$selected_string = ' selected="selected"';
					
				} else {
					
					$selected_string = '';
					
				}
				
				echo '
									<option value="'.$values[$i].'"'.$selected_string.'>'.$labels[$i].'</option>';
				
			}
			
			echo '
								</select>
							</p>
							<p class="pk_admin_helper"><strong class="pk_admin_helper_strong">? </strong>'.$helper.'</p>
						</td>
					</tr>';
			
		}
		
		function pk_add_input_color($name = '', $value = '', $helper = '') {
			
			echo '
						<td class="pk_admin_td">
							<p><input class="pk_admin_input_color color {hash:true, caps:false, pickerPosition:\'right\', pickerMode:\'HSV\', pickerFace:15, pickerFaceColor:\'#eeeeee\', pickerBorderColor:\'#d6d6d6\', pickerInsetColor:\'#f3f3f3\'}" type="text" name="'.$name.'" value="'.$value.'" /></p>
							<p class="pk_admin_helper"><strong class="pk_admin_helper_strong">? </strong>'.$helper.'</p>
						</td>
					</tr>';
			
		}
		
		function pk_add_input_slider($name = '', $value = '0', $min = '0', $max = '100', $uom = 'px', $helper = '') {
			
			echo '
						<td class="pk_admin_td">
							<div class="pk_admin_slider_div">
								<p><input class="pk_admin_input_slider" type="text" name="'.$name.'" value="'.$value.'" min="'.$min.'" max="'.$max.'"'.(($max == 1) ? ' step="0.01"' : '').' /> '.$uom.'</p>
							</div>
							<p class="pk_admin_helper"><strong class="pk_admin_helper_strong">? </strong>'.$helper.'</p>
						</td>
					</tr>';
			
		}
		
		function pk_add_input_category($name = '', $value = '', $taxonomy = 'category', $helper = '') {
			
			$categories = get_categories(array('hide_empty' => 1, 'hierarchical' => false, 'taxonomy' => trim($taxonomy)));
			
			if (!$categories) {
				
				echo '
						<td class="pk_admin_td">
							<p><strong>'.__('No categories available', 'pk_translate').'</strong></p>
							<p class="pk_admin_helper"><strong class="pk_admin_helper_strong">? </strong>'.$helper.'</p>
						</td>
					</tr>';
				
				return;
				
			}
			
			$values = array('');
			$labels = array(__('-- Select --', 'pk_translate'));
					
			foreach ($categories as $category) {
				
				array_push($values, $category -> term_id);
				array_push($labels, $category -> name);
				
			}
			
			$this -> pk_add_input_select($name, false, $values, $labels, $value, $helper);
			
		}
		
		function pk_add_input_categories($name = '', $value = '', $taxonomy = 'category', $helper = '') {
			
			$categories = get_categories(array('hide_empty' => 1, 'hierarchical' => false, 'taxonomy' => trim($taxonomy)));
			
			if (!$categories) {
				
				echo '
						<td class="pk_admin_td">
							<p><strong>'.__('No categories available', 'pk_translate').'</strong></p>
							<p class="pk_admin_helper"><strong class="pk_admin_helper_strong">? </strong>'.$helper.'</p>
						</td>
					</tr>';
				
				return;
				
			}
			
			$selected_categories = explode(',', $value);
			
			$total = count($categories);
			
			if ($total <= 12) {
					
				echo '
						<td class="pk_admin_td">
							<input class="pk_admin_multiple_field" type="hidden" name="'.$name.'" value="'.$value.'" />
							<div class="pk_admin_multiple_checkbox_div_wrapper">';
				
				$checkboxes_count = 0;
				
				foreach ($categories as $category) {
					
					$checked = '';
					
					if (in_array($category -> term_id, $selected_categories)) {
					
						$checked = ' checked="checked"';
					
					}
					
					echo '
								<div class="pk_admin_multiple_checkbox_div"><input data-main="'.$name.'" id="'.$name.'_checkbox_'.$checkboxes_count.'" class="pk_admin_multiple_checkbox pk_admin_radio_checkbox" type="checkbox" value="'.$category -> term_id.'"'.$checked.' /><label for="'.$name.'_checkbox_'.$checkboxes_count.'" class="pk_admin_multiple_checkbox_label">'.$category -> name.'</label></div>';
					
					$checkboxes_count++;
					
				}
				
				echo '
							</div>
							<p class="pk_admin_helper"><strong class="pk_admin_helper_strong">? </strong>'.$helper.'</p>
						</td>
					</tr>';
				
			} else {
				
				echo '
						<td class="pk_admin_td">
							<input class="pk_admin_multiple_field" type="hidden" name="'.$name.'" value="'.$value.'" />
							<select data-main="'.$name.'" class="pk_admin_input_select_multiple" multiple="multiple">';
				
				foreach ($categories as $category) {
					
					if (in_array($category -> term_id, $selected_categories)) {
					
						$selected_string = ' selected="selected"';
					
					} else {
					
						$selected_string = '';
					
					}
					
					echo '
									<option value="'.$category -> term_id.'"'.$selected_string.'>'.$category -> name.' ('.$category -> category_count.')</option>';
					
				}
				
				echo '
							</select>
							<p class="pk_admin_helper"><strong class="pk_admin_helper_strong">? </strong>'.$helper.'</p>
						</td>
					</tr>';
				
			}
			
		}
		
		function pk_add_input_page($name = '', $value = '', $post_type = 'page', $helper = '') {
			
			$pages = get_pages(array('hierarchical' => false, 'post_type' => trim($post_type)));
			
			if (!$pages) {
				
				echo '
						<td class="pk_admin_td">
							<p><strong>'.__('No pages available', 'pk_translate').'</strong></p>
							<p class="pk_admin_helper"><strong class="pk_admin_helper_strong">? </strong>'.$helper.'</p>
						</td>
					</tr>';
				
				return;
				
			}
			
			$values = array('');
			$labels = array(__('-- Select --', 'pk_translate'));
			
			foreach ($pages as $page) {
				
				array_push($values, $page -> ID);
				array_push($labels, $page -> post_title);
				
			}
			
			$this -> pk_add_input_select($name, false, $values, $labels, $value, $helper);
			
		}
		
		function pk_add_input_pages($name = '', $value = '', $post_type = 'page', $helper = '') {
			
			$pages = get_pages(array('hierarchical' => false, 'post_type' => trim($post_type)));
			
			if (!$pages) {
				
				echo '
						<td class="pk_admin_td">
							<p><strong>'.__('No pages available', 'pk_translate').'</strong></p>
							<p class="pk_admin_helper"><strong class="pk_admin_helper_strong">? </strong>'.$helper.'</p>
						</td>
					</tr>';
				
				return;
				
			}
			
			$selected_pages = explode(',', $value);
			
			$total = count($pages);
			
			if ($total <= 12) {
				
				echo '
						<td class="pk_admin_td">
							<input class="pk_admin_multiple_field" type="hidden" name="'.$name.'" value="'.$value.'" />
							<div class="pk_admin_multiple_checkbox_div_wrapper">';
				
				$checkboxes_count = 0;
				
				foreach ($pages as $page) {
					
					$checked = '';
					
					if (in_array($page -> ID, $selected_pages)) {
						
						$checked = ' checked="checked"';
						
					}
					
					echo '
								<div class="pk_admin_multiple_checkbox_div"><input data-main="'.$name.'" id="'.$name.'_checkbox_'.$checkboxes_count.'" class="pk_admin_multiple_checkbox pk_admin_radio_checkbox" type="checkbox" value="'.$page -> ID.'"'.$checked.' /><label for="'.$name.'_checkbox_'.$checkboxes_count.'" class="pk_admin_multiple_checkbox_label">'.$page -> post_title.'</label></div>';
					
					$checkboxes_count++;
					
				}
				
				echo '
							</div>
							<p class="pk_admin_helper"><strong class="pk_admin_helper_strong">? </strong>'.$helper.'</p>
						</td>
					</tr>';
				
			} else {
				
				echo '
						<td class="pk_admin_td">
							<input class="pk_admin_multiple_field" type="hidden" name="'.$name.'" value="'.$value.'" />
							<select data-main="'.$name.'" class="pk_admin_input_select_multiple" multiple="multiple">';
				
				foreach ($pages as $page) {
					
					if (in_array($page -> ID, $selected_pages)) {
						
						$selected_string = ' selected="selected"';
						
					} else {
						
						$selected_string = '';
						
					}
					
					echo '
									<option value="'.$page -> ID.'"'.$selected_string.'>'.$page -> post_title.'</option>';
					
				}
				
				echo '
							</select>
							<p class="pk_admin_helper"><strong class="pk_admin_helper_strong">? </strong>'.$helper.'</p>
						</td>
					</tr>';
				
			}
			
		}
		
		function pk_add_input_post($name = '', $value = '', $post_type = 'post', $helper = '') {
			
			$posts = get_posts(array('post_type' => trim($post_type), 'posts_per_page' => -1, 'orderby' => 'title', 'order' => 'ASC'));
			
			if (!$posts) {
				
				echo '
						<td class="pk_admin_td">
							<p><strong>'.__('No posts available', 'pk_translate').'</strong></p>
							<p class="pk_admin_helper"><strong class="pk_admin_helper_strong">? </strong>'.$helper.'</p>
						</td>
					</tr>';
				
				return;
				
			}
			
			$values = array('');
			$labels = array(__('-- Select --', 'pk_translate'));
			
			foreach ($posts as $post) {
				
				array_push($values, $post -> ID);
				array_push($labels, $post -> post_title);
				
			}
			
			$this -> pk_add_input_select($name, false, $values, $labels, $value, $helper);
			
		}
		
		function pk_add_input_posts($name = '', $value = '', $post_type = 'post', $helper = '') {
			
			$posts = get_posts(array('post_type' => trim($post_type), 'posts_per_page' => -1, 'orderby' => 'title', 'order' => 'ASC'));
			
			if (!$posts) {
				
				echo '
						<td class="pk_admin_td">
							<p><strong>'.__('No posts available', 'pk_translate').'</strong></p>
							<p class="pk_admin_helper"><strong class="pk_admin_helper_strong">? </strong>'.$helper.'</p>
						</td>
					</tr>';
				
				return;
				
			}
			
			$selected_posts = explode(',', $value);
			
			$total = count($posts);
			
			if ($total <= 12) {
				
				echo '
						<td class="pk_admin_td">
							<input class="pk_admin_multiple_field" type="hidden" name="'.$name.'" value="'.$value.'" />
							<div class="pk_admin_multiple_checkbox_div_wrapper">';
				
				$checkboxes_count = 0;
				
				foreach ($posts as $post) {
					
					$checked = '';
					
					if (in_array($post -> ID, $selected_posts)) {
						
						$checked = ' checked="checked"';
						
					}
					
					echo '
								<div class="pk_admin_multiple_checkbox_div"><input data-main="'.$name.'" id="'.$name.'_checkbox_'.$checkboxes_count.'" class="pk_admin_multiple_checkbox pk_admin_radio_checkbox" type="checkbox" value="'.$post -> ID.'"'.$checked.' /><label for="'.$name.'_checkbox_'.$checkboxes_count.'" class="pk_admin_multiple_checkbox_label">'.$post -> post_title.'</label></div>';
					
					$checkboxes_count++;
					
				}
				
				echo '
							</div>
							<p class="pk_admin_helper"><strong class="pk_admin_helper_strong">? </strong>'.$helper.'</p>
						</td>
					</tr>';
				
			} else {
				
				echo '
						<td class="pk_admin_td">
							<input class="pk_admin_multiple_field" type="hidden" name="'.$name.'" value="'.$value.'" />
							<select data-main="'.$name.'" class="pk_admin_input_select_multiple" multiple="multiple">';
				
				foreach ($posts as $post) {
					
					if (in_array($post -> ID, $selected_posts)) {
						
						$selected_string = ' selected="selected"';
						
					} else {
						
						$selected_string = '';
						
					}
					
					echo '
									<option value="'.$post -> ID.'"'.$selected_string.'>'.$post -> post_title.'</option>';
					
				}
				
				echo '
							</select>
							<p class="pk_admin_helper"><strong class="pk_admin_helper_strong">? </strong>'.$helper.'</p>
						</td>
					</tr>';
				
			}
			
		}
		
		function pk_add_input_tag($name = '', $value = '', $helper = '') {
			
			$tags = get_tags(array('hide_empty' => false));
			
			if (!$tags) {
				
				echo '
						<td class="pk_admin_td">
							<p><strong>'.__('No tags available', 'pk_translate').'</strong></p>
							<p class="pk_admin_helper"><strong class="pk_admin_helper_strong">? </strong>'.$helper.'</p>
						</td>
					</tr>';
				
				return;
				
			}
			
			$values = array('');
			$labels = array(__('-- Select --', 'pk_translate'));
			
			foreach ($tags as $tag) {
				
				array_push($values, $tag -> term_id);
				array_push($labels, $tag -> name);
				
			}
			
			$this -> pk_add_input_select($name, false, $values, $labels, $value, $helper);
			
		}
		
		function pk_add_input_tags($name = '', $value = '', $helper = '') {
			
			$tags = get_tags(array('hide_empty' => false));
			
			if (!$tags) {
				
				echo '
						<td class="pk_admin_td">
							<p><strong>'.__('No tags available', 'pk_translate').'</strong></p>
							<p class="pk_admin_helper"><strong class="pk_admin_helper_strong">? </strong>'.$helper.'</p>
						</td>
					</tr>';
				
				return;
				
			}
			
			$selected_tags = explode(',', $value);
			
			$total = count($tags);
			
			if ($total <= 12) {
				
				echo '
						<td class="pk_admin_td">
							<input class="pk_admin_multiple_field" type="hidden" name="'.$name.'" value="'.$value.'" />
							<div class="pk_admin_multiple_checkbox_div_wrapper">';
				
				$checkboxes_count = 0;
				
				foreach ($tags as $tag) {
					
					$checked = '';
					
					if (in_array($tag -> term_id, $selected_tags)) {
						
						$checked = ' checked="checked"';
						
					}
					
					echo '
								<div class="pk_admin_multiple_checkbox_div"><input data-main="'.$name.'" id="'.$name.'_checkbox_'.$checkboxes_count.'" class="pk_admin_multiple_checkbox pk_admin_radio_checkbox" type="checkbox" value="'.$tag -> term_id.'"'.$checked.' /><label for="'.$name.'_checkbox_'.$checkboxes_count.'" class="pk_admin_multiple_checkbox_label">'.$tag -> name.'</label></div>';
					
					$checkboxes_count++;
					
				}
				
				echo '
							</div>
							<p class="pk_admin_helper"><strong class="pk_admin_helper_strong">? </strong>'.$helper.'</p>
						</td>
					</tr>';
				
			} else {
				
				echo '
						<td class="pk_admin_td">
							<input class="pk_admin_multiple_field" type="hidden" name="'.$name.'" value="'.$value.'" />
							<select data-main="'.$name.'" class="pk_admin_input_select_multiple" multiple="multiple">';
				
				foreach ($tags as $tag) {
					
					if (in_array($tag -> term_id, $selected_tags)) {
						
						$selected_string = ' selected="selected"';
						
					} else {
						
						$selected_string = '';
						
					}
					
					echo '
									<option value="'.$tag -> term_id.'"'.$selected_string.'>'.$tag -> name.'</option>';
					
				}
				
				echo '
							</select>
							<p class="pk_admin_helper"><strong class="pk_admin_helper_strong">? </strong>'.$helper.'</p>
						</td>
					</tr>';
				
			}
			
		}
		
		function pk_open_table($id = '') {
			
			echo '
			<table cellspacing="0" id="pk_sc_table_'.$id.'" class="widefat">
				<tbody>';
			
		}
		
		function pk_close_table() {
			
			echo '
				</tbody>
			</table>';
			
		}
		
		function pk_open_form($options_key = '') {
			
			echo '
	<div>
		<form name="pk_form" method="post" action="">
			<input type="hidden" name="pk_form_data" value="true" />';
			
			wp_nonce_field($options_key, md5($options_key));
			
		}
		
		function pk_close_form() {
			
			echo '
		</form>
	</div>';
			
		}
		
		function pk_open_div() {
			
			echo '
	<!-- pk shortcodes manager metabox - start -->
	<div class="pk_sc_shortcodes_manager">';
			
		}
		
		function pk_close_div() {
			
			echo '
	</div>
	<!-- pk shortcodes manager metabox - end -->

';
			
		}
		
		function pk_add_buttons($type) {
			
			echo '
			<div class="pk_admin_shortcode_manager_buttons_div">
				<input id="pk_sc_table_'.$type.'_generate_shortcode_button" class="pk_admin_generate_shortcode_button button-primary" type="button" value="'.__('Generate HTML', 'pk_translate').'" />
			</div>';
			
		}
		
		function pk_open_shortcodes_manager_div($class = '') {
			
			echo '
		<div class="'.(($class != 'pk_sc_selector') ? 'pk_admin_sc_div pk_admin_padding_top_div ' : '').$class.'">';
			
		}
		
		function pk_close_shortcodes_manager_div() {
			
			echo '
		</div>';
			
		}
		
	}
	
}

if (class_exists('pk_shortcodes_manager') && !isset($pk_shortcodes_manager_instance)) {
	
	$pk_shortcodes_manager_instance = new pk_shortcodes_manager();
	
}

?>