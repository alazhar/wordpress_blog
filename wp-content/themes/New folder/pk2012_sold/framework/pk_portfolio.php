<?php

function pk_register_portfolio() {
	
	$labels = array(
		'name' => _x('Portfolio', 'post type general name', 'pk_translate'),
		'singular_name' => _x('Work', 'post type singular name', 'pk_translate'),
		'add_new' => _x('Add New Work', 'release', 'pk_translate'),
		'add_new_item' => __('Add New Work', 'pk_translate'),
		'edit_item' => __('Edit Work', 'pk_translate'),
		'new_item' => __('New Work', 'pk_translate'),
		'view_item' => __('View Work', 'pk_translate'),
		'search_items' => __('Search Works', 'pk_translate'),
		'not_found' => __('No works found', 'pk_translate'),
		'not_found_in_trash' => __('No works found in Trash', 'pk_translate')
	);
	
	$args = array(
		'labels' => $labels,
		'public' => true,
		'menu_position' => 5,
		'capability_type' => 'post',
		'hierarchical' => false,
		'supports' => array('title', 'editor', 'excerpt', 'comments', 'revisions', 'thumbnail', 'custom-fields'),
		'query_var' => true,
		'rewrite' => array('slug' => 'portfolio-archive', 'with_front' => false),
		'show_in_nav_menus' => true,
		'has_archive' => true
	);
	
	$taxonomy_labels = array(
		'name' => _x('Portfolio Categories', 'taxonomy general name', 'pk_translate'),
		'singular_name' => _x('Category', 'taxonomy singular name', 'pk_translate'),
		'search_items' => __('Search Category', 'pk_translate'),
		'popular_items' => __('Popular Categories', 'pk_translate'),
		'all_items' => __('All Categories', 'pk_translate'),
		'parent_item' => __('Parent Category', 'pk_translate'),
		'parent_item_colon' => __('Parent Category:', 'pk_translate'),
		'edit_item' => __('Edit Category', 'pk_translate'),
		'add_new_item' => __('Add New Category', 'pk_translate'),
		'new_item_name' => __('New Category Name', 'pk_translate')
	);
	
	$taxonomy_args = array(
		'labels' => $taxonomy_labels,
		'public' => true,
		'show_in_nav_menus' => true,
		'show_ui' => true,
		'hierarchical' => true,
		'rewrite' => array('slug' => 'portfolio-category', 'with_front' => false),
		'query_var' => true
	);
	
	register_post_type('portfolio', $args);
	
	register_taxonomy('taxonomy_portfolio', 'portfolio', $taxonomy_args);
	
	flush_rewrite_rules();
	
}

add_action('init', 'pk_register_portfolio');

function pk_portfolio_edit_columns($columns){
	
	$columns = array(
		'cb' => '<input type="checkbox" />',
		'title' => __('Title', 'pk_translate'),
		'portfolio_thumb' => __('Featured Image', 'pk_translate'),
		'author' => __('Author', 'pk_translate'),
		'taxonomy_portfolio' => __('Categories', 'pk_translate'),
		'comments' => $columns['comments'],
		'date' => $columns['date']
	);
	
	return $columns;
	
}

add_filter('manage_edit-portfolio_columns', 'pk_portfolio_edit_columns');

function pk_portfolio_custom_columns($column){
	
	global $post;
	
	switch ($column) {
		
		case 'portfolio_thumb':
			
			if (function_exists('the_post_thumbnail') && has_post_thumbnail()) {
				
				the_post_thumbnail(array(80, 80));
				
			}
			
  			break;
			
		case 'taxonomy_portfolio':
			
			echo get_the_term_list($post -> ID, 'taxonomy_portfolio', '', ', ', '');
			
  			break;
			
	}
	
}

add_action('manage_posts_custom_column', 'pk_portfolio_custom_columns');

function pk_save_portfolio_sort() {
	
	global $wpdb;
	
	$order = explode(',', $_POST['order']);
	
	$counter = 0;
 
	foreach ($order as $portfolio_id) {
		
		$wpdb -> update($wpdb -> posts, array('menu_order' => $counter), array('ID' => $portfolio_id));
		$counter++;
		
	}
	
	die();
	
}

add_action('wp_ajax_portfolio_sort', 'pk_save_portfolio_sort');

function pk_add_sort_portfolio_page() {
	
	add_submenu_page('edit.php?post_type=portfolio', __('Sort Works', 'pk_translate'), __('Sort Works', 'pk_translate'), 'edit_posts', basename(__FILE__), 'pk_create_sort_portfolio_page');
	
}

add_action('admin_menu', 'pk_add_sort_portfolio_page'); 

function pk_create_sort_portfolio_page() {
	
	$portfolio = new WP_Query('post_type=portfolio&posts_per_page=-1&orderby=menu_order&order=ASC');
	
?>

<!-- pk sort portfolio page - start -->
<div class="wrap">
	<div id="icon-options-general" class="icon32">
		<br />
	</div>
	<h2><?php _e('Sort Works', 'pk_translate'); ?> <a href="<?php echo admin_url('post-new.php').'?post_type=portfolio'; ?>" class="button add-new-h2"><?php _e('Add New', 'pk_translate'); ?></a></h2>
	<br />
	<div id="pk_admin_saving_sorting" class="updated">
		<p><strong><?php _e('Saving works order...', 'pk_translate'); ?></strong></p>
	</div>
	<div id="pk_admin_success_sorting" class="updated">
		<p><strong><?php _e('Works order saved!', 'pk_translate'); ?></strong></p>
	</div>
	<div id="pk_admin_error_sorting" class="updated">
		<p><strong><?php _e('Error saving works order, please try again.', 'pk_translate'); ?></strong></p>
	</div>
	<div>
		<ul id="pk_admin_sortable_list">
<?php if ($portfolio -> have_posts()) : while ($portfolio -> have_posts()) : $portfolio -> the_post(); ?>
			<li id="<?php the_id(); ?>" class="pk_admin_sortable_default">
				<?php if (function_exists('the_post_thumbnail') && has_post_thumbnail()) { the_post_thumbnail(array(80, 80), array('class' => 'pk_admin_sort_thumb')); } else { echo '<span class="pk_admin_sort_no_img"></span>'; } ?>
				
				<strong><a class="pk_admin_sort_titles" href="<?php echo admin_url('post.php').'?post='; the_id(); echo '&action=edit'; ?>"><?php the_title(); ?></a></strong>
				<p class="pk_admin_class_taxonomies"><?php the_taxonomies(); ?></p>
			</li>
<?php endwhile; ?>
		</ul>
<?php else :?>
		<p><?php _e('No works found', 'pk_translate'); ?></p>
<?php endif; ?>
	</div>
</div>
<!-- pk sort portfolio page - end -->
<?php

}

?>