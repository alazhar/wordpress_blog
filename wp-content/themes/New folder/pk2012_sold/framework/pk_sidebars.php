<?php

function pk_register_sidebars() {
	
	$custom_sidebars_for = pk_get_option('custom_sidebars_for', array());
	
	if (is_array($custom_sidebars_for) && count($custom_sidebars_for) > 0) : 
		
		foreach ($custom_sidebars_for as $key => $id) : 
			
			$title = get_the_title($id);
			
			register_sidebar(array(
					'name' => __('Sidebar - ', 'pk_translate').$title,
					'id' => 'custom_sidebar_'.$id,
					'description' => __('Applied to ', 'pk_translate').$title,
					'before_widget' => '<div id="%1$s" class="pk_widget %2$s">',
					'after_widget' => '</div>',
					'before_title' => '<h5 class="pk_heading_uppercase">',
					'after_title' => '</h5>'));
					
			$post_template = get_post_meta($id, '_wp_page_template', true);
			
			if (in_array(get_post_type($id), array('page', 'post', 'portfolio', 'product')) && !in_array($post_template, array('page-blog.php', 'page-portfolio.php', 'page-faq.php', 'page-home-agency.php', 'page-home-business.php', 'page-home-freelance.php', 'page-home-standard.php', 'page-home-jigoshop.php', 'page-home-woocommerce.php'))) register_sidebar(array(
					'name' => __('After Content - ', 'pk_translate').$title,
					'id' => 'custom_sidebar_after_content_'.$id,
					'description' => __('Applied to ', 'pk_translate').$title,
					'before_widget' => '<div id="%1$s" class="pk_widget %2$s">',
					'after_widget' => '</div>',
					'before_title' => '<h5 class="pk_heading_uppercase">',
					'after_title' => '</h5>'));
			
		endforeach;
		
	endif;
	
	register_sidebar(array(
					'name' => __('Sidebar - All', 'pk_translate'),
					'id' => 'sidebar_all',
					'description' => __('Always applied.', 'pk_translate'),
					'before_widget' => '<div id="%1$s" class="pk_widget %2$s">',
					'after_widget' => '</div>',
					'before_title' => '<h5 class="pk_heading_uppercase">',
					'after_title' => '</h5>'));
	
	register_sidebar(array(
					'name' => __('Sidebar - Front Page', 'pk_translate'),
					'id' => 'sidebar_front_page',
					'description' => __('Applied to the front page.', 'pk_translate'),
					'before_widget' => '<div id="%1$s" class="pk_widget %2$s">',
					'after_widget' => '</div>',
					'before_title' => '<h5 class="pk_heading_uppercase">',
					'after_title' => '</h5>'));
	
	register_sidebar(array(
					'name' => __('Sidebar - JigoShop', 'pk_translate'),
					'id' => 'sidebar_jigoshop',
					'description' => __('Applied to the JigoShop pages.', 'pk_translate'),
					'before_widget' => '<div id="%1$s" class="pk_widget %2$s">',
					'after_widget' => '</div>',
					'before_title' => '<h5 class="pk_heading_uppercase">',
					'after_title' => '</h5>'));
	
	register_sidebar(array(
					'name' => __('Sidebar - WooCommerce', 'pk_translate'),
					'id' => 'sidebar_woocommerce',
					'description' => __('Applied to the WooCommerce pages.', 'pk_translate'),
					'before_widget' => '<div id="%1$s" class="pk_widget %2$s">',
					'after_widget' => '</div>',
					'before_title' => '<h5 class="pk_heading_uppercase">',
					'after_title' => '</h5>'));
	
	register_sidebar(array(
					'name' => __('Sidebar - BBPress Forum', 'pk_translate'),
					'id' => 'sidebar_bbpress_forum',
					'description' => __('Applied to the BBPress forum pages.', 'pk_translate'),
					'before_widget' => '<div id="%1$s" class="pk_widget %2$s">',
					'after_widget' => '</div>',
					'before_title' => '<h5 class="pk_heading_uppercase">',
					'after_title' => '</h5>'));
	
	register_sidebar(array(
					'name' => __('Sidebar - Pages', 'pk_translate'),
					'id' => 'sidebar_pages',
					'description' => __('Applied to every page.', 'pk_translate'),
					'before_widget' => '<div id="%1$s" class="pk_widget %2$s">',
					'after_widget' => '</div>',
					'before_title' => '<h5 class="pk_heading_uppercase">',
					'after_title' => '</h5>'));
	
	register_sidebar(array(
					'name' => __('After Page Content', 'pk_translate'),
					'id' => 'sidebar_after_page_content',
					'description' => __('Applied after every page content.', 'pk_translate'),
					'before_widget' => '<div id="%1$s" class="pk_widget %2$s">',
					'after_widget' => '</div>',
					'before_title' => '<h5 class="pk_heading_uppercase">',
					'after_title' => '</h5>'));
	
	register_sidebar(array(
					'name' => __('Sidebar - Blog', 'pk_translate'),
					'id' => 'sidebar_blog',
					'description' => __('Applied to every blog page.', 'pk_translate'),
					'before_widget' => '<div id="%1$s" class="pk_widget %2$s">',
					'after_widget' => '</div>',
					'before_title' => '<h5 class="pk_heading_uppercase">',
					'after_title' => '</h5>'));
	
	register_sidebar(array(
					'name' => __('After Blog Post Content', 'pk_translate'),
					'id' => 'sidebar_after_post_content',
					'description' => __('Applied after every blog post content.', 'pk_translate'),
					'before_widget' => '<div id="%1$s" class="pk_widget %2$s">',
					'after_widget' => '</div>',
					'before_title' => '<h5 class="pk_heading_uppercase">',
					'after_title' => '</h5>'));
	
	register_sidebar(array(
					'name' => __('Sidebar - Portfolio', 'pk_translate'),
					'id' => 'sidebar_portfolio',
					'description' => __('Applied to every portfolio grid page.', 'pk_translate'),
					'before_widget' => '<div id="%1$s" class="pk_widget %2$s">',
					'after_widget'=> '</div>',
					'before_title'=> '<h5 class="pk_heading_uppercase">',
					'after_title' => '</h5>'));
	
	register_sidebar(array(
					'name' => __('After Work Content', 'pk_translate'),
					'id' => 'sidebar_after_work_content',
					'description' => __('Applied after every work content.', 'pk_translate'),
					'before_widget' => '<div id="%1$s" class="pk_widget %2$s">',
					'after_widget' => '</div>',
					'before_title' => '<h5 class="pk_heading_uppercase">',
					'after_title' => '</h5>'));
	
	register_sidebar(array(
					'name' => __('Sidebar - Search', 'pk_translate'),
					'id' => 'sidebar_search',
					'description' => __('Applied to the search results pages.', 'pk_translate'),
					'before_widget' => '<div id="%1$s" class="pk_widget %2$s">',
					'after_widget' => '</div>',
					'before_title' => '<h5 class="pk_heading_uppercase">',
					'after_title' => '</h5>'));
	
	register_sidebar(array(
					'name' => __('Sidebar - 404', 'pk_translate'),
					'id' => 'sidebar_404',
					'description' => __('Applied to the "Error 404" page.', 'pk_translate'),
					'before_widget' => '<div id="%1$s" class="pk_widget %2$s">',
					'after_widget' => '</div>',
					'before_title' => '<h5 class="pk_heading_uppercase">',
					'after_title' => '</h5>'));
	
	register_sidebar(array(
					'name' => __('Sidebar - Home Agency - Slider', 'pk_translate'),
					'id' => 'sidebar_home_page_agency_slider',
					'description' => __('Applied to the "Home Page Agency" page.', 'pk_translate'),
					'before_widget' => '<div id="%1$s" class="pk_widget %2$s">',
					'after_widget' => '</div>',
					'before_title' => '<h5 class="pk_heading_uppercase">',
					'after_title' => '</h5>'));
	
	register_sidebar(array(
					'name' => __('Sidebar - Home Agency - Left', 'pk_translate'),
					'id' => 'sidebar_home_page_agency_left',
					'description' => __('Applied to the "Home Page Agency" page.', 'pk_translate'),
					'before_widget' => '<div id="%1$s" class="pk_widget %2$s">',
					'after_widget' => '</div>',
					'before_title' => '<h5 class="pk_heading_uppercase">',
					'after_title' => '</h5>'));
	
	register_sidebar(array(
					'name' => __('Sidebar - Home Agency - Middle', 'pk_translate'),
					'id' => 'sidebar_home_page_agency_middle',
					'description' => __('Applied to the "Home Page Agency" page.', 'pk_translate'),
					'before_widget' => '<div id="%1$s" class="pk_widget %2$s">',
					'after_widget' => '</div>',
					'before_title' => '<h5 class="pk_heading_uppercase">',
					'after_title' => '</h5>'));
	
	register_sidebar(array(
					'name' => __('Sidebar - Home Agency - Right', 'pk_translate'),
					'id' => 'sidebar_home_page_agency_right',
					'description' => __('Applied to the "Home Page Agency" page.', 'pk_translate'),
					'before_widget' => '<div id="%1$s" class="pk_widget %2$s">',
					'after_widget' => '</div>',
					'before_title' => '<h5 class="pk_heading_uppercase">',
					'after_title' => '</h5>'));
	
	register_sidebar(array(
					'name' => __('Sidebar - Home Business - Left', 'pk_translate'),
					'id' => 'sidebar_home_page_business_left',
					'description' => __('Applied to the "Home Page Business" page.', 'pk_translate'),
					'before_widget' => '<div id="%1$s" class="pk_widget %2$s">',
					'after_widget' => '</div>',
					'before_title' => '<h5 class="pk_heading_uppercase">',
					'after_title' => '</h5>'));
	
	register_sidebar(array(
					'name' => __('Sidebar - Home Business - Middle', 'pk_translate'),
					'id' => 'sidebar_home_page_business_middle',
					'description' => __('Applied to the "Home Page Business" page.', 'pk_translate'),
					'before_widget' => '<div id="%1$s" class="pk_widget %2$s">',
					'after_widget' => '</div>',
					'before_title' => '<h5 class="pk_heading_uppercase">',
					'after_title' => '</h5>'));
	
	register_sidebar(array(
					'name' => __('Sidebar - Home Business - Right', 'pk_translate'),
					'id' => 'sidebar_home_page_business_right',
					'description' => __('Applied to the "Home Page Business" page.', 'pk_translate'),
					'before_widget' => '<div id="%1$s" class="pk_widget %2$s">',
					'after_widget' => '</div>',
					'before_title' => '<h5 class="pk_heading_uppercase">',
					'after_title' => '</h5>'));
	
	register_sidebar(array(
					'name' => __('Sidebar - Home Freelance - Left', 'pk_translate'),
					'id' => 'sidebar_home_page_freelance_left',
					'description' => __('Applied to the "Home Page Freelance" page.', 'pk_translate'),
					'before_widget' => '<div id="%1$s" class="pk_widget %2$s">',
					'after_widget' => '</div>',
					'before_title' => '<h5 class="pk_heading_uppercase">',
					'after_title' => '</h5>'));
	
	register_sidebar(array(
					'name' => __('Sidebar - Home Freelance - Middle', 'pk_translate'),
					'id' => 'sidebar_home_page_freelance_middle',
					'description' => __('Applied to the "Home Page Freelance" page.', 'pk_translate'),
					'before_widget' => '<div id="%1$s" class="pk_widget %2$s">',
					'after_widget' => '</div>',
					'before_title' => '<h5 class="pk_heading_uppercase">',
					'after_title' => '</h5>'));
	
	register_sidebar(array(
					'name' => __('Sidebar - Home Freelance - Right', 'pk_translate'),
					'id' => 'sidebar_home_page_freelance_right',
					'description' => __('Applied to the "Home Page Freelance" page.', 'pk_translate'),
					'before_widget' => '<div id="%1$s" class="pk_widget %2$s">',
					'after_widget' => '</div>',
					'before_title' => '<h5 class="pk_heading_uppercase">',
					'after_title' => '</h5>'));
	
	register_sidebar(array(
					'name' => __('Sidebar - Home Standard - Left', 'pk_translate'),
					'id' => 'sidebar_home_page_standard_left',
					'description' => __('Applied to the "Home Page Standard" page.', 'pk_translate'),
					'before_widget' => '<div id="%1$s" class="pk_widget %2$s">',
					'after_widget' => '</div>',
					'before_title' => '<h5 class="pk_heading_uppercase">',
					'after_title' => '</h5>'));
	
	register_sidebar(array(
					'name' => __('Sidebar - Home Standard - Right', 'pk_translate'),
					'id' => 'sidebar_home_page_standard_right',
					'description' => __('Applied to the "Home Page Standard" page.', 'pk_translate'),
					'before_widget' => '<div id="%1$s" class="pk_widget %2$s">',
					'after_widget' => '</div>',
					'before_title' => '<h5 class="pk_heading_uppercase">',
					'after_title' => '</h5>'));
	
	register_sidebar(array(
					'name' => __('Sidebar - Home JigoShop - Left', 'pk_translate'),
					'id' => 'sidebar_home_page_jigoshop_left',
					'description' => __('Applied to the "Home Page JigoShop" page.', 'pk_translate'),
					'before_widget' => '<div id="%1$s" class="pk_widget %2$s">',
					'after_widget' => '</div>',
					'before_title' => '<h5 class="pk_heading_uppercase">',
					'after_title' => '</h5>'));
	
	register_sidebar(array(
					'name' => __('Sidebar - Home JigoShop - Right', 'pk_translate'),
					'id' => 'sidebar_home_page_jigoshop_right',
					'description' => __('Applied to the "Home Page JigoShop" page.', 'pk_translate'),
					'before_widget' => '<div id="%1$s" class="pk_widget %2$s">',
					'after_widget' => '</div>',
					'before_title' => '<h5 class="pk_heading_uppercase">',
					'after_title' => '</h5>'));
	
	register_sidebar(array(
					'name' => __('Sidebar - Home WooCommerce - Left', 'pk_translate'),
					'id' => 'sidebar_home_page_woocommerce_left',
					'description' => __('Applied to the "Home Page WooCommerce" page.', 'pk_translate'),
					'before_widget' => '<div id="%1$s" class="pk_widget %2$s">',
					'after_widget' => '</div>',
					'before_title' => '<h5 class="pk_heading_uppercase">',
					'after_title' => '</h5>'));
	
	register_sidebar(array(
					'name' => __('Sidebar - Home WooCommerce - Right', 'pk_translate'),
					'id' => 'sidebar_home_page_woocommerce_right',
					'description' => __('Applied to the "Home Page WooCommerce" page.', 'pk_translate'),
					'before_widget' => '<div id="%1$s" class="pk_widget %2$s">',
					'after_widget' => '</div>',
					'before_title' => '<h5 class="pk_heading_uppercase">',
					'after_title' => '</h5>'));
	
	register_sidebar(array(
					'name' => __('Footer Column 1', 'pk_translate'),
					'id' => 'footer_column_1',
					'description' => __('The first column in the footer widgets area (Set the number of columns in the footer options panel.)', 'pk_translate'),
					'before_widget' => '<div id="%1$s" class="pk_widget %2$s">',
					'after_widget' => '</div>',
					'before_title' => '<h5 class="pk_heading_uppercase">',
					'after_title' => '</h5>'));
	
	register_sidebar(array(
					'name' => __('Footer Column 2', 'pk_translate'),
					'id' => 'footer_column_2',
					'description' => __('The second column in the footer widgets area (Set the number of columns in the footer options panel.)', 'pk_translate'),
					'before_widget' => '<div id="%1$s" class="pk_widget %2$s">',
					'after_widget' => '</div>',
					'before_title' => '<h5 class="pk_heading_uppercase">',
					'after_title' => '</h5>'));
	
	register_sidebar(array(
					'name' => __('Footer Column 3', 'pk_translate'),
					'id' => 'footer_column_3',
					'description' => __('The third column in the footer widgets area (Set the number of columns in the footer options panel.)', 'pk_translate'),
					'before_widget' => '<div id="%1$s" class="pk_widget %2$s">',
					'after_widget' => '</div>',
					'before_title' => '<h5 class="pk_heading_uppercase">',
					'after_title' => '</h5>'));
	
	register_sidebar(array(
					'name' => __('Footer Column 4', 'pk_translate'),
					'id' => 'footer_column_4',
					'description' => __('The fourth column in the footer widgets area (Set the number of columns in the footer options panel.)', 'pk_translate'),
					'before_widget' => '<div id="%1$s" class="pk_widget %2$s">',
					'after_widget' => '</div>',
					'before_title' => '<h5 class="pk_heading_uppercase">',
					'after_title' => '</h5>'));
	
	register_sidebar(array(
					'name' => __('Footer Column 5', 'pk_translate'),
					'id' => 'footer_column_5',
					'description' => __('The fifth column in the footer widgets area (Set the number of columns in the footer options panel.)', 'pk_translate'),
					'before_widget' => '<div id="%1$s" class="pk_widget %2$s">',
					'after_widget' => '</div>',
					'before_title' => '<h5 class="pk_heading_uppercase">',
					'after_title' => '</h5>'));
	
	register_sidebar(array(
					'name' => __('Footer Column 6', 'pk_translate'),
					'id' => 'footer_column_6',
					'description' => __('The sixth column in the footer widgets area (Set the number of columns in the footer options panel.)', 'pk_translate'),
					'before_widget' => '<div id="%1$s" class="pk_widget %2$s">',
					'after_widget' => '</div>',
					'before_title' => '<h5 class="pk_heading_uppercase">',
					'after_title' => '</h5>'));
	
}

add_action('widgets_init', 'pk_register_sidebars');

?>