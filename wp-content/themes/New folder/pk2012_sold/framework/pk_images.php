<?php

function pk_get_post_gallery_images($size = 'thumbnail') {
	
	$post_type = get_post_type(get_the_ID());
	$post_format = '';
	
	if ($post_type == 'post') $post_format = (!get_post_format(get_the_ID())) ? 'standard' : get_post_format(get_the_ID());
	if ($post_type == 'portfolio') $post_format = get_post_meta(get_the_ID(), '_work_format', true);
	if ($post_type == 'page') $post_format = get_post_meta(get_the_ID(), '_page_format', true);
	
	$post_images = array();
	
	if($images = get_posts(array(
		'post_parent'    => get_the_ID(),
		'post_type'      => 'attachment',
		'numberposts'    => -1,
		'post_status'    => 'null',
		'post_mime_type' => 'image',
		'orderby'        => 'menu_order',
		'order'          => 'ASC',
		'exclude'        => (($post_format == 'slider') ? 'null' : get_post_thumbnail_id(get_the_ID()))
	))) {
		
		$count = 0;
		
		foreach($images as $image) {
			
			$exclude_from_gallery = get_post_meta($image -> ID, '_exclude_from_gallery', true);
			
			if ($exclude_from_gallery == 'yes') continue;
			
			$img = wp_get_attachment_image($image -> ID, $size);
			$post_images[$count]['img'] = $img;
			$post_images[$count]['id'] = $image -> ID;
			$post_images[$count]['url'] = $image -> guid;
			$post_images[$count]['title'] = $image -> post_title;
			$post_images[$count]['description'] = $image -> post_content;
			
			$count++;
			
		}
		
	}
	
	return $post_images;
	
}

function pk_get_featured_image($size = 'full') {
	
	$image = '';
	
	if (has_post_thumbnail()) {
		
		$image = wp_get_attachment_image_src(get_post_thumbnail_id(), $size);
		$image = $image[0];
		
	}
	
	return $image;
	
}

?>