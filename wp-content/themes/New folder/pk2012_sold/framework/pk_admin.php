<?php

if (!class_exists('pk_admin')) {
	
	class pk_admin {
		
		function pk_admin() {
			
			require_once(PK_FRAMEWORK.'/docs/pk_docs.php');
			require_once(PK_FRAMEWORK.'/shortcodes/pk_shortcodes.php');
			require_once(PK_FRAMEWORK.'/pk_admin_setup.php');
			require_once(PK_FRAMEWORK.'/pk_metaboxes.php');
			require_once(PK_FRAMEWORK.'/pk_options.php');
			require_once(PK_FRAMEWORK.'/pk_plugins.php');

		}
		
	}
	
}

if (class_exists('pk_admin') && !isset($admin_instance)) {
	
	$admin_instance = new pk_admin();
	
}

?>