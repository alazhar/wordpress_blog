<?php

add_action('admin_init', 'pk_theme_options', 1);

function pk_theme_options() {
	
	$saved_settings = get_option('option_tree_settings', array());
	
	$custom_settings = array(
		'sections' => array(
			array(
				'id' => 'general_default',
				'title' => __('WP L OC KE R. CO M - General', 'pk_translate')
			),
			array(
				'id' => 'sidebar',
				'title' => __('Sidebar', 'pk_translate')
			),
			array(
				'id' => 'custom_sidebars',
				'title' => __('Custom sidebars', 'pk_translate')
			),
			array(
				'id' => 'blog',
				'title' => __('Blog and blog archives', 'pk_translate')
			),
			array(
				'id' => 'portfolio',
				'title' => __('Portfolio archives', 'pk_translate')
			),
			array(
				'id' => 'footer',
				'title' => __('Footer', 'pk_translate')
			),
			array(
				'id' => 'social_networks',
				'title' => __('Footer social networks', 'pk_translate')
			),
			array(
				'id' => 'skin',
				'title' => __('Skin', 'pk_translate')
			),
			array(
				'id' => 'typography',
				'title' => __('Typography', 'pk_translate')
			),
			array(
				'id' => 'custom_css',
				'title' => __('Custom CSS', 'pk_translate')
			),
			array(
				'id' => 'custom_js',
				'title' => __('Custom JS', 'pk_translate')
			),
			array(
				'id' => 'advanced',
				'title' => __('Advanced', 'pk_translate')
			),
			array(
				'id' => 'additional_options',
				'title' => __('Additional options', 'pk_translate')
			)
		),
		'settings' => array(
			array(
				'id' => 'logo',
				'label' => __('Logo', 'pk_translate'),
				'desc' => __('Theme shared on W P L O C K E R .C O M - Upload the main logo image.', 'pk_translate'),
				'section' => 'general_default',
				'type' => 'upload',
				'std' => PK_THEME_DIR.'/images/logo.png'
			),
			array(
				'id' => 'logo_width',
				'label' => __('Logo width', 'pk_translate'),
				'desc' => __('Set the theme&#039;s logo width (pixels). Enter numbers only.', 'pk_translate'),
				'section' => 'general_default',
				'type' => 'text',
				'std' => '130'
			),
			array(
				'id' => 'logo_height',
				'label' => __('Logo height', 'pk_translate'),
				'desc' => __('Set the theme&#039;s logo height (pixels). Enter numbers only.', 'pk_translate'),
				'section' => 'general_default',
				'type' => 'text',
				'std' => '60'
			),
			array(
				'id' => 'header_text',
				'label' => __('Header text', 'pk_translate'),
				'desc' => __('Enter a text to display on the top right corner of the theme.', 'pk_translate'),
				'section' => 'general_default',
				'type' => 'text',
				'std' => ''
			),
			array(
				'id' => 'favicon',
				'label' => __('Favicon', 'pk_translate'),
				'desc' => __('Upload a favicon.', 'pk_translate'),
				'section' => 'general_default',
				'type' => 'upload',
				'std' => PK_THEME_DIR.'/images/favicon.ico'
			),
			array(
				'id' => 'title_separator',
				'label' => __('Title separator', 'pk_translate'),
				'desc' => __('Enter the separator character for the browser&#039;s title.', 'pk_translate'),
				'section' => 'general_default',
				'type' => 'text',
				'std' => '|'
			),
			array(
				'id' => 'welcome_image',
				'label' => __('Welcome image', 'pk_translate'),
				'desc' => __('Enter the URL of an image to automatically display when an user makes his first access to the site. You can use this feature to promote your latest works or products.', 'pk_translate'),
				'section' => 'general_default',
				'type' => 'upload',
				'std' => ''
			),
			array(
				'id' => 'welcome_link',
				'label' => __('Welcome image link', 'pk_translate'),
				'desc' => __('Enter an URL for the welcome image. You can link your welcome image to a theme&#039;s page or to any external website as well.', 'pk_translate'),
				'section' => 'general_default',
				'type' => 'text',
				'std' => ''
			),
			array(
				'id' => 'sidebar_position',
				'label' => __('Sidebar position', 'pk_translate'),
				'desc' => __('Select the position of the sidebar.', 'pk_translate'),
				'section' => 'sidebar',
				'type' => 'radio_image',
				'std' => 'right',
				'choices' => array(
					array(
						'value' => 'left',
						'label' => __('Left', 'pk_translate'),
						'src' => (defined('OT_URL')) ? OT_URL.'/assets/images/layout/left-sidebar.png' : ''
					),
					array(
						'value' => 'right',
						'label' => __('Right', 'pk_translate'),
						'src' => (defined('OT_URL')) ? OT_URL.'/assets/images/layout/right-sidebar.png' : ''
					)
				)
			),
			array(
				'id' => 'force_sidebar',
				'label' => __('Force sidebar', 'pk_translate'),
				'desc' => __('Select an option to override any sidebar setting. The value you select will affect the whole theme. Select &quot;No Forcing&quot; to keep indipendent sidebar settings', 'pk_translate'),
				'section' => 'sidebar',
				'type' => 'radio',
				'std' => 'no forcing',
				'choices' => array(
					array(
						'value' => 'show',
						'label' => __('Show', 'pk_translate')
					),
					array(
						'value' => 'hide',
						'label' => __('Hide', 'pk_translate')
					),
					array(
						'value' => 'no forcing',
						'label' => __('No Forcing', 'pk_translate')
					)
				)
			),
			array(
				'id' => 'blog_sidebar',
				'label' => __('Blog and blog archives sidebar', 'pk_translate'),
				'desc' => __('Select an option to show/hide the sidebar for the main blog and for all the blog archives.', 'pk_translate'),
				'section' => 'sidebar',
				'type' => 'radio',
				'std' => 'show',
				'choices' => array(
					array(
						'value' => 'show',
						'label' => __('Show', 'pk_translate')
					),
					array(
						'value' => 'hide',
						'label' => __('Hide', 'pk_translate')
					)
				)
			),
			array(
				'id' => 'portfolio_sidebar',
				'label' => __('Portfolio archives sidebar', 'pk_translate'),
				'desc' => __('Select an option to show/hide the sidebar for the portfolio archives.', 'pk_translate'),
				'section' => 'sidebar',
				'type' => 'radio',
				'std' => 'hide',
				'choices' => array(
					array(
						'value' => 'show',
						'label' => __('Show', 'pk_translate')
					),
					array(
						'value' => 'hide',
						'label' => __('Hide', 'pk_translate')
					)
				)
			),
			array(
				'id' => 'shop_sidebar',
				'label' => __('Shop and product categories sidebar', 'pk_translate'),
				'desc' => __('Select an option to show/hide the sidebar for the shop and for the product categories pages.', 'pk_translate'),
				'section' => 'sidebar',
				'type' => 'radio',
				'std' => 'show',
				'choices' => array(
					array(
						'value' => 'show',
						'label' => __('Show', 'pk_translate')
					),
					array(
						'value' => 'hide',
						'label' => __('Hide', 'pk_translate')
					)
				)
			),
			array(
				'id' => 'bbpress_forum_sidebar',
				'label' => __('bbPress forum sidebar', 'pk_translate'),
				'desc' => __('Select an option to show/hide the sidebar for the bbPress forum pages.', 'pk_translate'),
				'section' => 'sidebar',
				'type' => 'radio',
				'std' => 'hide',
				'choices' => array(
					array(
						'value' => 'show',
						'label' => __('Show', 'pk_translate')
					),
					array(
						'value' => 'hide',
						'label' => __('Hide', 'pk_translate')
					)
				)
			),
			array(
				'id' => 'search_sidebar',
				'label' => __('Search results sidebar', 'pk_translate'),
				'desc' => __('Select an option to show/hide the sidebar for the search results.', 'pk_translate'),
				'section' => 'sidebar',
				'type' => 'radio',
				'std' => 'show',
				'choices' => array(
					array(
						'value' => 'show',
						'label' => __('Show', 'pk_translate')
					),
					array(
						'value' => 'hide',
						'label' => __('Hide', 'pk_translate')
					)
				)
			),
			array(
				'id' => '404_sidebar',
				'label' => __('404 error page sidebar', 'pk_translate'),
				'desc' => __('Select an option to show/hide the sidebar for the 404 error page.', 'pk_translate'),
				'section' => 'sidebar',
				'type' => 'radio',
				'std' => 'show',
				'choices' => array(
					array(
						'value' => 'show',
						'label' => __('Show', 'pk_translate')
					),
					array(
						'value' => 'hide',
						'label' => __('Hide', 'pk_translate')
					)
				)
			),
			array(
				'id' => 'custom_sidebars_for',
				'label' => __('Generate custom sidebars for', 'pk_translate'),
				'desc' => __('For every selected page will be generated a custom sidebar and a custom widgets area (if supported by the page template), both different than the default ones loaded by the theme.<br /><br />After saving you will find the new custom sidebars and widgets areas in the Appearance -> Widgets admin page.', 'pk_translate'),
				'section' => 'custom_sidebars',
				'type' => 'page-checkbox',
				'std' => '',
				'post_type' => 'page, post, portfolio, product'
			),
			array(
				'id' => 'hide_default_sidebars_for',
				'label' => __('Hide default sidebars for', 'pk_translate'),
				'desc' => __('For every selected page the default sidebar and widgets area (if supported by the page template) will not be loaded.<br /><br />This option is valid only for the pages selected in the previous option.', 'pk_translate'),
				'section' => 'custom_sidebars',
				'type' => 'page-checkbox',
				'std' => '',
				'post_type' => 'page, post, portfolio, product'
			),
			array(
				'id' => 'blog_grid_layout',
				'label' => __('Blog and blog archives grid layout', 'pk_translate'),
				'desc' => __('Select the layout for the grid. This option is valid only if you select a 2+ columns layout in the next option.<br /><br />The &quot;normal&quot; layout follows an horizontal scheme, while the &quot;special&quot; layout follows a vertical scheme with uncropped thumbnails.', 'pk_translate'),
				'section' => 'blog',
				'type' => 'radio',
				'std' => 'normal',
				'choices' => array(
					array(
						'value' => 'normal',
						'label' => __('Normal', 'pk_translate')
					),
					array(
						'value' => 'special',
						'label' => __('Special', 'pk_translate')
					)
				)
			),
			array(
				'id' => 'blog_layout',
				'label' => __('Blog and blog archives layout (columns)', 'pk_translate'),
				'desc' => __('Select the number of columns.', 'pk_translate'),
				'section' => 'blog',
				'type' => 'radio',
				'std' => '1',
				'choices' => array(
					array(
						'value' => '1',
						'label' => __('1 Column', 'pk_translate')
					),
					array(
						'value' => '2',
						'label' => __('2 Columns', 'pk_translate')
					),
					array(
						'value' => '3',
						'label' => __('3 Columns', 'pk_translate')
					),
					array(
						'value' => '4',
						'label' => __('4 Columns', 'pk_translate')
					)
				)
			),
			array(
				'id' => 'blog_layout_rows',
				'label' => __('Blog and blog archives layout (rows)', 'pk_translate'),
				'desc' => __('Enter the number of rows.', 'pk_translate'),
				'section' => 'blog',
				'type' => 'text',
				'std' => '10'
			),
			array(
				'id' => 'blog_show_categories_filter',
				'label' => __('Blog and blog archives categories filter', 'pk_translate'),
				'desc' => __('Select true to display the categories filter.', 'pk_translate'),
				'section' => 'blog',
				'type' => 'radio',
				'std' => 'false',
				'choices' => array(
					array(
						'value' => 'true',
						'label' => __('Show', 'pk_translate')
					),
					array(
						'value' => 'false',
						'label' => __('Hide', 'pk_translate')
					)
				)
			),
			array(
				'id' => 'portfolio_page_id',
				'label' => __('Portfolio main page', 'pk_translate'),
				'desc' => __('Select your main portfolio page.<br /><br />If you don&#039;t have a portfolio page, create a new page using the &quot;Portfolio Template&quot;. Once created, select it here and save the options.<br /><br />This page will be the root page for all your works and it will be used by the theme to link your portfolio from other pages.', 'pk_translate'),
				'section' => 'portfolio',
				'type' => 'page-select',
				'std' => ''
			),
			array(
				'id' => 'portfolio_grid_layout',
				'label' => __('Portfolio archives grid layout', 'pk_translate'),
				'desc' => __('Select the layout for the grid. This option is valid only if you select a 2+ columns layout in the next option.<br /><br />The &quot;normal&quot; layout follows an horizontal scheme, while the &quot;special&quot; layout follows a vertical scheme with uncropped thumbnails.', 'pk_translate'),
				'section' => 'portfolio',
				'type' => 'radio',
				'std' => 'normal',
				'choices' => array(
					array(
						'value' => 'normal',
						'label' => __('Normal', 'pk_translate')
					),
					array(
						'value' => 'special',
						'label' => __('Special', 'pk_translate')
					)
				)
			),
			array(
				'id' => 'portfolio_layout',
				'label' => __('Portfolio archives layout (columns)', 'pk_translate'),
				'desc' => __('Select the number of columns.', 'pk_translate'),
				'section' => 'portfolio',
				'type' => 'radio',
				'std' => '4',
				'choices' => array(
					array(
						'value' => '1',
						'label' => __('1 Column', 'pk_translate')
					),
					array(
						'value' => '2',
						'label' => __('2 Columns', 'pk_translate')
					),
					array(
						'value' => '3',
						'label' => __('3 Columns', 'pk_translate')
					),
					array(
						'value' => '4',
						'label' => __('4 Columns', 'pk_translate')
					)
				)
			),
			array(
				'id' => 'portfolio_layout_rows',
				'label' => __('Portfolio archives layout (rows)', 'pk_translate'),
				'desc' => __('Enter the number of rows.', 'pk_translate'),
				'section' => 'portfolio',
				'type' => 'text',
				'std' => '3'
			),
			array(
				'id' => 'portfolio_show_categories_filter',
				'label' => __('Portfolio archives categories filter', 'pk_translate'),
				'desc' => __('Select true to display the categories filter.', 'pk_translate'),
				'section' => 'portfolio',
				'type' => 'radio',
				'std' => 'false',
				'choices' => array(
					array(
						'value' => 'true',
						'label' => __('Show', 'pk_translate')
					),
					array(
						'value' => 'false',
						'label' => __('Hide', 'pk_translate')
					)
				)
			),
			array(
				'id' => 'footer_layout',
				'label' => __('Layout', 'pk_translate'),
				'desc' => __('Enter the special string to compose the footer layout. You can enter a comma separated string of column identifiers, like: &quot;1/2,1/4,1/4&quot;. This string will generate 3 columns, the first will be 1/2 of the footer width and the other ones 1/4 of the total width.
	<br /><br />
	You can combine columns order and size as you need, from 1 (full width) to 1/6. The total must result in 1/1.
	<br /><br />
	These columns will load specific sidebars, that you can compose in the Appearance -> Widgets admin page. There are 6 sidebars registered for the footer (max number of columns for the footer layout). If you create a 3 columns layout only the first 3 sidebars will be loaded.', 'pk_translate'),
				'section' => 'footer',
				'type' => 'text',
				'std' => '1/3,1/3,1/3'
			),
			array(
				'id' => 'logo_footer',
				'label' => __('Logo', 'pk_translate'),
				'desc' => __('Upload the logo image for the footer.', 'pk_translate'),
				'section' => 'footer',
				'type' => 'upload',
				'std' => PK_THEME_DIR.'/images/logo_footer.png'
			),
			array(
				'id' => 'copyright',
				'label' => __('Copyright', 'pk_translate'),
				'desc' => __('Enter the text that will be displayed in the copyright area of the footer.', 'pk_translate'),
				'section' => 'footer',
				'type' => 'text',
				'std' => '&copy; parker&amp;kent 2012'
			),
			array(
				'id' => 'aim_link',
				'label' => __('AIM', 'pk_translate'),
				'desc' => __('Enter URL.', 'pk_translate'),
				'section' => 'social_networks',
				'type' => 'text',
				'std' => ''
			),
			array(
				'id' => 'apple_link',
				'label' => __('Apple', 'pk_translate'),
				'desc' => __('Enter URL.', 'pk_translate'),
				'section' => 'social_networks',
				'type' => 'text',
				'std' => ''
			),
			array(
				'id' => 'bebo_link',
				'label' => __('Bebo', 'pk_translate'),
				'desc' => __('Enter URL.', 'pk_translate'),
				'section' => 'social_networks',
				'type' => 'text',
				'std' => ''
			),
			array(
				'id' => 'behance_link',
				'label' => __('Behance', 'pk_translate'),
				'desc' => __('Enter URL.', 'pk_translate'),
				'section' => 'social_networks',
				'type' => 'text',
				'std' => ''
			),
			array(
				'id' => 'blogger_link',
				'label' => __('Blogger', 'pk_translate'),
				'desc' => __('Enter URL.', 'pk_translate'),
				'section' => 'social_networks',
				'type' => 'text',
				'std' => ''
			),
			array(
				'id' => 'brightkite_link',
				'label' => __('Brightkite', 'pk_translate'),
				'desc' => __('Enter URL.', 'pk_translate'),
				'section' => 'social_networks',
				'type' => 'text',
				'std' => ''
			),
			array(
				'id' => 'cargo_link',
				'label' => __('Cargo', 'pk_translate'),
				'desc' => __('Enter URL.', 'pk_translate'),
				'section' => 'social_networks',
				'type' => 'text',
				'std' => ''
			),
			array(
				'id' => 'delicious_link',
				'label' => __('Delicious', 'pk_translate'),
				'desc' => __('Enter URL.', 'pk_translate'),
				'section' => 'social_networks',
				'type' => 'text',
				'std' => ''
			),
			array(
				'id' => 'designfloat_link',
				'label' => __('DesignFloat', 'pk_translate'),
				'desc' => __('Enter URL.', 'pk_translate'),
				'section' => 'social_networks',
				'type' => 'text',
				'std' => ''
			),
			array(
				'id' => 'designmoo_link',
				'label' => __('Designmoo', 'pk_translate'),
				'desc' => __('Enter URL.', 'pk_translate'),
				'section' => 'social_networks',
				'type' => 'text',
				'std' => ''
			),
			array(

				'id' => 'deviantart_link',
				'label' => __('DeviantART', 'pk_translate'),
				'desc' => __('Enter URL.', 'pk_translate'),
				'section' => 'social_networks',
				'type' => 'text',
				'std' => ''
			),
			array(
				'id' => 'digg_link',
				'label' => __('Digg', 'pk_translate'),
				'desc' => __('Enter URL.', 'pk_translate'),
				'section' => 'social_networks',
				'type' => 'text',
				'std' => ''
			),
			array(
				'id' => 'dopplr_link',
				'label' => __('Dopplr', 'pk_translate'),
				'desc' => __('Enter URL.', 'pk_translate'),
				'section' => 'social_networks',
				'type' => 'text',
				'std' => ''
			),
			array(
				'id' => 'dribbble_link',
				'label' => __('Dribbble', 'pk_translate'),
				'desc' => __('Enter URL.', 'pk_translate'),
				'section' => 'social_networks',
				'type' => 'text',
				'std' => ''
			),
			array(
				'id' => 'email_link',
				'label' => __('Email', 'pk_translate'),
				'desc' => __('Enter URL.', 'pk_translate'),
				'section' => 'social_networks',
				'type' => 'text',
				'std' => ''
			),
			array(
				'id' => 'ember_link',
				'label' => __('Ember', 'pk_translate'),
				'desc' => __('Enter URL.', 'pk_translate'),
				'section' => 'social_networks',
				'type' => 'text',
				'std' => ''
			),
			array(
				'id' => 'evernote_link',
				'label' => __('Evernote', 'pk_translate'),
				'desc' => __('Enter URL.', 'pk_translate'),
				'section' => 'social_networks',
				'type' => 'text',
				'std' => ''
			),
			array(
				'id' => 'facebook_link',
				'label' => __('Facebook', 'pk_translate'),
				'desc' => __('Enter URL.', 'pk_translate'),
				'section' => 'social_networks',
				'type' => 'text',
				'std' => ''
			),
			array(
				'id' => 'flickr_link',
				'label' => __('Flickr', 'pk_translate'),
				'desc' => __('Enter URL.', 'pk_translate'),
				'section' => 'social_networks',
				'type' => 'text',
				'std' => ''
			),
			array(
				'id' => 'forrst_link',
				'label' => __('Forrst', 'pk_translate'),
				'desc' => __('Enter URL.', 'pk_translate'),
				'section' => 'social_networks',
				'type' => 'text',
				'std' => ''
			),
			array(
				'id' => 'friendsfeed_link',
				'label' => __('FriendsFeed', 'pk_translate'),
				'desc' => __('Enter URL.', 'pk_translate'),
				'section' => 'social_networks',
				'type' => 'text',
				'std' => ''
			),
			array(
				'id' => 'gamespot_link',
				'label' => __('GameSpot', 'pk_translate'),
				'desc' => __('Enter URL.', 'pk_translate'),
				'section' => 'social_networks',
				'type' => 'text',
				'std' => ''
			),
			array(
				'id' => 'google_link',
				'label' => __('Google', 'pk_translate'),
				'desc' => __('Enter URL.', 'pk_translate'),
				'section' => 'social_networks',
				'type' => 'text',
				'std' => ''
			),
			array(
				'id' => 'googletalk_link',
				'label' => __('GoogleTalk', 'pk_translate'),
				'desc' => __('Enter URL.', 'pk_translate'),
				'section' => 'social_networks',
				'type' => 'text',
				'std' => ''
			),
			array(
				'id' => 'googlevoice_link',
				'label' => __('GoogleVoice', 'pk_translate'),
				'desc' => __('Enter URL.', 'pk_translate'),
				'section' => 'social_networks',
				'type' => 'text',
				'std' => ''
			),
			array(
				'id' => 'googlewave_link',
				'label' => __('GoogleWave', 'pk_translate'),
				'desc' => __('Enter URL.', 'pk_translate'),
				'section' => 'social_networks',
				'type' => 'text',
				'std' => ''
			),
			array(
				'id' => 'gowalla_link',
				'label' => __('Gowalla', 'pk_translate'),
				'desc' => __('Enter URL.', 'pk_translate'),
				'section' => 'social_networks',
				'type' => 'text',
				'std' => ''
			),
			array(
				'id' => 'grooveshark_link',
				'label' => __('Grooveshark', 'pk_translate'),
				'desc' => __('Enter URL.', 'pk_translate'),
				'section' => 'social_networks',
				'type' => 'text',
				'std' => ''
			),
			array(
				'id' => 'ilike_link',
				'label' => __('iLike', 'pk_translate'),
				'desc' => __('Enter URL.', 'pk_translate'),
				'section' => 'social_networks',
				'type' => 'text',
				'std' => ''
			),
			array(
				'id' => 'komodomedia_link',
				'label' => __('KomodoMedia', 'pk_translate'),
				'desc' => __('Enter URL.', 'pk_translate'),
				'section' => 'social_networks',
				'type' => 'text',
				'std' => ''
			),
			array(
				'id' => 'lastfm_link',
				'label' => __('LastFM', 'pk_translate'),
				'desc' => __('Enter URL.', 'pk_translate'),
				'section' => 'social_networks',
				'type' => 'text',
				'std' => ''
			),
			array(
				'id' => 'linkedin_link',
				'label' => __('LinkedIn', 'pk_translate'),
				'desc' => __('Enter URL.', 'pk_translate'),
				'section' => 'social_networks',
				'type' => 'text',
				'std' => ''
			),
			array(
				'id' => 'mixx_link',
				'label' => __('Mixx', 'pk_translate'),
				'desc' => __('Enter URL.', 'pk_translate'),
				'section' => 'social_networks',
				'type' => 'text',
				'std' => ''
			),
			array(
				'id' => 'mobileme_link',
				'label' => __('MobileMe', 'pk_translate'),
				'desc' => __('Enter URL.', 'pk_translate'),
				'section' => 'social_networks',
				'type' => 'text',
				'std' => ''
			),
			array(
				'id' => 'mynameise_link',
				'label' => __('MyNameIsE', 'pk_translate'),
				'desc' => __('Enter URL.', 'pk_translate'),
				'section' => 'social_networks',
				'type' => 'text',
				'std' => ''
			),
			array(
				'id' => 'myspace_link',
				'label' => __('MySpace', 'pk_translate'),
				'desc' => __('Enter URL.', 'pk_translate'),
				'section' => 'social_networks',
				'type' => 'text',
				'std' => ''
			),
			array(
				'id' => 'netvibes_link',
				'label' => __('Netvibes', 'pk_translate'),
				'desc' => __('Enter URL.', 'pk_translate'),
				'section' => 'social_networks',
				'type' => 'text',
				'std' => ''
			),
			array(
				'id' => 'newsvine_link',
				'label' => __('Newsvine', 'pk_translate'),
				'desc' => __('Enter URL.', 'pk_translate'),
				'section' => 'social_networks',
				'type' => 'text',
				'std' => ''
			),
			array(
				'id' => 'openid_link',
				'label' => __('OpenID', 'pk_translate'),
				'desc' => __('Enter URL.', 'pk_translate'),
				'section' => 'social_networks',
				'type' => 'text',
				'std' => ''
			),
			array(
				'id' => 'orkut_link',
				'label' => __('Orkut', 'pk_translate'),
				'desc' => __('Enter URL.', 'pk_translate'),
				'section' => 'social_networks',
				'type' => 'text',
				'std' => ''
			),
			array(
				'id' => 'pandora_link',
				'label' => __('Pandora', 'pk_translate'),
				'desc' => __('Enter URL.', 'pk_translate'),
				'section' => 'social_networks',
				'type' => 'text',
				'std' => ''
			),
			array(
				'id' => 'paypal_link',
				'label' => __('PayPal', 'pk_translate'),
				'desc' => __('Enter URL.', 'pk_translate'),
				'section' => 'social_networks',
				'type' => 'text',
				'std' => ''
			),
			array(
				'id' => 'picasa_link',
				'label' => __('Picasa', 'pk_translate'),
				'desc' => __('Enter URL.', 'pk_translate'),
				'section' => 'social_networks',
				'type' => 'text',
				'std' => ''
			),
			array(
				'id' => 'pinterest_link',
				'label' => __('Pinterest', 'pk_translate'),
				'desc' => __('Enter URL.', 'pk_translate'),
				'section' => 'social_networks',
				'type' => 'text',
				'std' => ''
			),
			array(
				'id' => 'playstation_link',
				'label' => __('Playstation', 'pk_translate'),
				'desc' => __('Enter URL.', 'pk_translate'),
				'section' => 'social_networks',
				'type' => 'text',
				'std' => ''
			),
			array(
				'id' => 'plurk_link',
				'label' => __('Plurk', 'pk_translate'),
				'desc' => __('Enter URL.', 'pk_translate'),
				'section' => 'social_networks',
				'type' => 'text',
				'std' => ''
			),
			array(
				'id' => 'posterous_link',
				'label' => __('Posterous', 'pk_translate'),
				'desc' => __('Enter URL.', 'pk_translate'),
				'section' => 'social_networks',
				'type' => 'text',
				'std' => ''
			),
			array(
				'id' => 'qik_link',
				'label' => __('Qik', 'pk_translate'),
				'desc' => __('Enter URL.', 'pk_translate'),
				'section' => 'social_networks',
				'type' => 'text',
				'std' => ''
			),
			array(
				'id' => 'readernaut_link',
				'label' => __('Readernaut', 'pk_translate'),
				'desc' => __('Enter URL.', 'pk_translate'),
				'section' => 'social_networks',
				'type' => 'text',
				'std' => ''
			),
			array(
				'id' => 'reddit_link',
				'label' => __('Reddit', 'pk_translate'),
				'desc' => __('Enter URL.', 'pk_translate'),
				'section' => 'social_networks',
				'type' => 'text',
				'std' => ''
			),
			array(
				'id' => 'roboto_link',
				'label' => __('Roboto', 'pk_translate'),
				'desc' => __('Enter URL.', 'pk_translate'),
				'section' => 'social_networks',
				'type' => 'text',
				'std' => ''
			),
			array(
				'id' => 'rss_link',
				'label' => __('RSS', 'pk_translate'),
				'desc' => __('Enter URL.', 'pk_translate'),
				'section' => 'social_networks',
				'type' => 'text',
				'std' => ''
			),
			array(
				'id' => 'sharethis_link',
				'label' => __('ShareThis', 'pk_translate'),
				'desc' => __('Enter URL.', 'pk_translate'),
				'section' => 'social_networks',
				'type' => 'text',
				'std' => ''
			),
			array(
				'id' => 'skype_link',
				'label' => __('Skype', 'pk_translate'),
				'desc' => __('Enter URL.', 'pk_translate'),
				'section' => 'social_networks',
				'type' => 'text',
				'std' => ''
			),
			array(
				'id' => 'squidoo_link',
				'label' => __('Squidoo', 'pk_translate'),
				'desc' => __('Enter URL.', 'pk_translate'),
				'section' => 'social_networks',
				'type' => 'text',
				'std' => ''
			),
			array(
				'id' => 'stumbleupon_link',
				'label' => __('StumbleUpon', 'pk_translate'),
				'desc' => __('Enter URL.', 'pk_translate'),
				'section' => 'social_networks',
				'type' => 'text',
				'std' => ''
			),
			array(
				'id' => 'technorati_link',
				'label' => __('Technorati', 'pk_translate'),
				'desc' => __('Enter URL.', 'pk_translate'),
				'section' => 'social_networks',
				'type' => 'text',
				'std' => ''
			),
			array(
				'id' => 'tumblr_link',
				'label' => __('Tumblr', 'pk_translate'),
				'desc' => __('Enter URL.', 'pk_translate'),
				'section' => 'social_networks',
				'type' => 'text',
				'std' => ''
			),
			array(
				'id' => 'twitter_link',
				'label' => __('Twitter', 'pk_translate'),
				'desc' => __('Enter URL.', 'pk_translate'),
				'section' => 'social_networks',
				'type' => 'text',
				'std' => ''
			),
			array(
				'id' => 'viddler_link',
				'label' => __('Viddler', 'pk_translate'),
				'desc' => __('Enter URL.', 'pk_translate'),
				'section' => 'social_networks',
				'type' => 'text',
				'std' => ''
			),
			array(
				'id' => 'vimeo_link',
				'label' => __('Vimeo', 'pk_translate'),
				'desc' => __('Enter URL.', 'pk_translate'),
				'section' => 'social_networks',
				'type' => 'text',
				'std' => ''
			),
			array(
				'id' => 'virb_link',
				'label' => __('Virb', 'pk_translate'),
				'desc' => __('Enter URL.', 'pk_translate'),
				'section' => 'social_networks',
				'type' => 'text',
				'std' => ''
			),
			array(
				'id' => 'windows_link',
				'label' => __('Windows', 'pk_translate'),
				'desc' => __('Enter URL.', 'pk_translate'),
				'section' => 'social_networks',
				'type' => 'text',
				'std' => ''
			),
			array(
				'id' => 'wordpress_link',
				'label' => __('WordPress', 'pk_translate'),
				'desc' => __('Enter URL.', 'pk_translate'),
				'section' => 'social_networks',
				'type' => 'text',
				'std' => ''
			),
			array(
				'id' => 'xing_link',
				'label' => __('Xing', 'pk_translate'),
				'desc' => __('Enter URL.', 'pk_translate'),
				'section' => 'social_networks',
				'type' => 'text',
				'std' => ''
			),
			array(
				'id' => 'yahoo_link',
				'label' => __('Yahoo', 'pk_translate'),
				'desc' => __('Enter URL.', 'pk_translate'),
				'section' => 'social_networks',
				'type' => 'text',
				'std' => ''
			),
			array(
				'id' => 'yahoobuzz_link',
				'label' => __('YahooBuzz', 'pk_translate'),
				'desc' => __('Enter URL.', 'pk_translate'),
				'section' => 'social_networks',
				'type' => 'text',
				'std' => ''
			),
			array(
				'id' => 'yelp_link',
				'label' => __('Yelp', 'pk_translate'),
				'desc' => __('Enter URL.', 'pk_translate'),
				'section' => 'social_networks',
				'type' => 'text',
				'std' => ''
			),
			array(
				'id' => 'youtube_link',
				'label' => __('YouTube', 'pk_translate'),
				'desc' => __('Enter URL.', 'pk_translate'),
				'section' => 'social_networks',
				'type' => 'text',
				'std' => ''
			),
			array(
				'id' => 'zootool_link',
				'label' => __('Zootool', 'pk_translate'),
				'desc' => __('Enter URL.', 'pk_translate'),
				'section' => 'social_networks',
				'type' => 'text',
				'std' => ''
			),
			array(
				'id' => 'register_myaccount_button_color',
				'label' => __('"Register" / "My account" button color', 'pk_translate'),
				'desc' => __('Select a color for the "Register" / "My account" button. This button will be visible on the top right corner of the theme only if the user registration is allowed in the Settings -> General admin page (Membership option).', 'pk_translate'),
				'section' => 'skin',
				'std' => 'white',
				'type' => 'select',
				'choices' => array(
					array(
						'value' => 'white',
						'label' => __('White', 'pk_translate')
					),
					array(
						'value' => 'grey',
						'label' => __('Grey', 'pk_translate')
					),
					array(
						'value' => 'black',
						'label' => __('Black', 'pk_translate')
					),
					array(
						'value' => 'lime',
						'label' => __('Lime', 'pk_translate')
					),
					array(
						'value' => 'orange',
						'label' => __('Orange', 'pk_translate')
					),
					array(
						'value' => 'yellow',
						'label' => __('Yellow', 'pk_translate')
					),
					array(
						'value' => 'green',
						'label' => __('Green', 'pk_translate')
					),
					array(
						'value' => 'blue',
						'label' => __('Blue', 'pk_translate')
					),
					array(
						'value' => 'purple',
						'label' => __('Purple', 'pk_translate')
					),
					array(
						'value' => 'red',
						'label' => __('Red', 'pk_translate')
					)
				)
			),
			array(
				'id' => 'selected_skin',
				'label' => __('Skin', 'pk_translate'),
				'desc' => __('By selecting the default light and dark skin options, the theme will load predefined standard CSS files. If you select the custom skin option, you will be able to create your own color scheme thanks to the following options in this page.', 'pk_translate'),
				'section' => 'skin',
				'std' => 'light',
				'type' => 'radio',
				'choices' => array(
					array(
						'value' => 'light',
						'label' => __('Default light skin', 'pk_translate')
					),
					array(
						'value' => 'dark',
						'label' => __('Default dark skin', 'pk_translate')
					),
					array(
						'value' => 'custom',
						'label' => __('Custom skin', 'pk_translate')
					)
				)
			),
			array(
				'id' => 'custom_skin_general_settings',
				'label' => '',
				'section' => 'skin',
				'desc' => '<h1>'.__('Custom skin general settings', 'pk_translate').'</h1>',
				'type' => 'textblock'
			),
			array(
				'id' => 'skin_type',
				'label' => __('Graphic elements style', 'pk_translate'),
				'desc' => __('Select light or dark to load the default graphic elements of the theme (icons, shadows, buttons, etc.) that better match with the color scheme that you are going to create.', 'pk_translate'),
				'section' => 'skin',
				'std' => 'light',
				'type' => 'radio',
				'choices' => array(
					array(
						'value' => 'light',
						'label' => __('Light', 'pk_translate')
					),
					array(
						'value' => 'dark',
						'label' => __('Dark', 'pk_translate')
					)
				)
			),
			array(
				'id' => 'site_bg_color',
				'label' => __('Site background color', 'pk_translate'),
				'desc' => __('Select a color.', 'pk_translate'),
				'section' => 'skin',
				'std' => '#ebe9e9',
				'type' => 'colorpicker'
			),
			array(
				'id' => 'theme_special_colors',
				'label' => '',
				'section' => 'skin',
				'desc' => '<h1>'.__('Theme special colors', 'pk_translate').'</h1>',
				'type' => 'textblock'
			),
			array(
				'id' => 'site_special_color',
				'label' => __('Site special color', 'pk_translate'),
				'desc' => __('Select a color.', 'pk_translate'),
				'section' => 'skin',
				'std' => '#9ab920',
				'type' => 'colorpicker'
			),
			array(
				'id' => 'site_special_primary_text_color',
				'label' => __('Site special primary text color', 'pk_translate'),
				'desc' => __('Select a color.', 'pk_translate'),
				'section' => 'skin',
				'std' => '#ffffff',
				'type' => 'colorpicker'
			),
			array(
				'id' => 'site_special_secondary_text_color',
				'label' => __('Site special secondary text color', 'pk_translate'),
				'desc' => __('Select a color.', 'pk_translate'),
				'section' => 'skin',
				'std' => '#d8e4a8',
				'type' => 'colorpicker'
			),
			array(
				'id' => 'header_text_colors',
				'label' => '',
				'section' => 'skin',
				'desc' => '<h1>'.__('Header text colors', 'pk_translate').'</h1>',
				'type' => 'textblock'
			),
			array(
				'id' => 'header_text_color',
				'label' => __('Header text color', 'pk_translate'),
				'desc' => __('Select a color.', 'pk_translate'),
				'section' => 'skin',
				'std' => '#888e90',
				'type' => 'colorpicker'
			),
			array(
				'id' => 'header_link_color',
				'label' => __('Header link color', 'pk_translate'),
				'desc' => __('Select a color.', 'pk_translate'),
				'section' => 'skin',
				'std' => '#373d3f',
				'type' => 'colorpicker'
			),
			array(
				'id' => 'header_link_rollover_color',
				'label' => __('Header link rollover color', 'pk_translate'),
				'desc' => __('Select a color.', 'pk_translate'),
				'section' => 'skin',
				'std' => '#000000',
				'type' => 'colorpicker'
			),
			array(
				'id' => 'main_menu_colors',
				'label' => '',
				'section' => 'skin',
				'desc' => '<h1>'.__('Main menu colors', 'pk_translate').'</h1>',
				'type' => 'textblock'
			),
			array(
				'id' => 'menu_bg_color',
				'label' => __('Main menu background color', 'pk_translate'),
				'desc' => __('Select a color.', 'pk_translate'),
				'section' => 'skin',
				'std' => '#a4a3a3',
				'type' => 'colorpicker'
			),
			array(
				'id' => 'menu_text_color',
				'label' => __('Main menu link color', 'pk_translate'),
				'desc' => __('Select a color.', 'pk_translate'),
				'section' => 'skin',
				'std' => '#ffffff',
				'type' => 'colorpicker'
			),
			array(
				'id' => 'menu_text_rollover_color',
				'label' => __('Main menu link rollover color', 'pk_translate'),
				'desc' => __('Select a color.', 'pk_translate'),
				'section' => 'skin',
				'std' => '#5a5a5a',
				'type' => 'colorpicker'
			),
			array(
				'id' => 'menu_text_rollover_bg_color',
				'label' => __('Main menu link rollover background color', 'pk_translate'),
				'desc' => __('Select a color.', 'pk_translate'),
				'section' => 'skin',
				'std' => '#ffffff',
				'type' => 'colorpicker'
			),
			array(
				'id' => 'sub_menu_bg_color',
				'label' => __('Sub menu background color', 'pk_translate'),
				'desc' => __('Select a color.', 'pk_translate'),
				'section' => 'skin',
				'std' => '#ffffff',
				'type' => 'colorpicker'
			),
			array(
				'id' => 'sub_menu_text_color',
				'label' => __('Sub menu link color', 'pk_translate'),
				'desc' => __('Select a color.', 'pk_translate'),
				'section' => 'skin',
				'std' => '#808080',
				'type' => 'colorpicker'
			),
			array(
				'id' => 'sub_menu_text_rollover_color',
				'label' => __('Sub menu link rollover color', 'pk_translate'),
				'desc' => __('Select a color.', 'pk_translate'),
				'section' => 'skin',
				'std' => '#5a5a5a',
				'type' => 'colorpicker'
			),
			array(
				'id' => 'mobile_menu_colors',
				'label' => '',
				'section' => 'skin',
				'desc' => '<h1>'.__('Mobile menu colors', 'pk_translate').'</h1>',
				'type' => 'textblock'
			),
			array(
				'id' => 'mobile_menu_bg_color',
				'label' => __('Mobile menu background color', 'pk_translate'),
				'desc' => __('Select a color.', 'pk_translate'),
				'section' => 'skin',
				'std' => '#838282',
				'type' => 'colorpicker'
			),
			array(
				'id' => 'mobile_menu_border_top_color',
				'label' => __('Mobile menu border top color', 'pk_translate'),
				'desc' => __('Select a color.', 'pk_translate'),
				'section' => 'skin',
				'std' => '#787676',
				'type' => 'colorpicker'
			),
			array(
				'id' => 'mobile_menu_borders_color',
				'label' => __('Mobile menu borders color', 'pk_translate'),
				'desc' => __('Select a color.', 'pk_translate'),
				'section' => 'skin',
				'std' => '#919090',
				'type' => 'colorpicker'
			),
			array(
				'id' => 'mobile_menu_text_color',
				'label' => __('Mobile menu link color', 'pk_translate'),
				'desc' => __('Select a color.', 'pk_translate'),
				'section' => 'skin',
				'std' => '#fafafa',
				'type' => 'colorpicker'
			),
			array(
				'id' => 'mobile_menu_selected_text_color',
				'label' => __('Mobile menu selected link color', 'pk_translate'),
				'desc' => __('Select a color.', 'pk_translate'),
				'section' => 'skin',
				'std' => '#fafafa',
				'type' => 'colorpicker'
			),
			array(
				'id' => 'mobile_menu_selected_bg_color',
				'label' => __('Mobile menu selected link background color', 'pk_translate'),
				'desc' => __('Select a color.', 'pk_translate'),
				'section' => 'skin',
				'std' => '#919090',
				'type' => 'colorpicker'
			),
			array(
				'id' => 'options_navigation_colors',
				'label' => '',
				'section' => 'skin',
				'desc' => '<h1>'.__('Options navigation colors', 'pk_translate').'</h1>',
				'type' => 'textblock'
			),
			array(
				'id' => 'option_nav_search_text_color',
				'label' => __('Options navigation search field text color', 'pk_translate'),
				'desc' => __('Select a color.', 'pk_translate'),
				'section' => 'skin',
				'std' => '#888e90',
				'type' => 'colorpicker'
			),
			array(
				'id' => 'option_nav_search_bg_color',
				'label' => __('Options navigation search field background color', 'pk_translate'),
				'desc' => __('Select a color.', 'pk_translate'),
				'section' => 'skin',
				'std' => '#ffffff',
				'type' => 'colorpicker'
			),
			array(
				'id' => 'option_nav_text_color',
				'label' => __('Options navigation text color', 'pk_translate'),
				'desc' => __('Select a color.', 'pk_translate'),
				'section' => 'skin',
				'std' => '#888e90',
				'type' => 'colorpicker'
			),
			array(
				'id' => 'main contents_area_colors',
				'label' => '',
				'section' => 'skin',
				'desc' => '<h1>'.__('Main contents area colors', 'pk_translate').'</h1>',
				'type' => 'textblock'
			),
			array(
				'id' => 'main_bg_color',
				'label' => __('Main contents background color', 'pk_translate'),
				'desc' => __('Select a color.', 'pk_translate'),
				'section' => 'skin',
				'std' => '#fafafa',
				'type' => 'colorpicker'
			),
			array(
				'id' => 'main_primary_text_color',
				'label' => __('Main contents primary text color', 'pk_translate'),
				'desc' => __('Select a color.', 'pk_translate'),
				'section' => 'skin',
				'std' => '#888e90',
				'type' => 'colorpicker'
			),
			array(
				'id' => 'main_secondary_text_color',
				'label' => __('Main contents secondary text color', 'pk_translate'),
				'desc' => __('Select a color.', 'pk_translate'),
				'section' => 'skin',
				'std' => '#a0a7a9',
				'type' => 'colorpicker'
			),
			array(
				'id' => 'main_titles_color',
				'label' => __('Main contents titles color', 'pk_translate'),
				'desc' => __('Select a color.', 'pk_translate'),
				'section' => 'skin',
				'std' => '#686c6e',
				'type' => 'colorpicker'
			),
			array(
				'id' => 'main_titles_bg_color',
				'label' => __('Main contents titles background color', 'pk_translate'),
				'desc' => __('Select a color.', 'pk_translate'),
				'section' => 'skin',
				'std' => '#f8f8f8',
				'type' => 'colorpicker'
			),
			array(
				'id' => 'main_link_color',
				'label' => __('Main contents link color', 'pk_translate'),
				'desc' => __('Select a color.', 'pk_translate'),
				'section' => 'skin',
				'std' => '#9ab920',
				'type' => 'colorpicker'
			),
			array(
				'id' => 'main_link_rollover_color',
				'label' => __('Main contents link rollover color', 'pk_translate'),
				'desc' => __('Select a color.', 'pk_translate'),
				'section' => 'skin',
				'std' => '#87a21d',
				'type' => 'colorpicker'
			),
			array(
				'id' => 'main_borders_color',
				'label' => __('Main contents borders color', 'pk_translate'),
				'desc' => __('Select a color.', 'pk_translate'),
				'section' => 'skin',
				'std' => '#e8e9e9',
				'type' => 'colorpicker'
			),
			array(
				'id' => 'main_input_text_color',
				'label' => __('Main contents input fields text color', 'pk_translate'),
				'desc' => __('Select a color.', 'pk_translate'),
				'section' => 'skin',
				'std' => '#888e90',
				'type' => 'colorpicker'
			),
			array(
				'id' => 'main_input_bg_color',
				'label' => __('Main contents input fields background color', 'pk_translate'),
				'desc' => __('Select a color.', 'pk_translate'),
				'section' => 'skin',
				'std' => '#ffffff',
				'type' => 'colorpicker'
			),
			array(
				'id' => 'mobile_sidebar_bg_color',
				'label' => __('Main contents mobile sidebar background color', 'pk_translate'),
				'desc' => __('Select a color.', 'pk_translate'),
				'section' => 'skin',
				'std' => '#f4f4f4',
				'type' => 'colorpicker'
			),
			array(
				'id' => 'boxes_main_bg_color',
				'label' => __('Main boxed contents background color', 'pk_translate'),
				'desc' => __('Select a color.', 'pk_translate'),
				'section' => 'skin',
				'std' => '#ffffff',
				'type' => 'colorpicker'
			),
			array(
				'id' => 'boxes_main_borders_color',
				'label' => __('Main boxed contents borders color', 'pk_translate'),
				'desc' => __('Select a color.', 'pk_translate'),
				'section' => 'skin',
				'std' => '#e8e9e9',
				'type' => 'colorpicker'
			),
			array(
				'id' => 'boxes_main_input_text_color',
				'label' => __('Main boxed contents input fields text color', 'pk_translate'),
				'desc' => __('Select a color.', 'pk_translate'),
				'section' => 'skin',
				'std' => '#888e90',
				'type' => 'colorpicker'
			),
			array(
				'id' => 'boxes_main_input_bg_color',
				'label' => __('Main boxed contents input fields background color', 'pk_translate'),
				'desc' => __('Select a color.', 'pk_translate'),
				'section' => 'skin',
				'std' => '#fafafa',
				'type' => 'colorpicker'
			),
			array(
				'id' => 'footer_widgets_area_colors',
				'label' => '',
				'section' => 'skin',
				'desc' => '<h1>'.__('Footer widgets area colors', 'pk_translate').'</h1>',
				'type' => 'textblock'
			),
			array(
				'id' => 'footer_wa_bg_color',
				'label' => __('Footer widgets area background color', 'pk_translate'),
				'desc' => __('Select a color.', 'pk_translate'),
				'section' => 'skin',
				'std' => '#373d3f',
				'type' => 'colorpicker'
			),
			array(
				'id' => 'footer_wa_primary_text_color',
				'label' => __('Footer widgets area primary text color', 'pk_translate'),
				'desc' => __('Select a color.', 'pk_translate'),
				'section' => 'skin',
				'std' => '#888e90',
				'type' => 'colorpicker'
			),
			array(
				'id' => 'footer_wa_secondary_text_color',
				'label' => __('Footer widgets area secondary text color', 'pk_translate'),
				'desc' => __('Select a color.', 'pk_translate'),
				'section' => 'skin',
				'std' => '#606566',
				'type' => 'colorpicker'
			),
			array(
				'id' => 'footer_wa_titles_color',
				'label' => __('Footer widgets area titles color', 'pk_translate'),
				'desc' => __('Select a color.', 'pk_translate'),
				'section' => 'skin',
				'std' => '#fafafa',
				'type' => 'colorpicker'
			),
			array(
				'id' => 'footer_wa_titles_bg_color',
				'label' => __('Footer widgets area titles background color', 'pk_translate'),
				'desc' => __('Select a color.', 'pk_translate'),
				'section' => 'skin',
				'std' => '#585e61',
				'type' => 'colorpicker'
			),
			array(
				'id' => 'footer_wa_link_color',
				'label' => __('Footer widgets area link color', 'pk_translate'),
				'desc' => __('Select a color.', 'pk_translate'),
				'section' => 'skin',
				'std' => '#9ab920',
				'type' => 'colorpicker'
			),
			array(
				'id' => 'footer_wa_link_rollover_color',
				'label' => __('Footer widgets area link rollover color', 'pk_translate'),
				'desc' => __('Select a color.', 'pk_translate'),
				'section' => 'skin',
				'std' => '#87a21d',
				'type' => 'colorpicker'
			),
			array(
				'id' => 'footer_wa_borders_color',
				'label' => __('Footer widgets area borders color', 'pk_translate'),
				'desc' => __('Select a color.', 'pk_translate'),
				'section' => 'skin',
				'std' => '#43484a',
				'type' => 'colorpicker'
			),
			array(
				'id' => 'footer_wa_input_text_color',
				'label' => __('Footer widgets area input fields color', 'pk_translate'),
				'desc' => __('Select a color.', 'pk_translate'),
				'section' => 'skin',
				'std' => '#888e90',
				'type' => 'colorpicker'
			),
			array(
				'id' => 'footer_wa_input_bg_color',
				'label' => __('Footer widgets area input fields background color', 'pk_translate'),
				'desc' => __('Select a color.', 'pk_translate'),
				'section' => 'skin',
				'std' => '#343a3c',
				'type' => 'colorpicker'
			),
			array(
				'id' => 'boxes_footer_wa_bg_color',
				'label' => __('Footer widgets area boxed contents background color', 'pk_translate'),
				'desc' => __('Select a color.', 'pk_translate'),
				'section' => 'skin',
				'std' => '#3b4143',
				'type' => 'colorpicker'
			),
			array(
				'id' => 'boxes_footer_wa_borders_color',
				'label' => __('Footer widgets area boxed contents borders color', 'pk_translate'),
				'desc' => __('Select a color.', 'pk_translate'),
				'section' => 'skin',
				'std' => '#343739',
				'type' => 'colorpicker'
			),
			array(
				'id' => 'boxes_footer_wa_input_text_color',
				'label' => __('Footer widgets area boxed contents input fields color', 'pk_translate'),
				'desc' => __('Select a color.', 'pk_translate'),
				'section' => 'skin',
				'std' => '#888e90',
				'type' => 'colorpicker'
			),
			array(
				'id' => 'boxes_footer_wa_input_bg_color',
				'label' => __('Footer widgets area boxed contents input fields background color', 'pk_translate'),
				'desc' => __('Select a color.', 'pk_translate'),
				'section' => 'skin',
				'std' => '#343a3c',
				'type' => 'colorpicker'
			),
			array(
				'id' => 'footer_colors',
				'label' => '',
				'section' => 'skin',
				'desc' => '<h1>'.__('Footer colors', 'pk_translate').'</h1>',
				'type' => 'textblock'
			),
			array(
				'id' => 'footer_bg_color',
				'label' => __('Footer background color', 'pk_translate'),
				'desc' => __('Select a color.', 'pk_translate'),
				'section' => 'skin',
				'std' => '#373d3f',
				'type' => 'colorpicker'
			),
			array(
				'id' => 'footer_text_color',
				'label' => __('Footer text color', 'pk_translate'),
				'desc' => __('Select a color.', 'pk_translate'),
				'section' => 'skin',
				'std' => '#707476',
				'type' => 'colorpicker'
			),
			array(
				'id' => 'footer_link_color',
				'label' => __('Footer link color', 'pk_translate'),
				'desc' => __('Select a color.', 'pk_translate'),
				'section' => 'skin',
				'std' => '#707476',
				'type' => 'colorpicker'
			),
			array(
				'id' => 'footer_link_rollover_color',
				'label' => __('Footer link rollover color', 'pk_translate'),
				'desc' => __('Select a color.', 'pk_translate'),
				'section' => 'skin',
				'std' => '#a3a8ab',
				'type' => 'colorpicker'
			),
			array(
				'id' => 'mobile_footer_colors',
				'label' => '',
				'section' => 'skin',
				'desc' => '<h1>'.__('Mobile footer colors', 'pk_translate').'</h1>',
				'type' => 'textblock'
			),
			array(
				'id' => 'mobile_footer_copyright_text_color',
				'label' => __('Mobile footer copyright text color', 'pk_translate'),
				'desc' => __('Select a color.', 'pk_translate'),
				'section' => 'skin',
				'std' => '#707476',
				'type' => 'colorpicker'
			),
			array(
				'id' => 'mobile_footer_copyright_bg_color',
				'label' => __('Mobile footer copyright text background color', 'pk_translate'),
				'desc' => __('Select a color.', 'pk_translate'),
				'section' => 'skin',
				'std' => '#303537',
				'type' => 'colorpicker'
			),
			array(
				'id' => 'mobile_footer_copyright_borders_color',
				'label' => __('Mobile footer copyright text borders color', 'pk_translate'),
				'desc' => __('Select a color.', 'pk_translate'),
				'section' => 'skin',
				'std' => '#3d4143',
				'type' => 'colorpicker'
			),
			array(
				'id' => 'business_home_tabs_colors',
				'label' => '',
				'section' => 'skin',
				'desc' => '<h1>'.__('Business home template tabs colors', 'pk_translate').'</h1>',
				'type' => 'textblock'
			),
			array(
				'id' => 'bh_tabs_bg_color',
				'label' => __('Business home template tabs background color', 'pk_translate'),
				'desc' => __('Select a color.', 'pk_translate'),
				'section' => 'skin',
				'std' => '#f2f2f2',
				'type' => 'colorpicker'
			),
			array(
				'id' => 'bh_tabs_text_color',
				'label' => __('Business home template tabs text color', 'pk_translate'),
				'desc' => __('Select a color.', 'pk_translate'),
				'section' => 'skin',
				'std' => '#686c6e',
				'type' => 'colorpicker'
			),
			array(
				'id' => 'bh_tabs_text_bg_color',
				'label' => __('Business home template tabs text background color', 'pk_translate'),
				'desc' => __('Select a color.', 'pk_translate'),
				'section' => 'skin',
				'std' => '#f6f6f6',
				'type' => 'colorpicker'
			),
			array(
				'id' => 'bh_tabs_selected_text_color',
				'label' => __('Business home template tabs selected text color', 'pk_translate'),
				'desc' => __('Select a color.', 'pk_translate'),
				'section' => 'skin',
				'std' => '#babdbe',
				'type' => 'colorpicker'
			),
			array(
				'id' => 'bh_tabs_borders_color',
				'label' => __('Business home template tabs borders color', 'pk_translate'),
				'desc' => __('Select a color.', 'pk_translate'),
				'section' => 'skin',
				'std' => '#f0f0f0',
				'type' => 'colorpicker'
			),
			array(
				'id' => 'jigoshop_woocommerce_specific_colors',
				'label' => '',
				'section' => 'skin',
				'desc' => '<h1>'.__('JigoShop / WooCommerce specific colors', 'pk_translate').'</h1>',
				'type' => 'textblock'
			),
			array(
				'id' => 'ec_products_price_color',
				'label' => __('JigoShop / WooCommerce products price text color', 'pk_translate'),
				'desc' => __('Select a color.', 'pk_translate'),
				'section' => 'skin',
				'std' => '#373d3f',
				'type' => 'colorpicker'
			),
			array(
				'id' => 'ec_product_no_stock_text_color',
				'label' => __('JigoShop / WooCommerce products not in stock text color', 'pk_translate'),
				'desc' => __('Select a color.', 'pk_translate'),
				'section' => 'skin',
				'std' => '#a501b4',
				'type' => 'colorpicker'
			),
			array(
				'id' => 'mobile_ec_tabs_selected_text_color',
				'label' => __('Mobile JigoShop / WooCommerce tabs selected text color', 'pk_translate'),
				'desc' => __('Select a color.', 'pk_translate'),
				'section' => 'skin',
				'std' => '#ffffff',
				'type' => 'colorpicker'
			),
			array(
				'id' => 'mobile_ec_tabs_selected_bg_color',
				'label' => __('Mobile JigoShop / WooCommerce tabs selected background color', 'pk_translate'),
				'desc' => __('Select a color.', 'pk_translate'),
				'section' => 'skin',
				'std' => '#b1b0b0',
				'type' => 'colorpicker'
			),
			array(
				'id' => 'mobile_ec_tabs_borders_color',
				'label' => __('Mobile JigoShop / WooCommerce tabs borders color', 'pk_translate'),
				'desc' => __('Select a color.', 'pk_translate'),
				'section' => 'skin',
				'std' => '#b1b0b0',
				'type' => 'colorpicker'
			),
			array(
				'id' => 'bbpress_forum_specific_colors',
				'label' => '',
				'section' => 'skin',
				'desc' => '<h1>'.__('bbPress forum specific colors', 'pk_translate').'</h1>',
				'type' => 'textblock'
			),
			array(
				'id' => 'bbp_sticky_bg_color',
				'label' => __('bbPress forum sticky posts background color', 'pk_translate'),
				'desc' => __('Select a color.', 'pk_translate'),
				'section' => 'skin',
				'std' => '#fffbe7',
				'type' => 'colorpicker'
			),
			array(
				'id' => 'bbp_super_sticky_bg_color',
				'label' => __('bbPress forum super sticky posts background color', 'pk_translate'),
				'desc' => __('Select a color.', 'pk_translate'),
				'section' => 'skin',
				'std' => '#fef6d1',
				'type' => 'colorpicker'
			),
			array(
				'id' => 'html5_players_colors',
				'label' => '',
				'section' => 'skin',
				'desc' => '<h1>'.__('HTML5 players colors', 'pk_translate').'</h1>',
				'type' => 'textblock'
			),
			array(
				'id' => 'controls_bars_light_bg_color',
				'label' => __('HTML5 players controls bar light background color', 'pk_translate'),
				'desc' => __('Select a color.', 'pk_translate'),
				'section' => 'skin',
				'std' => '#e8e7e7',
				'type' => 'colorpicker'
			),
			array(
				'id' => 'controls_bars_dark_bg_color',
				'label' => __('HTML5 players controls bar dark background color', 'pk_translate'),
				'desc' => __('Select a color.', 'pk_translate'),
				'section' => 'skin',
				'std' => '#232728',
				'type' => 'colorpicker'
			),
			array(
				'id' => 'controls_seek_bar_bg_color',
				'label' => __('HTML5 players controls seek bar background color', 'pk_translate'),
				'desc' => __('Select a color.', 'pk_translate'),
				'section' => 'skin',
				'std' => '#cac9c9',
				'type' => 'colorpicker'
			),
			array(
				'id' => 'post_sliders_colors',
				'label' => '',
				'section' => 'skin',
				'desc' => '<h1>'.__('Post sliders colors', 'pk_translate').'</h1>',
				'type' => 'textblock'
			),
			array(
				'id' => 'sliders_info_text_color',
				'label' => __('Post sliders info text color', 'pk_translate'),
				'desc' => __('Select a color.', 'pk_translate'),
				'section' => 'skin',
				'std' => '#fafafa',
				'type' => 'colorpicker'
			),
			array(
				'id' => 'sliders_info_bg_color',
				'label' => __('Post sliders info background color', 'pk_translate'),
				'desc' => __('Select a color.', 'pk_translate'),
				'section' => 'skin',
				'std' => '#a4a3a3',
				'type' => 'colorpicker'
			),
			array(
				'id' => 'sliders_overlay_bg_color',
				'label' => __('Post sliders overlay background color', 'pk_translate'),
				'desc' => __('Select a color.', 'pk_translate'),
				'section' => 'skin',
				'std' => '#403e3e',
				'type' => 'colorpicker'
			),
			array(
				'id' => 'pricing_tables_colors',
				'label' => '',
				'section' => 'skin',
				'desc' => '<h1>'.__('Pricing tables colors', 'pk_translate').'</h1>',
				'type' => 'textblock'
			),
			array(
				'id' => 'pt_bg_color',
				'label' => __('Pricing tables background color', 'pk_translate'),
				'desc' => __('Select a color.', 'pk_translate'),
				'section' => 'skin',
				'std' => '#ffffff',
				'type' => 'colorpicker'
			),
			array(
				'id' => 'pt_primary_text_color',
				'label' => __('Pricing tables primary text color', 'pk_translate'),
				'desc' => __('Select a color.', 'pk_translate'),
				'section' => 'skin',
				'std' => '#888e90',
				'type' => 'colorpicker'
			),
			array(
				'id' => 'pt_secondary_text_color',
				'label' => __('Pricing tables secondary text color', 'pk_translate'),
				'desc' => __('Select a color.', 'pk_translate'),
				'section' => 'skin',
				'std' => '#d0d0d0',
				'type' => 'colorpicker'
			),
			array(
				'id' => 'pt_borders_color',
				'label' => __('Pricing tables borders color', 'pk_translate'),
				'desc' => __('Select a color.', 'pk_translate'),
				'section' => 'skin',
				'std' => '#e8e9e9',
				'type' => 'colorpicker'
			),
			array(
				'id' => 'pt_alternate_color',
				'label' => __('Pricing tables alternate row color', 'pk_translate'),
				'desc' => __('Select a color.', 'pk_translate'),
				'section' => 'skin',
				'std' => '#f6f6f6',
				'type' => 'colorpicker'
			),
			array(
				'id' => 'pt_header_bg_color',
				'label' => __('Pricing tables header background color', 'pk_translate'),
				'desc' => __('Select a color.', 'pk_translate'),
				'section' => 'skin',
				'std' => '#a4a3a3',
				'type' => 'colorpicker'
			),
			array(
				'id' => 'pt_header_title_text_color',
				'label' => __('Pricing tables header title text color', 'pk_translate'),
				'desc' => __('Select a color.', 'pk_translate'),
				'section' => 'skin',
				'std' => '#ffffff',
				'type' => 'colorpicker'
			),
			array(
				'id' => 'pt_header_title_borders_color',
				'label' => __('Pricing tables header title borders color', 'pk_translate'),
				'desc' => __('Select a color.', 'pk_translate'),
				'section' => 'skin',
				'std' => '#c6c4c4',
				'type' => 'colorpicker'
			),
			array(
				'id' => 'pt_header_price_bg_color',
				'label' => __('Pricing tables header price background color', 'pk_translate'),
				'desc' => __('Select a color.', 'pk_translate'),
				'section' => 'skin',
				'std' => '#e1e1e1',
				'type' => 'colorpicker'
			),
			array(
				'id' => 'pt_header_price_primary_text_color',
				'label' => __('Pricing tables header price primary text color', 'pk_translate'),
				'desc' => __('Select a color.', 'pk_translate'),
				'section' => 'skin',
				'std' => '#454545',
				'type' => 'colorpicker'
			),
			array(
				'id' => 'pt_header_price_secondary_text_color',
				'label' => __('Pricing tables header price secondary text color', 'pk_translate'),
				'desc' => __('Select a color.', 'pk_translate'),
				'section' => 'skin',
				'std' => '#888e90',
				'type' => 'colorpicker'
			),
			array(
				'id' => 'pt_header_price_borders_color',
				'label' => __('Pricing tables header price borders color', 'pk_translate'),
				'desc' => __('Select a color.', 'pk_translate'),
				'section' => 'skin',
				'std' => '#d5d5d5',
				'type' => 'colorpicker'
			),
			array(
				'id' => 'pt_footer_bg_color',
				'label' => __('Pricing tables footer background color', 'pk_translate'),
				'desc' => __('Select a color.', 'pk_translate'),
				'section' => 'skin',
				'std' => '#f6f6f6',
				'type' => 'colorpicker'
			),
			array(
				'id' => 'pt_footer_borders_color',
				'label' => __('Pricing tables footer borders color', 'pk_translate'),
				'desc' => __('Select a color.', 'pk_translate'),
				'section' => 'skin',
				'std' => '#e8e9e9',
				'type' => 'colorpicker'
			),
			array(
				'id' => 'pretty_photo_colors',
				'label' => '',
				'section' => 'skin',
				'desc' => '<h1>'.__('PrettyPhoto lightbox colors', 'pk_translate').'</h1>',
				'type' => 'textblock'
			),
			array(
				'id' => 'pp_bg_color',
				'label' => __('PrettPhoto lightbox background color', 'pk_translate'),
				'desc' => __('Select a color.', 'pk_translate'),
				'section' => 'skin',
				'std' => '#f9f9f9',
				'type' => 'colorpicker'
			),
			array(
				'id' => 'pp_overlay_bg_color',
				'label' => __('PrettPhoto lightbox overlay background color', 'pk_translate'),
				'desc' => __('Select a color.', 'pk_translate'),
				'section' => 'skin',
				'std' => '#373d3f',
				'type' => 'colorpicker'
				),
			array(
				'id' => 'css_typography',
				'label' => __('Typography CSS', 'pk_translate'),
				'desc' => __('Define the CSS styles for the theme typography. Do not enter the style tags, they will be added automatically.', 'pk_translate'),
				'section' => 'typography',
				'type' => 'textarea_simple',
				'std' => '',
				'rows' => 20
			),
			array(
				'id' => 'css_custom',
				'label' => __('Custom CSS', 'pk_translate'),
				'desc' => __('Define the custom CSS styles for your theme. Do not enter the style tags, they will be added automatically.', 'pk_translate'),
				'section' => 'custom_css',
				'type' => 'textarea_simple',
				'std' => '',
				'rows' => 20
			),
			array(
				'id' => 'js_custom',
				'label' => __('Custom JS', 'pk_translate'),
				'desc' => __('Define the custom JS script for your theme. Do not enter the main script tags, they will be added automatically.', 'pk_translate'),
				'section' => 'custom_js',
				'type' => 'textarea_simple',
				'std' => '',
				'rows' => 20
			),
			array(
				'id' => 'optimize',
				'label' => __('Optimize performances', 'pk_translate'),
				'desc' => __('By selecting "Yes" the styles and the scripts of the theme will be minified and combined automatically by the system, resulting in a faster page loading time.', 'pk_translate'),
				'section' => 'advanced',
				'type' => 'radio',
				'std' => 'false',
				'choices' => array(
					array(
						'value' => 'true',
						'label' => __('Yes', 'pk_translate')
					),
					array(
						'value' => 'false',
						'label' => __('No', 'pk_translate')
					)
				)
			),
			array(
				'id' => 'homepages_latest_posts',
				'label' => __('Display latest posts on home pages', 'pk_translate'),
				'desc' => __('By selecting "No" the latest posts will not be displayed in the home page templates.', 'pk_translate'),
				'section' => 'additional_options',
				'type' => 'radio',
				'std' => 'true',
				'choices' => array(
					array(
						'value' => 'true',
						'label' => __('Yes', 'pk_translate')
					),
					array(
						'value' => 'false',
						'label' => __('No', 'pk_translate')
					)
				)
			),
			array(
				'id' => 'homepages_latest_works',
				'label' => __('Display latest works on home pages', 'pk_translate'),
				'desc' => __('By selecting "No" the latest works will not be displayed in the home page templates.', 'pk_translate'),
				'section' => 'additional_options',
				'type' => 'radio',
				'std' => 'true',
				'choices' => array(
					array(
						'value' => 'true',
						'label' => __('Yes', 'pk_translate')
					),
					array(
						'value' => 'false',
						'label' => __('No', 'pk_translate')
					)
				)
			),
			array(
				'id' => 'homepages_latest_products',
				'label' => __('Display latest products on home pages', 'pk_translate'),
				'desc' => __('By selecting "No" the latest products will not be displayed in the home page templates.', 'pk_translate'),
				'section' => 'additional_options',
				'type' => 'radio',
				'std' => 'true',
				'choices' => array(
					array(
						'value' => 'true',
						'label' => __('Yes', 'pk_translate')
					),
					array(
						'value' => 'false',
						'label' => __('No', 'pk_translate')
					)
				)
			),
			array(
				'id' => 'homepages_featured_products',
				'label' => __('Display featured products on home pages', 'pk_translate'),
				'desc' => __('By selecting "No" the featured products will not be displayed in the home page templates.', 'pk_translate'),
				'section' => 'additional_options',
				'type' => 'radio',
				'std' => 'true',
				'choices' => array(
					array(
						'value' => 'true',
						'label' => __('Yes', 'pk_translate')
					),
					array(
						'value' => 'false',
						'label' => __('No', 'pk_translate')
					)
				)
			),
			array(
				'id' => 'homepages_feed_reader',
				'label' => __('Display the RSS feed reader on the business home page template', 'pk_translate'),
				'desc' => __('By selecting "No" the RSS feed reader will not be displayed in the business home page template.', 'pk_translate'),
				'section' => 'additional_options',
				'type' => 'radio',
				'std' => 'true',
				'choices' => array(
					array(
						'value' => 'true',
						'label' => __('Yes', 'pk_translate')
					),
					array(
						'value' => 'false',
						'label' => __('No', 'pk_translate')
					)
				)
			)
		)
	);
	
	if ($saved_settings !== $custom_settings) update_option('option_tree_settings', $custom_settings);
	
}

?>