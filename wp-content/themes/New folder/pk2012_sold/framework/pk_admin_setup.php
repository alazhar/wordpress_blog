<?php

function pk_enqueue_admin_scripts() {
	
	wp_enqueue_script('jquery');
	
	wp_enqueue_script('jquery-ui-sortable');
	
	if (isset($_REQUEST['page']) && $_REQUEST['page'] == 'pk_portfolio.php') {
		
		wp_deregister_script('pk_sortable_portfolio');
		wp_register_script('pk_sortable_portfolio', PK_THEME_DIR.'/js/pk_sortable_portfolio.js');
		wp_enqueue_script('pk_sortable_portfolio');
		
	}
	
}

add_action('admin_init', 'pk_enqueue_admin_scripts');

function pk_enqueue_admin_styles() {
	
	if (isset($_REQUEST['page']) && $_REQUEST['page'] == 'pk_portfolio.php') {
		
		wp_enqueue_style('pk_sort', PK_THEME_DIR.'/css/pk_sort.css');
		
	}
	
}

add_action('admin_init', 'pk_enqueue_admin_styles');

function pk_admin_head() {
	
	echo '<style> #option-tree-version { visibility:hidden; } .option-tree-save-layout{ display:none; } </style>';
	
}

add_action('admin_head', 'pk_admin_head');

?>