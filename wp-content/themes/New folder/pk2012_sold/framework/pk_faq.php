<?php

function pk_register_faq() {
	
	$labels = array(
		'name' => _x('FAQ', 'post type general name', 'pk_translate'),
		'singular_name' => _x('FAQ', 'post type singular name', 'pk_translate'),
		'add_new' => _x('Add New FAQ', 'release', 'pk_translate'),
		'add_new_item' => __('Add New FAQ', 'pk_translate'),
		'edit_item' => __('Edit FAQ', 'pk_translate'),
		'new_item' => __('New FAQ', 'pk_translate'),
		'view_item' => __('View FAQ', 'pk_translate'),
		'search_items' => __('Search FAQs', 'pk_translate'),
		'not_found' => __('No FAQs found', 'pk_translate'),
		'not_found_in_trash' => __('No FAQs found in Trash', 'pk_translate')
	);
	
	$args = array(
		'labels' => $labels,
		'public' => true,
		'menu_position' => 5,
		'capability_type' => 'page',
		'hierarchical' => false,
		'supports' => array('title', 'editor', 'page-attributes'),
		'query_var' => true,
		'rewrite' => array('slug' => 'faq', 'with_front' => false),
		'show_in_nav_menus' => false,
		'has_archive' => false
	);
	
	register_post_type('faq', $args);
	
	flush_rewrite_rules();
	
}

add_action('init', 'pk_register_faq');

?>