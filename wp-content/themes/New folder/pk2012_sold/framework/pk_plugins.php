<?php

require_once (PK_FRAMEWORK.'/includes/tgm-plugin-activation/class-tgm-plugin-activation.php');

add_action('tgmpa_register', 'pk_register_required_plugins');

function pk_register_required_plugins() {
	
	$plugins = array(
		
		array(
			'name'     				=> 'Breadcrumb NavXT',
			'slug'     				=> 'breadcrumb-navxt',
			'required' 				=> false,
			'version' 				=> '',
			'force_activation' 		=> false,
			'force_deactivation' 	=> false,
			'external_url' 			=> 'http://wordpress.org/extend/plugins/breadcrumb-navxt/'
		),
		
		array(
			'name'     				=> 'Background Manager',
			'slug'     				=> 'background-manager',
			'required' 				=> false,
			'version' 				=> '',
			'force_activation' 		=> false,
			'force_deactivation' 	=> false,
			'external_url' 			=> 'http://wordpress.org/extend/plugins/background-manager/'
		),
		
		array(
			'name'     				=> 'Contact Form 7',
			'slug'     				=> 'contact-form-7',
			'required' 				=> false,
			'version' 				=> '',
			'force_activation' 		=> false,
			'force_deactivation' 	=> false,
			'external_url' 			=> 'http://wordpress.org/extend/plugins/contact-form-7/'
		),
		
		array(
			'name'     				=> 'qTranslate',
			'slug'     				=> 'qtranslate',
			'required' 				=> false,
			'version' 				=> '',
			'force_activation' 		=> false,
			'force_deactivation' 	=> false,
			'external_url' 			=> 'http://wordpress.org/extend/plugins/qtranslate/'
		),
		
		array(
			'name'     				=> 'WPML Multilingual CMS',
			'slug'     				=> 'sitepress-multilingual-cms',
			'required' 				=> false,
			'version' 				=> '',
			'force_activation' 		=> false,
			'force_deactivation' 	=> false,
			'external_url' 			=> 'http://www.wpml.org/'
		),
		
		array(
			'name'     				=> 'JigoShop',
			'slug'     				=> 'jigoshop',
			'required' 				=> false,
			'version' 				=> '',
			'force_activation' 		=> false,
			'force_deactivation' 	=> false,
			'external_url' 			=> 'http://wordpress.org/extend/plugins/jigoshop/'
		),
		
		array(
			'name'     				=> 'WooCommerce',
			'slug'     				=> 'woocommerce',
			'required' 				=> false,
			'version' 				=> '',
			'force_activation' 		=> false,
			'force_deactivation' 	=> false,
			'external_url' 			=> 'http://www.woothemes.com/woocommerce/',
		),
		
		array(
			'name'     				=> 'bbPress',
			'slug'     				=> 'bbpress',
			'required' 				=> false,
			'version' 				=> '',
			'force_activation' 		=> false,
			'force_deactivation' 	=> false,
			'external_url' 			=> 'http://wordpress.org/extend/plugins/bbpress/',
		)
		
	);

	$config = array(
		'domain'       		=> 'pk_translate',
		'default_path' 		=> '',
		'parent_menu_slug' 	=> 'themes.php',
		'parent_url_slug' 	=> 'themes.php',
		'menu'         		=> 'install-required-plugins',
		'has_notices'      	=> true,
		'is_automatic'    	=> false,
		'message' 			=> '',
		'strings'      		=> array(
			'page_title'                       			=> __('Install Required Plugins', 'pk_translate'),
			'menu_title'                       			=> __('Theme Plugins', 'pk_translate'),
			'installing'                       			=> __('Installing Plugin: %s', 'pk_translate'),
			'oops'                             			=> __('Something went wrong with the plugin API.', 'pk_translate'),
			'notice_can_install_required'     			=> _n_noop('This theme requires the following plugin: %1$s.', 'This theme requires the following plugins: %1$s.'),
			'notice_can_install_recommended'			=> _n_noop('This theme recommends the following plugin: %1$s.', 'This theme recommends the following plugins: %1$s.'),
			'notice_cannot_install'  					=> _n_noop('Sorry, but you do not have the correct permissions to install the %s plugin. Contact the administrator of this site for help on getting the plugin installed.', 'Sorry, but you do not have the correct permissions to install the %s plugins. Contact the administrator of this site for help on getting the plugins installed.'),
			'notice_can_activate_required'    			=> _n_noop('The following required plugin is currently inactive: %1$s.', 'The following required plugins are currently inactive: %1$s.'),
			'notice_can_activate_recommended'			=> _n_noop('The following recommended plugin is currently inactive: %1$s.', 'The following recommended plugins are currently inactive: %1$s.'),
			'notice_cannot_activate' 					=> _n_noop('Sorry, but you do not have the correct permissions to activate the %s plugin. Contact the administrator of this site for help on getting the plugin activated.', 'Sorry, but you do not have the correct permissions to activate the %s plugins. Contact the administrator of this site for help on getting the plugins activated.'),
			'notice_ask_to_update' 						=> _n_noop('The following plugin needs to be updated to its latest version to ensure maximum compatibility with this theme: %1$s.', 'The following plugins need to be updated to their latest version to ensure maximum compatibility with this theme: %1$s.'), // %1$s = plugin name(s)
			'notice_cannot_update' 						=> _n_noop('Sorry, but you do not have the correct permissions to update the %s plugin. Contact the administrator of this site for help on getting the plugin updated.', 'Sorry, but you do not have the correct permissions to update the %s plugins. Contact the administrator of this site for help on getting the plugins updated.'),
			'install_link' 					  			=> _n_noop('Begin installing plugin', 'Begin installing plugins'),
			'activate_link' 				  			=> _n_noop('Activate installed plugin', 'Activate installed plugins'),
			'return'                           			=> __('Return to Required Plugins Installer', 'pk_translate'),
			'plugin_activated'                 			=> __('Plugin activated successfully.', 'pk_translate'),
			'complete' 									=> __('All plugins installed and activated successfully. %s', 'pk_translate'),
			'nag_type'									=> 'updated'
		)
	);
	
	tgmpa($plugins, $config);
	
}