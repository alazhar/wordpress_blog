<?php
	remove_filter('pre_get_posts', 'pk_pre_get_posts');
	
	if (is_singular(array('page', 'post', 'portfolio', 'product')) && is_active_sidebar('custom_sidebar_'.get_the_ID())) : 
		
		dynamic_sidebar('custom_sidebar_'.get_the_ID());
		if (in_array(get_the_ID(), pk_get_option('hide_default_sidebars_for', array()))) return;
		
	endif;
	
	if (dynamic_sidebar('sidebar_all')) : endif;
	
	if (function_exists('is_jigoshop') && is_jigoshop() && dynamic_sidebar('sidebar_jigoshop')) : return; endif;
	
	if (function_exists('is_woocommerce') && is_woocommerce() && dynamic_sidebar('sidebar_woocommerce')) : return; endif;
	
	if (function_exists('is_bbpress') && is_bbpress() && dynamic_sidebar('sidebar_bbpress_forum')) : return; endif;
	
	if (is_404() && dynamic_sidebar('sidebar_404')) : return; endif;
	
	if (is_search() && dynamic_sidebar('sidebar_search')) : return; endif;
	
	if (is_front_page() && is_home() && dynamic_sidebar('sidebar_front_page') && dynamic_sidebar('sidebar_blog')) : return; endif;
	if (is_front_page() && dynamic_sidebar('sidebar_front_page')) : return; endif;
	
	if (is_home() && dynamic_sidebar('sidebar_blog')) : return; endif;
	if (is_archive() && !pk_is_portfolio() && dynamic_sidebar('sidebar_blog')) : return; endif;
	if (is_page_template('page-blog.php') && dynamic_sidebar('sidebar_blog')) : return; endif;
	
	if (pk_is_portfolio() && dynamic_sidebar('sidebar_portfolio')) : return; endif;
	if (is_page_template('page-portfolio.php') && dynamic_sidebar('sidebar_portfolio')) : return; endif;
	
	if (is_singular('page') && dynamic_sidebar('sidebar_pages')) : return; endif;
	if (is_singular('post') && dynamic_sidebar('sidebar_blog')) : return; endif;
	if (is_singular('portfolio') && dynamic_sidebar('sidebar_portfolio')) : return; endif;
?>