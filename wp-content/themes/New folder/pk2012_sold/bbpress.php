<?php
	get_header();
	
	while (have_posts()) : 
		
		the_post();
		
		do_action('pk_before_bbpress_page_content');
		
		get_template_part('format', 'page-standard');
		
		do_action('pk_after_bbpress_page_content');
		
	endwhile;
	
	get_footer();
?>