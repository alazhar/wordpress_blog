<?php
	$image_link = get_post_meta(get_post_thumbnail_id(), '_image_link', true);
	$image_link_target = get_post_meta(get_post_thumbnail_id(), '_image_link_target', true);
	
	if (has_post_thumbnail()) : 
?>

					<div class="pk_image">
						<div class="pk_image_wrapper<?php if ($image_link) echo ' pk_link_icon'; ?>">
<?php
		if ($image_link) : 
?>
							<a href="<?php echo esc_url($image_link); ?>" title="<?php echo __('Click to visit:', 'pk_translate').' '.$image_link; ?>"<?php echo ($image_link_target) ? ' target="'.$image_link_target.'"' : ' target="_self"'; ?>>
<?php
		endif;
?>
							<?php the_post_thumbnail(((is_page_template('page-full-width.php')) ? 'full-width' : 'big'), array('title' => '')); ?>

<?php
		if ($image_link) : 
?>
							<span class="pk_image_button_back_ground"></span>
							<span class="pk_image_button_icon"></span>
							</a>
<?php
		endif;
?>
						</div>
					</div>
<?php
	endif;
?>
					<?php
	add_filter('embed_defaults', 'pk_page_embed_defaults'); the_content();
?>
