<?php

# option-tree start
add_filter('ot_show_pages', '__return_false');
add_filter('ot_theme_mode', '__return_true');

require_once('option-tree/ot-loader.php');
# option-tree end

# framework start
define('PK_THEME_SLUG', 'pk_sold');
define('PK_THEME_DIR', get_template_directory_uri());
define('PK_FRAMEWORK', get_template_directory().'/framework');

require_once(PK_FRAMEWORK.'/pk_theme.php');
# framework end

# theme dir path shortcode start
function pk_theme_dir_path($atts, $content = null) {
	
	return PK_THEME_DIR;
	
}

add_shortcode('theme_dir_path', 'pk_theme_dir_path');
# theme dir path shortcode end

# prettyPhoto and jPlayer constants start
define('PK_PRETTYPHOTO_PARAMS', '{overlay_gallery:false, default_width:940, default_height:529, theme:"pp_default", social_tools:"", show_title:false, hideflash:true, wmode:"transparent", deeplinking:false}');
define('PK_JPLAYER_GUI', '<div class="jp-gui"><div class="jp-interface"><div class="jp-controls-holder"><a href="javascript:;" class="jp-play" tabindex="1" title="'.__("play", "pk_translate").'">'.__("play", "pk_translate").'</a><a href="javascript:;" class="jp-pause" tabindex="1" title="'.__("pause", "pk_translate").'">'.__("pause", "pk_translate").'</a><span class="separator sep-1"></span><div class="jp-progress"><div class="jp-seek-bar"><div class="jp-play-bar"><span></span></div></div></div><div class="jp-current-time"></div><span class="time-sep">/</span><div class="jp-duration"></div><span class="separator sep-2"></span><a href="javascript:;" class="jp-mute" tabindex="1" title="'.__("mute", "pk_translate").'">'.__("mute", "pk_translate").'</a><a href="javascript:;" class="jp-unmute" tabindex="1" title="'.__("unmute", "pk_translate").'">'.__("unmute", "pk_translate").'</a><div class="jp-volume-bar"><div class="jp-volume-bar-value"><span class="handle"></span></div></div><span class="separator sep-3"></span><a href="javascript:;" class="jp-full-screen" tabindex="1" title="'.__("full screen", "pk_translate").'">'.__("full screen", "pk_translate").'</a><a href="javascript:;" class="jp-restore-screen" tabindex="1" title="'.__("restore screen", "pk_translate").'">'.__("restore screen", "pk_translate").'</a></div></div></div><div class="jp-no-solution"><span>'.__("Error", "pk_translate").'</span><p>'.__("This audio file is not supported by your browser or the file does not exist anymore.", "pk_translate").'</p></div>');
# prettyPhoto and jPlayer constants end

# get options start
global $option_tree;
$option_tree = get_option('option_tree');
# get options end

?>