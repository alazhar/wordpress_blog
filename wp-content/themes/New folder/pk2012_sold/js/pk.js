var $pk_fj = jQuery.noConflict();
	

/*
	READY
-------------------------------------------------------------------------------------------------------------------------------------*/


$pk_fj(document).ready(function() {
	$pk_fj('.pk_navigation_wrapper').pk_menu();
	$pk_fj('.pk_option_nav').pk_option_menu();
	$pk_fj('.pk_testimonials').pk_testimonials();
	$pk_fj('.pk_business_tabs').pk_business_tabs();
	$pk_fj('.pk_tabs').pk_tabs();
	$pk_fj('.pk_toggles').pk_toggles();
	$pk_fj('.pk_zoom_icon, .pk_play_icon, .pk_page_icon, .pk_link_icon').pk_image_rollover();
	$pk_fj('.pk_login, .pk_meta_login, .pk_register, .pk_meta_register, .pk_lost_password').pk_form_box();
	$pk_fj(".lost_password").attr('href', '#pk_lost_password_form').pk_form_box();
	$pk_fj('.pk_call_to_action_news').pk_feed_rotator();
	$pk_fj('.pk_video').fitVids();

	pk();
});


/*
	MENU
-------------------------------------------------------------------------------------------------------------------------------------*/


(function($) {
	$.fn.pk_menu = function(options) {
		var defaults = {
			easing : 'easeOutCirc',
			speed : 400
		};

		var settings = $.extend({}, defaults, options);
		
		return this.each(function () {
			var root = $(this),
				lists = $('ul', this),
				buttons = lists.find('ul').parent();
			
			
			/*
				METHODS
			-------------------------------------------------------------------------------------------------------------------------*/
			
			var methods = {
				init : function() {
					var o_c = 'onorientationchange' in window,
    					w_e = (o_c) ? 'orientationchange' : 'resize';

					if($.browser.msie && parseInt($.browser.version) < 9 && !pk_detect_mobile()) {
						$(window).bind('resize.pkResizeIE8', methods.update);
					} else {
						window.addEventListener(w_e, methods.update, false);
					}
					
					$('ul > ul', root).each(function() {
						$('li:last-child', this).addClass('pk_last');
					});
					
					buttons.each(function() {
						var btn = $(this);
						
						btn.click(function() {
							if($('.pk_center_box').width() > 720) {
								var targetul = $(this).find('ul:first');
								targetul.hide();	
							}
						});
						btn.hoverIntent(function() {
							if($('.pk_center_box').width() > 720) {
								var targetul = $(this).find('ul:first');
								methods.showMenu(targetul);	
							}
						}, function() {
							if($('.pk_center_box').width() > 720) {
								var targetul = $(this).find('ul:first');
								methods.hideMenu(targetul, $(this));
							}
						});
					});
					
					$('.pk_button_menu', root).click(function(e) {
						e.preventDefault();
						
						if($(this).hasClass('pk_current')) {
							$(this).removeClass('pk_current');
							root.removeClass('pk_navigation_mobile');
						} else {
							$(this).addClass('pk_current');
							root.addClass('pk_navigation_mobile');
						}
					});
					
					/*
						WPML FIX PADDING
					-----------------------------------------------------------------------------------------------------------------*/
					
					if($('li.menu-item-language').length) {
						$('li.menu-item-language').each(function() {
							if(!$('img', this).length) {
								$(this).css('padding-left' , '0');
								$('ul.submenu-languages').css('margin-left' , '0');
							}
						});
					}
				},
				
				update : function() {
					if($('.pk_center_box').width() > 720) {
						$('.pk_button_menu', root).removeClass('pk_current');
						root.removeClass('pk_navigation_mobile');
					}
				},
				
				showMenu : function(element) {
					element.css({
						visibility : 'visible'
					}).stop(true, true).slideDown(settings.speed, settings.easing);
				},
				
				hideMenu : function(element, current) {
					element.stop(true, true).slideUp(settings.speed, settings.easing, function() {
						element.hide();
					});
				}
			}
			
			methods.init();
		});
	};
})(jQuery);


/*
	OPTION MENU
-------------------------------------------------------------------------------------------------------------------------------------*/


(function($) {
	$.fn.pk_option_menu = function() {
		return this.each(function () {
			var root = $(this),
				buttons = $('li.pk_option_item'),
				interval = undefined;
			
			if(pk_detect_mobile()) return;
				
			buttons.hoverIntent(function() {
				if($('.mini-cart', this).length) {
					clearTimeout(interval);
					$pk_fj(this).addClass('pk_option_selected_item');
					$pk_fj('.mini-cart').stop(true, true).slideDown(400, 'easeOutCirc');
					
					return;
				}
					
				if($pk_fj('#pk_search_form', this).length ) {
					if($('.pk_navigation_wrapper').width() <= 718) return;
					$pk_fj('#pk_search_form').stop().animate({ 'width' : '243px' }, 400, 'easeOutCirc');
						
					return;
				}
			}, function() {
				if($pk_fj('.mini-cart', this).length) {
					var button = $pk_fj(this);
					interval = setTimeout(function() { button.removeClass('pk_option_selected_item'); }, 400);
					$pk_fj('.mini-cart').stop(true, true).slideUp(400, 'easeOutCirc');
						
					return;
				}
			
				if($pk_fj('#pk_search_form', this).length) {
					if($('.pk_navigation_wrapper').width() <= 718) return;
					$pk_fj('#pk_search_form').stop().animate({ 'width' : '32px' }, 400, 'easeOutCirc');
						
					return;
				}
			});
		});
	}
})(jQuery);


/*
	SNAP POINTS
-------------------------------------------------------------------------------------------------------------------------------------*/


(function($) {
	$.fn.pk_snap_points = function(options) {
		var settings = $.extend({
				wrap : '',
				wrapWidth : 0,
				wrapHeight : 0,
				easing : 'easeOutCirc',
				speed : 400,
				output : ' \
					<div class="pk_tooltip"> \
						<div class="pk_tooltip_content"></div> \
						<span class="pk_tooltip_arrow"></span> \
					</div>'
			}, options || {});
		
		
		return this.each(function() {
			var root = $(this),
				items = $('ul > li', root),
				t_i = items.length,
				interval = undefined;
				
				
			/*
				METHODS
			-------------------------------------------------------------------------------------------------------------------------*/
			
			var methods = {
				init : function() {
					if(settings.wrap != 'content_slider') {
						$(window).unbind('resize.pkSnapPoints').bind('resize.pkSnapPoints', methods.resize);
					}
					
					for(var i = 0; i < t_i; i++) {
						var data = $('ul > li:eq(' + i + ')', root).attr('data-properties');
						if(data != '' && data != undefined) {
							root.append('<a href="#" class="pk_point_button" data-properties="' + data + '"></a>');
						}
					}
					
					$('.pk_point_button', root).each(function(i) {
						var x_p = methods.getData($(this), 'x') / settings.wrapWidth,
							y_p = methods.getData($(this), 'y') / settings.wrapHeight,
							p_c = methods.getData($(this), 'color');
					
						$(this).css({
							'left' : Math.floor(x_p * root.width()) + 'px',
							'top' : Math.floor(y_p * root.height()) + 'px'
						});
					
						$(this).click(function(e) {
							e.preventDefault();	
						});
					
						if(!pk_detect_mobile()) {
							$(this).bind('mouseenter', { id : i }, methods.buttonMouseEnter);
							$(this).bind('mouseleave', methods.buttonMouseLeave);
						} else {
							$(this).bind('touchstart', { id : i }, methods.buttonTouchStart);
							$(this).bind('touchend', methods.buttonTouchEnd);
						}
					
						if(p_c != undefined) {
							$(this).addClass('pk_button_' + p_c);
						}
					});
				},
				
				resize : function() {
					$('.pk_point_button', root).each(function() {
						var x_p = methods.getData($(this), 'x') / settings.wrapWidth,
							y_p = methods.getData($(this), 'y') / settings.wrapHeight;
						
						$(this).css({
							'left' : Math.floor(x_p * root.width()) + 'px',
							'top' : Math.floor(y_p * root.height()) + 'px'
						});
					});
				},
				
				showTooltip : function(id, offset, width, content) {
					$('.pk_tooltip').remove();
					
					$('body').append(settings.output);
					$('.pk_tooltip .pk_tooltip_content').html(content).css('width' , width + 'px');
					$('.pk_tooltip').attr({ 'data-id' : id, 'data-wrap' : settings.wrap }).stop().css({
						'left' : (offset.left - 21) + 'px',
						'top' : (offset.top - $('.pk_tooltip').outerHeight()) + 'px',
						'opacity' : 0
					});
					
					if(!pk_detect_mobile()) {
						$('.pk_tooltip').bind('mouseenter', methods.tooltipMouseEnter);
						$('.pk_tooltip').bind('mouseleave', methods.tooltipMouseLeave);
					}
					
					$('.pk_tooltip').animate({ 
						'opacity' : 1
					}, 200, 'linear');
				},
				
				hideTooltip : function() {
					if($('.pk_tooltip').css('display') === 'block') {
						$('.pk_tooltip').stop().animate({
							'opacity' : 0
						}, 100, 'linear', function() {
							$(this).remove();
						});
					}
				},
				
				
				/*
					EVENTS
				----------------------------------------------------------------------------------------------------------------------*/
			
				buttonMouseEnter : function(e) {
					clearTimeout(interval);
					
					if($('.pk_tooltip').length && $('.pk_tooltip').attr('data-id') == e.data.id) {
						return;
					}
					
					var offset = $(this).offset(),
						width = methods.getData($(this), 'tooltip_width'),
						content = items.filter(':eq(' + e.data.id + ')').html();
					
					methods.showTooltip(e.data.id, offset, width, content);
				},
				
				buttonMouseLeave : function() {
					interval = setTimeout(function() {
						methods.hideTooltip();
					}, 50);
				},
				
				buttonTouchStart: function(e) {
					e.preventDefault();
					
					var offset = $(this).offset(),
						width = methods.getData($(this), 'tooltip_width'),
						content = items.filter(':eq(' + e.data.id + ')').html();
					
					methods.showTooltip(e.data.id, offset, width, content);
				},
				
				buttonTouchEnd : function(e) {
					e.preventDefault();
					
					methods.hideTooltip();
				},
				
				tooltipMouseEnter : function() {
					clearTimeout(interval);
				},
				
				tooltipMouseLeave : function() {
					interval = setTimeout(function() {
						methods.hideTooltip();
					}, 50);
							
					if(settings.wrap == 'content_slider') {
						var c_t = $('#pk_featured_slider').data('pk_content_slider');
						c_t.public_methods.startSlideshow();
					}
				},
			
				
				/*
					GETTERS
				---------------------------------------------------------------------------------------------------------------------*/
				
				getData : function(item, name) {
					var obj = null;
					obj = eval(item.attr('data-properties'));
					if(name in obj) return obj[name];
				}
			}
			
			methods.init();
		});
	};
})(jQuery);


/*
	TESTIMONIALS
-------------------------------------------------------------------------------------------------------------------------------------*/


(function($) {
	$.fn.pk_testimonials = function() {
		return this.each(function() {
			var root = $(this),
				items = $('li', root),
				t_i = items.length,
				interval = 10,
				id = 0;
			
			
			/*
				METHODS
			-------------------------------------------------------------------------------------------------------------------------*/
			
			var methods = {
				init : function() {
					items.each(function(i) {
						$(this).css({
							'position' : 'absolute',
							'height' : '100%'
						}).append('<span class="pk_arrow" />').hide();
					});
					
					methods.load(true);
					
					$(window).unbind('resize.pkTestimonials').bind('resize.pkTestimonials', methods.resize);
				},
				
				load : function(init) {
					if(!init) {
						id = ((id + 1) >= t_i) ? 0 : id + 1;
					}
					
					var n_i = items.filter(':eq(' + id + ')');
					
					if(n_i.hasClass('pk_no_thumbnail')) {
						methods.change();
						return;
					}
					
					img_loader = '';
					img_loader = new Image();
					img_loader.onload = function() {
						methods.change(init);
					};
			
					img_loader.onerror = function() {
						console.log('Image not found. Sorry!');
					};
								
					img_loader.src = $('img', n_i).attr('src');
				},
				
				resize : function() {
					root.stop().css({
						'height' : methods.getHeight() + 'px'
					});
				},
				
				change : function(init) {
					var c_i = items.filter(':visible'),
						n_i = items.filter(':eq(' + id + ')');
					 
					if(init) {
						n_i.show();
						
						root.css({
							'height' : methods.getHeight() + 'px'
						});
						
						setTimeout(function() {
							methods.load();
						}, (interval * 1000));
					} else {
						c_i.fadeOut(200, function() {
							n_i.fadeIn(400, 'easeInOutExpo', function() {
								setTimeout(function() {
									methods.load();
								}, (interval * 1000));
							});
							
							root.animate({
								'height' : methods.getHeight() + 'px'
							}, 400, 'easeOutExpo');
						});
					}
				},
				
				getHeight : function() {
					var c_i = items.filter(':eq(' + id + ')'),
						c_i_h = $('blockquote', c_i).outerHeight(),
						c_i_i_h = $('img', c_i).height() + 30,
						n_h = (c_i_i_h >= c_i_h) ? c_i_i_h : c_i_h;
						
					return n_h;
				}
			}
			
			methods.init();
		});
	};
})(jQuery);


/*
	FEED ROTATOR
-------------------------------------------------------------------------------------------------------------------------------------*/


(function($) {
	$.fn.pk_feed_rotator = function(options) {
		var settings = $.extend({
				interval : 7
			}, options || {});
		
			
		/*
			METHODS
		-----------------------------------------------------------------------------------------------------------------------------*/
		
		return this.each(function() {
			var root = $(this),
				t_i = $('li', root).length,
				id = 0,
				interval = undefined;
				
			var methods = {
				init : function() {
					$(window).unbind('resize.pkFeedRotator').bind('resize.pkFeedRotator', methods.resize);
					
					$('ul', root).css('height' , $('li:eq(0)', root).height() + 'px');
					$('li', root).css('position' , 'absolute');
					
					interval = setTimeout(function() {
						methods.change();
					}, settings.interval * 1000);
				},
				
				resize : function() {
					$('ul', root).stop().css('height' , $('li:eq(' + id + ')', root).height() + 'px');
				},
				
				change : function() {
					clearTimeout(interval);
					$(window).unbind('resize.pkFeedRotator');
					
					id = (id + 1 <= t_i - 1) ? id + 1 : 0;
					
					var c_i = $('li:visible', root),
						n_i = $('li:eq(' + id + ')', root);
						
					c_i.fadeOut();
					n_i.delay(180).fadeIn(400, function() {
						interval = setTimeout(function() {
							methods.change();
						}, settings.interval * 1000);	
					});
					
					$('ul', root).stop().delay(180).animate({
						'height' : $('li:eq(' + id + ')', root).height() + 'px'
					}, 400, 'easeOutCirc', function() {
						$(window).bind('resize.pkFeedRotator', methods.resize);
					});
				}
			};
			
			methods.init();
		});
	};
})(jQuery);


/*
	FORMBOX
-------------------------------------------------------------------------------------------------------------------------------------*/


(function($) {
	$.fn.pk_form_box = function() {
		return this.each(function() {
			var button = $(this),
				wrap = '',
				c_w = 0,
				c_h = 0;
				
				
			button.unbind('click').bind('click', function(e) {
				if($(this).attr('href').substr(0, 1) != '#') return;
				
				e.preventDefault();
				
				wrap = $(this).attr('href');
				c_w = $(wrap).outerWidth();
				c_h = $(wrap).outerHeight();
				
				($('body').hasClass('pk_overlay_form')) ? methods.change() : methods.init();
			});
				
			
			/*
				METHODS
			--------------------------------------------------------------------------------------------------------------------------*/
			
			var methods = {
				init : function() {
					$('body').addClass('pk_overlay_form').append(' \
						<a href="#" class="pk_button_close">Close</a> \
						<div class="pk_form_box"></div> \
						<div class="pk_form_box_back_ground"></div>'
					);
					
					$('.pk_form_box').append($(wrap).html()).attr({ 'data-width' : c_w , 'data-height' : c_h });
					$(window).unbind('resize.pkFormBox').bind('resize.pkFormBox', methods.resize);
					
					methods.open();
				},
				
				open : function() {
					$('.pk_form_box').hide().fadeIn(400);
					$('.pk_form_box_back_ground').css('opacity' , .8);
					$('.pk_form_box_back_ground, .pk_button_close').hide().fadeIn(400, function() {
						$(this).click(function(e) {
							e.preventDefault();
							methods.close();
						});
					});
					
					methods.resize();
				},
				
				close : function() {
					$('.pk_form_box').fadeOut(400, function() {
						$('body').removeClass('pk_overlay_form');
						$(this).remove();
					});
					
					$('.pk_form_box_back_ground, .pk_button_close').fadeOut(400, function() {
						$(this).remove();
					})
				},
				
				resize : function() {
					var w_w = $(window).width(),
						w_h = $(window).height(),
						f_w = $('.pk_form_box').attr('data-width'),
						f_h = $('.pk_form_box').attr('data-height');
					
					$('.pk_form_box').css({
						'left' : (w_w / 2) - (f_w / 2) + 'px',
						'top' : (w_h / 2) - (f_h / 2) + 'px',
						'width' : f_w,
						'height' : f_h
					});
					
					$('.pk_button_close').css({
						'left' : (w_w / 2) - (f_w / 2) + (f_w - 18) + 'px',
						'top' : (w_h / 2) - (f_h / 2) - 12 + 'px'
					});
				},
				
				change : function() {
					$('.pk_button_close').fadeOut(400, 'easeOutExpo');
					$('.pk_form_box').animate({
						'opacity' : 0
					}, 400, 'easeOutExpo', function() {
						$('.pk_form_box').empty().append($(wrap).html()).attr({ 'data-width' : c_w , 'data-height' : c_h });
						
						methods.resize();
						
						$('.pk_button_close').fadeIn(400, 'easeOutExpo');
						$('.pk_form_box').animate({
							'opacity' : 1
						}, 400, 'easeOutExpo');
					});
				}
			};
		});
	}
})(jQuery);


/*
	JPLAYER (RESPONSIVE)
-------------------------------------------------------------------------------------------------------------------------------------*/


(function($) {
	$.fn.pk_jplayer_resize = function(options) {
		var settings = $.extend({
				wrap : ''
			}, options || {});
			
			
		return this.each(function() {
			var root = $(this),
				player = $('.jp-jplayer', root),
				p_w = 0,
				p_h = 0;

			var methods = {
				init : function() {
					$(window).unbind('resize.pkjPlayerResize').bind('resize.pkjPlayerResize', methods.resize);
					
					p_w = player.width();
					p_h = player.height();
					
					methods.resize();
				},
				
				resize : function() {
					var n_w = (settings.wrap == 'prettyPhoto') ? $('.pp_inline').width() : root.width();
						n_h = Math.round((n_w / p_w) * p_h),
						c_w = n_w - 145;
					
					player.jPlayer('option', 'size', { width: n_w + 'px', height: n_h + 'px' });
					if(settings.wrap == 'prettyPhoto') {
						$('.jp-progress').css('width' , c_w + 'px');
					}
				}
			};
			
			methods.init();
		});
	}
})(jQuery);


/*
	IMAGE ROLLOVER
-------------------------------------------------------------------------------------------------------------------------------------*/


(function($) {
	$.fn.pk_image_rollover = function() {
		return this.each(function() {
			var root = $(this);

			if(!pk_detect_mobile()) {
				root.hover(function(e) {
					e.preventDefault();
					
					if($.browser.msie && parseInt($.browser.version) < 9) {
						$(".pk_image_button_icon", this).show();
						$(".pk_image_button_back_ground", this).show();
					} else {
						$(".pk_image_button_icon", this).hide().stop(true, true).fadeIn(400);
						$(".pk_image_button_back_ground", this).hide().stop(true, true).fadeIn(400);
					}
				}, function(e) {
					e.preventDefault();
					
					if($.browser.msie && parseInt($.browser.version) < 9) {
						$(".pk_image_button_icon", this).hide();
						$(".pk_image_button_back_ground", this).hide();
					} else {
						$(".pk_image_button_icon", this).show().stop(true, true).fadeOut(400);
						$(".pk_image_button_back_ground", this).show().stop(true, true).fadeOut(400);
					}
				});
					
				root.click(function(e) {
					if($.browser.msie && parseInt($.browser.version) < 9) {
						$(".pk_image_button_icon", this).hide();
						$(".pk_image_button_back_ground", this).hide();
					} else {
						$(".pk_image_button_icon", this).show().stop(true, true).fadeOut(400);
						$(".pk_image_button_back_ground", this).show().stop(true, true).fadeOut(400);
					}
				});
			} else {
				$('.pk_image_button_icon, .pk_image_button_back_ground', root).remove();
				root.hover(function(e) {
					e.preventDefault();
					return;
				}, function() { });
			}
		});
	};
})(jQuery);


/*
	TABS
-------------------------------------------------------------------------------------------------------------------------------------*/


(function($) {
	$.fn.pk_business_tabs = function() {
		return this.each(function() {
			var root = $(this),
				buttons = $('.pk_tabs_navigation a', root);
			
			
			/*
				METHODS
			-------------------------------------------------------------------------------------------------------------------------*/
				
			var methods = {
				init : function() {
					buttons.each(function(i) {
						$(this).click(function(e) {
							e.preventDefault();
								
							$('.pk_tab:visible', root).hide();
							$('.pk_tab:eq(' + i + ')', root).show();
		
							methods.updateNavigation($(this));
						});
					});
					
					$('.pk_tabs_navigation a:eq(0)', root).trigger('click');
				},
				
				updateNavigation : function(button) {
					buttons.each(function(i) {
						$(this).parent().removeClass('pk_active_tab');
					});
								
					button.parent().addClass('pk_active_tab');
				}
			};
			
			methods.init();
		});
	};
})(jQuery);

(function($) {
	$.fn.pk_tabs = function() {
		return this.each(function() {
			var root = $(this),
				buttons = $('.pk_tabs_navigation a', root);
			
			
			/*
				METHODS
			-------------------------------------------------------------------------------------------------------------------------*/
			
			var methods = {
				init : function() {
					$('.pk_tabs_label', root).show();
					$('.pk_tabs_navigation', root).show().css({
						'margin-top' : '-1px',
						'right' : '0',
						'top' : '0'
					});
					$('.pk_tabs_navigation li:last', root).css('margin-right' , '0');
					root.css('padding-top' , '38px');
					
					buttons.each(function(i) {
						$(this).click(function(e) {
							e.preventDefault();
							
							$('.pk_tab:visible', root).hide();
							$('.pk_tab:eq(' + i + ')', root).show();
							
							methods.updateNavigation($(this).parent());
						});
					});
	
					$('.pk_tabs_navigation a:eq(0)', root).trigger('click');
				},
				
				updateNavigation : function(button) {
					buttons.each(function(i) {
						$(this).parent().removeClass('pk_active_tab').css('height' , '39px');
						$(this).css('padding-bottom' , '0');
					});
					
					button.addClass('pk_active_tab').css('height' , '40px');
				}
			};
			
			methods.init();
		});
	};
})(jQuery);


/*
	TOGGLES
-------------------------------------------------------------------------------------------------------------------------------------*/


(function($) {
	$.fn.pk_toggles = function() {
		return this.each(function() {
			var root = $(this),
				t_t = (root.hasClass('pk_accordion')) ? 'accordion' : 'toggle',
				buttons = $(".pk_toggle_button", root);
				
				
			/*
				METHODS
			-------------------------------------------------------------------------------------------------------------------------*/
			
			var methods = {
				init : function() {
					$('.pk_toggle_content_wrapper', root).css('height' , '0').hide();
					buttons.each(function(i) {
						$(this).click(function() {
							if($(this).hasClass('pk_selected')) {
								methods.close($(this));
								return;
							}
							
							(t_t == 'accordion') ? methods.accordion(i, $(this)) : methods.open($(this));
						});
					});
				},
				
				open : function(button) {
					button.addClass('pk_selected');		
					button.next().show().stop().animate({
						'height' : $('.pk_toggle_content', button.next()).outerHeight() + 'px'
					}, 600, 'easeOutExpo');
				},
				
				close : function(button) {
					button.removeClass('pk_selected');
					button.next().stop().animate({
						'height' : '0'
					}, 600, 'easeOutExpo', function() {
						$(this).hide();	
					});
				},
				
				accordion : function(id, button) {
					buttons.each(function(i) {
						if($(this).hasClass('pk_selected')) {
							$(this).removeClass('pk_selected');
							$(this).next().stop().animate({
								'height' : '0'
							}, 600, 'easeOutExpo', function() {
								$(this).hide();	
							});
						}
					});
					
					button.addClass('pk_selected');
					button.next().show().stop().animate({
						'height' : $('.pk_toggle_content', button.next()).outerHeight() + 'px'
					}, 600, 'easeOutExpo');
				}
			};
			
			methods.init();
		});
	};
})(jQuery);


/*
	REVIEW FORM
-------------------------------------------------------------------------------------------------------------------------------------*/


(function($) {
	$.fn.pk_review_form = function(options) {
		var settings = $.extend({
				button_close_label : 'Close'
			}, options || {});
			
			
		return this.each(function() {
			var root = $(this);
			
			root.click(function(e) {
				e.preventDefault();
				
				if($('#review_form_wrapper').hasClass('review_open')) return;
				
				if($('.add_review').length) {
					$('.add_review').slideUp(400, 'easeOutSine', function() {
						methods.show();
					});
				} else {
					methods.show();
				}
			});
			
			/*
				METHODS
			-------------------------------------------------------------------------------------------------------------------------*/
			
			var methods = {
				show : function() {
					$('#review_form_wrapper').slideDown(800, 'easeInOutExpo').addClass('review_open');
					$('#reply-title').append('<small><a href="#" class="review_button_close">' + settings.button_close_label + '</a></small>');
					$('.review_button_close').click(function(e) {
						e.preventDefault();
							
						methods.hide();
					});
				},
				
				hide : function() {
					$('#review_form_wrapper').slideUp(600, 'easeOutExpo', function() {
						$('#reply-title small').remove();
						if($('.add_review').length) {
							$('.add_review').slideDown(400, 'easeInOutSine');
						}
					}).removeClass('review_open');
				}
			};
		});
	};
})(jQuery);


/*
	OTHERS
-------------------------------------------------------------------------------------------------------------------------------------*/


function pk_detect_mobile() {
	if(navigator.userAgent.match(/Android/i)
		|| navigator.userAgent.match(/webOS/i)
		|| navigator.userAgent.match(/iPhone/i)
		|| navigator.userAgent.match(/iPad/i)
		|| navigator.userAgent.match(/iPod/i)
		|| navigator.userAgent.match(/BlackBerry/i)
	) {
		var mobile = true;
	} else {
		var mobile = false;	
	}
	
	return mobile;
}

function pk() {
	$pk_fj('.pk_page').parent().before(' \
		<div class="pk_wrapper pk_top_shadow" style="margin-top:5px;"> \
			<span class="pk_center_box pk_page_top_shadow"></span> \
		</div>'
	);
	
	/*
		LOADING
	---------------------------------------------------------------------------------------------------------------------------------*/
	
	if($pk_fj('#pk_featured_text img').length) {
		img_loader = '';
		img_loader = new Image();
		img_loader.onload = function() {
			if($pk_fj('#pk_featured_text .pk_snap_points').length) {
				$pk_fj('#pk_featured_text .pk_snap_points').pk_snap_points({
					wrapWidth : 980,
					wrapHeight : this.height	
				});
			}
		};
		img_loader.src = $pk_fj('#pk_featured_text img').attr('src');
	}
	
	/*
		JIGOSHOP - WOOCOMMERCE
	---------------------------------------------------------------------------------------------------------------------------------*/
	
	$pk_fj('.pk_entry_price, .price, .js_widget_product_price').each(function() {
		if($pk_fj('br', this).length) {
			$pk_fj('ins', this).remove();
		}
	});
	
	$pk_fj('.woocommerce ul.products').addClass('pk_clear_top');
	$pk_fj('.woocommerce_ordering').show().addClass('pk_ordering').appendTo('.pk_call_to_action_content');
	$pk_fj('.woocommerce_ordering select').each(function() {
		var title = $pk_fj(this).attr('title');
		
		if($pk_fj('option:selected', this).val() != '') title = $pk_fj('option:selected', this).text();
		
		$pk_fj(this).css({
			'z-index' : 10,
			'opacity' : 0
		}).after('<span class="pk_select">' + title + '</span>').change(function(){
			val = $pk_fj('option:selected',this).text();
			
			$pk_fj(this).next().text(val);
		});
	});
	$pk_fj('.woocommerce ul.products li').each(function() {
		if(!$pk_fj('.button', this).length) {
			$pk_fj(this).addClass('pk_products_category');
			
			var c_l = $pk_fj('mark.count', this).text();
			var l_l = c_l.length;
			
			$pk_fj('mark.count', this).text(c_l.substr(1, (l_l - 2)));
		}
	});
	
	if($pk_fj.browser.msie && parseInt($pk_fj.browser.version) < 9) {
		if($pk_fj('.woocommerce-account .pk_page p').filter(':eq(0)').hasClass('form-row')) {
			$pk_fj('.woocommerce-account .pk_page p').filter(':eq(0)').css('margin-bottom', '0');
		}
	}
	
	$pk_fj(window).unbind('resize.pkEcommerce').bind('resize.pkEcommerce', function() {
		var box = $pk_fj('.pk_center_box');
		
		if(box.width() < 750) {
			$pk_fj('.jigoshop-myaccount table.shop_table tfoot tr').each(function() {
				$pk_fj('td', this).filter(':eq(0)').attr('colspan', '2');
			});
		} else {
			$pk_fj('.jigoshop-myaccount table.shop_table tfoot tr').each(function() {
				$pk_fj('td', this).filter(':eq(0)').attr('colspan', '3');
			});
		}
		
		if($pk_fj.browser.msie && parseInt($pk_fj.browser.version) < 9) {
			var table = $pk_fj('table.my_account_orders');
				
			if(box.width() < 750 && box.width() > 310) {
				$pk_fj('tr', table).each(function() {
					if($pk_fj('body').hasClass('jigoshop')) {
						$pk_fj('th, td', this).filter(':eq(0)').hide();
						$pk_fj('th, td', this).filter(':eq(1)').hide();
						$pk_fj('th, td', this).filter(':eq(2)').hide();
						$pk_fj('th, td', this).filter(':eq(4)').show();
					}
					
					if($pk_fj('body').hasClass('woocommerce-page')) {
						$pk_fj('th, td', this).filter(':eq(0)').hide();
						$pk_fj('th, td', this).filter(':eq(1)').hide();
						$pk_fj('th, td', this).filter(':eq(3)').show();
					}
				});
			} else if(box.width() < 310) {
				$pk_fj('tr', table).each(function() {
					if($pk_fj('body').hasClass('jigoshop')) {
						$pk_fj('th, td', this).filter(':eq(0)').hide();
						$pk_fj('th, td', this).filter(':eq(1)').hide();
						$pk_fj('th, td', this).filter(':eq(2)').hide();
						$pk_fj('th, td', this).filter(':eq(4)').hide();
					}
					
					if($pk_fj('body').hasClass('woocommerce-page')) {
						$pk_fj('th, td', this).filter(':eq(0)').hide();
						$pk_fj('th, td', this).filter(':eq(1)').hide();
						$pk_fj('th, td', this).filter(':eq(3)').hide();
					}
				});
			} else {
				$pk_fj('th, td', table).show();
			}
		}
	});
	$pk_fj(window).trigger('resize.pkEcommerce');
	
	/*
		IE8 FIX
	---------------------------------------------------------------------------------------------------------------------------------*/
	
	if($pk_fj.browser.msie && parseInt($pk_fj.browser.version) < 9) {
		var t_d = $pk_fj('.pk_option_nav .pk_option_item_divider').length;
		$pk_fj('.pk_option_nav .pk_option_item_divider').each(function(i) {
			if(i == (t_d - 1)) {
				$pk_fj(this).hide();
			}
		});
		$pk_fj('.jigoshop_login_widget p').filter(':eq(2)').css('margin-bottom' , '20px');
	}
}

/*global jQuery */
/*! 
* FitVids 1.0
*
* Copyright 2011, Chris Coyier - http://css-tricks.com + Dave Rupert - http://daverupert.com
* Credit to Thierry Koblentz - http://www.alistapart.com/articles/creating-intrinsic-ratios-for-video/
* Released under the WTFPL license - http://sam.zoy.org/wtfpl/
*
* Date: Thu Sept 01 18:00:00 2011 -0500
*/

(function( $ ){

  $.fn.fitVids = function( options ) {
    var settings = {
      customSelector: null
    }
    
    var div = document.createElement('div'),
        ref = document.getElementsByTagName('base')[0] || document.getElementsByTagName('script')[0];
        
  	div.className = 'fit-vids-style';
    div.innerHTML = '&shy;<style>         \
      .fluid-width-video-wrapper {        \
         width: 100%;                     \
         position: relative;              \
         padding: 0;                      \
      }                                   \
                                          \
      .fluid-width-video-wrapper iframe,  \
      .fluid-width-video-wrapper object,  \
      .fluid-width-video-wrapper embed {  \
         position: absolute;              \
         top: 0;                          \
         left: 0;                         \
         width: 100%;                     \
         height: 100%;                    \
      }                                   \
    </style>';
                      
    ref.parentNode.insertBefore(div,ref);
    
    if ( options ) { 
      $.extend( settings, options );
    }
    
    return this.each(function(){
      var selectors = [
        "iframe[src*='player.vimeo.com']", 
        "iframe[src*='www.youtube.com']",  
        "iframe[src*='www.kickstarter.com']", 
        "object", 
        "embed"
      ];
      
      if (settings.customSelector) {
        selectors.push(settings.customSelector);
      }
      
      var $allVideos = $(this).find(selectors.join(','));

      $allVideos.each(function(){
        var $this = $(this);
        if (this.tagName.toLowerCase() == 'embed' && $this.parent('object').length || $this.parent('.fluid-width-video-wrapper').length) { return; } 
        var height = this.tagName.toLowerCase() == 'object' ? $this.attr('height') : $this.height(),
            aspectRatio = height / $this.width();
		if(!$this.attr('id')){
			var videoID = 'fitvid' + Math.floor(Math.random()*999999);
			$this.attr('id', videoID);
		}
        $this.wrap('<div class="fluid-width-video-wrapper"></div>').parent('.fluid-width-video-wrapper').css('padding-top', (aspectRatio * 100)+"%");
        $this.removeAttr('height').removeAttr('width');
      });
    });
  
  }
})( jQuery );

/**
* hoverIntent r6 // 2011.02.26 // jQuery 1.5.1+
* <http://cherne.net/brian/resources/jquery.hoverIntent.html>
* 
* @param  f  onMouseOver function || An object with configuration options
* @param  g  onMouseOut function  || Nothing (use configuration options object)
* @author    Brian Cherne brian(at)cherne(dot)net
*/
(function($){$.fn.hoverIntent=function(f,g){var cfg={sensitivity:7,interval:50,timeout:0};cfg=$.extend(cfg,g?{over:f,out:g}:f);var cX,cY,pX,pY;var track=function(ev){cX=ev.pageX;cY=ev.pageY};var compare=function(ev,ob){ob.hoverIntent_t=clearTimeout(ob.hoverIntent_t);if((Math.abs(pX-cX)+Math.abs(pY-cY))<cfg.sensitivity){$(ob).unbind("mousemove",track);ob.hoverIntent_s=1;return cfg.over.apply(ob,[ev])}else{pX=cX;pY=cY;ob.hoverIntent_t=setTimeout(function(){compare(ev,ob)},cfg.interval)}};var delay=function(ev,ob){ob.hoverIntent_t=clearTimeout(ob.hoverIntent_t);ob.hoverIntent_s=0;return cfg.out.apply(ob,[ev])};var handleHover=function(e){var ev=jQuery.extend({},e);var ob=this;if(ob.hoverIntent_t){ob.hoverIntent_t=clearTimeout(ob.hoverIntent_t)}if(e.type=="mouseenter"){pX=ev.pageX;pY=ev.pageY;$(ob).bind("mousemove",track);if(ob.hoverIntent_s!=1){ob.hoverIntent_t=setTimeout(function(){compare(ev,ob)},cfg.interval)}}else{$(ob).unbind("mousemove",track);if(ob.hoverIntent_s==1){ob.hoverIntent_t=setTimeout(function(){delay(ev,ob)},cfg.timeout)}}};return this.bind('mouseenter',handleHover).bind('mouseleave',handleHover)}})(jQuery);

/**
 * jQuery Plugin to obtain touch gestures from iPhone, iPod Touch and iPad, should also work with Android mobile phones (not tested yet!)
 * Common usage: wipe images (left and right to show the previous or next image)
 * 
 * @author Andreas Waltl, netCU Internetagentur (http://www.netcu.de)
 * @version 1.1.1 (9th December 2010) - fix bug (older IE's had problems)
 * @version 1.1 (1st September 2010) - support wipe up and wipe down
 * @version 1.0 (15th July 2010)
 */
 
(function($) {
$.fn.touchwipe = function(settings) {
    var config = {
            min_move_x: 20,
            min_move_y: 20,
            wipeLeft: function(e) { },
            wipeRight: function(e) { },
            wipeUp: function(e) { },
            wipeDown: function(e) { },
            preventDefaultEvents: true
    };

    if (settings) $.extend(config, settings);

    this.each(function() {
        var startX;
        var startY;
        var isMoving = false;

        function cancelTouch() {
            this.removeEventListener('touchmove', onTouchMove);
            startX = null;
            isMoving = false;
        }

        function onTouchMove(e) {
            if(config.preventDefaultEvents) {
                e.preventDefault();
            }
            if(isMoving) {
                var x = e.touches[0].pageX;
                var y = e.touches[0].pageY;
                var dx = startX - x;
                var dy = startY - y;
                if(Math.abs(dx) >= config.min_move_x) {
                    cancelTouch();
                    if(dx > 0) {
                        config.wipeLeft(e);
                    }
                    else {
                        config.wipeRight(e);
                    }
                }
                else if(Math.abs(dy) >= config.min_move_y) {
                        cancelTouch();
                        if(dy > 0) {
                            config.wipeDown(e);
                        }
                        else {
                            config.wipeUp(e);
                        }
                    }
            }
        }

        function onTouchStart(e)
        {
            if (e.touches.length == 1) {
                startX = e.touches[0].pageX;
                startY = e.touches[0].pageY;
                isMoving = true;
                this.addEventListener('touchmove', onTouchMove, false);
            }
        }
        if ('ontouchstart' in document.documentElement) {
            this.addEventListener('touchstart', onTouchStart, false);
        }
    });

    return this;
};

})(jQuery);
