<?php
header("Content-type: text/javascript; charset: UTF-8");
header("Cache-Control: max-age=".(60 * 60 * 24 * 7).", must-revalidate");
header("Expires: ".gmdate("D, d M Y H:i:s", time() + (60 * 60 * 24 * 7))." GMT");

function minify($buffer) {
	
	require_once('../framework/includes/minify/jsmin.php');
	return JSMin::minify($buffer);
}

ob_start('minify');

require_once('jquery.easing.js');
require_once('jquery.jplayer.min.js');
require_once('../prettyPhoto/js/jquery.prettyPhoto.js');
require_once('pk_slider.js');
require_once('pk_content_slider.js');
require_once('pk.js');

ob_end_flush();
?>