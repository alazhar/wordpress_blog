/*
	CONTENT SLIDER
-------------------------------------------------------------------------------------------------------------------------------------*/


(function($){
	var pkContentSlider = function(root, options) {
		var root = $(root),
			obj = this,
			settings = $.extend({
				sliderWidth : 980,
				sliderHeight : 380,
				sliderBackground : '',
				sliderNavigationAlign : 'center',
				slideshowPauseOnHover : true,
				slideshowAutoStart : false,
				slideshowInterval : 5,
				transition : 'slide',
				easing : 'easeOutExpo',
				speed : 800
			}, options || {});
		
		
		/*
			PUBLIC
		-----------------------------------------------------------------------------------------------------------------------------*/
		
		obj.is_slideshow = (settings.slideshowAutoStart) ? true : false;
		obj.is_over = false;
		
		
		obj.public_methods = {
			startSlideshow : function() {
				obj.is_over = false;
				private_methods.startSlideshow();
			}
		};

		
		/*
			PRIVATE
		-----------------------------------------------------------------------------------------------------------------------------*/
		
		var t_i = $('.pk_slider_item', root).length,
			id = 0,
			is_loading = false,
			interval = undefined,
			array_p = ['left', 'right'];
		
		
		var private_methods = {
			init : function () {
				$(window).unbind('resize.pkContentSlider').bind('resize.pkContentSlider', private_methods.resize);
				
				if(settings.sliderBackground != '') {
					root.css({
						'background-image' : 'url("' + settings.sliderBackground + '")',
						'background-position' : 'center center',
						'background-repeat' : 'none'
					});
				}
						
				$('.pk_slider_content', root).css('height' , settings.sliderHeight + 'px');
				$('.pk_slider_item', root).each(function(i) {
					$(this).append('<span class="pk_overlay" />').css({
						'position' : 'absolute',
						'overflow' : 'hidden'
					});
					
					$('.pk_overlay', this).css('opacity' , .6);
							
					private_methods.initTitle($('.pk_slider_item_title', this));
					private_methods.initCaption($('.pk_slider_item_caption', this));
					private_methods.initPoints($(this));
		
					$(this).hide();
				});
						
				if(!pk_detect_mobile()) {
					root.bind('mouseenter', private_methods.mouseEnter);
					root.bind('mouseleave', private_methods.mouseLeave);
				} else {
					$('.pk_slider_content', root).bind('touchstart', private_methods.mouseEnter);
					$('.pk_slider_content', root).bind('touchend', private_methods.mouseLeave);
					
					root.touchwipe({
						preventDefaultEvents: false,
						wipeLeft: private_methods.wipeLeft,
						wipeRight: private_methods.wipeRight,
    					min_move_x: 20
     				});
				}
						
				private_methods.initNavigation();
				private_methods.load(true);
			},
			
			resize : function() {
				var c_i = $('.pk_slider_item:eq(' + id + ')', root);
						
				$('.pk_slider_content', root).css('height' , c_i.height() + 'px');
						
				private_methods.resizeImage($('img', c_i));
				private_methods.resizeTitle($('.pk_slider_item_title', c_i));
				private_methods.resizeCaption($('.pk_slider_item_caption', c_i));
				private_methods.resizePoints(c_i);
				private_methods.resizeNavigation();
			},
			
			load : function(init) {
				is_loading = true;
							
				private_methods.showLoader();
				private_methods.stopSlideshow();
							
				var n_i = $('.pk_slider_item_media:eq(' + id + ')', root);
				
				this.img_loader = '';
				this.img_loader = new Image();
				this.img_loader.onload = function() {	
					private_methods.hideLoader();
					private_methods.change(init);
				};
			
				this.img_loader.onerror = function() {
					console.log('Image not found. Sorry!');
				};
								
				this.img_loader.src = $('img', n_i).attr('src');
			},
			
			change : function(init) {
				var c_i = $('.pk_slider_item:visible', root);
					n_i = $('.pk_slider_item:eq(' + id + ')', root);
					
				c_i.css('zIndex' , 1);
				n_i.css('zIndex' , 2);
				
				var divider = settings.sliderWidth / Number($('.attachment-full:eq(' + id + ')', root).attr('width'));
				settings.sliderHeight = Number($('.attachment-full:eq(' + id + ')', root).attr('height')) * divider;
				
				$('img', n_i).css('visibility' , 'visible');
				
				if(init) {
					n_i.fadeIn(settings.speed, function() {
						is_loading = false;
							
						private_methods.startSlideshow();
					});
				} else {
					switch(settings.transition) {
						case 'fade' :
							n_i.stop(true, true).fadeIn(settings.speed, 'linear', function() {
								is_loading = false;
								
								private_methods.startSlideshow();
								c_i.hide();
							});
							break;
									
						case 'slide' :
							if(id > c_i.index()) {
								c_i.stop().animate({
									'margin-left' : root.width() + 'px'
								}, settings.speedOut, 'easeInOutExpo', function() {
									$(this).hide();
								});
								n_i.stop().css('margin-left' , -root.width() + 'px');
							} else {
								c_i.stop().animate({
									'margin-left' : -root.width() + 'px'
								}, settings.speedOut, 'easeInOutExpo', function() {
									$(this).hide();
								});
								n_i.stop().css('margin-left' , root.width() + 'px');
							}
								
							n_i.show().animate({
								'margin-left' : '0px'
							}, settings.speedIn, 'easeInOutExpo', function() {
								is_loading = false;
									
								private_methods.startSlideshow();
							});
							break;		
					}
				}
					
				if($('.pk_tooltip').length && $('.pk_tooltip').attr('data-wrap') == 'content_slider') $('.pk_tooltip').remove();
				
				$('.pk_slider_content', root).css('height' , n_i.height() + 'px');
						
				private_methods.resizeImage($('img', n_i));
				private_methods.showTitle($('.pk_slider_item_title', n_i));
				private_methods.showCaption($('.pk_slider_item_caption', n_i));
				private_methods.showPoints(n_i);
				private_methods.updateNavigation();
			},
			
			
			/*
				NAVIGATION
			-------------------------------------------------------------------------------------------------------------------------*/
					
			initNavigation : function() {
				if(t_i <= 1) return;
					
				var button_prev = '<a href="#" class="pk_button_prev">prev</a>',
					button_next = '<a href="#" class="pk_button_next">next</a>';
					
				$('.pk_slider_content', root).after('<div class="pk_slider_navigation"><div class="pk_slider_thumbs"></div></div>' + button_prev + button_next);
				$('.pk_slider_navigation', root).css({
					'width' : (12 * t_i) + 'px',
					'z-index' : t_i + 2
				}).show();
						
				for(i = 0; i < t_i; i++) {
					$('.pk_slider_thumbs', root).append('<a href="#" title="" class="pk_slider_navigation_button">' + (i + 1) + '</a>');
				}
				
				$('.pk_slider_navigation', root).css('z-index' , t_i + 3);
				$('.pk_slider_navigation_button', root).each(function(i) {
					$(this).click(function(e) {
						e.preventDefault();
							
						if(is_loading || i == id) {
							return;
						}
						
						id = i;
						private_methods.load();
					});
				});
				
				$('.pk_button_prev, .pk_button_next', root).css('z-index' , t_i + 2);
				$('.pk_button_prev', root).click(function(e) {
					e.preventDefault();
						
					var index = id;
					(index - 1 < 0) ? index = t_i - 1 : index = index - 1;
					$('.pk_slider_navigation_button:eq(' + index + ')', root).trigger('click');
				});

				$('.pk_button_next', root).click(function(e) {
					e.preventDefault();
						
					var index = id;
					(index + 1 > t_i - 1) ? index = 0 : index = index + 1;
					$('.pk_slider_navigation_button:eq(' + index + ')', root).trigger('click');
				});
				
				private_methods.resizeNavigation();
			},
				
			updateNavigation : function() {
				$('.pk_slider_navigation_button', root).each(function(i) {
					if($(this).hasClass('pk_selected_button')) {
						$(this).removeClass('pk_selected_button');
					}
				});
				$('.pk_slider_navigation_button:eq(' + id + ')', root).addClass('pk_selected_button');
				
				private_methods.resizeNavigation();
			},
					
			resizeNavigation : function() {
				switch(settings.sliderNavigationAlign) {
					case 'left' :
						$('.pk_slider_navigation', root).css('left' , '0');
						break;
							
					case 'center' :
						$('.pk_slider_navigation', root).css({
							'left' : (root.width() / 2) - ($('.pk_slider_navigation', root).outerWidth() / 2) + 'px'
						});
						break;
								
					case 'right' :
						$('.pk_slider_navigation', root).css('right' , '0');
						break;
				}
			},
			
			
			/*
				TITLES
			-------------------------------------------------------------------------------------------------------------------------*/
					
			initTitle : function(title) {
				if(!title.length) return;
						
				var t_p = (private_methods.getData(title, 'bg_color') != 'none') ? '3px 15px' : '0';
							
				title.show().css({
					'padding' : t_p,
					'width' : private_methods.getData(title, 'width') + 'px',
					'color' : private_methods.getData(title, 'color'),
					'background-color' : private_methods.getData(title, 'bg_color')
				});
			},
				
			showTitle : function(title) {
				if(!title.length) return;
						
				private_methods.hideTitle(title);
						
				var d_y_p = private_methods.getData(title, 'y') / settings.sliderHeight,
					d_x_p = private_methods.getData(title, 'x') / settings.sliderWidth,
					y_p = (root.width() > 460) ? Math.floor(d_y_p * root.height()) : 40,
					x_p = (root.width() > 460) ? Math.floor(d_x_p * root.width()) : 30;
						
				title.stop().css('margin-top' , y_p + 'px');
				title.delay(500).animate({
					'margin-left' : x_p + 'px'
				}, settings.speed, settings.easing);
			},
					
			hideTitle : function(title) {
				if(!title.length) return;
						
				title.stop().css('margin-left' , private_methods.getPosition(title) + 'px');
			},
				
			resizeTitle : function(title) {
				if(!title.length) return;
						
				if(root.width() > 460) {
					var x_p = private_methods.getData(title, 'x') / settings.sliderWidth,
						y_p = private_methods.getData(title, 'y') / settings.sliderHeight;
										
					title.css({
						'margin-left' : Math.floor(x_p * root.width()) + 'px',
						'margin-top' : Math.floor(y_p * root.height()) + 'px'
					});
				} else {
					title.css({
						'margin-left' : '30px',
						'margin-top' : '40px'
					});
				}
			},
			
		
			/*
				CAPTIONS
			-------------------------------------------------------------------------------------------------------------------------*/
					
			initCaption : function(caption) {
				if(!caption.length) return;
						
				var c_p = (private_methods.getData(caption, 'bg_color') != 'none') ? '3px 15px' : '0';
						
				caption.show().css({
					'padding' : c_p,
					'width' : private_methods.getData(caption, 'width') + 'px',
					'color' : private_methods.getData(caption, 'color'),
					'background-color' : private_methods.getData(caption, 'bg_color')
				});
			},
					
			showCaption : function(caption) {
				if(!caption.length) return;
						
				private_methods.hideCaption(caption);
					
				var d_y_p = private_methods.getData(caption, 'y') / settings.sliderHeight,
					d_x_p = private_methods.getData(caption, 'x') / settings.sliderWidth,
					y_p = (!caption.prev().hasClass('pk_slider_item_title')) ? 40 : 60 + caption.prev().outerHeight(),
					x_p = (root.width() > 460) ? Math.floor(d_x_p * root.width()) : 30,
					m_t = (root.width() > 460) ? Math.floor(d_y_p * root.height()) : y_p,
					delay = (!caption.prev().hasClass('pk_slider_item_title')) ? 500 : 700;
						
				caption.stop().css('margin-top' , m_t + 'px');
				caption.delay(delay).animate({
					'margin-left' : x_p + 'px'
				}, settings.speed, settings.easing);
			},
					
			hideCaption : function(caption) {
				if(!caption.length) return;
					
				caption.stop().css('margin-left' , private_methods.getPosition(caption) + 'px');
			},
					
			resizeCaption : function(caption) {
				if(!caption.length) return;
				
				if(root.width() > 460) {
					var x_p = private_methods.getData(caption, 'x') / settings.sliderWidth,
						y_p = private_methods.getData(caption, 'y') / settings.sliderHeight;
										
					caption.css({
						'margin-left' : Math.floor(x_p * root.width()) + 'px',
						'margin-top' : Math.floor(y_p * root.height()) + 'px'
					});
				} else {
					var m_t = (!caption.prev().hasClass('pk_slider_item_title')) ? 40 : 60 + caption.prev().outerHeight()
						
					caption.css({
						'margin-left' : '30px',
						'margin-top' : m_t + 'px'
					});
				}
			},
			
			
			/*
				SNAP POINTS
			-------------------------------------------------------------------------------------------------------------------------*/
			
			initPoints : function(item) {
				if(!$('.pk_snap_points', item).length) return;
				
				$('.pk_snap_points', item).pk_snap_points({
					wrap : 'content_slider',
					wrapWidth : item.width(),
					wrapHeight : item.height()
				});
				
				if($('img', item).parent().attr('href') != undefined) {
					var btn_class = ($('img', item).parent().attr('class') != undefined) ? ' ' + $('img', item).parent().attr('class') : '',
						btn_link = $('img', item).parent().attr('href'),
						btn_label = $('img', item).attr('title');
						
					$('.pk_snap_points', item).append('<a class="pk_link_clone' + btn_class + '" href="' + btn_link + '">' + btn_label + '</a>');
				}
			},
					
			showPoints : function(item) {
				if(!$('.pk_snap_point', item).length) return;
				
				private_methods.hidePoints(item);
					
				interval_points = setTimeout(function() {
					$('.pk_point_button', item).each(function(i) {
						var x_p = private_methods.getData($(this), 'x') / settings.sliderWidth,
							y_p = private_methods.getData($(this), 'y') / settings.sliderHeight;
							
						$(this).stop().css('top' , Math.floor(y_p * root.height()) + 'px' );
						$(this).delay(150 * i).animate({
							'left' : Math.floor(x_p * root.width()) + 'px'
						}, settings.speed, settings.easing);
					});
				}, 600);
			},
					
			hidePoints : function(item) {
				if(!$('.pk_snap_point', item).length) return;
	
				$('.pk_point_button', item).each(function(i) {
					$(this).css('left' , private_methods.getPosition($(this)) + 'px');
				});
			},
					
			resizePoints : function(item) {
				if(!$('.pk_snap_point', item).length) return;
						
				$('.pk_point_button', item).each(function(i) {
					var x_p = private_methods.getData($(this), 'x') / settings.sliderWidth,
						y_p = private_methods.getData($(this), 'y') / settings.sliderHeight;
							
					$(this).css({
						'left' : Math.floor(x_p * root.width()) + 'px',
						'top' : Math.floor(y_p * root.height()) + 'px'
					});
				});
			},
			
			
			/*
				VARIOUS
			-------------------------------------------------------------------------------------------------------------------------*/
					
			resizeImage : function(image) {
				var i_m_l = (root.width() > 460) ? 0 : (root.width() / 2) - (image.width() / 2);	
				image.css('margin-left' , i_m_l + 'px');
			},
				
			startSlideshow : function() {
				private_methods.stopSlideshow();
					
				if(!obj.is_slideshow || obj.is_over) return;
				
				interval = setTimeout(function() {
					$('.pk_button_next', root).trigger('click');
				}, (settings.slideshowInterval * 1000) + 600);
			},
					
			stopSlideshow : function() {
				clearTimeout(interval);
			},
					
					
			/*
				LOADER
			-------------------------------------------------------------------------------------------------------------------------*/
					
			showLoader : function() {
				root.append('<span class="pk_loader" />');
				if(settings.sliderNavigationAlign == 'right') {
					$('.pk_loader', root).css({
						'right' : 'auto',
						'left' : '10px'
					});	
				}
				$('.pk_loader', root).stop(true, true).hide().fadeIn(400, 'easeInOutExpo');
			},
					
			hideLoader : function() {
				$('.pk_loader', root).remove();
			},

					
			/*
				EVENTS
			-------------------------------------------------------------------------------------------------------------------------*/
					
			mouseEnter : function() {
				obj.is_over = true;
					
				private_methods.stopSlideshow();
					
				if(root.width() > 460 && !pk_detect_mobile()) {
					$('.pk_button_prev', root).show().stop().animate({
						'left' : '0'
					}, 800, 'easeOutExpo');
						
					$('.pk_button_next', root).show().stop().animate({
						'right' : '0'
					}, 800, 'easeOutExpo');
				}
			},
				
			mouseLeave : function(e) {
				if(e.pageX > root.offset().left && e.pageX < (root.offset().left + root.width()) && e.pageY > root.offset().top && e.pageY < (root.offset().top + root.height())) {
					return;
				}
					
				obj.is_over = false;
				private_methods.startSlideshow();
					
				if(root.width() > 460 && !pk_detect_mobile()) {
					$('.pk_button_prev', root).stop().animate({
						'left' : '-60px'
					}, 800, 'easeOutExpo', function() {
						$(this).hide();
					});
						
					$('.pk_button_next', root).stop().animate({
						'right' : '-60px'
					}, 800, 'easeOutExpo', function() {
						$(this).hide();
					});
				}
			},
					
			wipeLeft : function() {
				$('.pk_button_next', root).trigger('click');
			},
					
			wipeRight : function() {
				$('.pk_button_prev', root).trigger('click');
			},
			
			
			/*
				GETTERS
			-------------------------------------------------------------------------------------------------------------------------*/
				
			getData : function(item, name) {
				var obj = null;
				obj = eval(item.attr('data-properties'));
				if(name in obj) return obj[name];
			},
					
			getPosition : function(item) {
				switch(private_methods.getRandomValue(array_p)) {
					case 'left':	
						return -($(item).width() + 60);
						break;
						
					case 'right':
						return root.width();			
						break;
				}
			},
					
			getRandomValue : function(array) {
				return array[Math.floor(Math.random() * array.length)];
			}
		};
		
		private_methods.init();
	};
	
	
	/*
		CONSTRUCTOR
	---------------------------------------------------------------------------------------------------------------------------------*/
	
	$.fn.pk_content_slider = function(options) {
		return this.each(function() {
			var root = $(this);
			
			if(root.data('pk_content_slider')) return;
			
			var pk_content_slider = new pkContentSlider(this, options);
			
			root.data('pk_content_slider', pk_content_slider);
		});
	};
})(jQuery);