<?php
	$video_url = get_post_meta(get_post_thumbnail_id(), '_video_url', true);
	$video_urls = explode(',', get_post_meta(get_post_thumbnail_id(), '_video_urls', true));
	
	if (is_array($video_urls) && count($video_urls) > 0) : 
		
		$supplied = '';
		$media = '';
		
		for ($i = 0; $i < count($video_urls); $i++) :
			
			$supplied .= str_replace('.', '', strrchr(trim($video_urls[$i]), '.')).(($i < count($video_urls) - 1) ? ',' : '');
			$media .= str_replace(array('.'), array(''), strrchr(trim($video_urls[$i]), '.')).':"'.trim($video_urls[$i]).'"'.(($i < count($video_urls) - 1) ? ',' : '');
			
		endfor;
		
	endif;
	
	if ($video_url) : 
?>

					<div class="pk_video">
						<?php add_filter('embed_defaults', 'pk_page_embed_defaults'); echo str_replace(array('&', 'feature=oembed'), array('&amp;', 'feature=oembed&amp;wmode=transparent'), wp_oembed_get(esc_url($video_url))); ?>

					</div>
<?php
	endif;
	
	if (is_array($video_urls) && count($video_urls) > 0 && $video_urls[0] != '') : 
?>

					<div id="jp_container_<?php the_ID(); ?>" class="jp-video jp-video-normal-width">
						<div id="jquery_jplayer_<?php the_ID(); ?>" class="jp-jplayer"></div>
						<?php echo PK_JPLAYER_GUI; ?>

					</div>
					<script type="text/javascript">
						jQuery(document).ready(function(){
							jQuery("#jquery_jplayer_<?php the_ID(); ?>").jPlayer({
								ready: function () {
									jQuery(this).jPlayer("setMedia", {
										<?php echo $media.',poster:"'.((!pk_sidebar()) ? pk_get_featured_image('full-width') : pk_get_featured_image('big')).'"'; ?>

									});
									jQuery("#jp_container_<?php the_ID(); ?>").pk_jplayer_resize();
								},
								play: function() {
									jQuery(this).jPlayer("pauseOthers");
								},
								swfPath:"<?php echo PK_THEME_DIR; ?>/js",
								solution:"flash,html",
								supplied:"<?php echo $supplied; ?>",
								cssSelectorAncestor:"#jp_container_<?php the_ID(); ?>",
								size: {
									width:"<?php echo (is_page_template('page-full-width.php')) ? '940' : '580'; ?>px",
									height:"<?php echo (is_page_template('page-full-width.php')) ? '529' : '326'; ?>px",
									cssClass:"jp-video-normal-width"
								},
								sizeFull: {
									width:"100%",
									height:"100%",
									cssClass:"jp-video-full"
								}
							});
						});
					</script>
<?php
	endif;
?>
					<?php
	add_filter('embed_defaults', 'pk_page_embed_defaults'); the_content();
?>
