<?php
	$image_link = get_post_meta(get_post_thumbnail_id(), '_image_link', true);
	$image_link_target = get_post_meta(get_post_thumbnail_id(), '_image_link_target', true);
?>

					<article id="post-<?php the_ID(); ?>" <?php post_class('pk_entry pk_entry_blog pk_entry_standard pk_boxed pk_full_content'.((is_singular()) ? ' pk_entry_single' : '')); ?>>
<?php
	if (has_post_thumbnail()) : 
?>
						<div class="pk_image">
							<div class="pk_image_wrapper<?php if (!is_singular() || $image_link) : echo ($image_link) ? ' pk_link_icon' : ' pk_page_icon'; endif;?>">
<?php
		if (!is_singular() || $image_link) : 
?>
								<a href="<?php echo ($image_link) ? esc_url($image_link) : get_permalink(); ?>" title="<?php echo ($image_link) ? __('Click to visit:', 'pk_translate').' '.$image_link : the_title_attribute(array('echo' => 0)); ?>"<?php echo ($image_link_target) ? ' target="'.$image_link_target.'"' : ' target="_self"'; ?>>
<?php
		endif;
?>
								<?php the_post_thumbnail(((pk_sidebar()) ? 'big' : 'full-width'), array('title' => '')); ?>

								<span class="pk_image_button_back_ground"></span>
								<span class="pk_image_button_icon"></span>
<?php
		if (!is_singular() || $image_link) : 
?>
								</a>
<?php
		endif;
?>
							</div>
						</div>
<?php
	endif;
?>
						<div class="pk_entry_content">
							<header>
<?php
	if (!is_singular()) : 
?>
								<h3><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h3>
<?php
	else : 
?>
								<h3><?php the_title(); ?></h3>
<?php
	endif;
?>
								<ul class="pk_entry_meta">
									<li class="pk_entry_author"><?php _e('By:', 'pk_translate'); ?> <?php the_author_posts_link(); ?></li>
									<li class="pk_entry_date"><?php echo get_the_date(); ?></li>
									<li class="pk_entry_categories"><?php the_category(', '); ?></li>
									<li class="pk_comments_count"><?php comments_popup_link(__('0', 'pk_translate'), __('1', 'pk_translate'), __('%', 'pk_translate'), 'pk_meta_tot_comments', __('Off', 'pk_translate')); ?></li>
								</ul>
							</header>
							<?php if (is_singular()) : add_filter('embed_defaults', 'pk_post_embed_defaults'); the_content(); else : add_filter('excerpt_length', 'pk_blog_excerpt_filter'); the_excerpt(); remove_filter('excerpt_length', 'pk_blog_excerpt_filter'); endif; ?>
							<footer>
<?php
	if (!is_singular()) : 
?>
								<a href="<?php the_permalink(); ?>" title="<?php _e('Read more', 'pk_translate'); ?>" class="pk_button_read_more"><?php _e('Read more', 'pk_translate'); ?></a>
<?php
	else : 
?>
								<?php the_tags('<p>'.__('tags:', 'pk_translate').' </p><ul class="pk_entry_tags"><li>','</li><li>','</li></ul>'); ?>
<?php
	endif;
?>

							</footer>
						</div>
					</article>
