
					<article id="post-<?php the_ID(); ?>" <?php post_class('pk_entry pk_entry_blog pk_entry_standard pk_boxed pk_full_content pk_entry_single'); ?>>
						<div class="pk_entry_content">
							<header>
								<h3><?php the_title(); ?></h3>
								<ul class="pk_entry_meta">
									<li class="pk_entry_author"><?php _e('By:', 'pk_translate'); ?> <?php the_author_posts_link(); ?></li>
									<li class="pk_entry_date"><?php echo get_the_date(); ?></li>
									<li class="pk_entry_categories"><?php the_terms(get_the_ID(), 'taxonomy_portfolio'); ?></li>
									<li class="pk_comments_count"><?php comments_popup_link(__('0', 'pk_translate'), __('1', 'pk_translate'), __('%', 'pk_translate'), 'pk_meta_tot_comments', __('Off', 'pk_translate')); ?></li>
								</ul>
							</header>
							<?php add_filter('embed_defaults', 'pk_post_embed_defaults'); the_content(); ?>
						</div>
					</article>
