
					<div class="pk_message_box pk_warning_box">
						<div class="pk_message_box_content_wrapper">
							<div class="pk_message_box_content"><?php _e('It seems we can\'t find what you\'re looking for. Perhaps searching can help.', 'pk_translate'); ?></div>
						</div>
					</div>
