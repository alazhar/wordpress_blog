<?php
	get_header();
	
	global $grid;
	
	$grid = pk_get_option('blog_grid_layout', 'normal');
	$columns = pk_get_option('blog_layout', '1');
	$rows = pk_get_option('blog_layout_rows', '10');
	$columns_class = str_replace(array('1', '2', '3', '4'), array('pk_full_width', 'pk_one_half', 'pk_one_third', 'pk_one_fourth'), $columns);
	
	if (pk_get_option('blog_show_categories_filter', 'false') == 'false') remove_action('pk_blog_categories', 'pk_blog_categories_filter');
	
	do_action('pk_before_blog');
	do_action('pk_blog_categories');
	
	if (have_posts() && $columns == 1) : 
		
		while (have_posts()) : 
			
			the_post();
			(!get_post_format()) ? get_template_part('format', 'blog-standard') : get_template_part('format', 'blog-'.get_post_format());
			
		endwhile;
		
	elseif (have_posts() && $columns > 1 && $grid == 'normal') : 
		
		$c = 0;
		
		while (have_posts()) : 
			
			$c++;
			the_post();
?>

					<div class="<?php echo $columns_class; ?> pk_fixed<?php if ($c == $columns) : ?> pk_last<?php endif; ?>">
<?php
			get_template_part('format', 'grid-item');
?>

					</div>
<?php
			if ($c == $columns) : 
				
				$c = 0;
?>

					<span class="pk_clear_both"></span>
<?php
			endif;
			
		endwhile;
		
	elseif (have_posts() && $columns > 1 && $grid == 'special') : 
		
		for ($x = 1; $x <= $columns; $x++) : $i = 0;
?>

					<div class="<?php echo $columns_class; ?> pk_fixed<?php if ($x == $columns) : ?> pk_last<?php endif; ?>">
<?php			
			while (have_posts()) : 
				
				the_post();
				
				if ($i < ($rows * $x) && $i >= (($x - 1) * $rows)) : 
					
					get_template_part('format', 'grid-item');
					
				endif;
				
				$i++;
				
			endwhile;
			
			rewind_posts();
?>

					</div>
<?php			
		endfor;
?>

					<span class="pk_clear_both"></span>
<?php
		
	else : 
	
		get_template_part('format', 'no-results');
		
	endif;
	
	if ($wp_query -> max_num_pages > 1) : 
?>

					<!-- pk start pagination -->
					<div class="pk_pagination">
						<?php $pk_check_paged = (is_front_page() && !is_home()) ? 'page' : 'paged'; $pk_paged = (get_query_var($pk_check_paged)) ? get_query_var($pk_check_paged) : 1; pk_pagination($wp_query -> max_num_pages, $pk_paged, 2); ?>

					</div>
					<!-- pk end pagination -->
<?php
	endif;
	
	do_action('pk_after_blog');
	
	get_footer();
?>