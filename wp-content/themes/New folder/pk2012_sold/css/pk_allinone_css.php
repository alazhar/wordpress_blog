<?php
header("Content-type: text/css; charset: UTF-8");
header("Cache-Control: max-age=".(60 * 60 * 24 * 7).", must-revalidate");
header("Expires: ".gmdate("D, d M Y H:i:s", time() + (60 * 60 * 24 * 7))." GMT");

$skin = $_GET['skin'];
$ecommerce = $_GET['ecommerce'];
$bbpress = $_GET['bbpress'];

function minify($buffer) {
	
	require_once('../framework/includes/minify/cssmin.php');
	$buffer = str_replace('../images/prettyPhoto/', '../prettyPhoto/images/prettyPhoto/', $buffer);
	return CssMin::minify($buffer);
	
}

ob_start('minify');

require_once('pk_reset.css');

if ($ecommerce == 'jigoshop') {
	
	require_once('pk_jigoshop.css');
	require_once('pk_jigoshop_widgets.css');
	
} else {
	
	require_once('pk_woocommerce.css');
	require_once('pk_woocommerce_widgets.css');
	
}

if ($bbpress == 'yes') {
	
	require_once('pk_bbpress.css');
	require_once('pk_bbpress_widgets.css');
	
}

require_once('pk_shortcodes.css');
require_once('pk_widgets.css');
require_once('pk_style.css');
if ($skin != 'custom') require_once('pk_skin_'.$skin.'.css');
if ($skin == 'custom') require_once('pk_skin_'.$skin.'.php');
require_once('../prettyPhoto/css/prettyPhoto.css');
require_once('../style.css');

ob_end_flush();
?>