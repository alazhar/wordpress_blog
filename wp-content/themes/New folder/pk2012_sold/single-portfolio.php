<?php
	get_header();
	
	while (have_posts()) : 
		
		the_post();
		
		do_action('pk_before_work_content');
		
		$post_meta = get_post_custom(get_the_ID());
		
		(isset($post_meta['_work_format'][0]) && $post_meta['_work_format'][0]) ? get_template_part('format', 'portfolio-'.$post_meta['_work_format'][0]) : get_template_part('format', 'portfolio-standard');
		
		wp_link_pages(array('echo' => 1, 'before' => '<div class="pk_posts_link_pages"><p>'.__('Pages:', 'pk_translate'), 'after' => '</p></div>'));
?>

					<div class="pk_previous_next_post_links"><div class="pk_previous_post_links"><?php previous_post_link('%link', '%title', false); ?></div><div class="pk_next_post_links"><?php next_post_link('%link', '%title', false); ?></div></div>

<?php
		if (is_active_sidebar('sidebar_after_work_content') || is_active_sidebar('custom_sidebar_after_content_'.get_the_ID())) : 
?>
					<!-- pk start widgets area -->
					<div id="pk_after_work_content" class="pk_clearfix">

<?php
			if (is_active_sidebar('custom_sidebar_after_content_'.get_the_ID())) : 
				
				dynamic_sidebar('custom_sidebar_after_content_'.get_the_ID());
				if (!in_array(get_the_ID(), pk_get_option('hide_default_sidebars_for', array()))) : dynamic_sidebar('sidebar_after_work_content'); endif;
				
			else : 
				
				dynamic_sidebar('sidebar_after_work_content');
				
			endif;
?>
					</div>
					<!-- pk end widgets area -->

<?php
		endif;
		
		do_action('pk_after_work_content');
		
		comments_template();
		
	endwhile;
	
	get_footer();
?>