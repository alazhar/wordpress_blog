<?php
	$post_images = pk_get_post_gallery_images('full');
	
	if (has_post_thumbnail()) : 
?>

					<div class="pk_image">
						<div class="pk_image_wrapper pk_zoom_icon">
							<a class="prettyPhoto<?php the_ID(); ?>" href="<?php echo pk_get_featured_image(); ?>" title="<?php the_title(); ?>">
								<?php the_post_thumbnail(((is_page_template('page-full-width.php')) ? 'full-width' : 'big'), array('title' => '')); ?>

								<span class="pk_image_button_back_ground"></span>
								<span class="pk_image_button_icon"></span>
							</a>
<?php
		for ($i = 0; $i < count($post_images); $i++) : 
			
			$video_url = get_post_meta($post_images[$i]['id'], '_video_url', true);
?>
							<a class="prettyPhoto<?php the_ID(); ?>" href="<?php echo ($video_url) ? esc_url($video_url) : $post_images[$i]['url']; ?>" title="<?php echo $post_images[$i]['description']; ?>"></a>
<?php
		endfor;
?>
							<script type="text/javascript">
								jQuery(document).ready(function() {
									jQuery(".prettyPhoto<?php the_ID(); ?>").attr('rel', 'prettyPhoto[gallery_<?php the_ID(); ?>]').prettyPhoto(<?php echo PK_PRETTYPHOTO_PARAMS; ?>);
								});
							</script>
						</div>
					</div>
<?php
	endif;
?>
					<?php
	add_filter('embed_defaults', 'pk_page_embed_defaults'); the_content();
?>
