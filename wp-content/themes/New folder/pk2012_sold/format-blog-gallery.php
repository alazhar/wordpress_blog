<?php
	$post_meta = get_post_custom(get_the_ID());
	$post_images = pk_get_post_gallery_images(((pk_sidebar()) ? 'big' : 'full-width'));
	$post_images_full = pk_get_post_gallery_images('full');
?>

					<article id="post-<?php the_ID(); ?>" <?php post_class('pk_entry pk_entry_blog pk_entry_gallery pk_boxed pk_full_content'.((is_singular()) ? ' pk_entry_single' : '')); ?>>
						<div id="pk_slider_<?php the_ID(); ?>" class="pk_slider">
							<div class="pk_slider_content">
<?php
	for ($i = 0; $i < count($post_images); $i++) : 
		
		$video_url = get_post_meta($post_images[$i]['id'], '_video_url', true);
		
		$image_link = get_post_meta($post_images[$i]['id'], '_image_link', true);
		$image_link_target = get_post_meta($post_images[$i]['id'], '_image_link_target', true);
?>
								<div class="pk_slider_item">
									<div class="pk_slider_item_media">
										<div class="pk_image">
											<div class="pk_image_wrapper<?php echo ($video_url) ? ' pk_play_icon' : (($image_link)? ' pk_link_icon' : ' pk_zoom_icon'); ?>">
												<a <?php if (!$image_link) : ?>class="prettyPhoto<?php echo the_ID(); ?>" <?php endif; ?>href="<?php if ($image_link) : echo esc_url($image_link); else : echo ($video_url) ? esc_url($video_url) : $post_images_full[$i]['url']; endif; ?>"<?php echo ($image_link_target) ? ' target="'.$image_link_target.'"' : ' target="_self"'; ?> title="<?php echo $post_images[$i]['description']; ?>">
													<?php echo $post_images[$i]['img']; ?>

													<span class="pk_image_button_back_ground"></span>
													<span class="pk_image_button_icon"></span>
												</a>
											</div>
										</div>
									</div>
<?php
		if ($post_images[$i]['description']) : 
?>
									<div class="pk_slider_item_info">
										<div class="pk_slider_item_info_background"></div>
										<div class="pk_slider_item_info_content"><?php echo $post_images[$i]['description']; ?></div>
									</div>
<?php
		endif;
?>
								</div>
<?php
	endfor;
?>
							</div>
						</div>
						<script type="text/javascript">
							jQuery(document).ready(function() {
								jQuery(".prettyPhoto<?php the_ID(); ?>").attr('rel', 'prettyPhoto[gallery_<?php the_ID(); ?>]').prettyPhoto(<?php echo PK_PRETTYPHOTO_PARAMS; ?>);
								jQuery("#pk_slider_<?php the_ID(); ?>").pk_slider({
									sliderWidth:580,
									sliderHeight:0,
									buttonInfoOpenLabel:"<?php echo (isset($post_meta['_slider_info_button_open_label'][0])) ? $post_meta['_slider_info_button_open_label'][0] : __('info', 'pk_translate'); ?>",
									buttonInfoCloseLabel:"<?php echo (isset($post_meta['_slider_info_button_close_label'][0])) ? $post_meta['_slider_info_button_close_label'][0] : __('close info', 'pk_translate'); ?>",
									infoTextAlign:"left",
									infoBackgroundAlpha:0.9,
									slideshow:<?php echo (isset($post_meta['_slider_slideshow'][0])) ? $post_meta['_slider_slideshow'][0] : 'true'; ?>,
									slideshowAutoStart:<?php echo (isset($post_meta['_slider_slideshow_auto_start'][0])) ? $post_meta['_slider_slideshow_auto_start'][0] : 'false'; ?>,
									slideshowInterval:<?php echo (isset($post_meta['_slider_slideshow_interval'][0])) ? $post_meta['_slider_slideshow_interval'][0] : '5'; ?>,
									easing: "easeOutExpo",
									speedIn:400,
									speedOut:400
								});
							});
						</script>
						<div class="pk_entry_content">
							<header>
<?php
	if (!is_singular()) : 
?>
								<h3><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h3>
<?php
	else : 
?>
								<h3><?php the_title(); ?></h3>
<?php
	endif;
?>
								<ul class="pk_entry_meta">
									<li class="pk_entry_author"><?php _e('By:', 'pk_translate'); ?> <?php the_author_posts_link(); ?></li>
									<li class="pk_entry_date"><?php echo get_the_date(); ?></li>
									<li class="pk_entry_categories"><?php the_category(', '); ?></li>
									<li class="pk_comments_count"><?php comments_popup_link(__('0', 'pk_translate'), __('1', 'pk_translate'), __('%', 'pk_translate'), 'pk_meta_tot_comments', __('Off', 'pk_translate')); ?></li>
								</ul>
							</header>
							<?php if (is_singular()) : add_filter('embed_defaults', 'pk_post_embed_defaults'); the_content(); else : add_filter('excerpt_length', 'pk_blog_excerpt_filter'); the_excerpt(); remove_filter('excerpt_length', 'pk_blog_excerpt_filter'); endif; ?>
							<footer>
<?php
	if (!is_singular()) : 
?>
								<a href="<?php the_permalink(); ?>" title="<?php _e('Read more', 'pk_translate'); ?>" class="pk_button_read_more"><?php _e('Read more', 'pk_translate'); ?></a>
<?php
	else : 
?>
								<?php the_tags('<p>'.__('tags:', 'pk_translate').' </p><ul class="pk_entry_tags"><li>','</li><li>','</li></ul>'); ?>
<?php
	endif;
?>

							</footer>
						</div>
					</article>
