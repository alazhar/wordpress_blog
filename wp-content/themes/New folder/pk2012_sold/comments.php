<?php

function pk_comments_navigation($navigation) {
	
	if (!$navigation) return;
	
	$button_prev = (get_previous_comments_link(__('&laquo;', 'pk_translate'))) ? str_replace('<a', '<a class="pk_button_prev"', get_previous_comments_link(__('&laquo;', 'pk_translate'))) : '<span class="pk_button_prev"></span>';
	$button_next = (get_next_comments_link(__('&raquo;', 'pk_translate'))) ? str_replace('<a', '<a class="pk_button_next"', get_next_comments_link(__('&raquo;', 'pk_translate'))) : '<span class="pk_button_next"></span>';
	
?>

							<!-- pk start comments pagination -->
							<div class="pk_pagination pk_pagination_comments">
								<?php
	echo $button_prev;
	
	for ($i = 0; $i < count($navigation); $i++) {
		
		echo str_replace('current', 'pk_current_page', $navigation[$i]);
		
	}
	
	echo $button_next;
?>

							</div>
							<!-- pk end comments pagination -->
<?php
	
}

function pk_comments_list($comment, $args, $depth) {
	
	$GLOBALS['comment'] = $comment;
	
	if (get_comment_type() != 'comment') :
		
		pk_trackbacks_list($comment, $args, $depth);
		
	else :
	
?>

								<!-- pk start comment -->
								<li id="li-comment-<?php comment_ID(); ?>" <?php comment_class('pk_comment'); ?>>
									<div id="comment-<?php comment_ID(); ?>" class="pk_message pk_boxed">
<?php if (get_avatar($comment, '40')) : ?>
										<div class="pk_gravatar">
											<?php echo get_avatar($comment, '40'); ?>

										</div>
<?php endif; ?>
										<div class="pk_comment_content">
											<div class="pk_comment_meta">
												<p class="pk_comment_author"><?php _e('By: ', 'pk_translate'); comment_author_link(); ?></p>
												<p class="pk_comment_date"><span><?php printf(__('on: %s', 'pk_translate'), get_comment_date()); ?></span> . <?php comment_reply_link(array_merge($args, array('reply_text' => __('Reply', 'pk_translate'), 'depth' => $depth, 'max_depth' => $args['max_depth']))); ?></p>
											</div>
											<div class="pk_comment_text">
												<?php
			if ($comment -> comment_approved == 0) : 
?>
												<p><em><?php _e('Your comment is awaiting moderation.', 'pk_translate'); ?></em></p>
<?php
			else : 
				
				comment_text();
				
			endif;
?>
											</div>
										</div>
									</div>
<?php	
	endif;
	
}

function pk_trackbacks_list($comment, $args, $depth) {
	
	$GLOBALS['comment'] = $comment;
	
?>
								<!-- pk start trackback comment -->
								<li id="li-trackback-comment-<?php comment_ID(); ?>" <?php comment_class('pk_comment'); ?>>
									<div id="trackback-comment-<?php comment_ID(); ?>" class="pk_message pk_boxed">
										<div class="pk_comment_meta"></div>
										<div class="pk_comment_text">
											<p><?php _e('Pingback: ', 'pk_translate'); comment_author_link(); ?></p>
										</div>
									</div>
<?php
	
}

function pk_check_referrer() {
	
	if (!isset($_SERVER['HTTP_REFERER']) || $_SERVER['HTTP_REFERER'] == '') {
		
		wp_die('Please enable referrers in your browser!');
		
	}
	
}
 
add_action('check_comment_flood', 'pk_check_referrer');

if (!empty($_SERVER['SCRIPT_FILENAME']) && 'comments.php' == basename($_SERVER['SCRIPT_FILENAME'])) die('Please do not load this page directly. Thanks!');

if (post_password_required()) : 
?>
					<p class="nocomments"><?php _e('This post is password protected. Enter the password to view and post comments.', 'pk_translate'); ?></p>
<?php
	return;
	
endif;

if (comments_open() && get_comments_number() > 0) : 
?>

					<!-- pk start comments -->
					<div id="comments" class="pk_clearfix">

						<!-- pk start pk_comments -->
						<div class="pk_comments pk_clearfix">

							<!-- pk start comments rss link -->
							<a class="pk_comments_rss" href="<?php echo get_post_comments_feed_link(); ?>" title="<?php _e('Subscribe to Comments via RSS', 'pk_translate'); ?>"><?php _e('RSS', 'pk_translate'); ?></a>
							<!-- pk end comments rss link -->

							<!-- pk start comments title -->
							<h4 class="pk_heading_uppercase"><?php printf(_n('One Comment', '%1$s Comments', get_comments_number(), 'pk_translate'), number_format_i18n(get_comments_number())); ?></h4>
							<!-- pk end comments title -->

							<!-- pk start comments list -->
							<ol class="comment_list">
<?php
	wp_list_comments(array('callback' => 'pk_comments_list'));
?>

							</ol>
							<!-- pk end comments list -->
<?php
	pk_comments_navigation(paginate_comments_links('prev_next=0&show_all=1&echo=0&type=array'));
?>

						</div>
						<!-- pk end pk_comments -->

					</div>
					<!-- pk end comments -->

<?php
endif;

if (comments_open()) : 
?>
					<!-- pk start comments form -->
					<?php
	$commenter = wp_get_current_commenter();
	$req = get_option('require_name_email');
	$aria_req = ($req ? " aria-required='true'" : '');
	
	$fields =  array(
		'author' => '<p class="comment-form-author">'.'<label for="author">'.__('Name', 'pk_translate').'</label> '.($req ? '<span class="required">*</span>' : '').'<input id="author" name="author" type="text" value="'.esc_attr($commenter['comment_author']).'" size="30"'.$aria_req.' /></p>',
		'email'  => '<p class="comment-form-email"><label for="email">'.__('Email', 'pk_translate').'</label> '.($req ? '<span class="required">*</span>' : '').'<input id="email" name="email" type="text" value="'.esc_attr($commenter['comment_author_email']).'" size="30"'.$aria_req.' /></p>',
		'url'    => '<p class="comment-form-url"><label for="url">'.__('Website', 'pk_translate').'</label>'.'<input id="url" name="url" type="text" value="'.esc_attr($commenter['comment_author_url']).'" size="30" /></p>',
	);
	
	$args = array(
		'fields' 					=> $fields,
		'comment_notes_before' 		=> '',
		'comment_notes_after' 		=> '',
		'title_reply' 				=> __('Reply', 'pk_translate'),
		'title_reply_to' 			=> __('Leave a Reply to %s', 'pk_translate'),
		'cancel_reply_link' 		=> __('Cancel Reply', 'pk_translate'),
		'label_submit' 				=> __('Post Comment', 'pk_translate'),
		'comment_field' 			=> '<p class="comment-form-comment"><label for="comment">'.__('Comment', 'pk_translate').'</label><textarea id="comment" name="comment" cols="45" rows="8" aria-required="true"></textarea></p>',
		'must_log_in' 				=> '<p class="must-log-in">'.sprintf(__('You must be <a href="%s">logged in</a> to post a comment.', 'pk_translate'), wp_login_url(apply_filters('the_permalink', get_permalink()))).'</p>',
		'logged_in_as' 				=> '<p class="logged-in-as">'.sprintf(__('Logged in as <a href="%1$s">%2$s</a>. <a href="%3$s" title="Log out of this account">Log out?</a>', 'pk_translate'), admin_url('profile.php'), $user_identity, wp_logout_url(apply_filters('the_permalink', get_permalink()))).'</p>'
	);
	
	function minify_form($buffer) {
		
		return str_replace(array("\t", "\n", "\r"), '', $buffer);
		
	}
	
	ob_start('minify_form');
	comment_form($args);
	ob_end_flush();
?>

					<!-- pk end comments form -->
<?php
endif;
?>