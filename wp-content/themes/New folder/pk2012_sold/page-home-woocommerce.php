<?php
/*
Template Name: Home Page WooCommerce Template
*/
?>
<?php
	get_header();
	
	do_action('woocommerce_before_shop_loop');
	
	global $woocommerce;
	
	while (have_posts()) : 
		
		the_post();
		add_filter('embed_defaults', 'pk_page_embed_defaults');
		
		if (get_the_content()) :
?>

					<!-- pk start contents -->
					<section id="pk_page_content" class="pk_clearfix">
						<?php
		the_content();
?>

					</section>
					<!-- pk end contents -->
<?php
		endif;
		
	endwhile;
	
	$post_meta = get_post_custom(get_the_ID());
	
	if (is_active_sidebar('sidebar_home_page_woocommerce_left') || is_active_sidebar('sidebar_home_page_woocommerce_right')) : 
?>

					<!-- pk start widgets areas -->
					<section id="pk_home_woocommerce_widgets" class="pk_clearfix">
						<div class="pk_one_half">

<?php if (dynamic_sidebar('sidebar_home_page_woocommerce_left')) : endif; ?>
						</div>
						<div class="pk_one_half pk_last">

<?php if (dynamic_sidebar('sidebar_home_page_woocommerce_right')) : endif; ?>
						</div>
					</section>
					<!-- pk end widgets areas -->
<?php
	endif;
	
	if (function_exists('woocommerce_product')) : 
		
		if (pk_get_option('homepages_latest_products', 'true') == 'true') : 
?>
					
					<!-- pk start latest products -->
					<section id="pk_latest_products" class="pk_clearfix">
						<h3 class="pk_heading_uppercase"><?php _e('Latest products', 'pk_translate'); ?></h3>
						<p class="pk_button_view_all"><a href="<?php echo ((function_exists('woocommerce_get_page_id')) ? get_permalink((function_exists('icl_object_id')) ? icl_object_id(woocommerce_get_page_id('shop'), 'page', true) : woocommerce_get_page_id('shop')) : '#'); ?>" title="<?php _e('View all', 'pk_translate'); ?>" class="pk_button_mini pk_button_<?php echo ((isset($post_meta['_view_all_button_color'][0])) ? $post_meta['_view_all_button_color'][0] : 'white'); ?>"><span><?php _e('View all', 'pk_translate'); ?></span></a></p>
<?php
			$total_recent_products = (isset($post_meta['_total_recent_products'][0])) ? (int)trim($post_meta['_total_recent_products'][0]) : 2;
			
			$temp_query = $wp_query;
			
			$query_args['post_type'] = 'product';
			$query_args['post_status'] = 'publish';
			$query_args['orderby'] = 'date';
			$query_args['order'] = 'desc';
			$query_args['meta_query'][] = $woocommerce -> query -> stock_status_meta_query();
			$query_args['meta_query'][] = $woocommerce -> query -> visibility_meta_query();
			$query_args['posts_per_page'] = $total_recent_products;
			
			$wp_query = new WP_Query($query_args);
			
			$c = 0;
			
			if (have_posts()) : 
				
				while (have_posts()) : 
					
					the_post();
					$c++;
					$product = new woocommerce_product(get_the_ID());
?>
						<div class="pk_one_half pk_fixed<?php if ($c == 2) echo ' pk_last'; ?>">
							<div class="pk_entry pk_entry_product pk_boxed pk_full_content">
								<?php woocommerce_show_product_sale_flash(get_the_ID(), $product); ?>
<?php
					if (has_post_thumbnail()) : 
?>
								<div class="pk_image">
									<div class="pk_image_wrapper pk_zoom_icon">
										<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
											<?php the_post_thumbnail('shop_large'); ?>

											<span class="pk_image_button_back_ground"></span>
											<span class="pk_image_button_icon"></span>
										</a>
									</div>
								</div>
<?php
					endif;
?>
								<div class="pk_entry_content">
									<header>
                                    	<p class="pk_entry_price"><?php echo $product -> get_price_html(); ?></p>
										<p class="pk_button_add_cart"><?php woocommerce_template_loop_add_to_cart(get_the_ID(), $product); ?></p>
                                    </header>
                                    <h4><?php the_title(); ?></h4>
									<p><?php echo wp_trim_words(get_the_excerpt(), 15, '&hellip;'); ?></p>
									<footer>
                                    	<a href="<?php the_permalink(); ?>" title="<?php _e('Read more', 'pk_translate'); ?>" class="pk_button_read_more"><?php _e('Details', 'pk_translate'); ?></a>
									</footer>
								</div>
							</div>
						</div>
<?php
					if ($c == 2) $c = 0;
					
				endwhile;
				
			endif;
?>
					</section>
					<!-- pk end latest products -->
<?php
			$wp_query = $temp_query;
			wp_reset_query();
			
			unset($query_args);
			
		endif;
		
		if (pk_get_option('homepages_featured_products', 'true') == 'true') : 
?>

					<!-- pk start featured products -->
					<section id="pk_featured_products" class="pk_last_section pk_clearfix">
						<h3 class="pk_heading_uppercase"><?php _e('Featured products', 'pk_translate'); ?></h3>
						<p class="pk_button_view_all"><a href="<?php echo ((function_exists('woocommerce_get_page_id')) ? get_permalink((function_exists('icl_object_id')) ? icl_object_id(woocommerce_get_page_id('shop'), 'page', true) : woocommerce_get_page_id('shop')) : '#'); ?>" title="<?php _e('View all', 'pk_translate'); ?>" class="pk_button_mini pk_button_<?php echo ((isset($post_meta['_view_all_button_color'][0])) ? $post_meta['_view_all_button_color'][0] : 'white'); ?>"><span><?php _e('View all', 'pk_translate'); ?></span></a></p>
<?php
			$total_featured_products = (isset($post_meta['_total_featured_products'][0]) && trim($post_meta['_total_featured_products'][0]) != '') ? (int)trim($post_meta['_total_featured_products'][0]) : 3;
			
			$temp_query = $wp_query;
			
			$query_args['post_type'] = 'product';
			$query_args['post_status'] = 'publish';
			$query_args['meta_query'][] = array('key' => '_featured', 'value' => 'yes');
			$query_args['meta_query'][] = $woocommerce -> query -> stock_status_meta_query();
			$query_args['meta_query'][] = $woocommerce -> query -> visibility_meta_query();
			$query_args['posts_per_page'] = $total_featured_products;
			
			$wp_query = new WP_Query($query_args);
			
			$c = 0;
			
			if (have_posts()) : 
				
				while (have_posts()) : 
					
					the_post();
					$c++;
					$product = new woocommerce_product(get_the_ID());
?>
						<div class="pk_one_third pk_fixed<?php if ($c == 3) echo ' pk_last'; ?>">
							<div class="pk_entry pk_entry_product pk_boxed pk_full_content">
								<?php woocommerce_show_product_sale_flash(get_the_ID(), $product); ?>
<?php
					if (has_post_thumbnail()) : 
?>
								<div class="pk_image">
									<div class="pk_image_wrapper pk_zoom_icon">
										<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
											<?php the_post_thumbnail('shop_large'); ?>

											<span class="pk_image_button_back_ground"></span>
											<span class="pk_image_button_icon"></span>
										</a>
									</div>
								</div>
<?php
					endif;
?>
								<div class="pk_entry_content">
									<header>
                                    	<p class="pk_entry_price"><?php echo $product -> get_price_html(); ?></p>
                                    </header>
                                    <h4><?php the_title(); ?></h4>
									<p><?php echo wp_trim_words(get_the_excerpt(), 7, '&hellip;'); ?></p>
                                    <a href="<?php the_permalink(); ?>" title="<?php _e('Read more', 'pk_translate'); ?>" class="pk_button_read_more"><?php _e('Details', 'pk_translate'); ?></a>
									<footer>
                                    	<p class="pk_button_add_cart"><?php woocommerce_template_loop_add_to_cart(get_the_ID(), $product); ?></p>
                                    </footer>
								</div>
							</div>
						</div>
<?php
					if ($c == 3) $c = 0;
					
				endwhile;
				
			endif;
?>
					</section>
					<!-- pk end featured products -->
<?php
			$wp_query = $temp_query;
			wp_reset_query();
			
		endif;
		
	endif;
	
	get_footer();
?>