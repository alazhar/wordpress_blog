<?php
	get_header();
	
	global $grid;
	
	$grid = pk_get_option('portfolio_grid_layout', 'normal');
	$columns = pk_get_option('portfolio_layout', '4');
	$rows = pk_get_option('portfolio_layout_rows', '3');
	$columns_class = str_replace(array('1', '2', '3', '4'), array('pk_full_width', 'pk_one_half', 'pk_one_third', 'pk_one_fourth'), $columns);
	
	if (pk_get_option('portfolio_show_categories_filter', 'true') == 'false') remove_action('pk_portfolio_categories', 'pk_portfolio_categories_filter');
	
	$temp_query = $wp_query;
	
	$check_paged = (is_front_page() && !is_home()) ? 'page' : 'paged';
	$paged = (get_query_var($check_paged)) ? get_query_var($check_paged) : 1;
	$posts_per_page = $columns * $rows;
	
	$query_args['post_type'] = 'portfolio';
	$query_args['posts_per_page'] = $posts_per_page;
	$query_args['orderby'] = 'menu_order';
	$query_args['order'] = 'ASC';
	$query_args['paged'] = $paged;
	
	$wp_query = new WP_Query($query_args);
	
	do_action('pk_before_portfolio');
	do_action('pk_portfolio_categories');
	
	if (have_posts() && $columns == 1) : 
		
		while (have_posts()) : 
			
			the_post();
			$c++;
			if ($columns > 1) : get_template_part('format', 'grid-item'); else : $post_meta = get_post_custom(get_the_ID()); get_template_part('format', 'blog-'.str_replace(array('gallery', 'slider'), array('image', 'gallery'), ((isset($post_meta['_work_format'][0])) ? $post_meta['_work_format'][0] : 'standard'))); endif;
			
		endwhile;
		
	elseif (have_posts() && $columns > 1 && $grid == 'normal') : 
		
		$c = 0;
		
		while (have_posts()) : 
			
			$c++;
			the_post();
?>

					<div class="<?php echo $columns_class; ?> pk_fixed<?php if ($c == $columns) : ?> pk_last<?php endif; ?>">
<?php
			get_template_part('format', 'grid-item');
?>

					</div>
<?php
			if ($c == $columns) : 
				
				$c = 0;
?>

					<span class="pk_clear_both"></span>
<?php
			endif;
			
		endwhile;
		
	elseif (have_posts() && $columns > 1 && $grid == 'special') : 
		
		for ($x = 1; $x <= $columns; $x++) : $i = 0;
?>

					<div class="<?php echo $columns_class; ?> pk_fixed<?php if ($x == $columns) : ?> pk_last<?php endif; ?>">
<?php			
			while (have_posts()) : 
				
				the_post();
				
				if ($i < ($rows * $x) && $i >= (($x - 1) * $rows)) : 
					
					get_template_part('format', 'grid-item');
					
				endif;
				
				$i++;
				
			endwhile;
			
			rewind_posts();
?>

					</div>
<?php			
		endfor;
?>

					<span class="pk_clear_both"></span>
<?php
		
	else : 
	
		get_template_part('format', 'no-results');
		
	endif;
	
	if ($wp_query -> max_num_pages > 1) : 
?>

					<!-- pk start pagination -->
					<div class="pk_pagination">
						<?php $check_paged = (is_front_page() && !is_home()) ? 'page' : 'paged'; $paged = (get_query_var($check_paged)) ? get_query_var($check_paged) : 1; pk_pagination($wp_query -> max_num_pages, $paged, 2); ?>

					</div>
					<!-- pk end pagination -->
<?php
	endif;
	
	$wp_query = $temp_query;
	wp_reset_query();
	
	do_action('pk_after_portfolio');
	
	get_footer();
?>