<!DOCTYPE html>
<!--[if IE 7 ]>    <html class="ie7"> <![endif]-->
<!--[if IE 8 ]>    <html class="ie8"> <![endif]-->
<html <?php language_attributes(); ?>>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <!-- Mobile Specific Metas
        ================================================== -->
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <?php current_title(); ?>
        <?php favicon(); ?>
        <!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
        <!--[if IE 7]><link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/font-awesome/font-awesome-ie7.min.css"><![endif]-->
        <?php
        facebookOpenGraphMeta();
        global $tw_end;
        $tw_start = $tw_end = '';
        if (tw_option('theme_layout') != "Fullwidth") {
            $tw_start = '<div class="theme-boxed">';
            $tw_end = '</div>';
        }
        ?>
        <?php wp_head(); ?>
    </head>
    <body <?php body_class() ?>>
        <?php echo $tw_start; ?>

        <?php if (tw_option('top_bar')) { ?>
            <div class="tw-top-bar">
                <div class="container">
                    <div class="row">
                        <div class="span6">
                            <div class="tw-top-bar-info"><?php echo tw_option('top_bar_text'); ?></div>
                        </div>
                        <div class="span6">
                            <ul class="tw-social-icon pull-right">
                                <?php tw_social(); ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
        <?php
        $header_layout0 = "default";
        if (is_page()) {
            $header_layout0 = get_metabox('header_layout');
        }
        $header_layout = ($header_layout0 == "default" || $header_layout0 == "") ? tw_option('header_layout') : $header_layout0;
        ?>
        <!-- Start Header -->
        <?php if ($header_layout == "Header style 1") { ?>        
            <header id="header" class="header_1">
                <div class="container">
                    <div class="row">
                        <div class="span3">
                            <?php theme_logo(); ?>
                        </div>
                        <div class="span9">
                            <nav class="menu-container visible-desktop">
                                <?php theme_menu(); ?>
                            </nav>
                            <div class="show-mobile-menu hidden-desktop clearfix">
                                <div class="mobile-menu-text"><?php _e('Menu', 'themewaves');?></div>
                                <div class="mobile-menu-icon">
                                    <span></span><span></span><span></span><span></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <nav id="mobile-menu">
                        <?php mobile_menu(); ?>
                    </nav>
                </div>            
            </header>
        <?php } elseif ($header_layout == "Header style 2") { ?>
            <header id="header" class="header_2">
                <div class="container">
                    <?php theme_logo(); ?>
                    <nav class="menu-container visible-desktop">
                        <?php theme_menu(); ?>
                    </nav>
                    <div class="show-mobile-menu hidden-desktop clearfix">
                        <div class="mobile-menu-text"><?php _e('Menu', 'themewaves');?></div>
                        <div class="mobile-menu-icon">
                            <span></span><span></span><span></span><span></span>
                        </div>
                    </div>
                    <nav id="mobile-menu">
                        <?php mobile_menu(); ?>
                    </nav>
                </div>            
            </header>
        <?php } else { ?>
            <header id="header" class="header_3">
                <div class="container">                        
                    <div class="row">
                        <div class="span3">
                            <?php theme_logo(); ?>
                        </div>
                        <div class="span9">
                            <nav class="menu-container visible-desktop">
                                <?php theme_menu(); ?>
                            </nav>
                        </div>
                        <div class="show-mobile-menu hidden-desktop clearfix">
                            <div class="mobile-menu-text"><?php _e('Menu', 'themewaves');?></div>
                            <div class="mobile-menu-icon">
                                <span></span><span></span><span></span><span></span>
                            </div>
                        </div>
                    </div>
                    <nav id="mobile-menu">
                        <?php mobile_menu(); ?>
                    </nav>
                </div>            
            </header>
        <?php } ?>
        <!-- End Header -->
        <?php
        get_template_part('slider');
        ?>
        <!-- Start Main -->
        <section id="main">
            <div class="container">