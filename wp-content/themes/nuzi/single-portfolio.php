<?php get_header(); ?>
<?php the_post(); ?>
<article id="post-<?php the_ID(); ?>" <?php post_class('single-portfolio');?>>
    <div class="row">
        <div class="span4">
            <?php
            echo do_shortcode('[tw_item_title title="'.__('Project Description', 'themewaves').'"]');
            the_content(); ?>
            <?php
            if (get_metabox("preview_url") != "") {
                $url = get_metabox("preview_url");
                if (!preg_match_all('!https?://[\S]+!', $url, $matches))
                    $url = "http://" . get_metabox("preview_url");
                ?>
                <a href="<?php echo $url; ?>" target="_blank" class="btn btn-small live-preview"><?php _e("Live preview", "themewaves"); ?></a>
            <?php } ?>
        </div>
        <div class="span8">
            <?php
            $ids = get_metabox('gallery_image_ids');
            $video_embed = get_metabox('format_video_embed');            
            $video_thumb = get_metabox('format_video_thumb');            
            $video_m4v = get_metabox('format_video_m4v');
            if($ids!="false" && $ids!="") {format_gallery(true);}
            elseif(!empty($video_embed)||!empty($video_m4v)) {format_video(true,$video_embed,$video_thumb,$video_m4v);}
            else {format_standard(true);}
            ?>
        </div>        
    </div>
</article>    

<div class="row">
    <div class="span12 related_portfolios">
        <?php related_portfolios(); ?>
    </div>
</div>

<?php get_footer(); ?>