<?php
global $tw_options, $tw_isArchive;
$block = isset($tw_options['block'])          ?intval($tw_options['block'])          :1;
$show_pagination = isset($tw_options['show_pagination'])?$tw_options['show_pagination']:true;

if($tw_isArchive || is_tag() || is_search()){
    $nofeatured = true;
    $block = 1;
    $tw_options['excerpt_count'] = 50;
}


if (have_posts ()) {
    echo $block == 4 ? "<div class='row'>" : "";
        while (have_posts ()) : the_post();            
            $format = get_post_format() == "" ? "standard" : get_post_format();
            $featured = true;
            if (!has_post_thumbnail($post->ID) || in_array($format, array('aside', 'chat'))) {
                if(in_array($format, array('aside', 'chat', 'standard')))
                    $featured = false;
            }?>         



                        <article id="post-<?php the_ID(); ?>" <?php post_class("loop");?>>
                                <?php 
                                    if($format == 'link' || $format == 'quote') { ?>
                                        <div class="loop-block">
                                            <div class="loop-format">
                                                <span class="post-format <?php echo $format; ?>"></span>
                                            </div>
                                            <div class="loop-media">
                                                    <?php call_user_func('format_'.$format); ?>
                                            </div>
                                        </div><?php
                                    } else {
                                        ?>
                                        <?php if(!isset($nofeatured) && $featured) { ?>
                                            <div class="loop-media">
                                                    <?php call_user_func('format_'.$format); ?>
                                            </div>
                                        <?php } ?>
                                        <div class="loop-block">
                                            <div class="loop-format">
                                                <span class="post-format <?php echo $format; ?>"></span>
                                            </div>
                                            <h2 class="loop-title"><a href="<?php the_permalink();?>"><?php the_title();?></a></h2>
                                            <div class="meta-container">
                                                <ul class="loop-meta inline">                                                
                                                    <li class="date"><i class="icon-calendar"></i><?php the_time('j M Y'); ?></li>
                                                    <?php echo comment_count(); ?>
                                                    <li class="author"><i class="icon-user"></i><?php the_author_posts_link() ?></li>
                                                    <li class="category"><i class="icon-tags"></i><?php echo get_the_category_list(', '); ?></li>
                                                </ul>
                                            </div>
                                            <div class="loop-content clearfix">
                                                <?php loop_content(); ?>
                                            </div>
                                        </div>
                                <?php } ?>
                        </article>


            
        <?php           

        endwhile;        
        echo $block == 4 ? "</div>" : "";
        if($show_pagination){pagination();}
        wp_reset_query();
}