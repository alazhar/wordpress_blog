<?php    
$selectsidebar = array();
$selectsidebar["Default sidebar"] = "Default sidebar";
$sidebars = get_option('sbg_sidebars');
if (!empty($sidebars)) {
    foreach ($sidebars as $sidebar) {
        $selectsidebar[$sidebar] = $sidebar;
    }
}
$header_type = array(
    'subtitle'=>'Title and Subtitle',
    'slider'=>'Slider',
    'image'=>'Featured Image',
    'map'=>'Google Map',
    'none'=>'None');
$header_layout = array(
    'default'=> 'Default',
    'Header style 1'=> 'Header style 1',
    'Header style 2'=> 'Header style 2',
    'Header style 3'=> 'Header style 3');
/* * *********************** */
/* Post options */
/* * *********************** */
$tw_post_settings = Array(
    Array(        
        'name' => __('Post Author Show?', 'themewaves'),
        'desc' => __('Default setting will take from theme options.', 'themewaves'),
        'id' => 'post_authorr',
        'std' => 'default',
        'type' => 'selectbox'),
    Array(        
        'name' => __('Featured Media Show?', 'themewaves'),
        'desc' => __('Default setting will take from theme options.', 'themewaves'),
        'id' => 'feature_show',
        'std' => 'default',
        'type' => 'selectbox'),
    Array(        
        'name' => __('Featured Image Height?', 'themewaves'),
        'desc' => __('Image height (px).', 'themewaves'),
        'id' => 'image_height',
        'std' => '350',
        'type' => 'text'),
    Array(        
        'name' => __('Breadcrumbs on this page?', 'themewaves'),
        'desc' => __('Default setting will take from theme options.', 'themewaves'),
        'id' => 'breadcrumps',
        'std' => 'default',
        'type' => 'selectbox'),
    Array(        
        'name' => __('Choose Post Layout?', 'themewaves'),
        'desc' => __('', 'themewaves'),
        'id' => 'layout',
        'std' => 'right',
        'type' => 'layout'),
    Array(        
        'name' => __('Choose Sidebar ?', 'themewaves'),
        'desc' => __('', 'themewaves'),
        'id' => 'sidebar',
        'options' => $selectsidebar,
        'std' => 'Default sidebar',
        'type' => 'select')
);


/* * *********************** */
/* Page options */
/* * *********************** */
$tw_page_settings = Array(
    Array(
        'name' => __('Header style ?', 'themewaves'),
        'desc' => __('', 'themewaves'),
        'id' => 'header_layout',
        'std' => 'default',
        'options' => $header_layout,
        'type' => 'select'),
    Array(
        'name' => __('Header type ?', 'themewaves'),
        'desc' => __('', 'themewaves'),
        'id' => 'header_type',
        'std' => 'subtitle',
        'options' => $header_type,
        'type' => 'select'),
    Array(
        'name' => __('Select Slideshow', 'themewaves'),
        'desc' => __('All of your created sliders will be included here.', 'themewaves'),
        'id' => 'slider_id',
        'type' => 'slideshow'),
    Array(
        'name' => __('Sub Title', 'themewaves'),
        'desc' => __('', 'themewaves'),
        'id' => 'subtitle',
        'type' => 'text'),
    Array(
        'name' => __('Background image of page title', 'themewaves'),
        'desc' => __('', 'themewaves'),
        'id' => 'bg_image',
        'type' => 'file'),
    Array(
        'name' => __('Google Map', 'themewaves'),
        'desc' => __('', 'themewaves'),
        'id' => 'googlemap',
        'type' => 'textarea'),
    Array(        
        'name' => __('Breadcrumbs on this post?', 'themewaves'),
        'desc' => __('Default setting will take from theme options.', 'themewaves'),
        'id' => 'breadcrumps',
        'std' => 'default',
        'type' => 'selectbox'),
);
if(!tw_option("pagebuilder")){
    $tw_page_settings[] = Array(        
        'name' => __('Page Layout ?', 'themewaves'),
        'desc' => __('Layout-aa songono uu', 'themewaves'),
        'id' => 'layout',
        'std' => 'full',
        'type' => 'layout');
    $tw_page_settings[] = Array(        
        'name' => __('Sidebar ?', 'themewaves'),
        'desc' => __('Sidebar-aa songono uu', 'themewaves'),
        'id' => 'sidebar',
        'options' => $selectsidebar,
        'std' => 'Default sidebar',
        'type' => 'select');
}


/* * *********************** */
/* Portfolio options */
/* * *********************** */
$tw_portfolio_settings = Array(
    Array(
        'name' => __('Preview url', 'themewaves'),
        'desc' => __('Preview url', 'themewaves'),
        'id' => 'preview_url',
        'type' => 'text'),
    Array(
        'name' => __('Image height', 'themewaves'),
        'desc' => __('Image height on single portfolio', 'themewaves'),
        'id' => 'image_height',
        'type' => 'text'),
);
$tw_portfolio_gallery = array(
//        array( "name" => __('Enable Lightbox','framework'),
//                        "desc" => __('Check this to enable the lightbox.','framework'),
//                        "id" => "format_image_lightbox",
//                        "type" => "select",
//                        'std' => 'no',
//                        'options' => array('yes' => 'Yes', 'no' => 'No'),
//                ),
        array( "name" => __('Upload images', 'framework'),
                        "desc" => __('Select the images that should be upload to this gallery', 'framework'),
                        "id" => "gallery_image_ids",
                        "type" => 'gallery'
                ),
        array( "name" => __('Gallery height', 'framework'),
                        "desc" => __('Gallery height on single portfolio', 'framework'),
                        "id" => "format_image_height",
                        "type" => 'text'
                )
);
$tw_portfolio_video = array(
        array( "name" => __('M4V File URL','framework'),
                        "desc" => __('The URL to the .m4v video file','framework'),
                        "id" => "format_video_m4v",
                        "type" => "text",
                        'std' => ''
                ),
        array( "name" => __('Video Thumbnail Image','framework'),
                        "desc" => __('The preivew image.','framework'),
                        "id" => "format_video_thumb",
                        "type" => "text",
                        'std' => ''
                ),
        array( "name" => __('Embeded Code','framework'),
                        "desc" => __('If you\'re not using self hosted video then you can include embeded code here.','framework'),
                        "id" => "format_video_embed",
                        "type" => "textarea",
                        'std' => ''
        )	
);


/* * *********************** */
/* Testimonial options */
/* * *********************** */
$tw_testimonial_settings = Array(
    Array(
        'name' => __('Name', 'themewaves'),
        'desc' => __('', 'themewaves'),
        'id' => 'name',
        'type' => 'text'),
    Array(
        'name' => __('Company name', 'themewaves'),
        'desc' => __('', 'themewaves'),
        'id' => 'company',
        'type' => 'text'),
    Array(
        'name' => __('Link to url', 'themewaves'),
        'desc' => __('', 'themewaves'),
        'id' => 'url',
        'type' => 'text'),
);


/* * *********************** */
/* Team options */
/* * *********************** */
$tw_team_settings = Array(
    Array(
        'name' => __('Position', 'themewaves'),
        'desc' => __('Member position', 'themewaves'),
        'id' => 'position',
        'type' => 'text'),
    Array(
        'name' => __('Facebook url', 'themewaves'),
        'desc' => __('Facebook url', 'themewaves'),
        'id' => 'facebook',
        'type' => 'text'),
    Array(
        'name' => __('Google Plus url', 'themewaves'),
        'desc' => __('Google Plus url', 'themewaves'),
        'id' => 'google',
        'type' => 'text'),
    Array(
        'name' => __('Twitter url', 'themewaves'),
        'desc' => __('Twitter url', 'themewaves'),
        'id' => 'twitter',
        'type' => 'text'),
    Array(
        'name' => __('Linkedin url', 'themewaves'),
        'desc' => __('Linkedin url', 'themewaves'),
        'id' => 'linkedin',
        'type' => 'text'),
    Array(
        'name' => __('Dribbble url', 'themewaves'),
        'desc' => __('Dribbble url', 'themewaves'),
        'id' => 'dribbble',
        'type' => 'text'),
);


/* * *********************** */
/* Partner options */
/* * *********************** */
$tw_partner_settings = Array(
    Array(
        'name' => __('Partner Link to URL', 'themewaves'),
        'desc' => __('', 'themewaves'),
        'id' => 'link',
        'type' => 'text'),
);


/* * *********************** */
/* Pricing table options */
/* * *********************** */
$tw_price_settings = Array(
    Array(
        'name' => __('Type', 'themewaves'),
        'desc' => __('', 'themewaves'),
        'id' => 'type',
        'options' => array('general' => 'General', 'featured' => 'Featured'),
        'type' => 'select'),
    Array(
        'name' => __('SubTitle', 'themewaves'),
        'desc' => __('This will displayed bottom of your Pricing Table title.', 'themewaves'),
        'id' => 'subtitle',
        'type' => 'text'),
    Array(
        'name' => __('Price', 'themewaves'),
        'desc' => __('Please insert with $ symbol.', 'themewaves'),
        'id' => 'price',
        'type' => 'text'),
    Array(
        'name' => __('Price Time', 'themewaves'),
        'desc' => __('Price time option. Example: Month, Day, Year etc.', 'themewaves'),        
        'id' => 'time',
        'std' => __("Month", "themewaves"),
        'type' => 'text'),
    Array(
        'name' => __('Button text', 'themewaves'),
        'desc' => __('', 'themewaves'),
        'id' => 'buttontext',
        'type' => 'text'),
    Array(
        'name' => __('Button URL', 'themewaves'),
        'desc' => __('', 'themewaves'),
        'id' => 'buttonlink',
        'type' => 'text'),
);


add_action('admin_init', 'post_settings_custom_box', 1);
if (!function_exists('post_settings_custom_box')) {
    function post_settings_custom_box() {
        global $tw_post_settings;
        add_meta_box('post_meta_settings',__( 'Post settings', 'themewaves' ),'metabox_render','post','normal','core',$tw_post_settings);
    }
}

add_action('admin_init', 'page_settings_custom_box', 1);
if (!function_exists('page_settings_custom_box')) {
    function page_settings_custom_box() {
        global $tw_page_settings;
        add_meta_box('page_meta_settings',__( 'Page settings', 'themewaves' ),'metabox_render','page','normal','core',$tw_page_settings);
    }
}

add_action('admin_init', 'portfolio_settings_custom_box');
if (!function_exists('portfolio_settings_custom_box')) {
    function portfolio_settings_custom_box() {
        global $tw_portfolio_settings,$tw_portfolio_gallery,$tw_portfolio_video;
        add_meta_box('portfolio_meta_settings',__( 'Portfolio settings', 'themewaves' ),'metabox_render','portfolio','normal','high',$tw_portfolio_settings);
        add_meta_box('portfolio_gallery_settings',__( 'Portfolio gallery settings', 'themewaves' ),'metabox_render','portfolio','normal','high',$tw_portfolio_gallery);
        add_meta_box('portfolio_video_settings',__( 'Portfolio video settings', 'themewaves' ),'metabox_render','portfolio','normal','high',$tw_portfolio_video);
    }
}

add_action('admin_init', 'testimonial_settings_custom_box');
if (!function_exists('testimonial_settings_custom_box')) {
    function testimonial_settings_custom_box() {
        global $tw_testimonial_settings;
        add_meta_box('testimonial_meta_settings',__( 'Testimonial settings', 'themewaves' ),'metabox_render','testimonial','normal','high',$tw_testimonial_settings);
    }
}

add_action('admin_init', 'team_settings_custom_box');
if (!function_exists('team_settings_custom_box')) {
    function team_settings_custom_box() {
        global $tw_team_settings;
        add_meta_box('team_meta_settings',__( 'Team settings', 'themewaves' ),'metabox_render','team','normal','high',$tw_team_settings);
    }
}

add_action('admin_init', 'partner_settings_custom_box');
if (!function_exists('partner_settings_custom_box')) {
    function partner_settings_custom_box() {
        global $tw_partner_settings;
        add_meta_box('partner_meta_settings',__( 'Partner settings', 'themewaves' ),'metabox_render','partner','normal','high',$tw_partner_settings);
    }
}

add_action('admin_init', 'price_settings_custom_box');
if (!function_exists('price_settings_custom_box')) {
    function price_settings_custom_box() {
        global $tw_price_settings;
        add_meta_box('price_meta_settings',__( 'Price settings', 'themewaves' ),'metabox_render','price','normal','high',$tw_price_settings);
    }
} ?>