<?php
if (!function_exists('rowStart')) {
    function rowStart($colCounter,$size){
        if($colCounter===0||$colCounter===12||$colCounter+$size>12 ){return array($size,'true');}
        return array($colCounter+$size,'false');
    }
}
if (!function_exists('pbGetContentBuilderItem')) {
    function pbGetContentBuilderItem($item_array) {
        global $tw_pbItems,$tw_layoutSize;
        ob_start();
        $itemSlug = $item_array['slug'];
        $itemSettingsArray = $item_array['settings'];
        $defaultItem=$tw_pbItems[$itemSlug];
        $defaultItemSettingsArray=$defaultItem['settings'];
        $itemClass = !empty($item_array['item_custom_class']) ? $item_array['item_custom_class'] : '';
        if($item_array['size']!=='shortcode-size'){
            echo '[tw_item size="'.  pbTextToFoundation($item_array['size']).'" class="'.$itemClass.'" layout_size="'.pbTextToFoundation($tw_layoutSize).'" row_type="'.(isset($item_array['row-type'])?$item_array['row-type']:'row-fluid').'"]';
        }
            if(!empty($item_array['item_title'])){
                echo'[tw_item_title title="'.$item_array['item_title'].'"]';
            }
            $content_slug=  empty($defaultItem['content'])?'':$defaultItem['content'];
            echo '[tw_'.$itemSlug;
                if($itemSlug==='portfolio'){echo ' layout_size="'.pbTextToFoundation($tw_layoutSize).'"';}
                foreach($defaultItemSettingsArray as $settings_slug=>$default_settings_array){
                    if($content_slug!==$settings_slug&&$default_settings_array['type']!='category'&&$default_settings_array['type']!='button'&&$default_settings_array['type']!='fa'){
                        $settings_val=isset($itemSettingsArray[$settings_slug])?$itemSettingsArray[$settings_slug]:(isset($default_settings_array['default'])?$default_settings_array['default']:'');
                        echo ' '.$settings_slug.'="'.rawUrlDecode($settings_val).'"';
                    }
                }
            echo ']';
            if($content_slug){
                $settings_val='';
                if($defaultItemSettingsArray[$content_slug]['type']==='container'&&isset($defaultItemSettingsArray[$content_slug]['default'][0])){
                    $defaultContainarItem=$defaultItemSettingsArray[$content_slug]['default'][0];
                    $containarItemArray =$itemSettingsArray[$content_slug];
                    foreach($containarItemArray as $index=>$containarItem){
                        $containarItemContent='';
                        $settings_val .= '[tw_'.$itemSlug.'_item';
                        foreach($containarItem as $slug=>$value){
                            if($defaultContainarItem[$slug]['type']!='category'&&$defaultContainarItem[$slug]['type']!='button'&&$defaultContainarItem[$slug]['type']!='fa'){
                                if($defaultContainarItem[$slug]['type']==='textArea'){
                                    $containarItemContent=rawUrlDecode($value);
                                }else{
                                    $settings_val .= ' '.$slug.'="'.rawUrlDecode($value).'"';
                                }
                            }
                        }
                        $settings_val .= ']';
                        if(!empty($containarItemContent)){
                            $settings_val .= $containarItemContent.'[/tw_'.$itemSlug.'_item]';
                        }
                    }
                }else{
                    $settings_val=isset($itemSettingsArray[$content_slug])?$itemSettingsArray[$content_slug]:$defaultItemSettingsArray[$content_slug]['default'];
                    $settings_val=rawUrlDecode($settings_val);
                }
                echo $settings_val.'[/tw_'.$itemSlug.']';
            }
        if($item_array['size']!=='shortcode-size'){ echo '[/tw_item]';}
        $output = ob_get_clean();
        return $output;
    }
}
if (!function_exists('pbGetContentBuilder')) {
    function pbGetContentBuilder() {
        global $post, $tw_startPrinted,$tw_pbItems;
        $endPrint=false;
        ob_start();
        $_pb_layout_array=json_decode(rawUrlDecode(get_post_meta($post->ID, '_pb_content', true)),true);
        if(empty($_pb_layout_array)){
            return false;
            echo $post->post_content;
        }else{
            $layoutsEcho='';
            $layoutColCounter=0;
            $layoutStart='true';
            foreach($_pb_layout_array as $_pb_layout){
                $tw_startPrinted = false;
                if($_pb_layout['size']!=='size-0-0'){
                    global $tw_layoutSize;
                    $tw_layoutSize = $_pb_layout['size'];
                    list($layoutColCounter,$layoutStart)=rowStart($layoutColCounter,$tw_layoutSize);
                    $layoutsEcho .= '[tw_layout size="'.pbTextToFoundation($_pb_layout['size']).'" start="'.$layoutStart.'"]';
                    $tw_startPrinted=false;    
                    $colCounter=0;
                    $start='true';
                    foreach ($_pb_layout['items'] as $item_array){
                        list($colCounter,$start)=rowStart($colCounter,pbTextToFoundation($_pb_layout['size'])==='span3'?12:pbTextToInt($item_array['size']));
                        $endPrint=true;
                        $rowClass = $item_array['row-type'] = !empty($tw_pbItems[$item_array['slug']]['row-type'])?$tw_pbItems[$item_array['slug']]['row-type']:'row-fluid';
                        if($start === "true") {
                            if($tw_startPrinted){
                                $layoutsEcho .= '</div><div class="'.$rowClass.'">';
                            }else{
                                $tw_startPrinted=true;
                                $layoutsEcho .= '<div class="'.$rowClass.'">';
                            }
                        }                        
                        $layoutsEcho .= pbGetContentBuilderItem($item_array);
                    }
                    //row Close
                    if($tw_startPrinted){$layoutsEcho.="</div>";}
                    $layoutsEcho .= '[/tw_layout]';
                    
                }
            }
            if($endPrint){
                echo $layoutsEcho;
            }else{
                return false;
                echo $post->post_content;
            }
        }
        $output = ob_get_clean();
        return $output;
    }
} ?>