<div class="fontawesome-ajax-data">
    <style>
        .fontawesome-field-container {margin: 0;font-family: proxima-nova,"Helvetica Neue",Helvetica,Arial,sans-serif;font-size: 14px;line-height: 20px;color: #333;background-color: #fff;}
        .fontawesome-field-container .container {margin-right: auto;margin-left: auto;}
        .fontawesome-field-container .container::before,.fontawesome-field-container .container::after {display: table;content: "";line-height: 0;}
        .fontawesome-field-container .container::after {clear: both;}
        .fontawesome-field-container h1,.fontawesome-field-container  h2,.fontawesome-field-container  h3,.fontawesome-field-container  h4,.fontawesome-field-container  h5,.fontawesome-field-container  h6 {font-family: museo-slab,"Helvetica Neue",Helvetica,Arial,sans-serif;}
        .fontawesome-field-container section {padding-top: 40px;}
        .fontawesome-field-container ul.unstyled,.fontawesome-field-container  ol.unstyled {margin-left: 0;list-style: none;}
        .fontawesome-field-container li {color: #555; line-height: 22px; margin-left: 15px; margin-right: 14px; font-size: 14px; display: inline-block;}
        .fontawesome-field-container li span {display: none;}
        .fontawesome-field-container .muted {color: #999;}
        .fontawesome-field-container .fa-viewer{margin: 20px auto; text-align: center;}
        .fontawesome-field-container .unstyled li:hover{background-color: #fbf4f4;}
        .fontawesome-field-container .unstyled li:hover i{-webkit-transform: rotate(0deg) scale(2.5);-o-transform: rotate(0deg) scale(2.5);-moz-transform: rotate(0deg) scale(2.5);-ms-transform: rotate(0deg) scale(2.5);transform: rotate(0deg) scale(2.5);}
        [class^="icon-"], [class*=" icon-"]{display: inline-block;}
    </style>
    <div class="general-field-container">
        <div class="field-item type-text">
            <div class="field-title">Font Size</div>
            <div class="field-data">
                <input data-name="fa_size" data-type="text" class="field" value="" placeholder="" data-selector="" data-save-to="" type="text" />
            </div>
            <div class="field-desc">Size.</div>
        </div>
        <div class="field-item type-text">
            <div class="field-title">Font Padding</div>
            <div class="field-data">
                <input data-name="fa_padding" data-type="text" class="field" value="" placeholder="" data-selector="" data-save-to="" type="text" />
            </div>
            <div class="field-desc">Padding.</div>
        </div>
        <div class="field-item type-color">
            <div class="field-title">Font Color</div>
            <div class="field-data">
                <input data-name="fa_color" data-type="color" class="field" value="" placeholder="" type="text" />
                <div class="color-info"></div>
                <div class="color-view"></div>
            </div>
            <div class="field-desc">Color.</div>
        </div>
        <div class="field-item type-color">
            <div class="field-title">Border Color</div>
            <div class="field-data">
                <input data-name="fa_bg_color" data-type="color" class="field" value="" placeholder="" type="text" />
                <div class="color-info"></div>
                <div class="color-view"></div>
            </div>
            <div class="field-desc">Background Color.</div>
        </div>
        <div class="field-item type-text">
            <div class="field-title">Border width Size</div>
            <div class="field-data">
                <input data-name="fa_rounded" data-type="text" class="field" value="" placeholder="" data-selector="" data-save-to="" type="text" />
            </div>
            <div class="field-desc">Border width Size.</div>
        </div>
        <div class="field-item hidden type-hidden">
            <div class="field-title">Icon</div>
            <div class="field-data">
                <input data-name="fa_icon" data-type="hidden" class="field" value="" placeholder="" data-selector="" data-save-to="" type="hidden" />
            </div>
            <div class="field-desc">Icon.</div>
        </div>
    </div>
    <div class="fontawesome-field-container">
        <div class="container">
            <div class="fa-viewer"></div>
            <ul class="unstyled">
                <li><i class="icon-">&#xf000;</i><span> icon-glass </span><span class="muted">(&amp;#xf000;)</span></li>
                <li><i class="icon-">&#xf001;</i><span> icon-music </span><span class="muted">(&amp;#xf001;)</span></li>
                <li><i class="icon-">&#xf002;</i><span> icon-search </span><span class="muted">(&amp;#xf002;)</span></li>
                <li><i class="icon-">&#xf003;</i><span> icon-envelope </span><span class="muted">(&amp;#xf003;)</span></li>
                <li><i class="icon-">&#xf004;</i><span> icon-heart </span><span class="muted">(&amp;#xf004;)</span></li>
                <li><i class="icon-">&#xf005;</i><span> icon-star </span><span class="muted">(&amp;#xf005;)</span></li>
                <li><i class="icon-">&#xf006;</i><span> icon-star-empty </span><span class="muted">(&amp;#xf006;)</span></li>
                <li><i class="icon-">&#xf007;</i><span> icon-user </span><span class="muted">(&amp;#xf007;)</span></li>
                <li><i class="icon-">&#xf008;</i><span> icon-film </span><span class="muted">(&amp;#xf008;)</span></li>
                <li><i class="icon-">&#xf009;</i><span> icon-th-large </span><span class="muted">(&amp;#xf009;)</span></li>
                <li><i class="icon-">&#xf00a;</i><span> icon-th </span><span class="muted">(&amp;#xf00a;)</span></li>
                <li><i class="icon-">&#xf00b;</i><span> icon-th-list </span><span class="muted">(&amp;#xf00b;)</span></li>
                <li><i class="icon-">&#xf00c;</i><span> icon-ok </span><span class="muted">(&amp;#xf00c;)</span></li>
                <li><i class="icon-">&#xf00d;</i><span> icon-remove </span><span class="muted">(&amp;#xf00d;)</span></li>
                <li><i class="icon-">&#xf00e;</i><span> icon-zoom-in </span><span class="muted">(&amp;#xf00e;)</span></li>
                <li><i class="icon-">&#xf010;</i><span> icon-zoom-out </span><span class="muted">(&amp;#xf010;)</span></li>
                <li><i class="icon-">&#xf011;</i><span> icon-off </span><span class="muted">(&amp;#xf011;)</span></li>
                <li><i class="icon-">&#xf012;</i><span> icon-signal </span><span class="muted">(&amp;#xf012;)</span></li>
                <li><i class="icon-">&#xf013;</i><span> icon-cog </span><span class="muted">(&amp;#xf013;)</span></li>
                <li><i class="icon-">&#xf014;</i><span> icon-trash </span><span class="muted">(&amp;#xf014;)</span></li>
                <li><i class="icon-">&#xf015;</i><span> icon-home </span><span class="muted">(&amp;#xf015;)</span></li>
                <li><i class="icon-">&#xf016;</i><span> icon-file </span><span class="muted">(&amp;#xf016;)</span></li>
                <li><i class="icon-">&#xf017;</i><span> icon-time </span><span class="muted">(&amp;#xf017;)</span></li>
                <li><i class="icon-">&#xf018;</i><span> icon-road </span><span class="muted">(&amp;#xf018;)</span></li>
                <li><i class="icon-">&#xf019;</i><span> icon-download-alt </span><span class="muted">(&amp;#xf019;)</span></li>
                <li><i class="icon-">&#xf01a;</i><span> icon-download </span><span class="muted">(&amp;#xf01a;)</span></li>
                <li><i class="icon-">&#xf01b;</i><span> icon-upload </span><span class="muted">(&amp;#xf01b;)</span></li>
                <li><i class="icon-">&#xf01c;</i><span> icon-inbox </span><span class="muted">(&amp;#xf01c;)</span></li>
                <li><i class="icon-">&#xf01d;</i><span> icon-play-circle </span><span class="muted">(&amp;#xf01d;)</span></li>
                <li><i class="icon-">&#xf01e;</i><span> icon-repeat </span><span class="muted">(&amp;#xf01e;)</span></li>
                <li><i class="icon-">&#xf021;</i><span> icon-refresh </span><span class="muted">(&amp;#xf021;)</span></li>
                <li><i class="icon-">&#xf022;</i><span> icon-list-alt </span><span class="muted">(&amp;#xf022;)</span></li>
                <li><i class="icon-">&#xf023;</i><span> icon-lock </span><span class="muted">(&amp;#xf023;)</span></li>
                <li><i class="icon-">&#xf024;</i><span> icon-flag </span><span class="muted">(&amp;#xf024;)</span></li>
                <li><i class="icon-">&#xf025;</i><span> icon-headphones </span><span class="muted">(&amp;#xf025;)</span></li>
                <li><i class="icon-">&#xf026;</i><span> icon-volume-off </span><span class="muted">(&amp;#xf026;)</span></li>
                <li><i class="icon-">&#xf027;</i><span> icon-volume-down </span><span class="muted">(&amp;#xf027;)</span></li>
                <li><i class="icon-">&#xf028;</i><span> icon-volume-up </span><span class="muted">(&amp;#xf028;)</span></li>
                <li><i class="icon-">&#xf029;</i><span> icon-qrcode </span><span class="muted">(&amp;#xf029;)</span></li>
                <li><i class="icon-">&#xf02a;</i><span> icon-barcode </span><span class="muted">(&amp;#xf02a;)</span></li>
                <li><i class="icon-">&#xf02b;</i><span> icon-tag </span><span class="muted">(&amp;#xf02b;)</span></li>
                <li><i class="icon-">&#xf02c;</i><span> icon-tags </span><span class="muted">(&amp;#xf02c;)</span></li>
                <li><i class="icon-">&#xf02d;</i><span> icon-book </span><span class="muted">(&amp;#xf02d;)</span></li>
                <li><i class="icon-">&#xf02e;</i><span> icon-bookmark </span><span class="muted">(&amp;#xf02e;)</span></li>
                <li><i class="icon-">&#xf02f;</i><span> icon-print </span><span class="muted">(&amp;#xf02f;)</span></li>
                <li><i class="icon-">&#xf030;</i><span> icon-camera </span><span class="muted">(&amp;#xf030;)</span></li>
                <li><i class="icon-">&#xf031;</i><span> icon-font </span><span class="muted">(&amp;#xf031;)</span></li>
                <li><i class="icon-">&#xf032;</i><span> icon-bold </span><span class="muted">(&amp;#xf032;)</span></li>
                <li><i class="icon-">&#xf033;</i><span> icon-italic </span><span class="muted">(&amp;#xf033;)</span></li>
                <li><i class="icon-">&#xf034;</i><span> icon-text-height </span><span class="muted">(&amp;#xf034;)</span></li>
                <li><i class="icon-">&#xf035;</i><span> icon-text-width </span><span class="muted">(&amp;#xf035;)</span></li>
                <li><i class="icon-">&#xf036;</i><span> icon-align-left </span><span class="muted">(&amp;#xf036;)</span></li>
                <li><i class="icon-">&#xf037;</i><span> icon-align-center </span><span class="muted">(&amp;#xf037;)</span></li>
                <li><i class="icon-">&#xf038;</i><span> icon-align-right </span><span class="muted">(&amp;#xf038;)</span></li>
                <li><i class="icon-">&#xf039;</i><span> icon-align-justify </span><span class="muted">(&amp;#xf039;)</span></li>
                <li><i class="icon-">&#xf03a;</i><span> icon-list </span><span class="muted">(&amp;#xf03a;)</span></li>
                <li><i class="icon-">&#xf03b;</i><span> icon-indent-left </span><span class="muted">(&amp;#xf03b;)</span></li>
                <li><i class="icon-">&#xf03c;</i><span> icon-indent-right </span><span class="muted">(&amp;#xf03c;)</span></li>
                <li><i class="icon-">&#xf03d;</i><span> icon-facetime-video </span><span class="muted">(&amp;#xf03d;)</span></li>
                <li><i class="icon-">&#xf03e;</i><span> icon-picture </span><span class="muted">(&amp;#xf03e;)</span></li>
                <li><i class="icon-">&#xf040;</i><span> icon-pencil </span><span class="muted">(&amp;#xf040;)</span></li>
                <li><i class="icon-">&#xf041;</i><span> icon-map-marker </span><span class="muted">(&amp;#xf041;)</span></li>
                <li><i class="icon-">&#xf042;</i><span> icon-adjust </span><span class="muted">(&amp;#xf042;)</span></li>
                <li><i class="icon-">&#xf043;</i><span> icon-tint </span><span class="muted">(&amp;#xf043;)</span></li>
                <li><i class="icon-">&#xf044;</i><span> icon-edit </span><span class="muted">(&amp;#xf044;)</span></li>
                <li><i class="icon-">&#xf045;</i><span> icon-share </span><span class="muted">(&amp;#xf045;)</span></li>
                <li><i class="icon-">&#xf046;</i><span> icon-check </span><span class="muted">(&amp;#xf046;)</span></li>
                <li><i class="icon-">&#xf047;</i><span> icon-move </span><span class="muted">(&amp;#xf047;)</span></li>
                <li><i class="icon-">&#xf048;</i><span> icon-step-backward </span><span class="muted">(&amp;#xf048;)</span></li>
                <li><i class="icon-">&#xf049;</i><span> icon-fast-backward </span><span class="muted">(&amp;#xf049;)</span></li>
                <li><i class="icon-">&#xf04a;</i><span> icon-backward </span><span class="muted">(&amp;#xf04a;)</span></li>
                <li><i class="icon-">&#xf04b;</i><span> icon-play </span><span class="muted">(&amp;#xf04b;)</span></li>
                <li><i class="icon-">&#xf04c;</i><span> icon-pause </span><span class="muted">(&amp;#xf04c;)</span></li>
                <li><i class="icon-">&#xf04d;</i><span> icon-stop </span><span class="muted">(&amp;#xf04d;)</span></li>
                <li><i class="icon-">&#xf04e;</i><span> icon-forward </span><span class="muted">(&amp;#xf04e;)</span></li>
                <li><i class="icon-">&#xf050;</i><span> icon-fast-forward </span><span class="muted">(&amp;#xf050;)</span></li>
                <li><i class="icon-">&#xf051;</i><span> icon-step-forward </span><span class="muted">(&amp;#xf051;)</span></li>
                <li><i class="icon-">&#xf052;</i><span> icon-eject </span><span class="muted">(&amp;#xf052;)</span></li>
                <li><i class="icon-">&#xf053;</i><span> icon-chevron-left </span><span class="muted">(&amp;#xf053;)</span></li>
                <li><i class="icon-">&#xf054;</i><span> icon-chevron-right </span><span class="muted">(&amp;#xf054;)</span></li>
                <li><i class="icon-">&#xf055;</i><span> icon-plus-sign </span><span class="muted">(&amp;#xf055;)</span></li>
                <li><i class="icon-">&#xf056;</i><span> icon-minus-sign </span><span class="muted">(&amp;#xf056;)</span></li>
                <li><i class="icon-">&#xf057;</i><span> icon-remove-sign </span><span class="muted">(&amp;#xf057;)</span></li>
                <li><i class="icon-">&#xf058;</i><span> icon-ok-sign </span><span class="muted">(&amp;#xf058;)</span></li>
                <li><i class="icon-">&#xf059;</i><span> icon-question-sign </span><span class="muted">(&amp;#xf059;)</span></li>
                <li><i class="icon-">&#xf05a;</i><span> icon-info-sign </span><span class="muted">(&amp;#xf05a;)</span></li>
                <li><i class="icon-">&#xf05b;</i><span> icon-screenshot </span><span class="muted">(&amp;#xf05b;)</span></li>
                <li><i class="icon-">&#xf05c;</i><span> icon-remove-circle </span><span class="muted">(&amp;#xf05c;)</span></li>
                <li><i class="icon-">&#xf05d;</i><span> icon-ok-circle </span><span class="muted">(&amp;#xf05d;)</span></li>
                <li><i class="icon-">&#xf05e;</i><span> icon-ban-circle </span><span class="muted">(&amp;#xf05e;)</span></li>
                <li><i class="icon-">&#xf060;</i><span> icon-arrow-left </span><span class="muted">(&amp;#xf060;)</span></li>
                <li><i class="icon-">&#xf061;</i><span> icon-arrow-right </span><span class="muted">(&amp;#xf061;)</span></li>
                <li><i class="icon-">&#xf062;</i><span> icon-arrow-up </span><span class="muted">(&amp;#xf062;)</span></li>
                <li><i class="icon-">&#xf063;</i><span> icon-arrow-down </span><span class="muted">(&amp;#xf063;)</span></li>
                <li><i class="icon-">&#xf064;</i><span> icon-share-alt </span><span class="muted">(&amp;#xf064;)</span></li>
                <li><i class="icon-">&#xf065;</i><span> icon-resize-full </span><span class="muted">(&amp;#xf065;)</span></li>
                <li><i class="icon-">&#xf066;</i><span> icon-resize-small </span><span class="muted">(&amp;#xf066;)</span></li>
                <li><i class="icon-">&#xf067;</i><span> icon-plus </span><span class="muted">(&amp;#xf067;)</span></li>
                <li><i class="icon-">&#xf068;</i><span> icon-minus </span><span class="muted">(&amp;#xf068;)</span></li>
                <li><i class="icon-">&#xf069;</i><span> icon-asterisk </span><span class="muted">(&amp;#xf069;)</span></li>
                <li><i class="icon-">&#xf06a;</i><span> icon-exclamation-sign </span><span class="muted">(&amp;#xf06a;)</span></li>
                <li><i class="icon-">&#xf06b;</i><span> icon-gift </span><span class="muted">(&amp;#xf06b;)</span></li>
                <li><i class="icon-">&#xf06c;</i><span> icon-leaf </span><span class="muted">(&amp;#xf06c;)</span></li>
                <li><i class="icon-">&#xf06d;</i><span> icon-fire </span><span class="muted">(&amp;#xf06d;)</span></li>
                <li><i class="icon-">&#xf06e;</i><span> icon-eye-open </span><span class="muted">(&amp;#xf06e;)</span></li>
                <li><i class="icon-">&#xf070;</i><span> icon-eye-close </span><span class="muted">(&amp;#xf070;)</span></li>
                <li><i class="icon-">&#xf071;</i><span> icon-warning-sign </span><span class="muted">(&amp;#xf071;)</span></li>
                <li><i class="icon-">&#xf072;</i><span> icon-plane </span><span class="muted">(&amp;#xf072;)</span></li>
                <li><i class="icon-">&#xf073;</i><span> icon-calendar </span><span class="muted">(&amp;#xf073;)</span></li>
                <li><i class="icon-">&#xf074;</i><span> icon-random </span><span class="muted">(&amp;#xf074;)</span></li>
                <li><i class="icon-">&#xf075;</i><span> icon-comment </span><span class="muted">(&amp;#xf075;)</span></li>
                <li><i class="icon-">&#xf076;</i><span> icon-magnet </span><span class="muted">(&amp;#xf076;)</span></li>
                <li><i class="icon-">&#xf077;</i><span> icon-chevron-up </span><span class="muted">(&amp;#xf077;)</span></li>
                <li><i class="icon-">&#xf078;</i><span> icon-chevron-down </span><span class="muted">(&amp;#xf078;)</span></li>
                <li><i class="icon-">&#xf079;</i><span> icon-retweet </span><span class="muted">(&amp;#xf079;)</span></li>
                <li><i class="icon-">&#xf07a;</i><span> icon-shopping-cart </span><span class="muted">(&amp;#xf07a;)</span></li>
                <li><i class="icon-">&#xf07b;</i><span> icon-folder-close </span><span class="muted">(&amp;#xf07b;)</span></li>
                <li><i class="icon-">&#xf07c;</i><span> icon-folder-open </span><span class="muted">(&amp;#xf07c;)</span></li>
                <li><i class="icon-">&#xf07d;</i><span> icon-resize-vertical </span><span class="muted">(&amp;#xf07d;)</span></li>
                <li><i class="icon-">&#xf07e;</i><span> icon-resize-horizontal </span><span class="muted">(&amp;#xf07e;)</span></li>
                <li><i class="icon-">&#xf080;</i><span> icon-bar-chart </span><span class="muted">(&amp;#xf080;)</span></li>
                <li><i class="icon-">&#xf081;</i><span> icon-twitter-sign </span><span class="muted">(&amp;#xf081;)</span></li>
                <li><i class="icon-">&#xf082;</i><span> icon-facebook-sign </span><span class="muted">(&amp;#xf082;)</span></li>
                <li><i class="icon-">&#xf083;</i><span> icon-camera-retro </span><span class="muted">(&amp;#xf083;)</span></li>
                <li><i class="icon-">&#xf084;</i><span> icon-key </span><span class="muted">(&amp;#xf084;)</span></li>
                <li><i class="icon-">&#xf085;</i><span> icon-cogs </span><span class="muted">(&amp;#xf085;)</span></li>
                <li><i class="icon-">&#xf086;</i><span> icon-comments </span><span class="muted">(&amp;#xf086;)</span></li>
                <li><i class="icon-">&#xf087;</i><span> icon-thumbs-up </span><span class="muted">(&amp;#xf087;)</span></li>
                <li><i class="icon-">&#xf088;</i><span> icon-thumbs-down </span><span class="muted">(&amp;#xf088;)</span></li>
                <li><i class="icon-">&#xf089;</i><span> icon-star-half </span><span class="muted">(&amp;#xf089;)</span></li>
                <li><i class="icon-">&#xf08a;</i><span> icon-heart-empty </span><span class="muted">(&amp;#xf08a;)</span></li>
                <li><i class="icon-">&#xf08b;</i><span> icon-signout </span><span class="muted">(&amp;#xf08b;)</span></li>
                <li><i class="icon-">&#xf08c;</i><span> icon-linkedin-sign </span><span class="muted">(&amp;#xf08c;)</span></li>
                <li><i class="icon-">&#xf08d;</i><span> icon-pushpin </span><span class="muted">(&amp;#xf08d;)</span></li>
                <li><i class="icon-">&#xf08e;</i><span> icon-external-link </span><span class="muted">(&amp;#xf08e;)</span></li>
                <li><i class="icon-">&#xf090;</i><span> icon-signin </span><span class="muted">(&amp;#xf090;)</span></li>
                <li><i class="icon-">&#xf091;</i><span> icon-trophy </span><span class="muted">(&amp;#xf091;)</span></li>
                <li><i class="icon-">&#xf092;</i><span> icon-github-sign </span><span class="muted">(&amp;#xf092;)</span></li>
                <li><i class="icon-">&#xf093;</i><span> icon-upload-alt </span><span class="muted">(&amp;#xf093;)</span></li>
                <li><i class="icon-">&#xf094;</i><span> icon-lemon </span><span class="muted">(&amp;#xf094;)</span></li>
                <li><i class="icon-">&#xf095;</i><span> icon-phone </span><span class="muted">(&amp;#xf095;)</span></li>
                <li><i class="icon-">&#xf096;</i><span> icon-check-empty </span><span class="muted">(&amp;#xf096;)</span></li>
                <li><i class="icon-">&#xf097;</i><span> icon-bookmark-empty </span><span class="muted">(&amp;#xf097;)</span></li>
                <li><i class="icon-">&#xf098;</i><span> icon-phone-sign </span><span class="muted">(&amp;#xf098;)</span></li>
                <li><i class="icon-">&#xf099;</i><span> icon-twitter </span><span class="muted">(&amp;#xf099;)</span></li>
                <li><i class="icon-">&#xf09a;</i><span> icon-facebook </span><span class="muted">(&amp;#xf09a;)</span></li>
                <li><i class="icon-">&#xf09b;</i><span> icon-github </span><span class="muted">(&amp;#xf09b;)</span></li>
                <li><i class="icon-">&#xf09c;</i><span> icon-unlock </span><span class="muted">(&amp;#xf09c;)</span></li>
                <li><i class="icon-">&#xf09d;</i><span> icon-credit-card </span><span class="muted">(&amp;#xf09d;)</span></li>
                <li><i class="icon-">&#xf09e;</i><span> icon-rss </span><span class="muted">(&amp;#xf09e;)</span></li>
                <li><i class="icon-">&#xf0a0;</i><span> icon-hdd </span><span class="muted">(&amp;#xf0a0;)</span></li>
                <li><i class="icon-">&#xf0a1;</i><span> icon-bullhorn </span><span class="muted">(&amp;#xf0a1;)</span></li>
                <li><i class="icon-">&#xf0a2;</i><span> icon-bell </span><span class="muted">(&amp;#xf0a2;)</span></li>
                <li><i class="icon-">&#xf0a3;</i><span> icon-certificate </span><span class="muted">(&amp;#xf0a3;)</span></li>
                <li><i class="icon-">&#xf0a4;</i><span> icon-hand-right </span><span class="muted">(&amp;#xf0a4;)</span></li>
                <li><i class="icon-">&#xf0a5;</i><span> icon-hand-left </span><span class="muted">(&amp;#xf0a5;)</span></li>
                <li><i class="icon-">&#xf0a6;</i><span> icon-hand-up </span><span class="muted">(&amp;#xf0a6;)</span></li>
                <li><i class="icon-">&#xf0a7;</i><span> icon-hand-down </span><span class="muted">(&amp;#xf0a7;)</span></li>
                <li><i class="icon-">&#xf0a8;</i><span> icon-circle-arrow-left </span><span class="muted">(&amp;#xf0a8;)</span></li>
                <li><i class="icon-">&#xf0a9;</i><span> icon-circle-arrow-right </span><span class="muted">(&amp;#xf0a9;)</span></li>
                <li><i class="icon-">&#xf0aa;</i><span> icon-circle-arrow-up </span><span class="muted">(&amp;#xf0aa;)</span></li>
                <li><i class="icon-">&#xf0ab;</i><span> icon-circle-arrow-down </span><span class="muted">(&amp;#xf0ab;)</span></li>
                <li><i class="icon-">&#xf0ac;</i><span> icon-globe </span><span class="muted">(&amp;#xf0ac;)</span></li>
                <li><i class="icon-">&#xf0ad;</i><span> icon-wrench </span><span class="muted">(&amp;#xf0ad;)</span></li>
                <li><i class="icon-">&#xf0ae;</i><span> icon-tasks </span><span class="muted">(&amp;#xf0ae;)</span></li>
                <li><i class="icon-">&#xf0b0;</i><span> icon-filter </span><span class="muted">(&amp;#xf0b0;)</span></li>
                <li><i class="icon-">&#xf0b1;</i><span> icon-briefcase </span><span class="muted">(&amp;#xf0b1;)</span></li>
                <li><i class="icon-">&#xf0b2;</i><span> icon-fullscreen </span><span class="muted">(&amp;#xf0b2;)</span></li>
                <li><i class="icon-">&#xf0c0;</i><span> icon-group </span><span class="muted">(&amp;#xf0c0;)</span></li>
                <li><i class="icon-">&#xf0c1;</i><span> icon-link </span><span class="muted">(&amp;#xf0c1;)</span></li>
                <li><i class="icon-">&#xf0c2;</i><span> icon-cloud </span><span class="muted">(&amp;#xf0c2;)</span></li>
                <li><i class="icon-">&#xf0c3;</i><span> icon-beaker </span><span class="muted">(&amp;#xf0c3;)</span></li>
                <li><i class="icon-">&#xf0c4;</i><span> icon-cut </span><span class="muted">(&amp;#xf0c4;)</span></li>
                <li><i class="icon-">&#xf0c5;</i><span> icon-copy </span><span class="muted">(&amp;#xf0c5;)</span></li>
                <li><i class="icon-">&#xf0c6;</i><span> icon-paper-clip </span><span class="muted">(&amp;#xf0c6;)</span></li>
                <li><i class="icon-">&#xf0c7;</i><span> icon-save </span><span class="muted">(&amp;#xf0c7;)</span></li>
                <li><i class="icon-">&#xf0c8;</i><span> icon-sign-blank </span><span class="muted">(&amp;#xf0c8;)</span></li>
                <li><i class="icon-">&#xf0c9;</i><span> icon-reorder </span><span class="muted">(&amp;#xf0c9;)</span></li>
                <li><i class="icon-">&#xf0ca;</i><span> icon-list-ul </span><span class="muted">(&amp;#xf0ca;)</span></li>
                <li><i class="icon-">&#xf0cb;</i><span> icon-list-ol </span><span class="muted">(&amp;#xf0cb;)</span></li>
                <li><i class="icon-">&#xf0cc;</i><span> icon-strikethrough </span><span class="muted">(&amp;#xf0cc;)</span></li>
                <li><i class="icon-">&#xf0cd;</i><span> icon-underline </span><span class="muted">(&amp;#xf0cd;)</span></li>
                <li><i class="icon-">&#xf0ce;</i><span> icon-table </span><span class="muted">(&amp;#xf0ce;)</span></li>
                <li><i class="icon-">&#xf0d0;</i><span> icon-magic </span><span class="muted">(&amp;#xf0d0;)</span></li>
                <li><i class="icon-">&#xf0d1;</i><span> icon-truck </span><span class="muted">(&amp;#xf0d1;)</span></li>
                <li><i class="icon-">&#xf0d2;</i><span> icon-pinterest </span><span class="muted">(&amp;#xf0d2;)</span></li>
                <li><i class="icon-">&#xf0d3;</i><span> icon-pinterest-sign </span><span class="muted">(&amp;#xf0d3;)</span></li>
                <li><i class="icon-">&#xf0d4;</i><span> icon-google-plus-sign </span><span class="muted">(&amp;#xf0d4;)</span></li>
                <li><i class="icon-">&#xf0d5;</i><span> icon-google-plus </span><span class="muted">(&amp;#xf0d5;)</span></li>
                <li><i class="icon-">&#xf0d6;</i><span> icon-money </span><span class="muted">(&amp;#xf0d6;)</span></li>
                <li><i class="icon-">&#xf0d7;</i><span> icon-caret-down </span><span class="muted">(&amp;#xf0d7;)</span></li>
                <li><i class="icon-">&#xf0d8;</i><span> icon-caret-up </span><span class="muted">(&amp;#xf0d8;)</span></li>
                <li><i class="icon-">&#xf0d9;</i><span> icon-caret-left </span><span class="muted">(&amp;#xf0d9;)</span></li>
                <li><i class="icon-">&#xf0da;</i><span> icon-caret-right </span><span class="muted">(&amp;#xf0da;)</span></li>
                <li><i class="icon-">&#xf0db;</i><span> icon-columns </span><span class="muted">(&amp;#xf0db;)</span></li>
                <li><i class="icon-">&#xf0dc;</i><span> icon-sort </span><span class="muted">(&amp;#xf0dc;)</span></li>
                <li><i class="icon-">&#xf0dd;</i><span> icon-sort-down </span><span class="muted">(&amp;#xf0dd;)</span></li>
                <li><i class="icon-">&#xf0de;</i><span> icon-sort-up </span><span class="muted">(&amp;#xf0de;)</span></li>
                <li><i class="icon-">&#xf0e0;</i><span> icon-envelope-alt </span><span class="muted">(&amp;#xf0e0;)</span></li>
                <li><i class="icon-">&#xf0e1;</i><span> icon-linkedin </span><span class="muted">(&amp;#xf0e1;)</span></li>
                <li><i class="icon-">&#xf0e2;</i><span> icon-undo </span><span class="muted">(&amp;#xf0e2;)</span></li>
                <li><i class="icon-">&#xf0e3;</i><span> icon-legal </span><span class="muted">(&amp;#xf0e3;)</span></li>
                <li><i class="icon-">&#xf0e4;</i><span> icon-dashboard </span><span class="muted">(&amp;#xf0e4;)</span></li>
                <li><i class="icon-">&#xf0e5;</i><span> icon-comment-alt </span><span class="muted">(&amp;#xf0e5;)</span></li>
                <li><i class="icon-">&#xf0e6;</i><span> icon-comments-alt </span><span class="muted">(&amp;#xf0e6;)</span></li>
                <li><i class="icon-">&#xf0e7;</i><span> icon-bolt </span><span class="muted">(&amp;#xf0e7;)</span></li>
                <li><i class="icon-">&#xf0e8;</i><span> icon-sitemap </span><span class="muted">(&amp;#xf0e8;)</span></li>
                <li><i class="icon-">&#xf0e9;</i><span> icon-umbrella </span><span class="muted">(&amp;#xf0e9;)</span></li>
                <li><i class="icon-">&#xf0ea;</i><span> icon-paste </span><span class="muted">(&amp;#xf0ea;)</span></li>
                <li><i class="icon-">&#xf0eb;</i><span> icon-lightbulb </span><span class="muted">(&amp;#xf0eb;)</span></li>
                <li><i class="icon-">&#xf0ec;</i><span> icon-exchange </span><span class="muted">(&amp;#xf0ec;)</span></li>
                <li><i class="icon-">&#xf0ed;</i><span> icon-cloud-download </span><span class="muted">(&amp;#xf0ed;)</span></li>
                <li><i class="icon-">&#xf0ee;</i><span> icon-cloud-upload </span><span class="muted">(&amp;#xf0ee;)</span></li>
                <li><i class="icon-">&#xf0f0;</i><span> icon-user-md </span><span class="muted">(&amp;#xf0f0;)</span></li>
                <li><i class="icon-">&#xf0f1;</i><span> icon-stethoscope </span><span class="muted">(&amp;#xf0f1;)</span></li>
                <li><i class="icon-">&#xf0f2;</i><span> icon-suitcase </span><span class="muted">(&amp;#xf0f2;)</span></li>
                <li><i class="icon-">&#xf0f3;</i><span> icon-bell-alt </span><span class="muted">(&amp;#xf0f3;)</span></li>
                <li><i class="icon-">&#xf0f4;</i><span> icon-coffee </span><span class="muted">(&amp;#xf0f4;)</span></li>
                <li><i class="icon-">&#xf0f5;</i><span> icon-food </span><span class="muted">(&amp;#xf0f5;)</span></li>
                <li><i class="icon-">&#xf0f6;</i><span> icon-file-alt </span><span class="muted">(&amp;#xf0f6;)</span></li>
                <li><i class="icon-">&#xf0f7;</i><span> icon-building </span><span class="muted">(&amp;#xf0f7;)</span></li>
                <li><i class="icon-">&#xf0f8;</i><span> icon-hospital </span><span class="muted">(&amp;#xf0f8;)</span></li>
                <li><i class="icon-">&#xf0f9;</i><span> icon-ambulance </span><span class="muted">(&amp;#xf0f9;)</span></li>
                <li><i class="icon-">&#xf0fa;</i><span> icon-medkit </span><span class="muted">(&amp;#xf0fa;)</span></li>
                <li><i class="icon-">&#xf0fb;</i><span> icon-fighter-jet </span><span class="muted">(&amp;#xf0fb;)</span></li>
                <li><i class="icon-">&#xf0fc;</i><span> icon-beer </span><span class="muted">(&amp;#xf0fc;)</span></li>
                <li><i class="icon-">&#xf0fd;</i><span> icon-h-sign </span><span class="muted">(&amp;#xf0fd;)</span></li>
                <li><i class="icon-">&#xf0fe;</i><span> icon-plus-sign-alt </span><span class="muted">(&amp;#xf0fe;)</span></li>
                <li><i class="icon-">&#xf100;</i><span> icon-double-angle-left </span><span class="muted">(&amp;#xf100;)</span></li>
                <li><i class="icon-">&#xf101;</i><span> icon-double-angle-right </span><span class="muted">(&amp;#xf101;)</span></li>
                <li><i class="icon-">&#xf102;</i><span> icon-double-angle-up </span><span class="muted">(&amp;#xf102;)</span></li>
                <li><i class="icon-">&#xf103;</i><span> icon-double-angle-down </span><span class="muted">(&amp;#xf103;)</span></li>
                <li><i class="icon-">&#xf104;</i><span> icon-angle-left </span><span class="muted">(&amp;#xf104;)</span></li>
                <li><i class="icon-">&#xf105;</i><span> icon-angle-right </span><span class="muted">(&amp;#xf105;)</span></li>
                <li><i class="icon-">&#xf106;</i><span> icon-angle-up </span><span class="muted">(&amp;#xf106;)</span></li>
                <li><i class="icon-">&#xf107;</i><span> icon-angle-down </span><span class="muted">(&amp;#xf107;)</span></li>
                <li><i class="icon-">&#xf108;</i><span> icon-desktop </span><span class="muted">(&amp;#xf108;)</span></li>
                <li><i class="icon-">&#xf109;</i><span> icon-laptop </span><span class="muted">(&amp;#xf109;)</span></li>
                <li><i class="icon-">&#xf10a;</i><span> icon-tablet </span><span class="muted">(&amp;#xf10a;)</span></li>
                <li><i class="icon-">&#xf10b;</i><span> icon-mobile-phone </span><span class="muted">(&amp;#xf10b;)</span></li>
                <li><i class="icon-">&#xf10c;</i><span> icon-circle-blank </span><span class="muted">(&amp;#xf10c;)</span></li>
                <li><i class="icon-">&#xf10d;</i><span> icon-quote-left </span><span class="muted">(&amp;#xf10d;)</span></li>
                <li><i class="icon-">&#xf10e;</i><span> icon-quote-right </span><span class="muted">(&amp;#xf10e;)</span></li>
                <li><i class="icon-">&#xf110;</i><span> icon-spinner </span><span class="muted">(&amp;#xf110;)</span></li>
                <li><i class="icon-">&#xf111;</i><span> icon-circle </span><span class="muted">(&amp;#xf111;)</span></li>
                <li><i class="icon-">&#xf112;</i><span> icon-reply </span><span class="muted">(&amp;#xf112;)</span></li>

                <li><i class="icon-expand-alt"></i><span> icon-expand-alt</span></li>
                <li><i class="icon-collapse-alt"></i><span> icon-collapse-alt</span></li>
                <li><i class="icon-smile"></i><span> icon-smile</span></li>
                <li><i class="icon-frown"></i><span> icon-frown</span></li>
                <li><i class="icon-meh"></i><span> icon-meh</span></li>
                <li><i class="icon-gamepad"></i><span> icon-gamepad</span></li>
                <li><i class="icon-keyboard"></i><span> icon-keyboard</span></li>
                <li><i class="icon-flag-alt"></i><span> icon-flag-alt</span></li>
                <li><i class="icon-flag-checkered"></i><span> icon-flag-checkered</span></li>
                <li><i class="icon-terminal"></i><span> icon-terminal</span></li>
                <li><i class="icon-code"></i><span> icon-code</span></li>
                <li><i class="icon-mail-forward"></i><span> icon-mail-forward</span> <span class="muted">(alias)</span></li>
                <li><i class="icon-mail-reply"></i><span> icon-mail-reply</span> <span class="muted">(alias)</span></li>
                <li><i class="icon-reply-all"></i><span> icon-reply-all</span></li>
                <li><i class="icon-mail-reply-all"></i><span> icon-mail-reply-all</span> <span class="muted">(alias)</span></li>
                <li><i class="icon-star-half-empty"></i><span> icon-star-half-empty</span></li>
                <li><i class="icon-star-half-full"></i><span> icon-star-half-full </span><span class="muted">(alias)</span></li>
                <li><i class="icon-location-arrow"></i><span> icon-location-arrow</span></li>
                <li><i class="icon-rotate-left"></i><span> icon-rotate-left </span><span class="muted">(alias)</span></li>
                <li><i class="icon-rotate-right"></i><span> icon-rotate-right </span><span class="muted">(alias)</span></li>
                <li><i class="icon-crop"></i><span> icon-crop</span></li>
                <li><i class="icon-code-fork"></i><span> icon-code-fork</span></li>
                <li><i class="icon-unlink"></i><span> icon-unlink</span></li>
                <li><i class="icon-question"></i><span> icon-question</span></li>
                <li><i class="icon-info"></i><span> icon-info</span></li>
                <li><i class="icon-exclamation"></i><span> icon-exclamation</span></li>
                <li><i class="icon-superscript"></i><span> icon-superscript</span></li>
                <li><i class="icon-subscript"></i><span> icon-subscript</span></li>
                <li><i class="icon-eraser"></i><span> icon-eraser</span></li>
                <li><i class="icon-puzzle-piece"></i><span> icon-puzzle-piece</span></li>
                <li><i class="icon-microphone"></i><span> icon-microphone</span></li>
                <li><i class="icon-microphone-off"></i><span> icon-microphone-off</span></li>
                <li><i class="icon-shield"></i><span> icon-shield</span></li>
                <li><i class="icon-calendar-empty"></i><span> icon-calendar-empty</span></li>
                <li><i class="icon-fire-extinguisher"></i><span> icon-fire-extinguisher</span></li>
                <li><i class="icon-rocket"></i><span> icon-rocket</span></li>
                <li><i class="icon-maxcdn"></i><span> icon-maxcdn</span></li>
                <li><i class="icon-chevron-sign-left"></i><span> icon-chevron-sign-left</span></li>
                <li><i class="icon-chevron-sign-right"></i><span> icon-chevron-sign-right</span></li>
                <li><i class="icon-chevron-sign-up"></i><span> icon-chevron-sign-up</span></li>
                <li><i class="icon-chevron-sign-down"></i><span> icon-chevron-sign-down</span></li>
                <li><i class="icon-html5"></i><span> icon-html5</span></li>
                <li><i class="icon-css3"></i><span> icon-css3</span></li>
                <li><i class="icon-anchor"></i><span> icon-anchor</span></li>
                <li><i class="icon-unlock-alt"></i><span> icon-unlock-alt</span></li>
                <li><i class="icon-bullseye"></i><span> icon-bullseye</span></li>
                <li><i class="icon-ellipsis-horizontal"></i><span> icon-ellipsis-horizontal</span></li>
                <li><i class="icon-ellipsis-vertical"></i><span> icon-ellipsis-vertical</span></li>
                <li><i class="icon-rss-sign"></i><span> icon-rss-sign</span></li>
                <li><i class="icon-play-sign"></i><span> icon-play-sign</span></li>
                <li><i class="icon-ticket"></i><span> icon-ticket</span></li>
                <li><i class="icon-minus-sign-alt"></i><span> icon-minus-sign-alt</span></li>
                <li><i class="icon-check-minus"></i><span> icon-check-minus</span></li>
                <li><i class="icon-level-up"></i><span> icon-level-up</span></li>
                <li><i class="icon-level-down"></i><span> icon-level-down</span></li>
                <li><i class="icon-check-sign"></i><span> icon-check-sign</span></li>
                <li><i class="icon-edit-sign"></i><span> icon-edit-sign</span></li>
                <li><i class="icon-external-link-sign"></i><span> icon-external-link-sign</span></li>
                <li><i class="icon-share-sign"></i><span> icon-share-sign</span></li>
            </ul>
        </div>
    </div>
</div>