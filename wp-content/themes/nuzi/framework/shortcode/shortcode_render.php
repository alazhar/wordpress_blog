<?php
/* ================================================================================== */
/*      Accordion Shortcode
/* ================================================================================== */

// Accordion container
if (!function_exists('shortcode_tw_accordion')) {
    function shortcode_tw_accordion($atts, $content) {
        $output = '<div class="tw-accordion">';
        $output .= do_shortcode($content);
        $output .= '</div>';
        return $output;
    }
}
add_shortcode('tw_accordion', 'shortcode_tw_accordion');
// Accordion Item
if (!function_exists('shortcode_tw_accordion_item')) {
    function shortcode_tw_accordion_item($atts, $content) {
        $expand=(!empty($atts['item_expand'])&&$atts['item_expand']=='true')?true:false;
        
        $output = '<div class="accordion-group">';
            $output .= '<div class="accordion-heading">';
                $output .= '<a class="accordion-toggle '.($expand?' active':'').'" data-toggle="collapse" data-parent="" href="#">';
                    $output .= $atts['item_title'];
                    $output .= '<span class="tw-check"><i class="icon-plus"></i><i class="icon-minus"></i></span>';
                $output .= '</a>';
            $output .= '</div>';
            $output .= '<div class="accordion-body collapse'.($expand?' in':'').'" >';
                $output .= '<div class="accordion-inner">';
                    $output .= apply_filters("the_content",$content);
                $output .= '</div>';
            $output .= '</div>';
        $output .= '</div>';
        
        
        return $output;
    }
}
add_shortcode('tw_accordion_item', 'shortcode_tw_accordion_item');




/* ================================================================================== */
/*      List Shortcode
/* ================================================================================== */

// List container
if (!function_exists('shortcode_tw_list')) {
    function shortcode_tw_list($atts, $content) {
        $output = '<ul class="tw-list">';
        $output .= do_shortcode($content);
        $output .= '</ul>';
        return $output;
    }
}
add_shortcode('tw_list', 'shortcode_tw_list');
// List Item
if (!function_exists('shortcode_tw_list_item')) {
    function shortcode_tw_list_item($atts, $content) {
        $output = '<li><i class="' . $atts['item_icon'] . '"></i>' . do_shortcode($content) . '</li>';
        return $output;
    }
}
add_shortcode('tw_list_item', 'shortcode_tw_list_item');





/* ================================================================================== */
/*      Toggle Shortcode
/* ================================================================================== */

// Toggle container
if (!function_exists('shortcode_tw_toggle')) {
    function shortcode_tw_toggle($atts, $content) {
        $output = '<div class="tw-toggle">';
        $output .= do_shortcode($content);
        $output .= '</div>';
        return $output;
    }
}
add_shortcode('tw_toggle', 'shortcode_tw_toggle');
// Toggle Item
if (!function_exists('shortcode_tw_toggle_item')) {
    function shortcode_tw_toggle_item($atts, $content) {
        $expand=(!empty($atts['item_expand'])&&$atts['item_expand']=='true')?true:false;
        
        $output = '<div class="accordion-group">';
            $output .= '<div class="accordion-heading '.($expand?' active':'').'">';
                $output .= '<a class="accordion-toggle toggle '.($expand?' active':'').'" data-toggle="collapse" href="#">';
                    $output .= $atts['item_title'];
                    $output .= '<span class="tw-check"><i class="icon-plus"></i><i class="icon-minus"></i></span>';
                $output .= '</a>';
            $output .= '</div>';
            $output .= '<div class="accordion-body collapse'.($expand?' in':'').'" >';
                $output .= '<div class="accordion-inner">';
                    $output .= apply_filters("the_content",$content);
                $output .= '</div>';
            $output .= '</div>';
        $output .= '</div>';
        
        
        return $output;
    }
}
add_shortcode('tw_toggle_item', 'shortcode_tw_toggle_item');





/* ================================================================================== */
/*      Tab Shortcode
/* ================================================================================== */

// Tab container
if (!function_exists('shortcode_tw_tab')) {
    function shortcode_tw_tab($atts, $content) {
        $position=(!empty($atts['position'])||$atts['position']!='top')?(' tabs-'.$atts['position']):'';
        $output = '<div class="tw-tab tabbable'.$position.'">';
        $output .= do_shortcode($content);
        $output .= '<ul class="nav nav-tabs"></ul>';        
        $output .= '<div class="tab-content"></div>';        
        $output .= '</div>';
        return $output;
    }
}
add_shortcode('tw_tab', 'shortcode_tw_tab');
// Tab Item
if (!function_exists('shortcode_tw_tab_item')) {
    function shortcode_tw_tab_item($atts, $content) {
        $output = '<li><a href=""><span>'.$atts['title'].'</span></a></li>';
        $output .= '<div class="tab-pane" id="">';
            $output .= apply_filters("the_content",$content);
        $output .= '</div>';      
        return $output;
    }
}
add_shortcode('tw_tab_item', 'shortcode_tw_tab_item');






/* ================================================================================== */
/*      Blog Shortcode
/* ================================================================================== */

if (!function_exists('shortcode_tw_blog')) {
    function shortcode_tw_blog($atts, $content) {
        global $paged,$tw_options;
        $output = '<div class="tw-blog">';
        $query = Array(
            'post_type' => 'post',
            'posts_per_page' => $atts['post_count'],
            'paged' => $paged,
        );
        $cats = empty($atts['category_ids']) ? false : explode(",", $atts['category_ids']);
        if ($cats) { 
            $query['tax_query'] = Array(Array(
                    'taxonomy' => 'category',
                    'terms' => $cats,
                    'field' => 'id'
                )
            );
        }
        $tw_options['show_pagination'] = $atts['pagination'] == 'true' ? true : false;
        $tw_options['excerpt_count'] = $atts['excerpt_count'];
        $tw_options['more_text'] = $atts['more_text'];
        query_posts($query);
        ob_start();
        get_template_part("loop");
        $output .= ob_get_clean();
        wp_reset_query();
        $output .= '</div>';
        return $output;
    }
}
add_shortcode('tw_blog', 'shortcode_tw_blog');





/* ================================================================================== */
/*      Column Shortcode
/* ================================================================================== */

if (!function_exists('shortcode_tw_column')) {
    function shortcode_tw_column($atts, $content) {
        $output = apply_filters("the_content",$content);
        return $output;
    }
}
add_shortcode('tw_column', 'shortcode_tw_column');





/* ================================================================================== */
/*      Item Shortcode Container
/* ================================================================================== */

if (!function_exists('shortcode_tw_item')) {
    function shortcode_tw_item($atts, $content) {
        if($atts['row_type']==='row'){
            $atts['size']=$atts['layout_size'];
        }else{
            if($atts['layout_size']==='span3'){
                $atts['size']='span12';
            }
        }
        $output='<div class="'.$atts['size'].' '.$atts['class'].'">'.do_shortcode($content).'</div>';
        return $output;
    }
}
add_shortcode('tw_item', 'shortcode_tw_item');





/* ================================================================================== */
/*      Item Title Shortcode
/* ================================================================================== */
 
if (!function_exists('shortcode_tw_item_title')) {
    function shortcode_tw_item_title($atts, $content) {
        $output='<div class="tw-title-container"><h3 class="tw-title">'.$atts['title'].'</h3><span class="tw-title-border"></span></div>';
        return $output;
    }
}
add_shortcode('tw_item_title', 'shortcode_tw_item_title');





/* ================================================================================== */
/*      Layout Shortcode
/* ================================================================================== */

if (!function_exists('shortcode_tw_layout')) {
    function shortcode_tw_layout($atts, $content) {
        $output = '<div class="'.(pbTextToFoundation($atts['size'])==="span3"?'span3 sidebar-container':pbTextToFoundation($atts['size'])).'">'.do_shortcode($content).'</div>';
        return $output;
    }
}
add_shortcode('tw_layout', 'shortcode_tw_layout');





/* ================================================================================== */
/*      Core Content Shortcode
/* ================================================================================== */

if (!function_exists('shortcode_tw_content')) {
    function shortcode_tw_content() {
        return apply_filters("the_content",get_the_content());
    }
}
add_shortcode('tw_content', 'shortcode_tw_content');





/* ================================================================================== */
/*      Service Shortcode
/* ================================================================================== */

if (!function_exists('shortcode_tw_service')) {
    function shortcode_tw_service($atts, $content) {
        $output = '<div class="tw-service">';
        $output .= do_shortcode($content);
        $output .= '</div>';
        return $output;
    }
}
add_shortcode('tw_service', 'shortcode_tw_service');


// Service Item
if (!function_exists('shortcode_tw_service_item')) {
    function shortcode_tw_service_item($atts, $content) {
        $style='';
        $thumb='';
        $thumbType=isset($atts['thumb_type'])&&$atts['thumb_type']==='image'?'image':'fa';
        $style_for_desc='';
        $margin_for_desc='';
        if($atts['service_style'] === 'style_2'){ $style='left-service'; $style_for_desc='desc_unstyle'; $margin_for_desc = 'margin-left:'.($thumbType==='fa'?($atts['fa_size']+$atts['fa_padding']+$atts['fa_padding'] + 30):(intval($atts['service_thumb_width']) + 30)).'px';}
        if($thumbType==='image'){
            $thumb=isset($atts['service_thumb'])?'<img title="'.$atts['title'].'" width="'.$atts['service_thumb_width'].'" src="'.$atts['service_thumb'].'" />':'';
        }else{
            $thumb=do_shortcode('[tw_fontawesome fa_size="'.$atts['fa_size'].'" fa_padding="'.$atts['fa_padding'].'" fa_color="'.$atts['fa_color'].'" fa_bg_color="'.$atts['fa_bg_color'].'" fa_rounded="'.$atts['fa_rounded'].'" fa_icon="'.$atts['fa_icon'].'"]');
        }
        $output = '<div class="tw-service-box '.$style.'">';
        $output .= '<div class="tw-service-icon">'.$thumb.'</div>';
        $output .= '<div class="tw-service-content '.$style_for_desc.'" style="'.$margin_for_desc.'">';
        $output .= '<h3>'.$atts['title'].'</h3>';
        $output .= '<span></span><p>'.do_shortcode($content).'</p><span></span>';
        if(!empty($atts['more_url']))
            $output .= '<p><a href="'.$atts['more_url'].'" target="'.$atts['more_target'].'">'.$atts['more_text'].'</a></p>';
        $output .= '</div>';
        $output .= "</div>";
        return $output;
    }
}
add_shortcode('tw_service_item', 'shortcode_tw_service_item');










/* ================================================================================== */
/*      Font Awesome Shortcode
/* ================================================================================== */

if (!function_exists('shortcode_tw_fontawesome')) {
    function shortcode_tw_fontawesome($atts, $content) {
        $style  ='text-align:center;';
        $style .='font-size:'       .$atts['fa_size'].';';
        $style .='width:'           .$atts['fa_size'].';';
        $style .='line-height:'     .$atts['fa_size'].';';
        $style .='padding:'         .$atts['fa_padding'].';';
        $style .='color:'           .$atts['fa_color'].';';
        $style .='border-color:'.$atts['fa_bg_color'].';';
        $style .='border-width:'   .$atts['fa_rounded'].';';
        $output='<i class="tw-font-awesome '.$atts['fa_icon'].'" style="display: inline-block;border-radius: 50%;-moz-border-radius: 50%;-webkit-border-radius: 50%; border-style: solid;'.$style.'"></i>';
        return $output;
    }
}
add_shortcode('tw_fontawesome', 'shortcode_tw_fontawesome');





/* ================================================================================== */
/*      Divider Shortcode
/* ================================================================================== */

if (!function_exists('shortcode_tw_divider')) {
    function shortcode_tw_divider($atts, $content) {
        if($atts['type'] == 'space')
            $output = '<div style="margin-bottom:'.$atts['height'].'px;"></div>';
        else
            $output = '<div class="tw-divider"></div>';
        return $output;
    }
}
add_shortcode('tw_divider', 'shortcode_tw_divider');





/* ================================================================================== */
/*      Image Slider Shortcode
/* ================================================================================== */
//  Image Slider Container
if (!function_exists('shortcode_tw_image')) {
    function shortcode_tw_image($atts, $content) {
        $output = '<div class="gallery-container clearfix">';
            $output .= '<div class="gallery-slide">';
                $output .= do_shortcode($content);
            $output .= '</div>';
            $output .= '<div class="carousel-arrow">';
                $output .= '<a class="carousel-prev" href="#"><i class="icon-chevron-left"></i></a>';
                $output .= '<a class="carousel-next" href="#"><i class="icon-chevron-right"></i></a>';
            $output .= '</div>';
        $output .= '</div>';
        return $output;
    }
}
add_shortcode('tw_image', 'shortcode_tw_image');
//  Image Slider Item
if (!function_exists('shortcode_tw_image_item')) {
    function shortcode_tw_image_item($atts, $content) {
        $output = '<div class="slide-item">';
            $output .= '<img src="'.$atts['url'].'" alt="'.get_the_title().'" style="width:100%;">';
        $output .= '</div>';
        return $output;
    }
}
add_shortcode('tw_image_item', 'shortcode_tw_image_item');































/* ================================================================================== */
/*      Messagebox Shortcode
/* ================================================================================== */

// Messagebox container
if (!function_exists('shortcode_message_box')) {
    function shortcode_message_box($atts, $content) {
        $output = '<div class="tw-message-box">';
        $output .= do_shortcode($content);
        $output .= '</div>';
        return $output;
    }
}
add_shortcode('tw_message_box', 'shortcode_message_box');
// Messagebox Item
if (!function_exists('shortcode_tw_message_box_item')) {
    function shortcode_tw_message_box_item($atts, $content) {
        $type="alert-default";
        $icon = '';
        if($atts['type'] == 'default') {

        } elseif($atts['type'] == 'alert') {
            $type="";
            $icon = '<i class="icon-warning-sign"></i>';
        } elseif($atts['type'] == 'info') {
            $type="alert-info";
            $icon = '<i class="icon-info-sign"></i>';
        } elseif($atts['type'] == 'success') {
            $type="alert-success";
            $icon = '<i class="icon-ok-sign"></i>';
        } elseif($atts['type'] == 'error') {
            $type="alert-error";
            $icon = '<i class="icon-remove"></i>';
        }
        $output = '<div class="alert '.$type.'">';
            $output .= '<button type="button" class="close" data-dismiss="alert">&times;</button>';
            $output .= do_shortcode($content);
            $output .= $icon;
        $output .= '</div>';

        return $output;
    }
}
add_shortcode('tw_message_box_item', 'shortcode_tw_message_box_item');
































/* ================================================================================== */
/*      Progress Shortcode
/* ================================================================================== */
if (!function_exists('shortcode_tw_progress')) {
    function shortcode_tw_progress($atts, $content) {            
        return do_shortcode($content);
    }
}
add_shortcode('tw_progress', 'shortcode_tw_progress');
if (!function_exists('shortcode_tw_progress_item')) {
    function shortcode_tw_progress_item($atts, $content) {            
        $output = '<div class="progress">';
        if($atts['type'] == 'animated')
            $output = '<div class="progress progress-striped active">';
        elseif($atts['type'] == 'striped')
            $output = '<div class="progress progress-striped">';
        $output .= '<div class="bar '.($atts['type'] == 'default'? 'tw-bi':'').'" style="width: '.$atts['percent'].'%;background-color: '.$atts['color'].'">'.$atts['progress_title'].'</div>';
        $output .= '</div>';
        return $output;
    }
}
add_shortcode('tw_progress_item', 'shortcode_tw_progress_item');





/* ================================================================================== */
/*      Sidebar Shortcode
/* ================================================================================== */
if (!function_exists('shortcode_tw_sidebar')) {
    function shortcode_tw_sidebar($atts, $content) {            
        ob_start();
        echo '<section id="sidebar">';
        if(!dynamic_sidebar($atts['name'])) {
                print 'There is no widget. You should add your widgets into <strong>';
                print $atts['name'];
                print '</strong> sidebar area on <strong>Appearance => Widgets</strong> of your dashboard. <br/><br/>';
        }
        echo '</section>';
        $output = ob_get_clean();
        return $output;
    }
}
add_shortcode('tw_sidebar', 'shortcode_tw_sidebar');






/* ================================================================================== */
/*      Video Shortcode
/* ================================================================================== */
if (!function_exists('shortcode_tw_video')) {
    function shortcode_tw_video($atts, $content) {           

        $video_embed = $content;
        $video_thumb = $atts['video_thumb'];
        $video_m4v = $atts['video_m4v'];
        $video_type = $atts['insert_type'];
        
        ob_start();

        if ($video_type == 'type_embed') {
            echo apply_filters("the_content", $video_embed);
        } elseif (!empty($video_m4v)) {
            global $post;
            add_action('wp_footer', 'jplayer_script');
            ?>

            <div id="jquery_jplayer_<?php echo $post->ID; ?>" class="jp-jplayer jp-jplayer-video" data-pid="<?php echo $post->ID; ?>" data-m4v="<?php echo $video_m4v; ?>" data-thumb="<?php echo $video_thumb; ?>"></div>
            <div class="jp-video-container">
                <div class="jp-video">
                    <div class="jp-type-single">
                        <div id="jp_interface_<?php echo $post->ID; ?>" class="jp-interface">
                            <ul class="jp-controls">
                                <li><div class="seperator-first"></div></li>
                                <li><div class="seperator-second"></div></li>
                                <li><a href="#" class="jp-play" tabindex="1">play</a></li>
                                <li><a href="#" class="jp-pause" tabindex="1">pause</a></li>
                                <li><a href="#" class="jp-mute" tabindex="1">mute</a></li>
                                <li><a href="#" class="jp-unmute" tabindex="1">unmute</a></li>
                            </ul>
                            <div class="jp-progress-container">
                                <div class="jp-progress">
                                    <div class="jp-seek-bar">
                                        <div class="jp-play-bar"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="jp-volume-bar-container">
                                <div class="jp-volume-bar">
                                    <div class="jp-volume-bar-value"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php
        }
        $output = ob_get_clean();
        return $output;
    }
}
add_shortcode('tw_video', 'shortcode_tw_video');





/* ================================================================================== */
/*      Callout Shortcode
/* ================================================================================== */

if (!function_exists('shortcode_tw_callout')) {
    function shortcode_tw_callout($atts, $content) {
        
        $Callout_bt=isset($atts['btn_text'])?$atts['btn_text']:'';
        $Callout_bt_url=!empty($atts['btn_url'])?to_url($atts['btn_url']):'#';
        $Callout_bt_color=!empty($atts['btn_url'])?(' style="background-color:'.$atts['btn_color'].'"'):'#';
        $Callout_bt_target=isset($atts['btn_target'])?$atts['btn_target']:'_blank';
        $Callout_bt_full='<a href="'.$Callout_bt_url.'"'.$Callout_bt_color.' target="'.$Callout_bt_target.'" class="btn btn-flat btn-large">'.$Callout_bt.'</a>';
        
        $output = '<div class="tw-callout'.(!empty($Callout_bt) ? ' with-button' : '').'">';
        $output .= '<div class="callout-text">';
        $output .= '<h1>'.do_shortcode($content).'</h1>';
        $output .= '<p>'.$atts['description'].'</p>';        
        $output .= !empty($Callout_bt) ? $Callout_bt_full : '';
        $output .= '</div>';
        $output .= '<div class="callout-botline"></div></div>';

        return $output;
    }
}
add_shortcode('tw_callout', 'shortcode_tw_callout');





/* ================================================================================== */
/*      Revolution Slider Shortcode
/* ================================================================================== */

if (!function_exists('shortcode_tw_slider')) {
    function shortcode_tw_slider($atts, $content) {
        if($atts["id"]>0){
            $output = do_shortcode('[rev_slider '.$atts["id"].']');
        }else{
            $output = '<pre>'.__('Choose Slider','themewaves').'</pre>';
        }
        
        return $output;
    }
}
add_shortcode('tw_slider', 'shortcode_tw_slider');





/* ================================================================================== */
/*      Pricing Table Shortcode
/* ================================================================================== */

if (!function_exists('shortcode_tw_pricing_table')) {
    function shortcode_tw_pricing_table($atts, $content) {
        $output = '<div class="tw-pricing clearfix">';
        $query = Array(
            'post_type' => 'price',
            'posts_per_page' => $atts['column'],
        );
        $cats = empty($atts['price_category_list']) ? false : explode(",", $atts['price_category_list']);
        if ($cats) { 
            $query['tax_query'] = Array(Array(
                    'taxonomy' => 'prices',
                    'terms' => $cats,
                    'field' => 'id'
                )
            );
        }
        switch($atts['column']){
            case'2':{$columnWidth='tw-pricing-two';  break;}
            case'3':{$columnWidth='tw-pricing-three';break;}
            case'4':{$columnWidth='tw-pricing-four'; break;}
            case'5':{$columnWidth='tw-pricing-five'; break;}
        }
        query_posts($query);
        while (have_posts()){ the_post();
            $isFeat = get_metabox('type') == 'featured' ? true : false;
            $output .= '<div class="'.$columnWidth.' tw-pricing-col">';
                $output .= '<div class="tw-pricing-box'.($isFeat ? ' featured' : '').'">';
                    $output .= '<div class="tw-pricing-header">';
                        $output .= '<h1>'.get_the_title().'</h1>';
                        if(get_metabox('subtitle')!="") $output .= ('<p>'.get_metabox('subtitle').'</p>');
                    $output .= '</div>';
                    $output .= '<div class="tw-pricing-top">';
                        $output .= '<p>'.get_metabox('price').'</p><span>'.get_metabox('time').'</span>';
                    $output .= '</div>';
                    $output .= '<div class="tw-pricing-bottom">';
                        $output .= get_the_content();
                    $output .= '</div>';
                    if(get_metabox('buttontext')!="") {
                        $output .= '<div class="tw-pricing-footer">';  
                        $output .= '<a href="'. (get_metabox('buttonlink')!="" ? to_url(get_metabox('buttonlink')) : "#") .'" class="btn btn-small'.($isFeat ? ' btn-dark' : '').'">'.get_metabox('buttontext').'</a>';
                        $output .= '</div>';
                    }
                $output .= '</div>';
            $output .= '</div>';
        }
        wp_reset_query();
        $output .= '</div>';

        return $output;
    }
}
add_shortcode('tw_pricing_table', 'shortcode_tw_pricing_table');





/* ================================================================================== */
/*      Testimonials Shortcode
/* ================================================================================== */

if (!function_exists('shortcode_tw_testimonials')) {
    function shortcode_tw_testimonials($atts, $content) {
        $direction= empty($atts['direction'])?'up'   :$atts['direction'];
        $duration = empty($atts['duration']) ?'1000' :$atts['duration'];
        $timeout  = empty($atts['timeout'])  ?'2000' :$atts['timeout'];
        $output = '<div class="tw-testimonials clearfix" data-direction="'.$direction.'" data-duration="'.$duration.'" data-timeout="'.$timeout.'"><ul>';
        $query = Array(
            'post_type' => 'testimonial',
            'posts_per_page' => $atts['count'],
        );
        $cats = empty($atts['category_ids']) ? false : explode(",", $atts['category_ids']);
        if ($cats) { 
            $query['tax_query'] = Array(Array(
                    'taxonomy' => 'testimonials',
                    'terms' => $cats,
                    'field' => 'id'
                )
            );
        }
        query_posts($query);
        while (have_posts()){ the_post();
            $output .= '<li class="testimonial-item">';
                $output .= '<blockquote>';
                        $output .= get_the_content();
                $output .= '</blockquote>';
                $output .= '<div class="testimonial-author clearfix">';
                    $output .= post_image_show(40,40);
                    $output .= '<p>'.get_metabox('name').' <a href="'.get_metabox('url').'">/ '.get_metabox('company').' /</a></p>';
                $output .= '</div>';
            $output .= '</li>';
        }
        wp_reset_query();
        $output .= '</ul>';
        $output .= '<div class="carousel-arrow">';
            $output .= '<a class="carousel-prev" href="#">‹</a>';
            $output .= '<a class="carousel-next" href="#">›</a>';
        $output .= '</div>';
        $output .= '</div>';

        return $output;
    }
}
add_shortcode('tw_testimonials', 'shortcode_tw_testimonials');





/* ================================================================================== */
/*      Team Shortcode
/* ================================================================================== */

if (!function_exists('shortcode_tw_team')) {
    function shortcode_tw_team($atts, $content) {
        $output = '<div class="tw-our-team">';
        $query = Array(
            'post_type' => 'team',
            'posts_per_page' => $atts['count'],
        );
        $cats = empty($atts['category_ids']) ? false : explode(",", $atts['category_ids']);
        if ($cats) { 
            $query['tax_query'] = Array(Array(
                    'taxonomy' => 'position',
                    'terms' => $cats,
                    'field' => 'id'
                )
            );
        }
        $width = 270;
        $height = $atts['height'];
        query_posts($query);
        while (have_posts()){ the_post();
            $output .= '<div class="team-member">';                
                $output .= '<div class="loop-image">';
                    $output .= post_image_show($width, $height);
                    $output .= '<div class="image-overlay"></div>';                
                    $output .= '<div class="member-social"><div class="tw-social-icon clearfix">';
                    if(get_metabox('facebook')!="")
                        $output .= '<a href="'.to_url (get_metabox('facebook')).'" target="_blank" class="facebook"><span class="tw-icon-facebook"></span></a>';
                    if(get_metabox('google')!="")
                        $output .= '<a href="'.to_url (get_metabox('google')).'" target="_blank" class="gplus"><span class="tw-icon-gplus"></span></a>';
                    if(get_metabox('twitter')!="")
                        $output .= '<a href="'.to_url (get_metabox('twitter')).'" target="_blank" class="twitter"><span class="tw-icon-twitter"></span></a>';
                    if(get_metabox('linkedin')!="")
                        $output .= '<a href="'.to_url (get_metabox('linkedin')).'" target="_blank" class="linkedin"><span class="tw-icon-linkedin"></span></a>';
                    if(get_metabox('dribbble')!="")
                        $output .= '<a href="'.to_url (get_metabox('dribbble')).'" target="_blank" class="dribbble"><span class="tw-icon-dribbble"></span></a>';
                    $output .= '</div></div>';
                $output .= '</div>';
                $output .= '<div class="member-title">';
                    $output .= '<h2>'.get_the_title().'</h2>';
                    if(get_metabox('position')!="")
                    $output .= '<p>'.get_metabox('position').'</p>';
                $output .= '</div>';
            $output .= '</div>';
        }
        wp_reset_query();        
        $output .= '</div>';

        return $output;
    }
}
add_shortcode('tw_team', 'shortcode_tw_team');





/* ================================================================================== */
/*      Twitter Shortcode
/* ================================================================================== */

if (!function_exists('shortcode_tw_twitter')) {
    function shortcode_tw_twitter($atts, $content) {
        require_once (THEME_PATH . "/framework/twitteroauth.php");
        $atts = shortcode_atts(array(
            'consumerkey'       => tw_option('consumerkey'),
            'consumersecret'    => tw_option('consumersecret'),
            'accesstoken'       => tw_option('accesstoken'),
            'accesstokensecret' => tw_option('accesstokensecret'),
            'cachetime'         => '1',
            'username'          => 'themewaves',
            'tweetstoshow'      => '10',
	), $atts);
        $output='';
        //check settings and die if not set
        if(empty($atts['consumerkey']) || empty($atts['consumersecret']) || empty($atts['accesstoken']) || empty($atts['accesstokensecret']) || empty($atts['cachetime']) || empty($atts['username'])){
            return '<strong>'.__('Fill options !!!','themewaves').'</strong>';
        }
        //check if cache needs update
        $tw_twitter_last_cache_time = get_option('tw_twitter_last_cache_time');
        $diff = time() - $tw_twitter_last_cache_time;
        $crt = $atts['cachetime'] * 3600;

        //yes, it needs update			
        if($diff >= $crt || empty($tw_twitter_last_cache_time)){
            $connection = new TwitterOAuth($atts['consumerkey'], $atts['consumersecret'], $atts['accesstoken'], $atts['accesstokensecret']);
            $tweets = $connection->get("https://api.twitter.com/1.1/statuses/user_timeline.json?screen_name=".$atts['username']."&count=10") or die('Couldn\'t retrieve tweets! Wrong username?');
            if(!empty($tweets->errors)){
                if($tweets->errors[0]->message == 'Invalid or expired token'){
                    return '<strong>'.$tweets->errors[0]->message.'!</strong><br />You\'ll need to regenerate it <a href="https://dev.twitter.com/apps" target="_blank">here</a>!';
                }else{
                    return '<strong>'.$tweets->errors[0]->message.'</strong>';
                }
                return;
            }
            $tweets_array=array();
            for($i=0;is_array($tweets)&&$i<=count($tweets);$i++){
                if(!empty($tweets[$i])){
                    $tweets_array[$i]['created_at'] = $tweets[$i]->created_at;
                    $tweets_array[$i]['text'] = $tweets[$i]->text;			
                    $tweets_array[$i]['status_id'] = $tweets[$i]->id_str;			
                }	
            }							
            //save tweets to wp option 		
            update_option('tw_twitter_tweets',serialize($tweets_array));							
            update_option('tw_twitter_last_cache_time',time());
            echo '<!-- twitter cache has been updated! -->';
        }
        //convert links to clickable format
        if(!function_exists('convert_links')){
            function convert_links($status,$targetBlank=true,$linkMaxLen=250){
                // the target
                $target=$targetBlank ? " target=\"_blank\" " : "";
                // convert link to url
                $status = preg_replace("/((http:\/\/|https:\/\/)[^ )]+)/e", "'<a href=\"$1\" title=\"$1\" $target >'. ((strlen('$1')>=$linkMaxLen ? substr('$1',0,$linkMaxLen).'...':'$1')).'</a>'", $status);
                // convert @ to follow
                $status = preg_replace("/(@([_a-z0-9\-]+))/i","<a href=\"http://twitter.com/$2\" title=\"Follow $2\" $target >$1</a>",$status);
                // convert # to search
                $status = preg_replace("/(#([_a-z0-9\-]+))/i","<a href=\"https://twitter.com/search?q=$2\" title=\"Search $1\" $target >$1</a>",$status);
                // return the status
                return $status;
            }
        }
        //convert dates to readable format
        if(!function_exists('relative_time')){
            function relative_time($a) {
                //get current timestampt
                $b = strtotime("now"); 
                //get timestamp when tweet created
                $c = strtotime($a);
                //get difference
                $d = $b - $c;
                //calculate different time values
                $minute = 60;
                $hour = $minute * 60;
                $day = $hour * 24;
                $week = $day * 7;
                if(is_numeric($d) && $d > 0) {
                    //if less then 3 seconds
                    if($d < 3) return "right now";
                    //if less then minute
                    if($d < $minute) return floor($d) . " seconds ago";
                    //if less then 2 minutes
                    if($d < $minute * 2) return "about 1 minute ago";
                    //if less then hour
                    if($d < $hour) return floor($d / $minute) . " minutes ago";
                    //if less then 2 hours
                    if($d < $hour * 2) return "about 1 hour ago";
                    //if less then day
                    if($d < $day) return floor($d / $hour) . " hours ago";
                    //if more then day, but less then 2 days
                    if($d > $day && $d < $day * 2) return "yesterday";
                    //if less then year
                    if($d < $day * 365) return floor($d / $day) . " days ago";
                    //else return more than a year
                    return "over a year ago";
                }
            }
        }
        $tw_twitter_tweets = maybe_unserialize(get_option('tw_twitter_tweets'));
        if(!empty($tw_twitter_tweets)){
            $output.='<div class="tw-twitter">';
                $output.='<ul class="jtwt">';
                    $fctr = '1';
                    foreach($tw_twitter_tweets as $tweet){								
                        $output.='<li><span>'.convert_links($tweet['text']).'</span><br /><a class="twitter_time" target="_blank" href="http://twitter.com/'.$atts['username'].'/statuses/'.$tweet['status_id'].'">'.relative_time($tweet['created_at']).'</a></li>';
                        if($fctr == $atts['tweetstoshow']){ break; }
                        $fctr++;
                    }
                $output.='</ul>';
                $output.='<div class="twitter-follow">'.__('Follow','themewaves').' <a target="_blank" href="http://twitter.com/'.$atts['username'].'">@'.$atts['username'].'</a></div>';
            $output.='</div>';
        }
        return $output;
    }
}
add_shortcode('tw_twitter', 'shortcode_tw_twitter');





/* ================================================================================== */
/*      Portfolio Shortcode
/* ================================================================================== */

if (!function_exists('shortcode_tw_portfolio')) {
    function shortcode_tw_portfolio($atts, $content) {
        $atts = shortcode_atts(array(
		'layout_size' => 'span12',
		'pagination' => 'simple',
                'height' => '',
                'column' => '4',
                'count' => 12,
                'filter' => 'none',
                'category_ids' => ''
	), $atts);
        global $paged, $tw_options;
        if($atts['layout_size']==='span3'){
            $tw_options['column']='1';
        }elseif($atts['layout_size']==='span9'){
            $tw_options['column']='3';
        }elseif($atts['layout_size']==='span12'){
            switch ($atts['column']){
                case '2':{
                    $tw_options['column']='6';
                    break;   
                }
                case '3':{
                    $tw_options['column']='4';
                    break;   
                }
                case '4':{
                    $tw_options['column']='3';
                    break;   
                }
            }
        }
        
        if( get_query_var( 'paged' ) )
            $my_page = get_query_var( 'paged' );
        else {
            if( get_query_var( 'page' ) )
                $my_page = get_query_var( 'page' );
            else
                $my_page = 1;
            set_query_var( 'paged', $my_page );
        }
        add_action( 'wp_footer', 'portfolio_script');
        $tw_options['pagination'] = $atts['pagination'];
        $tw_options['height'] = $atts['height'];
        $query = Array(
            'post_type' => 'portfolio',
            'posts_per_page' => $atts['count'],
        );
        if($tw_options['pagination']=="simple"||$tw_options['pagination']=="infinite"){
            $query['paged'] = $my_page;
        }
        $cats = empty($atts['category_ids']) ? false : explode(",", $atts['category_ids']);
        if ($cats) {
            $query['tax_query'] = Array(Array(
                    'taxonomy' => 'portfolios',
                    'terms' => $cats,
                    'field' => 'id'
                )
            );
        }
        $output = '<div class="tw-portfolio">';
        $filter=(!empty($atts['filter'])&&$atts['filter']=='true')?true:false;
        if($filter){
            $output .= '<div class="tw-filter">';
            $output .= '<ul class="filters option-set clearfix post-category inline" data-option-key="filter">';
            $output .= '<li><a href="#filter" data-option-value="*" class="selected">'.__('Show All', 'themewaves').'</a></li>';
            if($cats) {
                $filters = $cats;
            } else {
                $filters = get_terms( 'portfolios' );
            }
            foreach ($filters as $category){
                if ($cats) {
                    $category = get_term_by('id', $category, 'portfolios');
                }
                $output .= '<li class="hidden"><a href="#filter" data-option-value=".category-' . $category->slug . '" title="' . $category->name . '">' . $category->name . '</a></li>';
            }
            $output .= '</ul></div>';
        }
        query_posts($query);
        ob_start();
        get_template_part("loop","portfolio");
        $output .= ob_get_clean();
        wp_reset_query();
        $output .= '</div>';
        return $output;
    }
}
function portfolio_script() {
    wp_enqueue_script('isotope', THEME_DIR.'/assets/js/jquery.isotope.min.js', false, false, true); ?>
<?php }
add_shortcode('tw_portfolio', 'shortcode_tw_portfolio');






/* ================================================================================== */
/*      Carousel Shortcode
/* ================================================================================== */

if (!function_exists('shortcode_tw_carousel')) {
    function shortcode_tw_carousel($atts, $content) {
        $post_type  = isset($atts['post_type']) ?$atts['post_type'] :'post';
        $post_count = isset($atts['post_count'])?$atts['post_count']:'';
        $desc_title = !empty($atts['description_title'])?$atts['description_title']:'';
        $desc_text = !empty($atts['description_text'])?$atts['description_text']:'';
        $post_category_list = isset($atts['post_category_list'])?$atts['post_category_list']:'';
        $port_category_list = isset($atts['port_category_list'])?$atts['port_category_list']:'';
        $arrow = '<div class="carousel-arrow">';
            $arrow .= '<a class="carousel-prev" href="#"><i class="icon-chevron-left"></i></a>';
            $arrow .= '<a class="carousel-next" href="#"><i class="icon-chevron-right"></i></a>';
        $arrow .= '</div>';
        $output = '';
        if(!empty($desc_text)){
            $output .= '<div class="row-fluid carousel-container">';
            $output .= '<div class="span3 carousel-text tw-title-container">';
            $output .= !empty($desc_title) ? ('<h3 class="tw-title">'.$desc_title.'</h3>') : '';
            $output .= '<p>'.$desc_text.'</p>';
            $output .= $arrow.'</div>';
            $output .= '<div class="span9">';
        } else {
            $output .= '<div class="carousel-container">';
        }
        
        
        $output .= '<div class="tw-carousel-'.$post_type.' list_carousel">';
            $output .= '<ul class="tw-carousel">';
                $query = Array(
                    'post_type'      => $post_type,
                    'posts_per_page' => $post_count,
                );
                $cats = $post_type==='post' ? explode(",", $post_category_list):explode(",", $port_category_list);
                $imgwidth = 270;
                if ($post_type == "post") {
                    if (!empty($cats[0])){
                        $query['tax_query'] = Array(Array(
                                'taxonomy' => 'category',
                                'terms' => $cats,
                                'field' => 'id'
                            )
                        );
                    }
                    $imgwidth = 370;
                } elseif ($post_type == "portfolio") {
                    if (!empty($cats[0])){
                        $query['tax_query'] = Array(Array(
                                'taxonomy' => 'portfolios',
                                'terms' => $cats,
                                'field' => 'id'
                            )
                        );
                    }
                }
                // START - LOOP
                query_posts($query);
                while (have_posts()){ the_post();
                    $format = get_post_format() == "" ? "standard" : get_post_format();
                    $imgheight = $atts['image_height'];
                    $output .= '<li>';
                        if($post_type==='partner'){
                            if(get_metabox('link')!=''){
                                $output .= '<a href="'. to_url(get_metabox('link')).'">';
                                    $output .= post_image_show($imgwidth,$imgheight);
                                $output .= '</a>';
                            } else {
                                $output .= post_image_show($imgwidth,$imgheight);
                            }
                        }
                        else {                      
                            $video = "";
                            $title = true;
                            if($post_type==='post'){
                                $title = false;
                                if(get_post_format()=="video"){
                                    $video = '<div class="carousel-video" style="height:'.$imgheight.'px;">';
                                    ob_start();
                                    format_video();
                                    $video .= ob_get_clean();
                                    $video .= '</div>';
                                }
                            } else {
                                if(get_metabox('format_video_embed')!=''){
                                    $video = '<div class="carousel-video">';
                                    ob_start();
                                    format_video();
                                    $video .= ob_get_clean();
                                    $video .= '</div>';
                                }
                            }
                            if(empty($video)){
                                if(post_image_show()){
                                    ob_start();
                                    portfolio_image($imgheight,$imgwidth,$title);
                                    $output .= ob_get_clean();
                                }
                            } else {$output .= $video;}
                            $output .= '<div class="carousel-content">';                            
                                if($post_type==='post'){
                                    $output .='<div class="post-format '.$format.'"></div>';
                                    $output .= '<div class="carousel-text">';
                                        $output .= '<h3><a href="'.get_permalink().'" class="carousel-post-title">'.get_the_title().'</a></h3>';
                                        $output .= '<div class="carousel-meta clearfix"><div class="date">'.get_the_time('j M Y').'</div>';
                                        $output .= '<div class="category">'.get_the_category_list(', ').'</div></div>';
                                        //$output .= '<p>'.to_excerpt(get_the_content(), 12).'</p>';
                                    $output .= '</div>';
                                }
                                //$output .= '<a href="'.get_permalink().'" class="carousel-post-title">'.get_the_title().'</a>';
                            $output .= '</div>';
                        }
                    $output .= '</li>';
                }
                wp_reset_query();
                // END   - LOOP
            $output .= '</ul>';
            $output .= '<div class="clearfix"></div>';
            if(empty($desc_text)) $output .= $arrow;
        $output .= '</div>';
        if(!empty($desc_text)){
            $output .= '</div>';            
        }
        $output .= '</div>';
        return $output;
    }
}
add_shortcode('tw_carousel', 'shortcode_tw_carousel');





/* ================================================================================== */
/*      Dropcap Shortcode
/* ================================================================================== */

if (!function_exists('shortcode_tw_dropcap')) {
    function shortcode_tw_dropcap($atts, $content) {
        $style=$atts['style']=='circle'?' circle':'';         
        return '<span class="tw-dropcap'.$style.'" style="background-color: '.$atts['color'].';">'.$content.'</span>';
    }
}
add_shortcode('tw_dropcap', 'shortcode_tw_dropcap');





/* ================================================================================== */
/*      Button Shortcode
/* ================================================================================== */

if (!function_exists('shortcode_tw_button')) {
    function shortcode_tw_button($atts, $content) {
        $link=!empty($atts['link']) ? to_url($atts['link']) : '#';        
        $style=!empty($atts['style']) ? (' btn-'.$atts['style']) : '';        
        $size=!empty($atts['size']) ? (' btn-'.$atts['size']) : '';        
        $target=!empty($atts['target']) ? ($atts['target']) : '_blank';        
        $color=!empty($atts['color']) ? ('style="background:'.$atts['color'].'"') : '';        
        return '<a href="'.$link.'" target="'.$target.'" class="btn'.$style.$size.'"'.$color.'>'.$content.'</a>';
    }
}
add_shortcode('tw_button', 'shortcode_tw_button');





/* ================================================================================== */
/*      Label Shortcode
/* ================================================================================== */

if (!function_exists('shortcode_tw_label')) {
    function shortcode_tw_label($atts, $content) {
        $color=!empty($atts['color']) ? (' style="background:'.$atts['color'].'"'):'';        
        return '<span class="label"'.$color.'>'.$content.'</span>';
    }
}
add_shortcode('tw_label', 'shortcode_tw_label');




/* ================================================================================== */
/*      ColumnShortcode Shortcode
/* ================================================================================== */

// ColumnShortcode container
if (!function_exists('shortcode_tw_sh_column')) {
    function shortcode_tw_sh_column($atts, $content) {
        $output = '<div class="tw-column-shortcode row-fluid">';
        $output .= do_shortcode($content);
        $output .= '</div>';
        return $output;
    }
}
add_shortcode('tw_sh_column', 'shortcode_tw_sh_column');
// ColumnShortcode Item
if (!function_exists('shortcode_tw_sh_column_item')) {
    function shortcode_tw_sh_column_item($atts, $content) {
        extract(shortcode_atts(array(
            'column_size'     => '1 / 3',
	), $atts));
        $output  = '<div class="'.pbTextToFoundation($column_size).'">';
            $output .= do_shortcode($content);
        $output .= '</div>';
        
        return $output;
    }
}
add_shortcode('tw_sh_column_item', 'shortcode_tw_sh_column_item');