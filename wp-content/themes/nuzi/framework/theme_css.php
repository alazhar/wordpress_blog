<?php

function theme_option_styles() {

    function hex2rgb($hex) {
        $hex = str_replace("#", "", $hex);

        if (strlen($hex) == 3) {
            $r = hexdec(substr($hex, 0, 1) . substr($hex, 0, 1));
            $g = hexdec(substr($hex, 1, 1) . substr($hex, 1, 1));
            $b = hexdec(substr($hex, 2, 1) . substr($hex, 2, 1));
        } else {
            $r = hexdec(substr($hex, 0, 2));
            $g = hexdec(substr($hex, 2, 2));
            $b = hexdec(substr($hex, 4, 2));
        }
        $rgb = array($r, $g, $b);
        return implode(",", $rgb); // returns the rgb values separated by commas
        //return $rgb; // returns an array with the rgb values
    }
    ?>

    <!-- Custom CSS -->

    <style>
        body {
            font-family: <?php echo tw_option('body_text_font', 'face'); ?>, Arial, Helvetica, sans-serif;
            font-size: <?php echo tw_option('body_text_font', 'size'); ?>; 
            font-weight: <?php echo tw_option('body_text_font', 'style'); ?>; 
            color: <?php echo tw_option('body_text_font', 'color'); ?>;
            <?php
            if (tw_option('theme_layout') == 'Boxed Layout') {
                if (tw_option('background_color') != "") {
                    echo 'background-color: ' . tw_option('background_color') . ';';
                }
                if (tw_option('background_image') != "") {
                    echo 'background-image: url(' . tw_option('background_image') . ');';
                }
                if (tw_option('background_repeat') != 'Strech Image') {
                    echo 'background-repeat: ' . tw_option('background_repeat') . ';';
                } else {
                    echo '-webkit-background-size: cover; -moz-background-size: cover; -o-background-size: cover; background-size: cover;';
                }
                echo "background-attachment: fixed;";
                echo "margin-top:" . tw_option('margin_top') . "px;";
                echo "margin-bottom:" . tw_option('margin_bottom') . "px";
            }
            ?>
        }
        h1,h2,h3,h4,h5,h6,
        .btn,button,
        .loop-media blockquote p,
        input[type="submit"],
        input[type="reset"],
        input[type="button"]{font-family: <?php echo tw_option('heading_font'); ?>;}
        h1{ font-size: <?php echo tw_option('h1_spec_font', 'size'); ?>; color: <?php echo tw_option('h1_spec_font', 'color'); ?>; }
        h2{ font-size: <?php echo tw_option('h2_spec_font', 'size'); ?>; color: <?php echo tw_option('h2_spec_font', 'color'); ?>; }
        h3{ font-size: <?php echo tw_option('h3_spec_font', 'size'); ?>; color: <?php echo tw_option('h3_spec_font', 'color'); ?>; }
        h4{ font-size: <?php echo tw_option('h4_spec_font', 'size'); ?>; color: <?php echo tw_option('h4_spec_font', 'color'); ?>; }
        h5{ font-size: <?php echo tw_option('h5_spec_font', 'size'); ?>; color: <?php echo tw_option('h5_spec_font', 'color'); ?>; }
        h6{ font-size: <?php echo tw_option('h6_spec_font', 'size'); ?>; color: <?php echo tw_option('h6_spec_font', 'color'); ?>; }

        a,.header_3 ul.sf-menu > li:hover > a,.header_3 .sf-menu > li.current-menu-item > a,.sf-menu > li.current-menu-item > a,.tw-callout h1 a,.tw-service-content a,#sidebar ul.menu .current_page_item a{ color: <?php echo tw_option('link_color'); ?>; }
        a:hover, a:focus,.loop-meta a:hover,article .loop-content a.more-link:hover{ color: <?php echo tw_option('link_hover_color'); ?>; }

        /* Top Bar ------------------------------------------------------------------------ */

        .tw-top-bar{ background: <?php echo tw_option('top_bar_bg'); ?>;}

        /* Header ------------------------------------------------------------------------ */  

        #header .logo{ margin-top: <?php echo tw_option('logo_top'); ?>; margin-bottom: <?php echo tw_option('logo_bottom'); ?>; }
        #header { background-color: <?php echo tw_option('header_background'); ?>; }

        /* Navigation ------------------------------------------------------------------------ */ 

        #header .tw-menu-container{ background-color: <?php echo tw_option('menu_background'); ?>; }
        ul.sf-menu > li a{ font-family: <?php echo tw_option('menu_font', 'face'); ?>, Arial, Helvetica, sans-serif; font-size: <?php echo tw_option('menu_font', 'size'); ?>; font-weight: <?php echo tw_option('menu_font', 'style'); ?>; color: <?php echo tw_option('menu_font', 'color'); ?>; }
        ul.sf-menu > li{ background-color: <?php echo tw_option('menu_hover_background'); ?>; }
        .sf-menu > li.current-page-item > a, ul.sf-menu > li a:hover,.sf-menu > li.current-menu-item > a,.sf-menu > li.current-menu-item[class^="icon-"]:before, .sf-menu > li.current-menu-item[class*=" icon-"]:before,.sf-menu > li.current_page_ancestor > a,.sf-menu > li.current_page_ancestor[class^="icon-"]:before, .sf-menu > li.current_page_ancestor[class*=" icon-"]:before{ color: <?php echo tw_option('menu_hover'); ?>; }
        ul.sf-menu li ul li { background: <?php echo tw_option('submenu_bg'); ?>; }
        ul.sf-menu li ul li:hover { background: <?php echo tw_option('submenu_hover_background'); ?>; }
        ul.sf-menu li ul li.current-menu-item a,ul.sf-menu li ul li a { color: <?php echo tw_option('submenu_link'); ?>; }
        ul.sf-menu li ul li a:hover,ul.sf-menu li ul li.current-menu-item a,ul.sf-menu li ul li.current_page_item a { color: <?php echo tw_option('submenu_hover'); ?>; }

        /* Main ------------------------------------------------------------------------ */  
        #main { background: <?php echo tw_option('body_background'); ?>; }

        /* Footer ------------------------------------------------------------------------ */  

        #footer{ background: <?php echo tw_option('footer_background'); ?>; }
        #footer{ color: <?php echo tw_option('footer_text_color'); ?>; }
        #footer a{ color: <?php echo tw_option('footer_link_color'); ?>; }
        #footer a:hover, #footer .tw-recent-posts-widget h4 a:hover{ color: <?php echo tw_option('footer_link_hover_color'); ?>; }
        #sidebar h3.widget-title, .sidebar-container .tw-title-container h3 { font-family: <?php echo tw_option('sidebar_widgets_title', 'face'); ?>, Arial, Helvetica, sans-serif; font-size: <?php echo tw_option('sidebar_widgets_title', 'size'); ?>; font-weight: <?php echo tw_option('sidebar_widgets_title', 'style'); ?>; color: <?php echo tw_option('sidebar_widgets_title', 'color'); ?>; }
        .sidebar-container .tw-title-container h3 span { color: <?php echo tw_option('sidebar_widgets_title', 'color'); ?>;}
        #footer h3.widget-title { font-family: <?php echo tw_option('footer_widgets_title', 'face'); ?>, Arial, Helvetica, sans-serif; font-size: <?php echo tw_option('footer_widgets_title', 'size'); ?>; font-weight: <?php echo tw_option('footer_widgets_title', 'style'); ?>; color: <?php echo tw_option('footer_widgets_title', 'color'); ?>; }

        /* General Color ------------------------------------------------------------------------ */ 

        ::selection{ background: <?php echo tw_option('primary_color'); ?>; }
        ::-moz-selection{ background: <?php echo tw_option('primary_color'); ?>; }
        .tagcloud a:hover, #footer .tagcloud a:hover{ background: <?php echo tw_option('primary_color'); ?> !important; }
        button,input[type="submit"],input[type="reset"],input[type="button"],.content-meta,.comment-block .comment span.comment-splash,.tw-pagination.pagination ul>li>a.selected{ background: <?php echo tw_option('primary_color'); ?>; }
        .team-member ul.tw-social-icon li a:hover,ul.tw-social-icon li a:hover,.carousel-arrow a:hover,.list_carousel a.carousel-prev:hover,.list_carousel a.carousel-next:hover{ background-color: <?php echo tw_option('primary_color'); ?>; }
        .featured,.tw-dropcap,.progress .bar,div.jp-jplayer.jp-jplayer-video,.pagination ul>li>a.current, .pagination ul>li>span.current,.carousel-content .post-format,span.post-format{ background-color: <?php echo tw_option('primary_color'); ?>; }
        footer#footer .tw-recent-posts-widget .meta a,.highlight,.tw-top-bar-info a,#bottom a,.tw-title-container h3 span{ color: <?php echo tw_option('primary_color'); ?>; }
        .tw-top-service-text div:last-child,h2.loop-title a:hover,#sidebar a:hover{ color: <?php echo tw_option('primary_color'); ?>; }
        blockquote,.pagination ul>li>a.current, .pagination ul>li>span.current,ul.sf-menu > li > ul,.team-member .loop-image{ border-color: <?php echo tw_option('primary_color'); ?>; }
        .sf-menu > li:hover,.sf-menu > li.current-menu-item,.sf-menu > li.current-page-item,.sf-menu > li.current_page_ancestor{ background:<?php echo tw_option('primary_color'); ?>; }
        .btn,#page-title h3, .tw-breadcrumb,ul.sf-menu > li:hover,.sf-menu > li.current_page_item {background-color:<?php echo tw_option('primary_color'); ?>;}

        .image-overlay{background-color: <?php echo "rgba(" . hex2rgb(tw_option('primary_color')) . ",.7)" ?> ; }
        <?php echo tw_option('custom_css'); ?>
    </style>

    <?php
}

add_action('wp_head', 'theme_option_styles', 100);
?>