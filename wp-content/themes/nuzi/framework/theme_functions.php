<?php
global $tw_socials;
$tw_socials = array(
        'facebook' => array(
            'name' =>  'facebook_username',
            'link' => 'http://www.facebook.com/*',
        ),
        'flickr'   => array(
            'name' =>  'flickr_username',
            'link' => 'http://www.flickr.com/photos/*'
        ),
        'gplus' => array(
            'name' => 'googleplus_username',
            'link' => 'https://plus.google.com/u/0/*'
        ),
        'twitter' => array(
            'name' => 'twitter_username',
            'link' => 'http://twitter.com/*',
        ),
        'instagram' => array(
            'name' => 'instagram_username',
            'link' => 'http://instagram.com/*',
        ),
        'pinterest' => array(
            'name' => 'pinterest_username',
            'link' => 'http://pinterest.com/*',
        ),
        'skype' => array(
            'name' => 'skype_username',
            'link' => 'skype:*'
        ),
        'vimeo' =>  array(
            'name' => 'vimeo_username',
            'link' => 'http://vimeo.com/*',
        ),
        'youtube' => array(
            'name' => 'youtube_username',
            'link' => 'http://www.youtube.com/user/*',
        ),
        'dribbble' => array(
            'name' => 'dribbble_username',
            'link' => 'http://dribbble.com/*',
        ),
        'linkedin' => array(
            'name' => 'linkedin_username',
            'link' => '*'
        ),
        'rss' => array(
            'name' => 'rss_username',
            'link' => 'http://*/feed'
        )
);
// To excerpt
//=======================================================
if (!function_exists('to_excerpt')) {

    function to_excerpt($str, $length) {
        $str = strip_tags($str);
        $str = explode(" ", $str);
        return implode(" ", array_slice($str, 0, $length));
    }

}

// Go url
//=======================================================
if (!function_exists('to_url')) {

    function to_url($url) {
        if (!preg_match_all('!https?://[\S]+!', $url, $matches))
            $url = "http://" . $url;
        return $url;
    }

}

// Page,Post get, print content
//=======================================================

function loop_content() {
    global $tw_options, $more;
    $more = 0;
    $readMore = !empty($tw_options['more_text']) ? $tw_options['more_text'] : __('Continue Reading -', 'themewaves');

    if (has_excerpt()) {
        the_excerpt();
    } elseif (has_more()) {
        the_content("");
        echo '<a href="' . get_permalink() . '" class="more-link">' . $readMore . '</a>';
    } elseif (!empty($tw_options['excerpt_count'])) {
        echo apply_filters("the_content", to_excerpt(strip_shortcodes(get_the_content()), $tw_options['excerpt_count']));
        echo '<a href="' . get_permalink() . '" class="more-link">' . $readMore . '</a>';
    } else {
        echo apply_filters("the_content", strip_shortcodes(get_the_content("")));
    }
}


function tw_option($index1, $index2=false) {
    global $smof_data;
    if($index2){
        $output = isset($smof_data[$index1][$index2]) ? $smof_data[$index1][$index2] : false;
        return $output;
    }
    $output = isset($smof_data[$index1]) ? $smof_data[$index1] : false;
    return $output;
}


// Page, Post custom metaboxes
//=======================================================
function get_metabox($name) {
    global $post;
    if ($post) {
        $metabox = get_post_meta($post->ID, 'themewaves_' . strtolower(THEMENAME) . '_options', true);
        return isset($metabox[$name]) ? $metabox[$name] : "";
    }
    return false;
}
function set_metabox($name,$val) {
    global $post;
    if ($post) {
        $metabox = get_post_meta($post->ID, 'themewaves_' . strtolower(THEMENAME) . '_options', true);
        $metabox[$name] = $val;
        return update_post_meta($post->ID, 'themewaves_' . strtolower(THEMENAME) . '_options', $metabox);
    }
    return false;
}

// Print menu
//=======================================================
function theme_menu(){
        wp_nav_menu( array(
            'container'       => 'div',
            'container_class' => 'tw-menu-container clearfix',
            'menu_id' => 'menu',
            'menu_class' => 'sf-menu',
            'fallback_cb' => 'no_main',
            'theme_location' => 'main' ) 
        );
}
function no_main(){
        echo "<div class='tw-menu-container clearfix'><ul id='menu' class='sf-menu'>";
            wp_list_pages(array('title_li' => ''));
        echo "</ul></div>";
}
function mobile_menu(){
        wp_nav_menu( array(
            'container' => false,
            'menu_id' => '',
            'menu_class' => 'clearfix',
            'fallback_cb' => 'no_mobile',
            'theme_location' => 'main' ) 
        );
}
function no_mobile(){
        echo "<ul class='clearfix'>";
            wp_list_pages(array('title_li' => ''));
        echo "</ul>";
}
		
// Footer menu
//=======================================================
function footer_menu(){
        wp_nav_menu( array('container' => false,
                            'menu_class'      => 'footer-menu inline',
                            'fallback_cb' => 'no_footer',
                            'depth'           => 1,
                            'theme_location' => 'footer' ) );
}
function no_footer(){
        //echo "<ul class='footer-menu inline'>"; wp_list_pages(array('depth'=>1,'title_li'=>'')); echo "</ul>";
}


// Print logo
//=======================================================
function theme_logo() {
    $top = tw_option('logo_top')!="" ? (' margin-top:'.tw_option('logo_top').'px;') : '';
    $bottom = tw_option('logo_bottom')!="" ? (' margin-bottom:'.tw_option('logo_bottom').'px;') : '';
    echo '<div class="tw-logo" style="'.$top.$bottom.'">';
    echo '<a class="logo" href="' . home_url() . '">';
    if (tw_option("theme_logo")=="") {
        bloginfo('name');
    } else {
        if(tw_option("logo_retina"))
            echo '<img class="logo-img" src="' . tw_option("theme_logo_retina") . '" style="width:'.tw_option('logo_width').'px" alt="' . get_bloginfo('name') . '" />';
        else
            echo '<img class="logo-img" src="' . tw_option("theme_logo") . '" alt="' . get_bloginfo('name') . '" />';
    }
    echo '</a>';
    echo '</div>';
    //echo '<div class="site-description">' . get_bloginfo('description') . '</div>';
}

// Get featured text
//=======================================================
function get_featuredtext() {
    global $post;

    if (is_singular()) {
        $return = "<h2>" . $post->post_title . "</h2>";
        return $return;
    } elseif (is_category()) {
        $return = "<h2>";
        $return .= __("Category", "themewaves") . " : " . single_cat_title("", false);
        $return .= "</h2>";
        return $return;
    } elseif (is_tax('portfolios')) {
        $return = "<h2>";
        $return .= __("Portfolio", "themewaves") . " : " . single_cat_title("", false);
        $return .= "</h2>";
        return $return;
    } elseif (is_tag()) {
        $return = "<h2>";
        $return .= __("Tag", "themewaves") . " : " . single_tag_title("", false);
        $return .= "</h2>";
        return $return;
    } elseif (is_404()) {
        $return = "<h2>" . __("Nothing Found!", "themewaves") . "</h1>";
        return $return;
    } elseif (is_author()) {
        global $tw_author;
        $userdata = get_userdata($tw_author);
        $return = "<h2>" . __("Author: ", "themewaves") . $userdata->display_name . "</h1>";
        return $return;
    } elseif (is_archive()) {
        $return = "<h2>";
        if (is_day()){
            $return .= __("Daily Archives", "themewaves") . " : " . get_the_date();
        }elseif (is_month()) {
            $return .= __("Monthly Archives", "themewaves") . " : " . get_the_date("F Y");
        }elseif (is_year()) {
            $return .= __("Yearly Archives", "themewaves") . " : " . get_the_date("Y");
        }else {
            $return .= __("Blog Archives", "themewaves");
        }
        $return .= "</h2>";
        return $return;
    } elseif (is_search()) {
        $return = "<h2>" . __("Search results for", "themewaves") . " : " . get_search_query() . "</h2>";
        return $return;
    }
}

// Sidebar
//=======================================================
function sidebar($sidebar = 'sidebar') {
    if (function_exists('dynamic_sidebar') && dynamic_sidebar($sidebar)) {
        
    }
}

function print_styles($links) {
    for ($i = 0; $i < count($links); $i++) {
        echo '<link type="text/css" rel="stylesheet" href="' . file_require(get_template_directory_uri() . '/' . $links[$i], true) . '" />';
    }

    echo '<link type="text/css" rel="stylesheet" href="http://fonts.googleapis.com/css?family=' . str_replace(" ", "+", get_settings_value('head_felement')) . ':300,300italic,400,400italic,700,700italic,600,600italic" />';
    if (get_settings_value('body_felement') != get_settings_value('head_felement')) {
        echo '<link type="text/css" rel="stylesheet" href="http://fonts.googleapis.com/css?family=' . str_replace(" ", "+", get_settings_value('body_felement')) . ':400,400italic,700,700italic" />';
    }
}

if (!function_exists("current_title")) {

    function current_title() {
        global $page, $paged;
        echo "<title>";
        wp_title('|', true, 'right');
        bloginfo('name');
        $site_description = get_bloginfo('description', 'display');
        if ($site_description && ( is_home() || is_front_page() ))
            echo " | $site_description";
        if ($paged >= 2 || $page >= 2)
            echo ' | ' . sprintf(__('Page %s', 'themewaves'), max($paged, $page));
        echo "</title>";
    }

}

function favicon() {
    if (tw_option('theme_favicon')=="") {
        echo '<link rel="shortcut icon" href="' . THEME_DIR . '/assets/img/favicon.ico"/>';
    } else {
        echo '<link rel="shortcut icon" href="' . tw_option('theme_favicon') . '"/>';
    }
    if(tw_option('favicon_retina')) {
        echo tw_option('favicon_iphone')!="" ? ('<link rel="apple-touch-icon" href="' . tw_option('favicon_iphone') . '"/>') : '';
        echo tw_option('favicon_iphone_retina')!="" ? ('<link rel="apple-touch-icon" sizes="114x114" href="' . tw_option('favicon_iphone_retina') . '"/>') : '';
        echo tw_option('favicon_ipad')!="" ? ('<link rel="apple-touch-icon" sizes="72x72" href="' . tw_option('favicon_ipad') . '"/>') : '';
        echo tw_option('favicon_ipad_retina')!="" ? ('<link rel="apple-touch-icon" sizes="144x144" href="' . tw_option('favicon_ipad_retina') . '"/>') : '';
    }
}

function portfolio_image($height="", $width=270, $title=false) {
    global $post;
    $class = $title ? ' with-title' : '';
    if (has_post_thumbnail($post->ID)) { ?>
        <div class="loop-image">
            <div class="without-a<?php echo $class;?>">
                <?php echo post_image_show($width,$height); ?>
                <div class="image-overlay">                    
                    <div class="image-links">   
                        <?php if($title) { ?>
                            <h2 class="portfolio-title"><a href="<?php the_permalink();?>"><?php the_title(); ?></a><span></span></h2>
                        <?php } ?>
                        <a class="hover-zoom" rel="prettyPhoto" href="<?php echo post_image_show(0,0,true);?>"></a>
                        <a class="hover-link" href="<?php the_permalink();?>"></a>
                    </div>
                </div>
            </div>
        </div>
        <?php
    }
}

function format_standard($portfolio=false, $height="", $width=270) {
    global $post;
    if (has_post_thumbnail($post->ID)) {        
        $overlay = "hover-link";
        $link = get_permalink();
        $rell = '';
        $width = $portfolio ? $width : 870;        
        $height = $portfolio ? $height : get_metabox('image_height');        
        if(is_single()) {
            $lrg_img = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full');
            $link = $lrg_img[0];
            $overlay = "hover-zoom";
            $rell = ' rel="prettyPhoto"';
            if($portfolio) {
                $width = 770;
                $height = get_metabox('image_height');
            }
        }
        ?>
        <div class="loop-image">
            <a href="<?php echo $link; ?>"<?php echo $rell ;?>>
                <?php echo post_image_show($width,$height); ?>
                <div class="image-overlay <?php echo $overlay; ?>"></div>
            </a>
        </div>
        <?php
    }
}

function format_gallery($portfolio=false,$height="",$width=270,$title=false) {
    global $post;
    $ids = get_post_meta($post->ID, 'gallery_image_ids', true);
    $gallery_height = get_post_meta($post->ID, 'format_image_height', true);
    $class = $title ? ' with-title' : '';
    if(!empty($ids)) {
        ?>
        <div class="loop-gallery gallery-container clearfix">
            <div class="without-a<?php echo $class;?>">
                <div class="gallery-slide">
                    <?php 
                        $width = $portfolio ? $width : 870;
                        $height = $portfolio ? $height : $gallery_height;
                        if(is_single()&&$portfolio) {
                            $width = 770;
                            $height = get_metabox('format_image_height');
                        }
                        $i = 0;
                        foreach(explode( ',', $ids ) as $id) {
                            if(!empty($id)) {
                                $i++;
                                $imgurl0 = aq_resize(wp_get_attachment_url($id), $width, $height, true);
                                $imgurl = !empty($imgurl0) ? $imgurl0 : wp_get_attachment_url($id);
                                ?>
                                <div class="slide-item">
                                    <img src="<?php echo $imgurl;?>" alt="<?php the_title(); ?>">
                                    <?php if($i > 1) { echo '<a href="'.wp_get_attachment_url($id).'" rel="prettyPhoto['.$post->ID.']"></a>'; } else {$img_url = wp_get_attachment_url($id);}?>                                    
                                </div>
                    <?php   }
                        }
                    ?>                  
                </div>
                <?php if(is_single()) { ?>
                    <a class="gallery-zoom" rel="prettyPhoto[<?php echo $post->ID;?>]" href="<?php echo isset($img_url) ? $img_url : '';?>">
                        <div class="image-overlay hover-zoom"></div>
                    </a>
                <?php
                } else {
                ?>
                <div class="image-overlay">                    
                    <div class="image-links">   
                        <?php if($title) { ?>
                            <h2 class="portfolio-title"><a href="<?php the_permalink();?>"><?php the_title(); ?></a><span></span></h2>
                        <?php } ?>
                        <a class="hover-zoom" rel="prettyPhoto[<?php echo $post->ID;?>]" href="<?php echo isset($img_url) ? $img_url : '';?>"></a>
                        <a class="hover-link" href="<?php the_permalink();?>"></a>
                    </div>
                </div>
                <?php } ?>
                
                <div class="carousel-arrow">
                    <a class="carousel-prev" href="#"><i class="icon-chevron-left"></i></a>
                    <a class="carousel-next" href="#"><i class="icon-chevron-right"></i></a>
                </div>
            </div>    
        </div>    
    <?php
    }
}

function format_image() {
    format_standard();
}

function jplayer_script() {
    wp_enqueue_script('jplayer_script', THEME_DIR.'/assets/js/jquery.jplayer.min.js', false, false, true);
}

function format_audio() {
    global $post;

    $audio_url = get_post_meta($post->ID, 'format_audio_mp3', true);
    $embed = get_post_meta($post->ID, 'format_audio_embed', true);
    if (!empty($embed)) {
        echo apply_filters("the_content", htmlspecialchars_decode($embed));
    } else {
        echo post_image_show();
        if (!empty($audio_url)) {
            add_action('wp_footer', 'jplayer_script');
            ?>
            <div id="jquery_jplayer_<?php echo $post->ID; ?>" class="jp-jplayer jp-jplayer-audio" data-pid="<?php echo $post->ID; ?>" data-mp3="<?php echo $audio_url; ?>"></div>
            <div class="jp-audio-container">
                <div class="jp-audio">
                    <div class="jp-type-single">
                        <div id="jp_interface_<?php echo $post->ID; ?>" class="jp-interface">
                            <ul class="jp-controls">
                                <li><div class="seperator-first"></div></li>
                                <li><div class="seperator-second"></div></li>
                                <li><a href="#" class="jp-play" tabindex="1">play</a></li>
                                <li><a href="#" class="jp-pause" tabindex="1">pause</a></li>
                                <li><a href="#" class="jp-mute" tabindex="1">mute</a></li>
                                <li><a href="#" class="jp-unmute" tabindex="1">unmute</a></li>
                            </ul>
                            <div class="jp-progress-container">
                                <div class="jp-progress">
                                    <div class="jp-seek-bar">
                                        <div class="jp-play-bar"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="jp-volume-bar-container">
                                <div class="jp-volume-bar">
                                    <div class="jp-volume-bar-value"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php
        }
    }
}

function format_video($portfolio=false,$video_embed="",$video_thumb="",$video_m4v="",$height="") {
    global $post;
    $start = $end = '';
    if($portfolio){
        if(!empty($height)) { $start = '<div class="video-height" style="height:'.$height.'px;">'; $end = '</div>'; } 
    } else {
        $video_embed = get_post_meta($post->ID, 'format_video_embed', true);
        $video_thumb = get_post_meta($post->ID, 'format_video_thumb', true);
        $video_m4v = get_post_meta($post->ID, 'format_video_m4v', true);
    }
    
    if (!empty($video_embed)) {
        echo $start;
        if($portfolio && !is_single()){
            echo '<a href="'.get_permalink().'">';
            echo vimeo_youtube_image(htmlspecialchars_decode($video_embed));
            echo '<div class="image-overlay hover-link"></div>';
            echo '</a>';            
        } else {
            echo apply_filters("the_content", htmlspecialchars_decode($video_embed));
        }
        echo $end;
    } elseif (!empty($video_m4v)) {
        add_action('wp_footer', 'jplayer_script');
        ?>

        <div id="jquery_jplayer_<?php echo $post->ID; ?>" class="jp-jplayer jp-jplayer-video" data-pid="<?php echo $post->ID; ?>" data-m4v="<?php echo $video_m4v; ?>" data-thumb="<?php echo $video_thumb; ?>"></div>
        <div class="jp-video-container">
            <div class="jp-video">
                <div class="jp-type-single">
                    <div id="jp_interface_<?php echo $post->ID; ?>" class="jp-interface">
                        <ul class="jp-controls">
                            <li><div class="seperator-first"></div></li>
                            <li><div class="seperator-second"></div></li>
                            <li><a href="#" class="jp-play" tabindex="1">play</a></li>
                            <li><a href="#" class="jp-pause" tabindex="1">pause</a></li>
                            <li><a href="#" class="jp-mute" tabindex="1">mute</a></li>
                            <li><a href="#" class="jp-unmute" tabindex="1">unmute</a></li>
                        </ul>
                        <div class="jp-progress-container">
                            <div class="jp-progress">
                                <div class="jp-seek-bar">
                                    <div class="jp-play-bar"></div>
                                </div>
                            </div>
                        </div>
                        <div class="jp-volume-bar-container">
                            <div class="jp-volume-bar">
                                <div class="jp-volume-bar-value"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
   <?php
   }   
}

function format_quote() {
    global $post;

    $quote_text = get_post_meta($post->ID, 'format_quote_text', true);
    $quote_author = get_post_meta($post->ID, 'format_quote_author', true);
    
    if (!empty($quote_text)) {
        echo '<blockquote>';
        echo "<p>" . $quote_text . "</p>";
        if (!empty($quote_author)) {
            echo "<span>- " . $quote_author . "</span>";
        }
        echo "</blockquote>";
    }
}

function format_link() {
    global $post;
    
    $link_url = get_post_meta($post->ID, 'format_link_url', true);
    $url = !empty($link_url) ? to_url($link_url) : "#";
    echo '<div class="link-content">';
    echo '<h2 class="link-text"><a href="' . $url . '" target="_blank">' . get_the_title() . '</a></h2>';
    echo '<a href="' . $url . '" target="_blank"><span class="sub-title">- ' . $url . '</span></a></div>';
}

function format_status() {
    global $post;
    
    $status_url = get_post_meta($post->ID, 'format_status_url', true);
    if (!empty($status_url)) {
        echo apply_filters("the_content", $status_url);
    }
}

function pagination() {
    global $wp_query;

    $pages = $wp_query->max_num_pages;
    $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

    if (empty($pages)) {
        $pages = 1;
    }

    if (1 != $pages) {
        
        $big = 9999; // need an unlikely integer
        echo "<div class='tw-pagination pagination'>";

        $pagination = paginate_links(
                array(
                    'base' => str_replace($big, '%#%', esc_url(get_pagenum_link($big))),
                    'end_size' => 3,
                    'mid_size' => 6,
                    'format' => '?paged=%#%',
                    'current' => max(1, get_query_var('paged')),
                    'total' => $wp_query->max_num_pages,
                    'type' => 'list',
                    'prev_text' => __('&laquo;', 'themewaves'),
                    'next_text' => __('&raquo;', 'themewaves'),
        ));

        echo $pagination;

        echo "</div>";

    }
}
if (!function_exists('infinite')) {

    function infinite() {
        global $wp_query;
        $pages = intval($wp_query->max_num_pages);
        $paged = (get_query_var('paged')) ? intval(get_query_var('paged')) : 1;
        if (empty($pages)) {
            $pages = 1;
        }
        if (1 != $pages) {
            echo '<div class="tw-pagination tw-infinite-scroll" data-has-next="'.($paged===$pages?'false':'true').'">';
            echo '<a class="btn btn-small no-more" href="#">' . __('No more posts', 'themewaves') . '</a>';
            echo '<a class="btn btn-small loading" href="#">' . __('Loading posts ...', 'themewaves') . '</a>';
            echo '<a class="btn btn-small next" href="' . get_pagenum_link($paged + 1) . '"><i class="icon-repeat"></i>' . __('Load More Items', 'themewaves') . '</a>';
            echo '</div>';
        }
    }

}
function comment_count() {

    if (comments_open()) {

        if (tw_option('facebook_comment')) {
            return '';//'<fb:comments-count data-href="' . get_permalink() . '"></fb:comments-count>';
        } else {

            $comment_count = get_comments_number('0', '1', '%');
                                if ($comment_count == 0) {
                                        $comment_trans = __('No comment', 'themewaves');
                                } elseif ($comment_count == 1) {
                                        $comment_trans = __('One comment', 'themewaves');
                                } else {
                                        $comment_trans = $comment_count . ' ' . __('comments', 'themewaves');
                                }
            return "<li class='comments'><i class='icon-comments'></i><a href='" . get_comments_link() . "' title='" . $comment_trans . "' class='comment-count'>" . $comment_trans . "</a></li>";
        }
    }
}

if (!function_exists('mytheme_comment')) {

    function mytheme_comment($comment, $args, $depth) {

        $GLOBALS['comment'] = $comment;
        print '<div class="comment-block">';
        ?>	
        <div class="comment" id="comment-<?php comment_ID();?>">
            <div class="comment-author"><span class="reply-line"></span>
                <?php echo get_avatar($comment, $size = '50'); ?>
                <div class="comment-meta">
                    <span class="comment-author-link">
                        <?php echo __('By','themewaves'). " " . get_comment_author_link() . " - "; ?>
                    </span>                            
                    <span class="comment-date">
                        <?php printf(__('%1$s', 'themewaves'), get_comment_date('j F Y'));?>
                    </span>
                    <span class="comment-replay-link"><?php comment_reply_link(array_merge($args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?></span>
                </div>
            </div>
            <div class="comment-body">
        <?php comment_text() ?>
            </div>
        </div><?php
    }

}






if (!function_exists('tw_comment_form')) {

    function tw_comment_form($fields) {
        global $id, $post_id;
        if (null === $post_id)
            $post_id = $id;
        else
            $id = $post_id;

        $commenter = wp_get_current_commenter();

        $req = get_option('require_name_email');
        $aria_req = ( $req ? " aria-required='true'" : '' );
        $fields = array(
            'author' => '<div class="row-fluid"><p class="comment-form-author span4">' .
            '<input placeholder="' . __('Name', 'themewaves') . '" id="author" name="author" type="text" value="' . esc_attr($commenter['comment_author']) . '" size="30"' . $aria_req . ' /></p>',
            'email' => '<p class="comment-form-email span4">' .
            '<input placeholder="' . __('Email', 'themewaves') . '" id="email" name="email" type="text" value="' . esc_attr($commenter['comment_author_email']) . '" size="30"' . $aria_req . ' /></p>',
            'url' => '<p class="comment-form-url span4">' .
            '<input placeholder="' . __('Website', 'themewaves') . '" id="url" name="url" type="text" value="' . esc_attr($commenter['comment_author_url']) . '" size="30" /></p></div>',
        );
        return $fields;
    }

    add_filter('comment_form_default_fields', 'tw_comment_form');

}
if (!function_exists('post_image_show')) {
    function post_image_show($width = 0, $height = "", $returnURL=false) {
        global $post;
        if (has_post_thumbnail($post->ID)) {
            $attachment = get_post(get_post_thumbnail_id($post->ID));
            if (isset($attachment)) {
                $alt = get_post_meta($attachment->ID, '_wp_attachment_image_alt', true);
                $alt = !empty($alt) ? $alt : $attachment->post_title;
                $lrg_img = wp_get_attachment_image_src($attachment->ID, 'full');
                $url = $lrg_img[0];
                if ($width != 0) {
                    $url = aq_resize($url, $width, $height, true);
                }
                $urll = !empty($url)?$url:$lrg_img[0];
                if($returnURL){
                    return $urll;
                }else{
                    return '<img src="' . $urll . '" alt="' . $alt . '"/>';
                }
            }
            else
                return false;
        }
        return false;
    }
}
if (!function_exists('about_author')) {
    function about_author() {
        $tw_author = false;
        if (get_metabox("post_authorr") == "true") {
            $tw_author = true;
        } else if (get_metabox("post_authorr") != "false") {
            if (tw_option('post_author')) {
                $tw_author = true;
            }
        }
        if ($tw_author) { ?>
            <div class="tw-author clearfix">
                <div class="author-image"><?php
                    $tw_author_email = get_the_author_meta('email');
                    echo get_avatar($tw_author_email, $size = '56'); ?>
                </div>
                <h3><?php _e("Written by ", "themewaves");
                    if (is_author())
                        the_author();
                    else
                        the_author_posts_link(); ?>
                </h3>
                <div class="author-title-line"></div>
                <p><?php
                    $description = get_the_author_meta('description');
                    if ($description != '')
                        echo $description;
                    else
                        _e('The author didnt add any Information to his profile yet', 'themewaves'); ?>
                </p>
            </div><?php
        }
    }
}

function related_portfolios() {

    global $post, $tw_options;


    $tags = wp_get_post_terms($post->ID, 'portfolios', array("fields" => "ids"));

    if ($tags) {
        $rel_title = __("Related portfolio items", "themewaves");
        echo do_shortcode('[tw_item_title title="' . $rel_title . '"]');
        $tag_ids = "";
        foreach ($tags as $tag)
            $tag_ids .= $tag . ",";


        $query = Array(
            'post_type' => 'portfolio',
            'posts_per_page' => '4',
            'post__not_in' => array($post->ID),
            'tax_query' => Array(Array(
                    'taxonomy' => 'portfolios',
                    'terms' => $tag_ids,
                    'field' => 'id'
                ))
        );
        query_posts($query);
        $tw_options['pagination'] = 'none';
        echo '<div class="tw-portfolio">';
        get_template_part("loop", "portfolio");
        echo '</div>';
    }
}


function tw_social() {
    global $tw_socials;
    foreach ($tw_socials as $key => $social) {
        if(tw_option($social['name'])!=""){
            echo '<a href="'.str_replace('*',tw_option($social['name']),$social['link']).'" target="_blank" title="'.$key.'" class="'.$key.'"><span class="tw-icon-'.$key.'"></span></a>';
        }
    }
}


function vimeo_youtube_image($embed) {

    preg_match('/src=\"(.*?)\"/si', $embed, $filteredContent);
    
    if(!empty($filteredContent[1])) {
        $url = $filteredContent[1];
        $youtube = strpos($url, 'youtube.com');
        $youtu = strpos($url, 'youtu.be');
        $vimeo = strpos($url, 'vimeo.com');

        $video_id = '';
        $spliturl = split("/", $url);
        $video_id = $spliturl[count($spliturl) - 1];
        if ($video_id == "") {
            $video_id = $spliturl[count($spliturl) - 2];
        }
    } else {
        $url = $embed;
        $youtube = strpos($url, 'youtube.com');
        $youtu = strpos($url, 'youtu.be');
        $vimeo = strpos($url, 'vimeo.com');

        $video_id = '';
        $spliturl = split("/", $url);
        $video_id = $spliturl[count($spliturl) - 1];
        if ($video_id == "") {
            $video_id = $spliturl[count($spliturl) - 2];            
        } else {
            $spliturl = split("=", $url);
            $video_id = $spliturl[count($spliturl) - 1];
        }
    }

    $video_img = '';

    if ($youtube || $youtu) {
        $video_img = '<img src="http://img.youtube.com/vi/' . $video_id . '/0.jpg" class="image_youtube" />';
    } else if ($vimeo) {
        $json = @file_get_contents("http://vimeo.com/api/oembed.json?url=http%3A//vimeo.com/" . $video_id, true);
        if (strpos($http_response_header[0], "200")) {
            $data = json_decode($json, true);
            $video_thumb = $data['thumbnail_url'];
            $video_thumb = str_replace("_1280", "_640", $video_thumb);
            $video_img = '<img src="' . $video_thumb . '" class="image_vimeo" />';
        }
        if (strlen($video_img) < 1) {
            $video_img = "";
        }
    }

    return $video_img;
}