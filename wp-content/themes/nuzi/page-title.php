<?php
if(!is_singular('post') || tw_option('blog_title')!="") {  
    if(is_singular('post') || is_home()) {
        if(tw_option('blog_title')!="") {
            $title = "<h2>".tw_option('blog_title')."</h2>";        
            $subtitle = tw_option('blog_subtitle')!="" ? ('<h3>'.tw_option('blog_subtitle').'</h3>') : '';
        }
    } else {
        $subtitle = "";
        if(is_page()) {
            $title = get_featuredtext();
            if (get_metabox("subtitle") != "") {
                $subtitle = "<h3>".do_shortcode(get_metabox("subtitle"))."</h3>";
            }
            if(get_metabox("bg_image") != "") {
                $bgimage = get_metabox("bg_image");
            }
        } else {
            $title = get_featuredtext();
        }
    }
}


$breadcrumb = false;
$class = 'span12';
if(get_metabox("breadcrumps")=="true") {
    $breadcrumb = true;
    $class = 'span6';
} else if(get_metabox("breadcrumps")!="false") {
    if(tw_option("breadcrumps")) {
        $breadcrumb = true;
        $class = 'span6';
    }
}


$background = isset($bgimage) ? $bgimage : tw_option('title_bg_image');

if(isset($title)) { ?>
    <!-- Start Feature -->
    <section id="page-title"<?php echo !empty($background) ? (' style="background-image: url('.$background.')"') : '';?>>
        <!-- Start Container -->
        <div class="container">
            <div class="row">
                <div class="<?php echo $class;?>">
                    <?php echo $title.$subtitle; ?>
                </div>                
                <?php if($breadcrumb){ echo '<div class="span6">'; breadcrumbs(); echo '</div>';} ?>
            </div>
        </div>
        <!-- End Container -->
    </section>
    <!-- End Feature -->
<?php } ?>