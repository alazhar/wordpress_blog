<?php get_header(); ?>
<div id="page">
    <div class="row"><?php
    the_post();
    if(tw_option('pagebuilder')&&pbGetContentBuilder()){
        echo do_shortcode(pbGetContentBuilder());
    }
    else {
        if(get_metabox('layout') == "left" || get_metabox('layout') == "right") {
            get_sidebar();
            echo "<div class='span9'>";
                the_content();
            echo "</div>";
        } else {
            echo "<div class='span12'>";
                the_content();
            echo "</div>";
        }
    } ?>
    </div>
</div>
<?php get_footer(); ?>