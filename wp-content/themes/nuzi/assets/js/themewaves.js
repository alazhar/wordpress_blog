jQuery(document).ready(function($) {
    "use strict";
    jQuery(window).scroll(function() {
        if (jQuery(this).scrollTop() > 50) {
            jQuery('#scrollUp').fadeIn();
        } else {
            jQuery('#scrollUp').fadeOut();
        }
    });
    jQuery('#scrollUp').click(function() {
        jQuery("html, body").animate({scrollTop: 0}, 500);
        return false;
    });

    /* navigation */
    $('ul#menu').superfish({
        delay: 200,
        animation: {
            opacity: 'show',
            height: 'show'
        },
        speed: 'fast',
        autoArrows: false,
        dropShadows: false
    });

    /* mobile navigation */
    jQuery('.show-mobile-menu').click(function() {
        jQuery('#mobile-menu').slideToggle('fast');
    });
    jQuery('#mobile-menu ul.sub-menu').each(function() {
        var $parentMenu = jQuery(this).parent('li');
        $parentMenu.addClass('has-children').prepend('<div class="action-expand"><span class="opened">-</span><span class="closed">+</span></div>');
        $parentMenu.children('.action-expand').click(function(e) {
            e.preventDefault();
            var $this = jQuery(this).closest('.has-children');
            $this.siblings('li.menu-open').removeClass('menu-open').children('.sub-menu').slideUp('fast');
            $this.toggleClass('menu-open');
            if ($this.hasClass('menu-open')) {
                $this.children('.sub-menu').slideDown('fast');
            } else {
                $this.children('.sub-menu').slideUp('fast');
            }
            return false;
        });


    });

    //Widget title
    $('h3.widget-title, h3.tw-title').each(function() {
        var txt = $(this).text();
        $(this).html('<span>' + txt.substring(0, (txt.indexOf(' '))) + '</span>' + txt.substring((txt.indexOf(' '))));
    });



    // Accordion
    $('.tw-accordion').each(function($index) {
        $(this).attr('id', 'accordion' + $index);
        $(this).find('.accordion-group').each(function($i) {
            $(this).find('.accordion-toggle').attr('data-parent', '#accordion' + $index).attr('href', '#accordion_' + $index + '_' + $i);
            $(this).find('.accordion-body').attr('id', 'accordion_' + $index + '_' + $i);
        });
        /* Bootstrap Accordian adding active class */
        jQuery(this).on('show', function(e) {
            jQuery(e.target).prev('.accordion-heading').find('.accordion-toggle').addClass('active');
        });
        jQuery(this).on('hide', function(e) {
            jQuery(this).find('.accordion-toggle').not(jQuery(e.target)).removeClass('active');
        });
    });

    // Toggle
    $('.tw-toggle').each(function($index) {
        $(this).find('.accordion-group').each(function($i) {
            $(this).find('.accordion-toggle').attr('href', '#toggle_' + $index + '_' + $i);
            $(this).find('.accordion-body').attr('id', 'toggle_' + $index + '_' + $i);
        });
        /* Bootstrap Accordian adding active class */
        jQuery(this).on('show', function(e) {
            jQuery(e.target).prev('.accordion-heading').find('.accordion-toggle').addClass('active');
        });
        jQuery(this).on('hide', function(e) {
            jQuery(e.target).prev('.accordion-heading').children('.accordion-toggle').removeClass('active');
        });
    });
    // Tab
    $('.tw-tab').each(function($index) {
        $(this).find(">li").each(function($i) {
            jQuery(this).appendTo(jQuery(this).closest('.tw-tab').find('ul.nav-tabs'));
            $(this).find(">a").attr('href', '#tabitem_' + $index + '_' + $i);
            if ($i === 0) {
                $(this).addClass('active');
            }
        });
        $(this).find(".tab-pane").each(function($in) {
            jQuery(this).appendTo(jQuery(this).closest('.tw-tab').find('div.tab-content'));
            $(this).attr('id', 'tabitem_' + $index + '_' + $in);
            if ($in === 0) {
                $(this).addClass('active');
            }
        });
    });
    $('.tw-tab>ul a').click(function(e) {
        e.preventDefault();
        jQuery(this).tab('show');
    });



    if (jQuery().jPlayer) {
        jQuery('.jp-jplayer-audio').each(function() {
            jQuery(this).jPlayer({
                ready: function() {
                    jQuery(this).jPlayer("setMedia", {
                        mp3: jQuery(this).data('mp3')
                    });
                },
                swfPath: "",
                cssSelectorAncestor: "#jp_interface_" + jQuery(this).data('pid'),
                supplied: "mp3, all"
            });
        });

        jQuery('.jp-jplayer-video').each(function() {
            jQuery(this).jPlayer({
                ready: function() {
                    jQuery(this).jPlayer("setMedia", {
                        m4v: jQuery(this).data('m4v'),
                        poster: jQuery(this).data('thumb')
                    });
                },
                size: {
                    width: '100%',
                    height: 'auto'
                },
                swfPath: "",
                cssSelectorAncestor: "#jp_interface_" + jQuery(this).data('pid'),
                supplied: "m4v, all"
            });
        });
    }


    // PrettyPhoto
    jQuery("a[rel^='prettyPhoto']").prettyPhoto({
        deeplinking: false,
        social_tools: ""
    });
    jQuery(window).resize();
});

jQuery('.loop-image, .loop-gallery, .video-height').live('hover', function(e) {
    var overImg = jQuery(this).find('.image-overlay');
    if (e.type === 'mouseenter') {
        overImg.stop().animate({opacity: 1}, "300");
    }
    if (e.type === 'mouseleave') {
        overImg.stop().animate({opacity: 0}, "700");
    }
});

jQuery(window).load(function() {
    "use strict";

    // Gallery
    img_slider();

    // Testimonials
    jQuery('.tw-testimonials').each(function() {
        var $easing   = 'quadratic';
        var $direction= jQuery(this).attr("data-direction");
        var $duration = jQuery(this).attr("data-duration");
        var $timeout = jQuery(this).attr("data-timeout");
        jQuery(this).find('>ul').carouFredSel({
            items: 1,
            direction: $direction,
            auto: {
                easing: $easing,
                duration: $duration,
                timeoutDuration: $timeout,
                pauseOnHover: true
            }
        });
    });
    // Carousel
    list_carousel();

    // Twitter
    if (jQuery().jtwt) {
        jQuery('.tw-twitter').each(function() {
            var currentTwitter = jQuery(this);
            currentTwitter.find('a').live("click", function() {
                jQuery(this).attr('target', "_blank");
            });
            currentTwitter.jtwt({
                count: currentTwitter.attr('data-count'),
                username: currentTwitter.attr('data-name'),
                image_size: 0
            });
            currentTwitter.children('.twitter-follow').appendTo(currentTwitter);
        });
    }

    // Resize
    jQuery(window).resize(function() {
        jQuery('.jp-audio-container, .jp-video-container').each(function() {
            jQuery(this).find('.jp-progress-container').width((jQuery(this).width() - 149 < 0) ? 0 : (jQuery(this).width() - 149));
            jQuery(this).find('.jp-progress').width((jQuery(this).width() - 152 < 0) ? 0 : (jQuery(this).width() - 152));
        });
        jQuery('.tw-testimonials').each(function() {
            jQuery(this).find('>.caroufredsel_wrapper').width(jQuery(this).width());
            jQuery(this).find('ul>li.testimonial-item').width(jQuery(this).width());
        });
    });
    jQuery(window).resize();
});


/* Item Top Bottom Height */
/* ------------------------------------------------------------------- */
function pbItemTB($item) {
    "use strict";
    $item = jQuery($item);
    var $itemMarginTB = parseInt($item.css('margin-top').replace('px', ''), 10) + parseInt($item.css('margin-bottom').replace('px', ''), 10);
    var $itemPaddingTB = parseInt($item.css('padding-top').replace('px', ''), 10) + parseInt($item.css('padding-bottom').replace('px', ''), 10);
//    var $itemBorderTB  = parseInt($item.css('border-top-width').replace('px',''),10) + parseInt($item.css('border-bottom-width').replace('px',''),10);
    var $itemBorderTB = 0;
    var $itemTB = $itemMarginTB + $itemPaddingTB + $itemBorderTB;
    return $itemTB;
}

function img_slider() {
    // Gallery
    jQuery('.gallery-container').each(function() {
        var $prev = jQuery(this).find(".carousel-prev");
        var $next = jQuery(this).find(".carousel-next");
        jQuery(this).find('.gallery-slide').carouFredSel({
            auto: false,
            responsive: true,
            scroll: {fx: 'fade'},
            width: '100%',
            heigth: 'auto',
            padding: 0,
            prev: $prev,
            next: $next,
            items: {
                width: 870,
                visible: {
                    min: 1,
                    max: 1
                }
            }
        });
    });
}
function list_carousel() {
    jQuery('.list_carousel').each(function() {
        var $prev = jQuery(this).closest('.carousel-container').find(".carousel-prev");
        var $next = jQuery(this).closest('.carousel-container').find(".carousel-next");
        var $width = 310;
        var $min = 1;
        var $max = 4;
        var $currentCrslPrnt = jQuery(this);
        var $currentCrsl = $currentCrslPrnt.children('ul.tw-carousel');
        if (jQuery(this).hasClass('tw-carousel-partner')) {
            $width = 200;
            $max = 6;
        }
        else if (jQuery(this).hasClass('tw-carousel-post')) {
            $width = 400;
            $max = 3;
        }
        $currentCrsl.carouFredSel({
            responsive: true,
            auto: false,
            prev: $prev,
            next: $next,
            width: '100%',
            heigth: 'auto',
            scroll: 1,
            items: {
                width: $width,
                visible: {
                    min: $min,
                    max: $max
                }
            },
            onCreate: function() {
                setTimeout(function() {
                    var $height = 0;
                    $currentCrsl.children('li').each(function() {
                        var $tmpH = pbItemTB(jQuery(this)) + jQuery(this).height();
                        if ($tmpH > $height) {
                            $height = $tmpH;
                        }
                    });
                    $currentCrslPrnt.css('height', $height + 'px');
                    $currentCrslPrnt.children('.caroufredsel_wrapper').css('height', $height + 'px');
                    $currentCrslPrnt.children('.caroufredsel_wrapper').children('ul').css('height', $height + 'px');
                }, 1000);
            }
        });
    });
}