<?php
get_header();
?>

<div class="row">
    <div class="span9">
        <section class="content">
            <div id="error404-container">
                <h3 class="error404"><?php _e("Sorry, the post you are looking for is not available. Maybe you need to perform a search?", "themewaves");?></h3>

                <?php get_search_form(); ?>
                <br/>

                <div class="tw-404-error"><p><?php _e("For best search results, mind the following suggestions:", "themewaves");?></p>
                    <ul class="borderlist">
                        <li><?php _e("Always double check your spelling.", "themewaves");?></li>
                        <li><?php _e("Try similar keywords, for example: tablet instead of laptop.", "themewaves");?></li>
                        <li><?php _e("Try using more than one keyword.", "themewaves");?></li>
                    </ul>
                </div>
            </div>
        </section>
    </div>
    <div class="span3">
        <div id="sidebar" class="right-sidebar">
            <aside class="widget" id="page-list-404">
                <div class="widget-title-container"><h3 class="widget-title"><?php _e("All Pages", "themewaves");?></h3></div>
                <ul>
                    <?php
                    $args = array(
                        'depth' => 0,
                        'show_date' => '',
                        'date_format' => get_option('date_format'),
                        'child_of' => 0,
                        'exclude' => '',
                        'include' => '',
                        'title_li' => '',
                        'echo' => 1,
                        'authors' => '',
                        'sort_column' => 'menu_order, post_title',
                        'link_before' => '',
                        'link_after' => '',
                        'walker' => '',
                        'post_type' => 'page',
                        'post_status' => 'publish'
                    );
                    ?>
                    <?php wp_list_pages($args); ?>
                </ul>
            </aside>
            <aside class="widget" id="archives-error-404">
                <div class="widget-title-container"><h3 class="widget-title"><?php _e("Archives", "themewaves");?></h3></div>
                <ul>
                    <?php
                    $args2 = array(
                        'type' => 'monthly',
                        'limit' => '',
                        'format' => 'html',
                        'before' => '',
                        'after' => '',
                        'show_post_count' => false,
                        'echo' => 1,
                        'order' => 'DESC'
                    );
                    ?>

                    <?php wp_get_archives($args2); ?>
                </ul>
            </aside>

        </div>
    </div>
</div>

<?php
get_footer();
?>