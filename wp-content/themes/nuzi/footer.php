</div>
</section>
<!-- End Main -->
<!-- Start Footer -->
<footer id="footer">
<?php if (tw_option("footer_widget")) { ?>

        <!-- Start Container-->
        <div class="container">
            <div class="row">
                <?php
                $grid = tw_option('footer_layout') ? tw_option('footer_layout') : '3-3-3-3';
                $i = 1;
                foreach (split('-', $grid) as $g) {
                    echo '<div class="span' . $g . ' col-' . $i . '">';
                    dynamic_sidebar("footer-sidebar-$i");
                    echo '</div>';
                    $i++;
                }
                ?>
            </div>
        </div>
        <!-- End Container -->
    
<?php } ?>
        <!-- End Footer -->
        <div id="bottom">
            <!-- Start Container -->
            <div class="container">
                <div class="row">
                    <?php $class = tw_option('footer_social') ? 'span6' : 'span12'; ?>
                    <div class="<?php echo $class;?>">
                        <div class="footer-logo">
                            <?php theme_logo();?>
                        </div>
                        <p class="copyright"><?php echo stripslashes(tw_option('copyright_text')); ?></p>
                    </div>
                    <?php if($class == 'span6'){
                        echo '<div class="span6">';
                            echo '<ul class="tw-social-icon pull-right">';
                                    tw_social();
                            echo '</ul>';
                        echo '</div>';
                    } ?>
                </div>
            </div>
            <!-- End Container -->
        </div>
</footer>
<?php
global $tw_end;
echo $tw_end;
?>
<?php
/* Google Analytics Code */
echo stripslashes(tw_option('tracking_code'));

$gotop = __('Scroll to top', 'themewaves');
echo '<a id="scrollUp" title="'.$gotop.'"><i class="icon-chevron-up"></i></a>';
?>
<?php wp_footer(); ?>
</body>
</html>