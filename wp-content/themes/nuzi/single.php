<?php get_header(); ?>


<?php 

the_post();
$col = "span9";
if(get_metabox("layout") == "full") {
    $col = "span12";
}
?>

<div class="row">
   
    <div class="<?php echo $col; echo " content-".get_metabox("layout");   ?>">
    
        <?php
        $feature = false; $class = "single";
        if(get_metabox("feature_show")=="true") {
            $feature = true;
        } else if(get_metabox("feature_show")!="false") {
            if(tw_option("feature_show")) {
                $feature = true;
            }
        }    

        $format_options = get_post_meta($post->ID, 'themewave_post_format', true);
        $format = get_post_format() == "" ? "standard" : get_post_format();
        if($format=="status"){
            if(preg_match("#http://instagr(\.am|am\.com)/p/.*#i", $format_options["status_url"]))
                    $class .= " instagram-post";
            elseif(preg_match("|https?://(www\.)?twitter\.com/(#!/)?@?([^/]*)|", $format_options["status_url"]))
                    $class .= " twitter-post";
        }

        ?>
        <article <?php post_class($class);?>>
            <?php 
                if($feature) {
                    echo '<div class="loop-media">';
                            call_user_func('format_'.$format, $format_options);
                    echo '</div>';
            } ?>
            <div class="content-block">
                <div class="loop-format">
                    <span class="post-format <?php echo $format; ?>"></span>
                </div>
                <h1 class="single-title"><?php the_title();?></h1>
                <div class="meta-container">
                    <ul class="loop-meta inline">                                                
                        <li class="date"><i class="icon-calendar"></i><?php the_time('j M Y'); ?></li>
                        <?php echo comment_count(); ?>
                        <li class="author"><i class="icon-user"></i><?php the_author_posts_link() ?></li>
                        <li class="category"><i class="icon-tags"></i><?php echo get_the_category_list(', '); ?></li>
                    </ul>
                </div>
                <?php the_content(); ?>
                <div class="clear"></div>
                <?php if(get_the_tag_list()) { ?>
                        <div class="meta-container tag clearfix">
                            <ul class="loop-meta inline">                        
                                    <li class="meta-tag"><?php _e("Tags: ", "themewaves"); echo get_the_tag_list('', ', ', ''); ?></li>                        
                            </ul>
                        </div>
                <?php } ?>                
            </div>     
            <?php about_author(); ?>
            <?php comments_template('', true); ?>
            <div class="pagination alignleft">
                <ul><li><?php previous_post_link('%link', '&#8592;', FALSE); ?></li></ul>
            </div>
            <div class="pagination alignright">
                <ul><li><?php next_post_link('%link', '&#8594;', FALSE); ?></li></ul>
            </div>            
        </article>
    </div>
    <?php
    if (get_metabox("layout") != "full") {
        get_sidebar();
    }
    ?>
</div>

<?php get_footer(); ?>