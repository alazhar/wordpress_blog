<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'pajangkarya');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'hv&P,=jDu:mS+}Mym}oS!b3Ij$x=*yE/USnX<PQCN,p)W,uP[[5=DI[XDZ=w:p|1');
define('SECURE_AUTH_KEY',  'a9ZQbx^Umpv^@0%@8If(Dgtonqq_QNCJMH8i7&~52A,D9P.A9pUYhJ=y,X UM*,>');
define('LOGGED_IN_KEY',    'C<e0^~*<KTRD?,>I>}O%.Ccky2H##`W~71v5rq;{M,),=GLrNTFnIIL/-|xdm%9e');
define('NONCE_KEY',        'Vq `85SD})_!xm8MkY8+)_}*QRaSbu[*>ESe6zAhm$oIm;h*^qW`/-O]q08@t%OZ');
define('AUTH_SALT',        'mvrJ5,.ko9Z6?ksgQ;j-a7!<`.#8j^}*oo9?0!J66?H:7$0D?xkil~De)bPbUXdu');
define('SECURE_AUTH_SALT', '&|1xm:FN+EHW7@R.Q{3M/*=Mt]>~3r7cEWbyBm$%(2@R:{%,j6OX d.wYS;cv%_n');
define('LOGGED_IN_SALT',   '=S_kWa~;9{^%0(z7Y16d9Pa!+(vHop0e7NL6GZ*^M-@.ajV8E+T;)CDXVyo,oIB7');
define('NONCE_SALT',       '[#uRT?? 2QB g!%c0kh$Syu/S)@bWa1^#zx kkx$^9OfjuCBv4q[V ah`<6;*a!0');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
